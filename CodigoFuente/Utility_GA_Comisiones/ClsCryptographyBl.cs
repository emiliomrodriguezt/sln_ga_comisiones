﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace Utility_GA_Comisiones
{
    public static class ClsCryptographyBl
    {
        private const string StrPwdMaster = "Grup0Alt@v15t@";

        public static string Sha256Hash(string strClave)
        {
            var sb = new StringBuilder();

            using (var hash = SHA256.Create())
            {
                var enc = Encoding.UTF8;
                var result = hash.ComputeHash(enc.GetBytes(strClave));

                foreach (var b in result)
                    sb.Append(b.ToString("x2"));
            }

            return sb.ToString();
        }

        public static string Sha1Hash(string strClave)
        {
            var sb = new StringBuilder();

            using (var hash = SHA1.Create())
            {
                var enc = Encoding.UTF8;
                var result = hash.ComputeHash(enc.GetBytes(strClave));

                foreach (var b in result)
                    sb.Append(b.ToString("x2"));
            }

            return sb.ToString();
        }

        public static string Md5Hash(string strClave)
        {
            var sb = new StringBuilder();

            using (var hash = MD5.Create())
            {
                var enc = Encoding.UTF8;
                var result = hash.ComputeHash(enc.GetBytes(strClave));

                foreach (var b in result)
                    sb.Append(b.ToString("x2"));
            }

            return sb.ToString();
        }

        private static TripleDES CrearDes(string strClave)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            TripleDES des = new TripleDESCryptoServiceProvider();
            des.Key = md5.ComputeHash(Encoding.Unicode.GetBytes(strClave));
            des.IV = new byte[des.BlockSize / 8];
            return des;
        }

        public static string EncodeString(string strCadena)
        {
            var textoPlanoBytes = Encoding.Unicode.GetBytes(strCadena);
            var flujoMemoria = new MemoryStream();
            var des = CrearDes(StrPwdMaster);
            var flujoEncriptacion = new CryptoStream(flujoMemoria, des.CreateEncryptor(), CryptoStreamMode.Write);
            flujoEncriptacion.Write(textoPlanoBytes, 0, textoPlanoBytes.Length);
            flujoEncriptacion.FlushFinalBlock();
            return Convert.ToBase64String(flujoMemoria.ToArray());
        }

        public static string DecodeString(string strCadena)
        {
            var bytesEncriptados = Convert.FromBase64String(strCadena);
            var flujoMemoria = new MemoryStream();
            var des = CrearDes(StrPwdMaster);
            var flujoDesencriptacion = new CryptoStream(flujoMemoria, des.CreateDecryptor(), CryptoStreamMode.Write);
            flujoDesencriptacion.Write(bytesEncriptados, 0, bytesEncriptados.Length);
            flujoDesencriptacion.FlushFinalBlock();
            return Encoding.Unicode.GetString(flujoMemoria.ToArray());
        }
    }
}