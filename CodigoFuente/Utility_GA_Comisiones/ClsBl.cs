﻿using System.Data;

namespace Utility_GA_Comisiones
{
    public static class ClsTransponerDataTbleBl
    {
        public static DataTable GenerateTransposedTable(this DataTable objDataTableOrg)
        {
            var objDataTable = new DataTable();
            objDataTable.Columns.Add(objDataTableOrg.Columns[0].ColumnName);

            foreach (DataRow inRow in objDataTableOrg.Rows)
            {
                objDataTable.Columns.Add(inRow[0].ToString());
            }

            var intCont = 0;
            var objDataRow = objDataTable.NewRow();
            objDataRow[0] = objDataTableOrg.Columns[0].ColumnName;
            foreach (DataRow inRow in objDataTableOrg.Rows)
            {
                intCont++;
                objDataRow[intCont] = inRow[0].ToString();
            }
            objDataTable.Rows.Add(objDataRow);

            for (var rCount = 1; rCount <= objDataTableOrg.Columns.Count - 1; rCount++)
            {
                objDataRow = objDataTable.NewRow();

                objDataRow[0] = objDataTableOrg.Columns[rCount].ColumnName;
                for (var cCount = 0; cCount <= objDataTableOrg.Rows.Count - 1; cCount++)
                {
                    objDataRow[cCount + 1] = objDataTableOrg.Rows[cCount][rCount].ToString();
                }
                objDataTable.Rows.Add(objDataRow);
            }

            return objDataTable;
        }

    }
}
