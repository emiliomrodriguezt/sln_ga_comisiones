﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;

namespace Utility_GA_Comisiones
{
    public static class ClsPdfBl
    {
        public static MemoryStream ExportToPdf(string strHtml, string strCss)
        {
            try
            {
                using (var objMemoryStream = new MemoryStream())
                {
                    using (var objDocument = new Document())
                    {
                        using (var writer = PdfWriter.GetInstance(objDocument, objMemoryStream))
                        {
                            objDocument.Open();
                            using (var objMemoryStreamCss = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(strCss)))
                            {
                                using (var objMemoryStreamHtml = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(strHtml)))
                                {
                                    XMLWorkerHelper.GetInstance().ParseXHtml(writer, objDocument, objMemoryStreamHtml, objMemoryStreamCss);
                                }
                            }
                            objDocument.Close();
                        }
                    }
                    return objMemoryStream;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("ExportToPdf: \n" + ex.Message);
            }
        }

        public static void DataTableToPdf(DataTable objDataTable, Document objDocument, Font objFont, int intWidthPercentage, bool isColumName = true)
        {
            var table = new PdfPTable(objDataTable.Columns.Count);
            var lstFloatWidths = new List<float>();
            for (var i = 0; i < objDataTable.Columns.Count; i++)
            {
                lstFloatWidths.Add(4f);
            }
            table.SetWidths(lstFloatWidths.ToArray());
            table.WidthPercentage = intWidthPercentage;
            table.HorizontalAlignment = Element.ALIGN_LEFT;

            if (isColumName)
            {
                foreach (DataColumn c in objDataTable.Columns)
                {

                    table.AddCell(new Phrase(c.ColumnName, objFont));
                }
            }

            foreach (DataRow itm in objDataTable.Rows)
            {
                for (var i = 0; i < itm.ItemArray.Length; i++)
                {
                    table.AddCell(new Phrase(itm[i].ToString(), objFont));
                }
            }
            objDocument.Add(table);
        }

        public static void HtmlToPdf(Document objDocument, PdfWriter objPdfWriter, string strHtml, string strCss)
        {
            using (var msCss = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(strCss)))
            {
                using (var msHtml = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(strHtml)))
                {
                    XMLWorkerHelper.GetInstance().ParseXHtml(objPdfWriter, objDocument, msHtml, msCss);
                }
            }

        }
    }
}
