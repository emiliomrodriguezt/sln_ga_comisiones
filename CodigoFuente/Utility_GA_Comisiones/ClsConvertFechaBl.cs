﻿using System;

namespace Utility_GA_Comisiones
{
    public static class ClsConvertFechaBl
    {
        public static DateTime? Fecha(object objFecha)
        {
            DateTime? dtResult = null;

            if (objFecha is string)
            {
                var strFecha = Convert.ToString(objFecha);
                if (strFecha.Length == 10)
                {
                    var arrStrFecha = strFecha.Split('-');
                    if (arrStrFecha.Length == 3)
                    {
                        if (arrStrFecha[0].Length == 2 &&
                            arrStrFecha[1].Length == 4 &&
                            arrStrFecha[2].Length == 2)
                        {
                            try
                            {
                                dtResult = new DateTime(Ano(arrStrFecha[2]), Mes(arrStrFecha[1]), Dia(arrStrFecha[0]));
                            }
                            catch (Exception)
                            {
                                // ignored
                            }
                        }
                    }
                }
            }

            return dtResult;
        }

        private static int Mes(string strMes)
        {
            int intResult;
            strMes = strMes.TrimEnd('.');
            switch (strMes)
            {
                case "ene":
                    intResult = 1;
                    break;
                case "feb":
                    intResult = 2;
                    break;
                case "mar":
                    intResult = 3;
                    break;
                case "abr":
                    intResult = 4;
                    break;
                case "may":
                    intResult = 5;
                    break;
                case "jun":
                    intResult = 6;
                    break;
                case "jul":
                    intResult = 7;
                    break;
                case "ago":
                    intResult = 8;
                    break;
                case "sep":
                    intResult = 9;
                    break;
                case "oct":
                    intResult = 10;
                    break;
                case "nov":
                    intResult = 11;
                    break;
                case "dic":
                    intResult = 12;
                    break;
                default:
                    intResult = 0;
                    break;
            }

            return intResult;
        }

        private static int Dia(string strDia)
        {
            int.TryParse(strDia, out int intResult);
            return intResult;
        }

        private static int Ano(string strAno)
        {
            int.TryParse(string.Concat("20", strAno), out int intResult);
            return intResult;
        }
    }
}
