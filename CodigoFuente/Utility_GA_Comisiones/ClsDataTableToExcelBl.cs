﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace Utility_GA_Comisiones
{
    public static class ClsDataTableToExcelBl
    {
        public static string ExcelContentType
        {
            get
            { return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"; }
        }

        public static MemoryStream ExportToExcel(DataTable objDataTable)
        {
            try
            {
                using (var excel = new ExcelPackage())
                {
                    excel.Workbook.Worksheets.Add("DataExport");
                    var headerRange = "A1:" + char.ConvertFromUtf32(objDataTable.Columns.Count + 64) + "1";
                    var worksheet = excel.Workbook.Worksheets["DataExport"];
                    worksheet.Cells[headerRange].LoadFromDataTable(objDataTable, true);

                    var arrBytFile = excel.GetAsByteArray();
                    var objMemoryStream = new MemoryStream(arrBytFile);

                    return objMemoryStream;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("ExportToExcel: \n" + ex.Message);
            }
        }

        public static MemoryStream ExportToExcel<T>(List<T> lstObjet)
        {
            try
            {
                var intCountProperties = typeof(T).GetProperties().Length;
                using (var objExcelPackage = new ExcelPackage())
                {
                    objExcelPackage.Workbook.Worksheets.Add("DataExport");
                    var headerRange = "A1:" + char.ConvertFromUtf32(intCountProperties + 64) + "1";
                    var worksheet = objExcelPackage.Workbook.Worksheets["DataExport"];
                    worksheet.Cells[headerRange].LoadFromCollection(lstObjet, true);
                    var arrBytFile = objExcelPackage.GetAsByteArray();
                    var objMemoryStream = new MemoryStream(arrBytFile);

                    return objMemoryStream;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("ExportToExcel: \n" + ex.Message);
            }
        }

        public static void test(DataTable objDataTable, ExcelWorksheet worksheet, int intRow) {
            var headerRange = char.ConvertFromUtf32(intRow + 64) + "1" + ":" + char.ConvertFromUtf32(objDataTable.Columns.Count + 64) + "1";
            worksheet.Cells[headerRange].LoadFromDataTable(objDataTable, true);
        }

        public static byte[] ExportExcel(DataTable datos, string titulo = "", bool agregaNumeracion = false, params string[] listaColumnas)
        {

            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                ExcelWorksheet workSheet = package.Workbook.Worksheets.Add(String.Format("Reporte {0} ", titulo));
                int startRowFrom = String.IsNullOrEmpty(titulo) ? 1 : 3;
                int columnaInicial = 1;

                if (agregaNumeracion)
                {
                    DataColumn dataColumn = datos.Columns.Add("#", typeof(int));
                    dataColumn.SetOrdinal(0);
                    int index = 1;
                    foreach (DataRow item in datos.Rows)
                    {
                        item[0] = index;
                        index++;
                    }
                    workSheet.Cells[startRowFrom, columnaInicial].Value = "#";
                    columnaInicial++;
                }


                if (listaColumnas == null)
                    workSheet.Cells["A" + startRowFrom].LoadFromDataTable(datos, true);
                else
                {
                    for (int indice = columnaInicial; indice < listaColumnas.Length + columnaInicial; indice++)
                    {
                        workSheet.Cells[startRowFrom, indice].Value = listaColumnas[indice - columnaInicial];
                    }
                    workSheet.Cells["A" + (startRowFrom + 1)].LoadFromDataTable(datos, false);
                }

                int i = 0;
                foreach(DataColumn col in datos.Columns)
                {
                    i++;
                    if (col.DataType == typeof(DateTime))
                    {
                        workSheet.Column(i).Style.Numberformat.Format = "dd/MM/yyyy";
                    }
                }

                for (int indice = 1; indice <= datos.Columns.Count; indice++)
                {
                    workSheet.Column(indice).AutoFit();
                }

                // Formato del encabezado
                using (ExcelRange r = workSheet.Cells[startRowFrom, 1, startRowFrom, datos.Columns.Count])
                {
                    r.Style.Font.Color.SetColor(System.Drawing.Color.White);
                    r.Style.Font.Bold = true;
                    r.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    r.Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#1fb5ad"));
                }

                // Formato del contenido
                using (ExcelRange r = workSheet.Cells[startRowFrom + 1, 1, startRowFrom + datos.Rows.Count, datos.Columns.Count])
                {
                    r.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    r.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    r.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    r.Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    r.Style.Border.Top.Color.SetColor(System.Drawing.Color.Black);
                    r.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                    r.Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                    r.Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                }

                if (!String.IsNullOrEmpty(titulo))
                {
                    workSheet.Cells["A1"].Value = titulo;
                    workSheet.Cells["A1"].Style.Font.Size = 20;

                    workSheet.InsertColumn(1, 1);
                    workSheet.InsertRow(1, 1);
                    workSheet.Column(1).Width = 5;
                }

                result = package.GetAsByteArray();
            }

            return result;
        }
    }
}
