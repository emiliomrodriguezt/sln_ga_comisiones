﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using System.Web.Security;
using BL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace UL_GA_Comisiones.Controllers
{
    public class TabuladorController : ApiController
    {
        [HttpPost]
        public List<List<string>> GetTabuladoresGerentes(dynamic objParametros)
        {
            try
            {
                var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)RequestContext.Principal.Identity).Ticket.UserData);
                var objClsTabulatorManagerBl = new ClsTabulatorManagerBl(objClsUsuarioActiveDirectoryEl);
                return objClsTabulatorManagerBl.GetTabulatorManager(0, (int)objParametros.IntIdAno, (int)objParametros.IntIdTipoTabulador).OrderByDescending(itm => itm.BolActivo)
                                                                                                                                          .ThenBy(itm => itm.DecCostoMensualGaMaximo)
                                                                                                                                          .Select(itm => new List<string>()
                {
                    itm.IntId.ToString(),
                    itm.ObjClsCatTipoTabuladorEl.StrNombre,
                    $"{itm.DecCostoMensualGaMaximo:c}",
                    $"{itm.DecMubMinimo:c}",
                    $"{itm.DecComision:c}",
                    ClsGetLinksBl.BolResult(itm.BolActivo),
                    ClsGetLinksBl.GetLinkAccion(objParametros.ObjClsDataTableEl.ToObject<ClsDataTableEl>(), itm.IntId, itm.BolActivo)

                }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(objParametros),
                    DtFecha = DateTime.Now
                });

                return new List<List<string>>() { new List<string>() { ex.Message } };
            }
        }

        [HttpGet]
        public ClsSimpleResultEl DelTabuladorGerentes(int id)
        {
            try
            {
                var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)RequestContext.Principal.Identity).Ticket.UserData);
                var objClsTabulatorManagerBl = new ClsTabulatorManagerBl(objClsUsuarioActiveDirectoryEl);
                return objClsTabulatorManagerBl.DelTabulatorManager(id);
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { id }),
                    DtFecha = DateTime.Now
                });

                return new ClsSimpleResultEl() { IntId = null, BolSuccess = false, StrMessage = ex.Message };
            }
        }

        [HttpPut]
        public ClsSimpleResultEl CopyTabuladorGerentes(dynamic objParametros)
        {
            try
            {
                var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)RequestContext.Principal.Identity).Ticket.UserData);
                var objClsTabulatorManagerBl = new ClsTabulatorManagerBl(objClsUsuarioActiveDirectoryEl);
                return objClsTabulatorManagerBl.CopyTabulatorManager((int)objParametros.IntAnoOld, (int)objParametros.IntAnoNew);
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(objParametros),
                    DtFecha = DateTime.Now
                });

                return new ClsSimpleResultEl() { IntId = null, BolSuccess = false, StrMessage = ex.Message };
            }
        }
    }
}
