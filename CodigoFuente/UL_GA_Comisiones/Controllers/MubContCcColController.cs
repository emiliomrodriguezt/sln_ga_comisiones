﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using System.Web.Security;
using BL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace UL_GA_Comisiones.Controllers
{
    public class MubContCcColController : ApiController
    {
        [HttpGet]
        public List<List<string>> FilltblMub(int id)
        {
            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)RequestContext.Principal.Identity).Ticket.UserData);
            
            try
            {
                var objClsDistributionMubContractedCenterCoststBl = new ClsDistributionMubContractedCenterCoststBl(objClsUsuarioActiveDirectoryEl);
                return objClsDistributionMubContractedCenterCoststBl.GetMubCenterCostColaborador(0, id, 0, 0).Select(itm => new List<string>()
                {
                    itm.IntId.ToString(),
                    itm.ObjGerente.StrNombreColaborador,
                    itm.ObjDirector.StrNombreColaborador,
                    $"{itm.DecPorcentaje:P2}",
                    $"<a data-toggle=\"tooltip\" class=\"tooltips\" title=\"Eliminar\" href=\"#\" onclick=\"fnEliminaMub({itm.IntId});\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a>"
                }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = objClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { id }),
                    DtFecha = DateTime.Now
                });

                return new List<List<string>>() { new List<string>() { ex.Message } };
            }
        }

        [HttpPost]
        public ClsSimpleResultEl SeTMUB(dynamic objParametros)
        {
            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)RequestContext.Principal.Identity).Ticket.UserData);
            try
            {

                var objClsDistributionMubContractedCenterCoststBll = new ClsDistributionMubContractedCenterCoststBl(objClsUsuarioActiveDirectoryEl);
                return objClsDistributionMubContractedCenterCoststBll.SetMubCenterCostColaborador(0, (int)objParametros.IntIdCentroCostos, (int)objParametros.IntIdGerente, 
                                                                                                  (int)objParametros.IntIdDirector, (decimal)objParametros.DecPorcentaje);
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = objClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(objParametros),
                    DtFecha = DateTime.Now
                });

                return new ClsSimpleResultEl() { IntId = null, BolSuccess = false, StrMessage = ex.Message };
            }
        }

        [HttpDelete]
        public ClsSimpleResultEl DelMub(int id)
        {
            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)RequestContext.Principal.Identity).Ticket.UserData);
            try
            {
                var objClsDistributionMubContractedCenterCoststBll = new ClsDistributionMubContractedCenterCoststBl(objClsUsuarioActiveDirectoryEl);
                return objClsDistributionMubContractedCenterCoststBll.DelMubCenterCostColaborador(id);
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = objClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject( new { id } ),
                    DtFecha = DateTime.Now
                });

                return new ClsSimpleResultEl() { IntId = null, BolSuccess = false, StrMessage = ex.Message };
            }
        }

    }
}
