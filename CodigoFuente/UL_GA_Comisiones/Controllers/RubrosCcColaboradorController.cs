﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using System.Web.Security;
using BL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace UL_GA_Comisiones.Controllers
{
    public class RubrosCcColaboradorController : ApiController
    {
        [HttpGet]
        public List<List<string>> FillRubrosCcCol(int id)
        {
            var objClsUsuarioActiveDirectoryEl =
                ClsUserBl.UserLogged(((FormsIdentity) RequestContext.Principal.Identity).Ticket.UserData);

            try
            {
                var objClsItemsCcCollaboratorBl = new ClsItemsCcCollaboratorBl(objClsUsuarioActiveDirectoryEl);
                return objClsItemsCcCollaboratorBl.GetItemsCcCollaborator(0, id, 0).Select(itm => new List<string>()
                {
                    itm.IntId.ToString(),
                    itm.ObjClsTblColaboradorEl.StrNombreColaborador,
                    $"{itm.DecPorcentaje:P2}",
                    $"<a data-toggle=\"tooltip\" class=\"tooltips\" title=\"Eliminar\" href=\"#\" onclick=\"fnEliminaCcRubro({itm.IntId});\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a>"
                }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = objClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new {id}),
                    DtFecha = DateTime.Now
                });

                return new List<List<string>>() {new List<string>() {ex.Message}};
            }
        }

        [HttpPost]
        public ClsSimpleResultEl SetRubrosCcCol(dynamic objParametros)
        {
            var objClsUsuarioActiveDirectoryEl =
                ClsUserBl.UserLogged(((FormsIdentity) RequestContext.Principal.Identity).Ticket.UserData);
            try
            {
                var objClsItemsCcCollaboratorBl = new ClsItemsCcCollaboratorBl(objClsUsuarioActiveDirectoryEl);
                return objClsItemsCcCollaboratorBl.SetItemsCcCollaborator(0, (int) objParametros.IntIdRubroCc,
                    (int) objParametros.IntIdColaborador, (decimal) objParametros.DecPorcentaje);
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = objClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(objParametros),
                    DtFecha = DateTime.Now
                });

                return new ClsSimpleResultEl() {IntId = null, BolSuccess = false, StrMessage = ex.Message};
            }
        }

        [HttpDelete]
        public ClsSimpleResultEl DelRubrosCcCol(int id)
        {
            var objClsUsuarioActiveDirectoryEl =
                ClsUserBl.UserLogged(((FormsIdentity)RequestContext.Principal.Identity).Ticket.UserData);
            try
            {
                var objClsItemsCcCollaboratorBl = new ClsItemsCcCollaboratorBl(objClsUsuarioActiveDirectoryEl);
                return objClsItemsCcCollaboratorBl.DelItemsCcCollaborator(id);
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = objClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { id }),
                    DtFecha = DateTime.Now
                });

                return new ClsSimpleResultEl() { IntId = null, BolSuccess = false, StrMessage = ex.Message };
            }
        }
    }
}
