﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using System.Web.Security;
using BL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace UL_GA_Comisiones.Controllers
{
    public class BonoController : ApiController
    {
        [HttpPost]
        public List<List<string>> GetBonos(ClsDataTableEl objParametros)
        {
            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)RequestContext.Principal.Identity).Ticket.UserData);
            try
            {
                var objClsBonusesBl = new ClsBonusesBl(objClsUsuarioActiveDirectoryEl);
                return objClsBonusesBl.GetBonuses(0, 0).OrderByDescending(itm => itm.BolActivo)
                                                       .ThenBy(itm => itm.ObjClsTblColaboradorEl.StrNombreColaborador).Select(itm => new List<string>()
                {
                    itm.ObjClsTblColaboradorEl.StrNoColaborador,
                    itm.ObjClsTblColaboradorEl.StrNombreColaborador,
                    itm.ObjClsTblColaboradorEl.ObjClsCatCentroCostosEl.StrNombre,
                    $"{itm.DecMontoBono:c}",
                    itm.IntAnio.ToString(),
                    ClsGetLinksBl.BolResult(itm.BolPagado),
                    ClsGetLinksBl.BolResult(itm.BolActivo),
                    ClsGetLinksBl.GetLinkAccion(objParametros, itm.IntId, itm.BolActivo)
                }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = objClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(objParametros),
                    DtFecha = DateTime.Now
                });

                return new List<List<string>>() { new List<string>() { ex.Message } };
            }
        }

        [HttpGet]
        public ClsSimpleResultEl DelBono(int id)
        {
            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)RequestContext.Principal.Identity).Ticket.UserData);
            try
            {
                var objClsBonusesBl = new ClsBonusesBl(objClsUsuarioActiveDirectoryEl);
                return objClsBonusesBl.DelBonus(id);
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = objClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { id }),
                    DtFecha = DateTime.Now
                });

                return new ClsSimpleResultEl() { IntId = null, BolSuccess = false, StrMessage = ex.Message };
            }
        }
    }
}
