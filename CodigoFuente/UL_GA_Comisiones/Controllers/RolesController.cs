﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using BL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace UL_GA_Comisiones.Controllers
{
    public class RolesController : ApiController
    {
        [HttpPost]
        public List<List<string>> GetRoles(ClsDataTableEl objParametros)
        {
            try
            {
                var objClsRolBl = new ClsRolBl();
                return objClsRolBl.GetRols(0, "").OrderByDescending(itm => itm.BolActivo).ThenBy(itm=> itm.StrNombre).Select(itm => new List<string>()
                {
                    itm.StrNombre,
                    ClsGetLinksBl.BolResult(itm.BolActivo),
                    ClsGetLinksBl.GetLinkAccion(objParametros, itm.IntId, itm.BolActivo)
                   
                }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(objParametros),
                    DtFecha = DateTime.Now
                });

                return new List<List<string>>() { new List<string>() { ex.Message } };
            }
        }

        [HttpGet]
        public ClsSimpleResultEl DelRol(int id)
        {
            try
            {
                var objClsRolBl = new ClsRolBl();
                return objClsRolBl.DelRol(id);
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { id }),
                    DtFecha = DateTime.Now
                });

                return new ClsSimpleResultEl(){ IntId = null, BolSuccess  = false, StrMessage = ex.Message };
            }
        }

    }
}
