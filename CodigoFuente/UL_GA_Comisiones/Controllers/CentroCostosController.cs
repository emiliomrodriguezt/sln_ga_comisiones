﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Web.Http;
using System.Web.Security;
using BL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace UL_GA_Comisiones.Controllers
{
    public class CentroCostosController : ApiController
    {
        [HttpPost]
        public List<List<string>> GetCentroCostos(ClsDataTableEl objParametros)
        {
            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)RequestContext.Principal.Identity).Ticket.UserData);
            try
            {
                var objClsCenterCostsBl = new ClsCenterCostsBl(objClsUsuarioActiveDirectoryEl);
                return objClsCenterCostsBl.GetCenterCosts(0, "").Where(itm => itm.StrClasificacionAlterna.Equals("PROYECTOS") || itm.StrClasificacionAlterna.Equals("OPORTUNIDADES"))
                                                                .OrderByDescending(itm => itm.BolActivo)
                                                                .ThenBy(itm => itm.StrNombre)
                                                                .Select(itm => new List<string>()
                {
                    itm.StrId,
                    itm.StrNombre,
                    itm.DtFechaInicio?.ToString("dd/MM/yyyy"),
                    itm.DtFechaCierreAdmin?.ToString("dd/MM/yyyy"),
                    itm.StrEmpresa,
                    itm.StrEstatus,
                    itm.StrUnidadNegocio,
                    itm.StrArea,
                    itm.StrZIdHomologado,
                    itm.StrZNombreHomologado,
                    itm.StrClasificacionAlterna,
                    ClsGetLinksBl.BolResult(itm.BolActivo),
                    ClsGetLinksBl.GetLinkAccion(objParametros, itm.IntId, itm.BolActivo, true)
                }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = objClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(objParametros),
                    DtFecha = DateTime.Now
                });

                return new List<List<string>>() { new List<string>() { ex.Message } };
            }
        }

        [HttpGet]
        public HttpResponseMessage GenerateXls()
        {
            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)RequestContext.Principal.Identity).Ticket.UserData);
            var objHttpResponseMessage = new HttpResponseMessage();

            try
            {
                var objClsCenterCostsBl = new ClsCenterCostsBl(objClsUsuarioActiveDirectoryEl);
                var objMemoryStream = objClsCenterCostsBl.GetCenterCostsExport(0, "");

                objHttpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new ByteArrayContent(objMemoryStream.ToArray())
                };

                objHttpResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = "Export.xlsx"
                };

                objHttpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");

            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = objClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(""),
                    DtFecha = DateTime.Now
                });
            }

            return objHttpResponseMessage;
        }

        [HttpPut]
        public List<ClsResultEl> UpdateCenterCosts()
        {
            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)RequestContext.Principal.Identity).Ticket.UserData);
            var lstClsResultUpdateCenterCostsEl = new List<ClsResultEl>();

            try
            {
                var objClsCenterCostsBl = new ClsCenterCostsBl(objClsUsuarioActiveDirectoryEl);
                return objClsCenterCostsBl.UpdateCenterCosts();

            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = objClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = "",
                    DtFecha = DateTime.Now
                });
            }

            return lstClsResultUpdateCenterCostsEl;
        }
    }
}
