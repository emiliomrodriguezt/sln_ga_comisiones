﻿using System;
using System.Reflection;
using System.Web.Http;
using System.Web.Security;
using BL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace UL_GA_Comisiones.Controllers
{
    public class ObservacionesComisonController : ApiController
    {
        [HttpPost]
        public ClsSimpleResultEl SetObservaciones(dynamic objParametros)
        {
            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)RequestContext.Principal.Identity).Ticket.UserData);
            try
            {
                var objClsCommissionCenterCostBl = new ClsCommissionCenterCostBl(objClsUsuarioActiveDirectoryEl);
                return objClsCommissionCenterCostBl.UpdateObservaciones((int)objParametros.IntId, objParametros.StrObservacion.ToString());
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = objClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(objParametros),
                    DtFecha = DateTime.Now
                });

                return new ClsSimpleResultEl { BolSuccess = false, StrMessage = ex.Message };
            }
        }
    }
}
