﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using BL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace UL_GA_Comisiones.Controllers
{
    public class UsuarioController : ApiController
    {
        [HttpPost]
        public List<List<string>> GetUser(ClsDataTableEl objParametros)
        {
            try
            {
                var objClsUserBl = new ClsUserBl();
                var objClsRolesBl = new ClsRolBl();
                return objClsUserBl.GetUsers(0, "","").OrderByDescending(itm => itm.BolActivo)
                                                      .ThenBy(itm => itm.StrNombre)
                                                      .Select(itm => new List<string>()
                {
                    itm.IntId.ToString(),
                    itm.StrUser,
                    itm.StrNombre,
                    objClsRolesBl.GetRols(itm.IntIdRol, "").FirstOrDefault()?.StrNombre ?? "",
                    ClsGetLinksBl.BolResult(itm.BolActivo),
                    ClsGetLinksBl.GetLinkAccion(objParametros, itm.IntId, itm.BolActivo)

                }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(objParametros),
                    DtFecha = DateTime.Now
                });

                return new List<List<string>>() { new List<string>() { ex.Message } };
            }
        }

        [HttpGet]
        public ClsSimpleResultEl DelUser(int id)
        {
            try
            {
                var objClsUserBl = new ClsUserBl();
                return objClsUserBl.DelUser(id);
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { id }),
                    DtFecha = DateTime.Now
                });

                return new ClsSimpleResultEl() { IntId = null, BolSuccess = false, StrMessage = ex.Message };
            }
        }

        [HttpGet]
        public List<ClsGenericCatEl> GetUserDirectory(string id)
        {
            try
            {
                return ClsActiveDirectoryBl.GetUserActiveDirectory().Where(itm => itm.StrDisplayName.ToUpper().Contains(id.ToUpper()))
                                                                    .Select(itm => new ClsGenericCatEl() { value = itm.StrSamAccountName, label = itm.StrDisplayName })
                                                                    .OrderBy(itm => itm.label).ToList();
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(id),
                    DtFecha = DateTime.Now
                });

                return new List<ClsGenericCatEl>();
            }
        }
    }
}
