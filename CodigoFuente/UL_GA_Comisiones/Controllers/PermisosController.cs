﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using BL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;
using System.Web.Security;

namespace UL_GA_Comisiones.Controllers
{
    public class PermisosController : ApiController
    {
        [HttpGet]
        public List<List<string>> GetPermisos(int id)
        {
            try
            {
                var objClsPermissionBl = new ClsPermissionBl(ClsUserBl.UserLogged(((FormsIdentity)RequestContext.Principal.Identity).Ticket.UserData));
                return objClsPermissionBl.GetPermits(id).Select(itm => new List<string>()
                {
                    itm.IntIdPemriso.ToString(),
                    itm.IntIdPagina.ToString(),
                    itm.BolEdit.ToString(),
                    itm.StrNombrePagina,
                    itm.StrDescripcionPage,
                    "",
                    ""
                }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new {id}),
                    DtFecha = DateTime.Now
                });

                return new List<List<string>>() {new List<string>() {ex.Message}};
            }
        }

        [HttpPost]
        public List<ClsSimpleResultEl> SetPermisos(List<ClsAddPermissionEl> objParametros)
        {
            var objClsPermissionBl = new ClsPermissionBl(ClsUserBl.UserLogged(((FormsIdentity)RequestContext.Principal.Identity).Ticket.UserData));
            return objClsPermissionBl.SetPermits(objParametros);
        }
    }
}
