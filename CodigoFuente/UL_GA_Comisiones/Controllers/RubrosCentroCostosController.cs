﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using System.Web.Security;
using BL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace UL_GA_Comisiones.Controllers
{
    public class RubrosCentroCostosController : ApiController
    {
        [HttpGet]
        public List<List<string>> FillRubrosCc(int id)
        {
            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)RequestContext.Principal.Identity).Ticket.UserData);

            try
            {
                var objClsItemsCenterCostBl = new ClsItemsCenterCostBl(objClsUsuarioActiveDirectoryEl);
                var x = objClsItemsCenterCostBl.GetItemsCenterCosts(0, 0, id);
                return objClsItemsCenterCostBl.GetItemsCenterCosts(0, 0, id).Select(itm => new List<string>()
                {
                    itm.IntId.ToString(),
                    itm.ObjClsRubroPagoEl.Nombre,
                    $"{itm.ObjClsRubroPagoEl.Porcentaje:P2}",
                    $"<a data-toggle=\"tooltip\" class=\"tooltips\" title=\"Agrega Colaboradores al rubro\" href=\"#\" onclick=\"fnAddColRubro({itm.IntId});\"><i class=\"fa fa-user\" aria-hidden=\"true\"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle=\"tooltip\" class=\"tooltips\" title=\"Eliminar\" href=\"#\" onclick=\"fnEliminaRubro({itm.IntId});\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a>   "
                }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = objClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { id }),
                    DtFecha = DateTime.Now
                });

                return new List<List<string>>() { new List<string>() { ex.Message } };
            }
        }

        [HttpPost]
        public ClsSimpleResultEl SetRubrosCc(dynamic objParametros)
        {
            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)RequestContext.Principal.Identity).Ticket.UserData);
            try
            {
                var objClsItemsCenterCostBl = new ClsItemsCenterCostBl(objClsUsuarioActiveDirectoryEl);
                return objClsItemsCenterCostBl.SetItemsCenterCost(0, (int)objParametros.IntIdRubroPago, (int)objParametros.IntIdCentroCostos);
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = objClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(objParametros),
                    DtFecha = DateTime.Now
                });

                return new ClsSimpleResultEl() { IntId = null, BolSuccess = false, StrMessage = ex.Message };
            }
        }

        [HttpDelete]
        public ClsSimpleResultEl DelRubrosCc(int id)
        {
            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)RequestContext.Principal.Identity).Ticket.UserData);
            try
            {
                var objClsItemsCenterCostBl = new ClsItemsCenterCostBl(objClsUsuarioActiveDirectoryEl);
                return objClsItemsCenterCostBl.DelItemsCenterCost(id);
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = objClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { id }),
                    DtFecha = DateTime.Now
                });

                return new ClsSimpleResultEl() { IntId = null, BolSuccess = false, StrMessage = ex.Message };
            }
        }
    }
}
