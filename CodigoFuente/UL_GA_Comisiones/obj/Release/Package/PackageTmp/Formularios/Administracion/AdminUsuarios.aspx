﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminUsuarios.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.Administracion.AdminUsuarios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
     <i class="fa fa-sitemap" aria-hidden="true"></i> Administración / Usuarios
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <div class="row">
        <div class="col-md-12 text-right">
            <a href="EditUsuario.aspx" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Agregar usuario</a>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped" id="dtResult"></table>
        </div>
    </div>
    <script type="text/javascript">
        var fnFillTable = function() {
            fnGetTable($("#dtResult"),
                {
                    BolEdit: jsonPermits.Edit,
                    BolDelete: true,
                    BolConsult: true,
                    StrPathEdit: $.cookie("strApplicationPath") + "/Formularios/Administracion/EditUsuario.aspx",
                    StrPathDelete: $.cookie("strApplicationPath") + "/api/Usuario/DelUser/"
                },
                [
                    { "sTitle": "No" },
                    { "sTitle": "Usuario" },
                    { "sTitle": "Nombre" },
                    { "sTitle": "Rol" },
                    { "sTitle": "Activo", "sClass": "text-center", "bSortable": false, "bSearchable": false},
                    { "sTitle": "Acciones", "sClass": "text-center", "bSortable": false, "bSearchable": false}
                ],
                $.cookie("strApplicationPath") + "/api/Usuario/GetUser/");
        }
        $(document).ready(function () {
            fnFillTable();
        });
    </script>
</asp:Content>
