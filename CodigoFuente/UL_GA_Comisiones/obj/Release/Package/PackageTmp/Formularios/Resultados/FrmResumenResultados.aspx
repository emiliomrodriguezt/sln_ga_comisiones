﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FrmResumenResultados.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.Resultados.FrmResumenResultados" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
     <i class="fa fa-sitemap" aria-hidden="true"></i> Resultados / Resumen
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <style type="text/css">
        #ulExport {
            min-width:100% !important;
        }
    </style>
    <div class="row">
        <div class="col-md-12 text-right">
            <div class="btn-group mr5">
            <button id="btnExporta" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-file-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Exporta <span class="caret"></span>
            </button>
            <ul id="ulExport" class="dropdown-menu text-left" role="menu">
              <li><a href="#" onclick="fnGuardarFirmas(1)"><i class="fa fa-file-pdf-o" aria-hidden="true">&nbsp;&nbsp;&nbsp;Pdf</i></a></li>
              <li><a href="#" onclick="fnGuardarFirmas(0)"><i class="fa fa-file-excel-o" aria-hidden="true">&nbsp;&nbsp;&nbsp;Excel</i></a></li>
            </ul>
          </div>
        </div>
    </div>
    <br />
    <form runat="server" ClientIDMode="Static" ID="frmResultados">
        <asp:HiddenField id="hidId" ClientIDMode="Static" runat="server" Value="0"/>
        <asp:HiddenField id="HidIsPdf" ClientIDMode="Static" runat="server" Value="1"/>
        <asp:HiddenField id="hidIColaborador"  ClientIDMode="Static" runat="server" Value="0"/>
        <asp:HiddenField id="hidAno" ClientIDMode="Static" runat="server" Value="0"/>
        <asp:HiddenField id="hidTotAPagar" ClientIDMode="Static" runat="server" value="0"/>
        <asp:HiddenField id="HidTotComison" ClientIDMode="Static" runat="server" value="0"/>
        <asp:HiddenField id="HidSas" ClientIDMode="Static" runat="server" value=""/>
        <asp:HiddenField id="HidAnticipos" ClientIDMode="Static" runat="server" value=""/>
        <asp:HiddenField id="HidBono" ClientIDMode="Static" runat="server" value=""/>
        <asp:HiddenField ID="__hidHtml" ClientIDMode="Static" runat="server" Value=""/>
        <div class="row">
            <div class="col-md-12 text-center">
                <h3>
                    Resultado de c&aacute;lculo de comisi&oacute;n <span class="ano"></span>
                </h3>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12">
                <p class="lead">
                    An&aacute;lisis de MUB contratado al 31 de diciembre de <span class="ano"></span>
                    <asp:Label ID="lblUnidadNegocio" runat="server"></asp:Label><br/>
                    Gerente de cuenta: <asp:Label ID="lblColaborador" runat="server"></asp:Label><br/>
                    Ingreso: <asp:Label ID="lblFechaingreso" runat="server"></asp:Label><br/>
                </p>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-6">
                <asp:GridView ClientIDMode="Static" ID="tblMubContratadoAno" runat="server" ShowHeader="False"  CssClass="table table-primary" emptydatatext="¡Sin datos encontrados!.">
                </asp:GridView>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-8">
                <asp:GridView ClientIDMode="Static" ID="tblMubContratado" runat="server" ShowHeader="False" CssClass="table table-primary cssClsSmallFont" emptydatatext="¡Sin datos encontrados!.">
                </asp:GridView>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12">
                 <asp:GridView ClientIDMode="Static" ID="tblComisionCentroCostos" runat="server" ShowHeader="False" CssClass="table table-primary cssClsSmallFont" emptydatatext="¡Sin datos encontrados!.">
                </asp:GridView>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12">
                Firmas Autorizacion
            </div>           
        </div>
        <div class="row">
            <div class="col-md-6">
                <label class="control-label" for="txtContraloria">Contralor&iacute;a:</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                    <asp:TextBox data-validar="requerido" runat="server" ClientIDMode="Static" ID="txtContraloria" MaxLength="100" CssClass="form-control" placeholder="Contraloria"></asp:TextBox>
                </div>
                <small class="form-text-error"></small>
            </div>   
            <div class="col-md-6">
               <label class="control-label" for="txtDirectorUn">Director UN:</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                    <asp:TextBox data-validar="requerido" runat="server" ClientIDMode="Static" ID="txtDirectorUn" MaxLength="100" CssClass="form-control" placeholder="Director UN"></asp:TextBox>
                </div>
                <small class="form-text-error"></small>
            </div>   
        </div>
        <br/>
        <div class="row">
            <div class="col-md-6">
                <label class="control-label" for="txtDirectorGrl">Director General GA:</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                    <asp:TextBox data-validar="requerido" runat="server" ClientIDMode="Static" ID="txtDirectorGrl" MaxLength="100" CssClass="form-control" placeholder="Director General GA"></asp:TextBox>
                </div>
                <small class="form-text-error"></small>
            </div>   
            <div class="col-md-6">
                <label class="control-label" for="txtPresidente">Presidente GA:</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                    <asp:TextBox data-validar="requerido" runat="server" ClientIDMode="Static" ID="txtPresidente" MaxLength="100" CssClass="form-control" placeholder="Presidente GA"></asp:TextBox>
                </div>
                <small class="form-text-error"></small>
            </div>   
        </div>
    </form>
    <div id="ModalObservciones" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button id="btnCloseModalObservciones" aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                    <h4 class="modal-title">
                        <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&iexcl;Seleccione Gerente y Porcentaje!.
                    </h4>
                </div>
                <div id="dataModalObservciones" class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label" for="txtTareaObservaciones">Observaciones:</label>
                            <div class="input-group input-group-lg mb15">
                                <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                <textarea data-validar="requerido" style="min-height: 100px !important" cols="5" id="txtTareaObservaciones" class="form-control"></textarea>
                                <input id="idObservacion" type="hidden" value=""/>
                            </div>
                            <small class="form-text-error"></small>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="fnSetObservaciones();"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var fnGuardarFirmas = function(typeDoc) {
            if (fnValidaDatos("frmResultados")) {
                $("#HidIsPdf").val(typeDoc);
                $("#frmResultados").submit();
            }
        }

        var fnObtenOtrosPagos = function () {
            var objAnticipos = JSON.parse($("#HidAnticipos").val());
            var objSsas = JSON.parse($("#HidSas").val());
            var objBono = JSON.parse($("#HidBono").val());

            var tfoot = "<tfoot><tr> <td colspan=\"10\">&nbsp;</td> <td> Total comisiones </td> <td>" + $("#HidTotComison").val() + "</td><td>&nbsp;</td></tr>";
            $.each(objAnticipos, function (index, value) {
                tfoot += "<tr> <td colspan=\"10\">&nbsp;</td> <td> " + value.tipo + " </td> <td>" + value.importe + "</td><td>&nbsp;</td></tr>";
            });
            $.each(objSsas, function (index, value) {
                tfoot += "<tr> <td colspan=\"10\">&nbsp;</td> <td> " + value.tipo + " </td> <td>" + value.importe + "</td><td>&nbsp;</td></tr>";
            });
            $.each(objBono, function (index, value) {
                tfoot += "<tr> <td colspan=\"10\">&nbsp;</td> <td> " + value.tipo + " </td> <td>" + value.importe + "</td><td>&nbsp;</td></tr>";
            });
            tfoot += "<tr> <td colspan=\"10\">&nbsp;</td> <td> Neto a pagar </td> <td>" + $("#hidTotAPagar").val() + "</td><td>&nbsp;</td></tr>";
            tfoot += "</tfoot>";
            $('#tblComisionCentroCostos').append(tfoot);
        }

        var fnSetObservaciones = function () {
            var intId = $("#idObservacion").val();
            var strObservacion = $("#txtTareaObservaciones").val();
            fnAjax($.cookie("strApplicationPath") + "/api/ObservacionesComison/SetObservaciones/",
                { IntId: intId, StrObservacion: strObservacion },
                "POST",
                function(dataResponse) {
                    var strType = "alert-danger";
                    if (dataResponse.BolSuccess) {
                        strType = "alert-success";
                        if (strObservacion === "") {
                            $("#" + intId).html("<a title=\"Agrega observaciones\" data-toggle=\"tooltip\" class=\"tooltips\"  href=\"javascript:void(0);\" onclick=\"fnShowModal('', '" + intId + "');\"><i class=\"fa fa-bars\" aria-hidden=\"true\"></i></a>");
                        } else {
                            $("#" + intId).html("<a title=\"Edita observaciones\" data-toggle=\"tooltip\" class=\"tooltips\"  href=\"javascript:void(0);\" onclick=\"fnShowModal('" + strObservacion + "', '" + intId + "');\">" + strObservacion + "</a>");
                        }
                    }
                    fnMessage(strType, dataResponse.StrMessage, true);
                    $("#btnCloseModalObservciones").trigger("click");
                }
            );
        }

        var fnShowModal = function (txt, id) {
            $("#txtTareaObservaciones").val(txt);
            $("#idObservacion").val(id);
            $("#ModalObservciones").modal("show");
        }

        var fnRecuperaAno = function () {
            $(".ano").html($("#hidAno").val());
            var anio = parseInt($("#hidAno").val());
            $(".ano1").html(anio-1);
        }

        var fnFilltblMubContratadoAno = function () {    
            $('#tblMubContratadoAno').append("<thead><tr> <th colspan=\"100\" class=\"text-center\"><p class=\"\">MUB CONTRATADO</p></th></tr></thead>");
            var bolEmpaty = $("#tblMubContratadoAno > tbody > tr > td").first().html() !== "¡Sin datos encontrados!.";
            if (bolEmpaty) {
                $('#tblMubContratadoAno').append("<tfoot><tr class=\"cssClsHidden\"><th colspan=\"3\" class=\"text-center\">&nbsp;</th></tr></tfoot>");
            }
        }

        var fnFilltblMubContratado = function() {
            $('#tblMubContratado').append("<thead><tr> <th>Año</th> <th>Proyecto</th> <th>No. C.C.</th> <th>MUB contratado</th></tr></thead>");
            var bolEmpaty = $("#tblMubContratado > tbody > tr > td").first().html() !== "¡Sin datos encontrados!.";
            if (bolEmpaty) {
                var total = 0.0;
                $("#tblMubContratado > tbody  > tr").each(function () {
                    var cantidad = $(this).children().next().next().next().html().replace(/,/gi, "").replace("$", "").replace(" ", "");
                    total += parseFloat(cantidad);
                });
                $('#tblMubContratado').append("<tfoot><tr> <td colspan=\"3\"> MUB Contratados</td> <td>" + accounting.formatMoney(total) + "</td></tr></tfoot>");
            }
        }

        var fnFilltblResultCalculo = function () {
            $('#tblComisionCentroCostos').append("<thead><tr> <th>No. C.C.</th> <th>Nombre C.C.</th> <th>Estatus</th> <th>Inicio</th> <th>Payback</th> <th>Término</th> <th>Total MUB acomulado <span class=\"ano\"><\span> </th> <th>Total MUB segun UN a <span class=\"ano\"><\span></th> <th>% de comisión</th> <th>Comisiones a <span class=\"ano\"><\span></th> <th>Importe pagado a dic <span class=\"ano1\"><\span></th> <th>Comisión a pagar <span class=\"ano\"><\span></th> <th> Observaciones </th></tr></thead>");
            var bolEmpaty = $("#tblComisionCentroCostos > tbody > tr > td").first().html() !== "¡Sin datos encontrados!.";
            if (bolEmpaty) {
                $("#tblComisionCentroCostos > tbody  > tr").each(function() {
                    var firstTd = $(this).children("td:first");
                    var lastTd = $(this).children("td:last");
                    $(firstTd).addClass("cssClsHidden");
                    var id = $(firstTd).html();
                    var strObservacion = $(lastTd).html();
                    $(lastTd).attr("id", id);
                    $(lastTd).addClass("text-center");
                    if (strObservacion === "&nbsp;") {
                        $(lastTd)
                            .html("<a title=\"Agrega observaciones\" data-toggle=\"tooltip\" class=\"tooltips\"  href=\"javascript:void(0);\" onclick=\"fnShowModal('', '" + id + "');\"><i class=\"fa fa-bars\" aria-hidden=\"true\"></i></a>");
                    } else {
                        $(lastTd)
                            .html("<a title=\"Edita observaciones\" data-toggle=\"tooltip\" class=\"tooltips\"  href=\"javascript:void(0);\" onclick=\"fnShowModal('" + strObservacion + "', '" + id + "');\">" + strObservacion + "</a>");
                    }
                });
                fnObtenOtrosPagos();
            }
        }

        var fnMuestraCalculo = function() {
            fnFilltblMubContratadoAno();
            fnFilltblMubContratado();
            fnFilltblResultCalculo();
        }  

        $(document).ready(function () {
            fnMuestraCalculo();
            fnRecuperaAno();

            $("#txtContraloria").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "GET",
                        url: $.cookie("strApplicationPath") + "/api/Usuario/GetUserDirectory/" + request.term,
                        dataType: "json",
                        success: function (data) {
                            response(data);
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    $("#txtContraloria").val(ui.item.label);
                    event.preventDefault();
                }
            });

            $("#txtDirectorUn").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "GET",
                        url: $.cookie("strApplicationPath") + "/api/Usuario/GetUserDirectory/" + request.term,
                        dataType: "json",
                        success: function (data) {
                            response(data);
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    $("#txtDirectorUn").val(ui.item.label);
                    event.preventDefault();
                }
            });

            $("#txtDirectorGrl").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "GET",
                        url: $.cookie("strApplicationPath") + "/api/Usuario/GetUserDirectory/" + request.term,
                        dataType: "json",
                        success: function (data) {
                            response(data);
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    $("#txtDirectorGrl").val(ui.item.label);
                    event.preventDefault();
                }
            });

            $("#txtPresidente").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "GET",
                        url: $.cookie("strApplicationPath") + "/api/Usuario/GetUserDirectory/" + request.term,
                        dataType: "json",
                        success: function (data) {
                            response(data);
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    $("#txtPresidente").val(ui.item.label);
                    event.preventDefault();
                }
            });
        });

    </script>
</asp:Content>
