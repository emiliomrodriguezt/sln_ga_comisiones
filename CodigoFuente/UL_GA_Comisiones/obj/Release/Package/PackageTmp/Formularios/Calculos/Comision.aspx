﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Comision.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.Calculos.Comision" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
    <i class="fa fa-sitemap" aria-hidden="true"></i> Operaciones / Calcular Comisión
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <main class="page">
        <form id="form1" runat="server">
            <div class="modal fade" id="modalRespuesta" role="dialog" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-lg" role="document" id="modalDialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button id="btnCloseModal" aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                            <h4 class="modal-title">Calcular comisión</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-8">
                                    <asp:Label ID="lblRespuesta" runat="server" CssClass="heading"></asp:Label>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar">Cerrar</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <div>
                <div class="row">
                    <div class="col-md-12 tituloPagina">
                        <asp:Label ID="lblTitulo" runat="server">C&aacute;lculo de comisi&oacute;n</asp:Label>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-2" style="text-align:right;">
                        <label for="ddlGerente" style="text-align:right">Gerente*:</label>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <asp:DropDownList data-validar="requerido" ID="ddlGerente" runat="server" style="text-align:center;text-align-last:center;" CssClass="form-control" AppendDataBoundItems="true">
                                <asp:ListItem Text="--- Seleccione ---" Value="0" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <small class="form-text-error"></small>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <asp:LinkButton  ID="btnBuscar" runat="server" CssClass="btn btn-primary validaForm" data-form="form1" OnClick="btnBuscar_Click"><i class="fa fa-search" aria-hidden="true"></i> Buscar</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2" style="text-align:right;">
                        <label for="ddlPeriodo" style="text-align:right">Año del c&aacute;lculo*:</label>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <asp:DropDownList ID="ddlPeriodo" data-validar="requerido" runat="server" style="text-align:center;text-align-last:center;" CssClass="form-control" AppendDataBoundItems="true">
                                <asp:ListItem Text="--- Seleccione ---" Value="0" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <small class="form-text-error"></small>
                    </div>
                    <div class="col-md-8"></div>
                </div>
                <%--<div class="row">
                    <div class="col-md-2" style="text-align:right;">
                        <label for="ddlMes" style="text-align:right">Mes de corte*:</label>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <asp:DropDownList data-validar="requerido" ID="ddlMes" runat="server" style="text-align:center;text-align-last:center;" CssClass="form-control">
                                <asp:ListItem Text="--- Seleccione ---" Value="0" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Enero" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Febrero" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Marzo" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Abril" Value="4"></asp:ListItem>
                                <asp:ListItem Text="Mayo" Value="5"></asp:ListItem>
                                <asp:ListItem Text="Junio" Value="6"></asp:ListItem>
                                <asp:ListItem Text="Julio" Value="7"></asp:ListItem>
                                <asp:ListItem Text="Agosto" Value="8"></asp:ListItem>
                                <asp:ListItem Text="Septiembre" Value="9"></asp:ListItem>
                                <asp:ListItem Text="Octubre" Value="10"></asp:ListItem>
                                <asp:ListItem Text="Noviembre" Value="11"></asp:ListItem>
                                <asp:ListItem Text="Diciembre" Value="12"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <small class="form-text-error"></small>
                    </div>
                    <div class="col-md-8"></div>
                </div>--%>
                <div class="row">
                    <div class="col-md-2" style="text-align:right;">
                        <label for="txtCuotas" style="text-align:right">Cuotas SAS*:</label>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <asp:DropDownList ID="ddlCuotas" runat="server" CssClass="form-control" data-validar="requerido" AppendDataBoundItems="true">
                            </asp:DropDownList>
                        </div>
                        <small class="form-text-error"></small>
                    </div>
                    <div class="col-md-9">

                    </div>
                </div>
                <div class="row" id="resultados" runat="server">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <table class="table">
                                <tr style="background-color:red;text-align:center;color:aliceBlue;font-size:18px;">
                                    <td>
                                        <asp:Label ID="lblUltimoMUB" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr style="background-color:#2F4F4F;">
                                    <td style="text-align:center;color:white;font-size:24px;">
                                        Proyectos
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:0;">
                                        <asp:GridView ID="gvProyectos" runat="server" AutoGenerateColumns="false" CssClass="table"
                                            ShowHeader="true" EmptyDataText="No se encontraron resultados" DataKeyNames="IdCC"
                                             OnRowDataBound="gvProyectos_RowDataBound" HeaderStyle-BackColor="#778899" HeaderStyle-Font-Size="16px" HeaderStyle-ForeColor="Black">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkProy" runat="server" Checked='<%#Eval("Seleccionado") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="No. C.C." DataField="StrCC" />
                                                <asp:BoundField HeaderText="Nombre C.C." DataField="NombreCC" />
                                                <asp:BoundField HeaderText="UN" DataField="StrUN" />
                                                <asp:BoundField HeaderText="Fecha payback" DataField="Payback" />
                                                <asp:BoundField HeaderText="MUB base de comisiones" DataField="MUBComision" DataFormatString="{0:c}" />
                                                <asp:BoundField HeaderText="Porcentaje de comisión" DataField="Porcentaje" DataFormatString="{0:P}" />
                                                <asp:BoundField HeaderText="Importe pagado" DataField="MontoPagado" DataFormatString="{0:c}" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <table class="table">
                                <tr style="background-color:#2F4F4F;">
                                    <td style="text-align:center;color:white;font-size:24px;">
                                        Otros importes
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:0;">
                                        <asp:GridView ID="gvOtros" runat="server" AutoGenerateColumns="false" CssClass="table"
                                            ShowHeader="true" EmptyDataText="No se encontraron resultados" DataKeyNames="IdOtro, IdTipo, Importe"
                                             HeaderStyle-BackColor="#778899" HeaderStyle-Font-Size="16px" HeaderStyle-ForeColor="Black" AlternatingRowStyle-BackColor="#f5f5f5">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkOtro" runat="server" Checked='<%#Eval("Seleccionado") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="No. C.C." DataField="StrCC" />
                                                <asp:BoundField HeaderText="Nombre C.C." DataField="NombreCC" />
                                                <asp:BoundField HeaderText="Concepto" DataField="Concepto" />
                                                <asp:BoundField HeaderText="Importe" DataField="Importe" DataFormatString="{0:c}" />
                                                <asp:TemplateField HeaderText="Importe a pagar">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtOtro" runat="server" CssClass="money" ></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="A&ntilde;o" DataField="Anio" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4" style="text-align:center;">
                            <div class="form-group">
                                <asp:LinkButton ID="btnGuardar" runat="server" CssClass="btn btn-primary validaForm" data-form="form1" OnClick="btnGuardar_Click"><i class="fa fa-save" aria-hidden="true"></i> Calcular comisión</asp:LinkButton>
                            </div>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
            </div>
        </form>
    </main>
    <script type="text/javascript">
        function muestraError()
        {
            $('#btnCerrar').show();
            $('#modalDialog').removeClass('modal-success');
            $('#modalDialog').addClass('modal-danger');
            $('#modalRespuesta').modal('show');
        }

        function muestraExito()
        {
            $('#btnCerrar').hide();
            $('#modalDialog').removeClass('modal-danger');
            $('#modalDialog').addClass('modal-success');
            $('#modalRespuesta').modal('show');
        }
    </script>
    <asp:Literal ID="litScripts" runat="server"></asp:Literal>
</asp:Content>
