﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AutorizarComision.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.Calculos.AutorizarComision" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
    <i class="fa fa-sitemap" aria-hidden="true"></i> Operaciones / Autorizar Comisión
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <main class="page">
        <form id="form1" runat="server">
            <div class="modal fade" id="modalRespuesta" role="dialog" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog" role="document" id="modalDialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button id="btnCloseModal" aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                            <h4 class="modal-title">Autorizar comisión</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-8">
                                    <label>¿Est&aacute; seguro de autorizar el pago de esta comisi&oacute;n?</label>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                            <div class="row">
                                &nbsp;
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-8">
                                    <label>Una vez autorizado no se podr&aacute;n realizar cambios a la comisi&oacute;n pagada</label>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnAutorizar" onclick="javascript:window.location='AutorizarComision.aspx?id='+$('#<%=hdnID.ClientID %>').val();">Autorizar</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal" id="btnCerrar">Cerrar</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <div class="row">
                <div class="col-md-12 tituloPagina">
                    <asp:Label ID="lblTitulo" runat="server">Autorizar comisi&oacute;n</asp:Label>
                </div>
            </div>
            <div class="row">&nbsp;</div>
            <div class="row">
                <div class="col-md-12">
                    <asp:GridView ID="gvComisiones" runat="server" AutoGenerateColumns="false" CssClass="table table-straped dataTables"
                        ShowHeader="true" EmptyDataText="No se encontraron resultados" >
                        <Columns>
                            <asp:BoundField HeaderText="ID" DataField="IdComision" />
                            <asp:BoundField HeaderText="Nombre del colaborador" DataField="Colaborador" />
                            <asp:BoundField HeaderText="Unidad de negocio" DataField="UnidadNegocio" />
                            <asp:BoundField HeaderText="A&ntilde;o" DataField="Anio" />
                            <asp:BoundField HeaderText="Monto a pagar" DataField="MontoAPagar" DataFormatString="{0:c}" />
                            <asp:TemplateField HeaderText="Formato">
                                <ItemTemplate>
                                    <a href="/Formularios/Resultados/FrmResumenResultados.aspx?id=<%#(HttpContext.Current.Server.UrlEncode(Utility_GA_Comisiones.ClsCryptographyBl.EncodeString(Eval("IdComision").ToString()))) %>">Ver</a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <a href='javascript:confirmar("<%#Eval("IdComision").ToString() %>");'>Autorizar</a>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <div class="row">
                &nbsp;
                <asp:HiddenField ID="hdnID" runat="server" />
            </div>
        </form>
    </main>
    <script type="text/javascript">
        function confirmar(id)
        {
            $('#<%=hdnID.ClientID%>').val(id);
            muestraConfirmacion();
        }

        function muestraConfirmacion()
        {
            $('#modalDialog').removeClass('modal-danger');
            $('#modalDialog').addClass('modal-success');
            $('#modalRespuesta').modal('show');
        }
    </script>
    <asp:Literal ID="litScripts" runat="server"></asp:Literal>
</asp:Content>
