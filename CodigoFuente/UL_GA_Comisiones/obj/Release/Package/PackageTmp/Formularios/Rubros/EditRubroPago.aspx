﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditRubroPago.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.Rubros.EditRubroPago" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
    <i class="fa fa-sitemap" aria-hidden="true"></i> Configuración / Conceptos de comisión / Actualizar Registro
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <main class="page">
        <form id="form1" runat="server">
            <div class="modal fade" id="modalRespuesta" role="dialog" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-lg" role="document" id="modalDialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button id="btnCloseModal" aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                            <h4 class="modal-title">Actualizar Registro</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-8">
                                    <asp:Label ID="lblRespuesta" runat="server" CssClass="heading"></asp:Label>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnCerrar">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="row">
                    <div class="col-md-6">
                        <asp:HiddenField ID="hdnId" runat="server" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="txtConcepto">Concepto de comisión*:</label>
                        <div class="input-group input-group-lg mb15">
                            <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                            <asp:TextBox ClientIDMode="Static" ID="txtConcepto" data-validar="requerido" runat="server" CssClass="form-control" placeholder="Concepto de comisión"></asp:TextBox>
                        </div>
                        <small class="form-text-error"></small>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-6">
                        <label for="txtPorcentaje">Porcentaje de pago*:</label>
                        <div class="input-group input-group-lg mb15">
                            <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                            <asp:TextBox ClientIDMode="Static" ID="txtPorcentaje" data-validar="requerido|numerico" MaxLength="10" runat="server" CssClass="form-control onlyNumber" placeholder="Porcentaje de pago"></asp:TextBox>
                        </div>
                        <small class="form-text-error"></small>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-6">
                        <label for="ddlEstatus">Estatus*:</label>
                        <div class="input-group input-group-lg mb15">
                            <span class="input-group-addon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                            <asp:DropDownList ClientIDMode="Static" ID="ddlEstatus" runat="server" CssClass="form-control" >
                                <asp:ListItem Text="Activo" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-6 text-right">
                        <div class="form-group">
                            <a href="<%= ResolveUrl("~/Formularios/Rubros/RubrosPago.aspx") %>" class="btn btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Regresar</a>
                        <button type="reset" class="btn btn-darkblue"><i class="fa fa-eraser" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Limpiar</button>
                            <asp:LinkButton  ID="LinkButton1" runat="server" CssClass="btn btn-primary validaForm" data-form="form1" OnClick="btnGuardar_Click"><i class="fa fa-save" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Guardar</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </main>
    <script type="text/javascript">
        function muestraError() {
            $('#btnCerrar').show();
            $('#modalDialog').removeClass('modal-success');
            $('#modalDialog').addClass('modal-danger');
            $('#modalRespuesta').modal('show');
        }

        function muestraExito() {
            $('#btnCerrar').show();
            $('#modalDialog').removeClass('modal-danger');
            $('#modalDialog').addClass('modal-success');
            $('#modalRespuesta').modal('show');
        }
    </script>
    <asp:Literal ID="litScripts" runat="server"></asp:Literal>
</asp:Content>
