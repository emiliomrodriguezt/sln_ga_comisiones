﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AltaEmpleado.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.Colaboradores.AltaEmpleado" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
    <i class="fa fa-sitemap" aria-hidden="true"></i> Configuración / Empleados / Nuevo Registro
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <main class="page">
        <form id="form1" action="AltaEmpleado.aspx" runat="server" ClientIDMode="Static">
            <div class="modal fade" id="modalRespuesta" role="dialog" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-lg" role="document" id="modalDialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button id="btnCloseModal" aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                            <h4 class="modal-title">Nuevo Registro</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-8">
                                    <asp:Label ID="lblRespuesta" runat="server" CssClass="heading"></asp:Label>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar">Cerrar</button>
                            <button type="button" class="btn btn-primary" id="btnAceptar" onclick="javascript:window.location.href='Empleados.aspx';return false;">Aceptar</button>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="row">
                    <div class="col-md-4">
                        <label class="control-label" for="txtNoEmpleado">No. de colaborador*:</label>
                        <div class="input-group input-group-lg mb15">
                            <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                            <asp:TextBox ID="txtNoEmpleado" ClientIDMode="Static" data-validar="requerido" runat="server" CssClass="form-control" placeholder="No. de colaborador"></asp:TextBox>
                        </div>
                        <small class="form-text-error"></small>
                    </div>    
                    <div class="col-md-4">
                        <label for="txtNombre">Nombre completo*:</label>
                        <div class="input-group input-group-lg mb15">
                           <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                           <asp:TextBox ClientIDMode="Static" ID="txtNombre" data-validar="requerido" MaxLength="300" runat="server" placeholder="Nombre completo" CssClass="form-control UpperCase"></asp:TextBox>
                        </div>
                        <small class="form-text-error"></small>
                    </div>
                    <div class="col-md-4">
                        <label for="ddlEstatus">Estatus*:</label>
                        <div class="input-group input-group-lg mb15">
                            <span class="input-group-addon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                            <asp:DropDownList ClientIDMode="Static" ID="ddlEstatus" runat="server" CssClass="form-control">
                                <asp:ListItem Text="Activo" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Baja" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="ddlPuesto">Puesto*:</label>
                        <div class="input-group input-group-lg mb15">
                            <span class="input-group-addon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                            <asp:DropDownList ClientIDMode="Static" ID="ddlPuesto" data-validar="requerido" runat="server" CssClass="form-control" AppendDataBoundItems="true">
                                <asp:ListItem Text="Seleccione" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <small class="form-text-error"></small>
                    </div>
                    <div class="col-md-4">
                        <label for="txtFechaAlta">Fecha de alta*:</label>
                        <div class="input-group input-group-lg mb15">
                            <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                            <asp:TextBox ClientIDMode="Static" ID="txtFechaAlta" data-validar="requerido|fecha" runat="server" CssClass="form-control datepickerEs" placeholder="dd/mm/yyyy"></asp:TextBox>
                        </div>
                        <small class="form-text-error"></small>
                    </div>
                    <div class="col-md-4">
                        <label for="txtFechaBaja">Fecha de baja:</label>
                        <div class="input-group input-group-lg mb15">
                            <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                            <asp:TextBox ClientIDMode="Static" ID="txtFechaBaja" data-validar="fecha" runat="server" CssClass="form-control datepickerEs " placeholder="dd/mm/yyyy"></asp:TextBox>
                        </div>
                        <small class="form-text-error"></small>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="ddlCentroCosto">Nombre UN/C.C.*:</label>
                        <div class="input-group input-group-lg mb15">
                            <span class="input-group-addon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                            <asp:DropDownList ClientIDMode="Static" ID="ddlCentroCosto" data-validar="requerido" runat="server" CssClass="form-control" AppendDataBoundItems="true">
                                <asp:ListItem Text="Seleccione" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <small class="form-text-error"></small>
                    </div>
                    <div class="col-md-4">
                        <label for="ddlEmpresa">Empresa*:</label>
                        <div class="input-group input-group-lg mb15">
                            <span class="input-group-addon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                            <asp:DropDownList ClientIDMode="Static" ID="ddlEmpresa" data-validar="requerido" runat="server" CssClass="form-control" AppendDataBoundItems="true">
                                <asp:ListItem Text="Seleccione" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <small class="form-text-error"></small>
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="txtNomina">Sueldo por nómina*:</label>
                        <div class="input-group input-group-lg mb15">
                            <span class="input-group-addon"><i class="fa fa-usd" aria-hidden="true"></i></span>
                            <asp:TextBox ClientIDMode="Static" ID="txtNomina" data-validar="requerido|numerico" runat="server" placeholder="Sueldo por nómina" CssClass="form-control money "></asp:TextBox>
                        </div>
                        <small class="form-text-error"></small>
                    </div>
                    <div class="col-md-4">
                        <label for="txtHonorarios">Honorarios*:</label>
                        <div class="input-group input-group-lg mb15">
                            <span class="input-group-addon"><i class="fa fa-usd" aria-hidden="true"></i></span>
                            <asp:TextBox ClientIDMode="Static" ID="txtHonorarios" data-validar="requerido|numerico" runat="server" placeholder="Honorarios" CssClass="form-control money"></asp:TextBox>
                        </div>
                        <small class="form-text-error"></small>
                    </div>
                    <div class="col-md-4">
                        <label for="txtMensual">Sueldo mensual:</label>
                        <div class="input-group input-group-lg mb15">
                            <span class="input-group-addon"><i class="fa fa-ban" aria-hidden="true"></i></span>
                            <asp:TextBox ClientIDMode="Static" ID="txtMensual" runat="server" CssClass="form-control money" Enabled="false" placeholder="Honorarios" ></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4"></div>
                    <div class="col-md-4 text-right">
                        <a href="<%= ResolveUrl("~/Formularios/Colaboradores/Empleados.aspx") %>" class="btn btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Regresar</a>
                        <button type="reset" class="btn btn-darkblue"><i class="fa fa-eraser" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Limpiar</button>
                        <asp:LinkButton  ID="btnGuardar" runat="server" CssClass="btn btn-primary validaForm" data-form="form1" OnClick="btnGuardar_Click"><i class="fa fa-save" aria-hidden="true"></i> Guardar</asp:LinkButton>
                    </div>
                </div>
            </div>
        </form>
    </main>
    <script type="text/javascript">
        function sumaSueldo() {
            var nomina = parseFloat($("#txtNomina").val().replace(/,/gi, "").replace("$", "").replace(" ", ""));
            var honorarios = parseFloat($("#txtHonorarios").val().replace(/,/gi, "").replace("$", "").replace(" ", ""));
            if (isNaN(nomina) || isNaN(honorarios)) {
                $("#txtMensual").val('');
                return;
            }
            $("#txtMensual").val(accounting.formatMoney((nomina + honorarios).toFixed(2)));
        }

        function muestraError()
        {
            $('#btnCerrar').show();
            $('#btnAceptar').hide();
            $('#modalDialog').removeClass('modal-success');
            $('#modalDialog').addClass('modal-danger');
            $('#modalRespuesta').modal('show');
        }

        function muestraExito()
        {
            $('#btnCerrar').hide();
            $('#btnAceptar').show();
            $('#modalDialog').removeClass('modal-danger');
            $('#modalDialog').addClass('modal-success');
            $('#modalRespuesta').modal('show');
        }

        $(document).ready(function () {
            sumaSueldo();
            $("#txtNomina").blur(sumaSueldo);
            $("#txtHonorarios").blur(sumaSueldo);
        });
    </script>
    <asp:Literal ID="litScripts" runat="server"></asp:Literal>
</asp:Content>
