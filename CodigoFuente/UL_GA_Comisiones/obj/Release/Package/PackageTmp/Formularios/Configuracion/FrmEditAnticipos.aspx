﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FrmEditAnticipos.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.Configuracion.FrmEditAnticipos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
    <i class="fa fa-sitemap" aria-hidden="true"></i> Configuraci&oacute;n / Anticipos / <label id="bredCrub">Nuevo Registro</label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <form runat="server" ClientIDMode="Static" ID="frmAnticipo" class="form-striped" action="FrmEditAnticipos.aspx" method="post">
        <asp:HiddenField runat="server" ClientIDMode="Static" id="hidId" Value="0"/>
        <div class="row">
            <div class="col-md-6">
                <label class="control-label" for="ddlColaborador">Colaborador*:</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                    <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlColaborador" data-validar="requerido" CssClass="form-control"/>    
                </div>
                <small class="form-text-error"></small>
            </div>
            <div class="col-md-6">
                <label class="control-label" for="ddlCentroCostos"> Centro Costos*:</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                    <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlCentroCostos" data-validar="requerido" CssClass="form-control"/>    
                </div>
                <small class="form-text-error"></small>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-6">
                <label class="control-label" for="txtImporteNeto">Importe Neto*:</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-usd" aria-hidden="true"></i></span>
                     <asp:TextBox runat="server" MaxLength="20" ClientIDMode="Static" ID="txtImporteNeto" data-validar="requerido|numerico" placeholder="Importe Neto" CssClass="form-control money"></asp:TextBox>
                </div>
                <small class="form-text-error"></small>
            </div>
            <div class="col-md-6">
                <label class="control-label" for="txtImporteBruto">Importe Bruto*:</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-usd" aria-hidden="true"></i></span>
                    <asp:TextBox runat="server" MaxLength="20" ClientIDMode="Static" ID="txtImporteBruto" data-validar="requerido|numerico" placeholder="Importe Bruto" CssClass="form-control money"></asp:TextBox>
                </div>
                <small class="form-text-error">hola</small>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-6">
                <label class="control-label" for="txtCheque">No. Cheque*:</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                     <asp:TextBox MaxLength="100" runat="server" ClientIDMode="Static" ID="txtCheque" data-validar="requerido" placeholder="No. Cheque" CssClass="form-control"></asp:TextBox>
                </div>
                <small class="form-text-error">hola</small>
            </div>
            <div class="col-md-6">
                <label class="control-label" for="txtPoliza">No. Poliza*:</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                    <asp:TextBox runat="server" MaxLength="100" ClientIDMode="Static" ID="txtPoliza" data-validar="requerido" placeholder="No de poliza" CssClass="form-control"></asp:TextBox>
                </div>
                <small class="form-text-error">hola</small>
            </div>
        </div>
         <br/>
        <div class="row">
            <div class="col-md-6">
                 <label class="control-label" for="ddlEmpresas">Empresa Pago*:</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                    <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlEmpresas" data-validar="requerido" CssClass="form-control"/>    
                </div>
                <small class="form-text-error">hola</small>
            </div>
            <div class="col-md-6">
                 <label class="control-label" for="txtFecha">Fecha:</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                    <asp:TextBox runat="server" MaxLength="15" ClientIDMode="Static" ID="txtFecha" data-validar="requerido" placeholder="dd/mm/yyyy" CssClass="form-control datepickerEs"></asp:TextBox>
                </div>
                <small class="form-text-error">hola</small>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12 text-right">
                <a href="<%= ResolveUrl("~/Formularios/Configuracion/FrmAdminAnticipos.aspx") %>" class="btn btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Regresar</a>
                <button type="reset" class="btn btn-darkblue"><i class="fa fa-eraser" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Limpiar</button>
                <button id="btnGuardar" type="button"  class="btn btn-primary validaForm"><i class="fa fa-save" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Guardar</button>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#btnGuardar").click(function () {
                if (fnValidaDatos("frmAnticipo")) {
                    var objNeto = $("#txtImporteNeto");
                    var objBruto = $("#txtImporteBruto");

                    var decImporteBruto =  parseFloat($(objBruto).val().replace(/,/gi, "").replace("$", "").replace(" ", ""));
                    var decImporteNeto = parseFloat($(objNeto).val().replace(/,/gi, "").replace("$", "").replace(" ", ""));

                    if (decImporteBruto <= decImporteNeto) {
                        $(objBruto).parent().next().show();
                        $(objBruto).parent().next().html("no es valido");
                        $(objBruto).addClass("form-control-error");
                        fnMessage("alert-danger", "El importe bruto [" + $(objBruto).val() + "] no puede ser menor o igual al impor neto [" + $(objNeto).val() + "] ", true)
                    } else {

                        $("#frmAnticipo").submit();
                    }
                }
            });
            if (fnGetQuery()["edit"] === "1") {
                fnDiSabledForm("frmAnticipo");
                $("#bredCrub").html("Consulta");
            } else if (fnGetQuery()["id"] != null) {
                $("#bredCrub").html("Actualiza");
            }
        });
    </script>
</asp:Content>
