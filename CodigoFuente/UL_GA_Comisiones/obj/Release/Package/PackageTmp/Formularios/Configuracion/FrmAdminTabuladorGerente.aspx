﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FrmAdminTabuladorGerente.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.Configuracion.FrmAdminTabuladores" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
    <i class="fa fa-sitemap" aria-hidden="true"></i> Configuraci&oacute;n / Tabuladores
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <form runat="server">
        <div class="row">
            <div class="col-md-4">
                <label for="ddlAno" class="control-label">Año Vigencia</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                    <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlAno" data-validar="requerido" CssClass="form-control"/>  
                </div>
                <small class="form-text-error"></small>            
            </div>
            <div class="col-md-4">
                <label for="ddlTipoTabulador" class="control-label">Tipo tabulador</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                    <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlTipoTabulador" data-validar="requerido" CssClass="form-control"/>    
                </div>
                <small class="form-text-error"></small>            
            </div>
            <div class="col-md-4 text-right">
                <br/><br/>
                <button onclick="fnShowCopyForm();" type="button" class="btn btn-success"><i class="fa fa-files-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Copiar</button>
                <button onclick="fnFillTable();" type="button" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Buscar</button>
                <a href="<%= ResolveUrl("~/Formularios/Configuracion/FrmEditTabuladorGerente.aspx") %>" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Agregar Tabulador</a>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped" id="dtResult"></table>
            </div>
        </div>
        <div id="ModalCopyTabulador" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" aria-hidden="false">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button id="btnCloseModalCopyTabulado" aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                        <h4 class="modal-title">
                            <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&iexcl;Seleccione Gerente y Porcentaje!.
                        </h4>
                    </div>
                    <div id="dataModalCopyTabulador" class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <label class="control-label" for="ddlAnoOrigenModalCopyTabulador">Año origen:</label>
                                <div class="input-group input-group-lg mb15">
                                    <span class="input-group-addon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                    <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlAnoOrigenModalCopyTabulador" data-validar="requerido" CssClass="form-control"/>
                                </div>
                                <small class="form-text-error"></small>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label class="control-label" for="ddlAnoDestinoModalCopyTabulador">Ano destino:</label>
                                <div class="input-group input-group-lg mb15">
                                    <span class="input-group-addon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                    <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlAnoDestinoModalCopyTabulador" data-validar="requerido" CssClass="form-control"/>
                                </div>
                                <small class="form-text-error"></small>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="fnCopyTabulador();"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        var fnCopyTabulador = function() {
            if (fnValidaDatos("dataModalCopyTabulador")) {
                fnAjax($.cookie("strApplicationPath") + "/api/Tabulador/CopyTabuladorGerentes/",
                {
                    IntAnoOld: $("#ddlAnoOrigenModalCopyTabulador").val(),
                    IntAnoNew: $("#ddlAnoDestinoModalCopyTabulador").val(),
                },
                "PUT",
                function (dataResponse) {
                    var strType = "alert-danger";
                    if (dataResponse.BolSuccess) {
                        fnFillTable();
                        strType = "alert-success";
                    }
                    fnMessage(strType, dataResponse.StrMessage, true);
                    $("#btnCloseModalCopyTabulado").trigger("click");
                });
            }
        }

        var fnShowCopyForm = function() {
            $("#ddlAnoOrigenModalCopyTabulador").val("0");
            $("#ddlAnoDestinoModalCopyTabulador").val("0");
            $("#ModalCopyTabulador").modal("show");
        }

        var fnFillTable = function() {
            fnGetTable($("#dtResult"),
                {
                    IntIdTipoTabulador: $("#ddlTipoTabulador").val(),
                    IntIdAno: $("#ddlAno").val(),
                    ObjClsDataTableEl : {
                        BolEdit: jsonPermits.Edit,
                        BolDelete: true,
                        BolConsult: true,
                        StrPathEdit: $.cookie("strApplicationPath") + "/Formularios/Configuracion/FrmEditTabuladorGerente.aspx",
                        StrPathDelete: $.cookie("strApplicationPath") + "/api/Tabulador/DelTabuladorGerentes/"
                    }
                },
                [
                    { "sTitle": "No" },
                    { "sTitle": "Tipo" },
                    { "sTitle": "Costo Mensual GA" },
                    { "sTitle": "MUB Mínimo" },
                    { "sTitle": "Comisión" },
                    { "sTitle": "Activo", "sClass": "text-center", "bSortable": false, "bSearchable": false },
                    { "sTitle": "Acciones", "sClass": "text-center", "bSortable": false, "bSearchable": false }
                ],
                $.cookie("strApplicationPath") + "/api/Tabulador/GetTabuladoresGerentes/");
        }

        $(document).ready(function () {
            fnFillTable();
        });
    </script>
</asp:Content>
