﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FrmEditBonos.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.Bonos.FrmEditBonos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
    <i class="fa fa-sitemap" aria-hidden="true"></i> Configuraci&oacute;n / Bonos / <label id="bredCrub">Nuevo Registro</label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <form runat="server" ClientIDMode="Static" ID="frmBono" class="form-striped" action="FrmEditBonos.aspx" method="post">
        <asp:HiddenField runat="server" ClientIDMode="Static" id="hidId" Value="0"/>
        <asp:HiddenField runat="server" ClientIDMode="Static" id="hidPagado" Value="false"/>
        <div class="row">
            <div class="col-md-6">
                <label class="control-label" for="ddlColaborador">Colaborador*:</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                    <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlColaborador" data-validar="requerido" CssClass="form-control"/>    
                </div>
                <small class="form-text-error"></small>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-6">
                <label class="control-label" for="txtMontoBono">Monto Bono*:</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-usd" aria-hidden="true"></i></span>
                     <asp:TextBox runat="server" MaxLength="20" ClientIDMode="Static" ID="txtMontoBono" data-validar="requerido|numerico" placeholder="Monto Bono" CssClass="form-control money"></asp:TextBox>
                </div>
                <small class="form-text-error"></small>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-6">
                 <label class="control-label" for="txtAnio">Fecha*:</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                    <asp:TextBox runat="server" MaxLength="4" ClientIDMode="Static" ID="txtAnio" data-validar="requerido|numerico" placeholder="Año" CssClass="form-control onlyNumber"></asp:TextBox>
                </div>
                <small class="form-text-error">hola</small>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-6 text-right">
                <a href="<%= ResolveUrl("~/Formularios/Bonos/FrmAdminBonos.aspx") %>" class="btn btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Regresar</a>
                <button type="reset" class="btn btn-darkblue"><i class="fa fa-eraser" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Limpiar</button>
                <button id="btnGuardar" type="submit" data-form="frmBono"  class="btn btn-primary validaForm"><i class="fa fa-save" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Guardar</button>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).ready(function () {
                if (fnGetQuery()["edit"] === "1") {
                    fnDiSabledForm("frmBono");
                    $("#bredCrub").html("Consulta");
                } else if (fnGetQuery()["id"] != null) {
                    $("#bredCrub").html("Actualiza");
                }
            });
        });
    </script>
</asp:Content>
