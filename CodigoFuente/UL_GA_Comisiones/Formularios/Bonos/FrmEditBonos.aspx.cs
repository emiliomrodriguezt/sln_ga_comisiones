﻿using System;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL_GA_Comisiones;
using EL_GA_Comisiones;
using Utility_GA_Comisiones;

namespace UL_GA_Comisiones.Formularios.Bonos
{
    public partial class FrmEditBonos : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["objClsResultEl"] != null)
            {
                var objClsResultEl = (ClsResultEl)Session["objClsResultEl"];
                Session["objClsResultEl"] = null;
                Page.Controls.Add(new LiteralControl($"<script>fnMessage(\" {(objClsResultEl.BolSuccess ? "alert-success" : "alert-danger")} \", \"{ objClsResultEl.StrMessage }\", true);</script>"));
            }

            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)Context.User.Identity).Ticket.UserData);

            if (Request.Form.Count == 0)
            {
                FillDdl(objClsUsuarioActiveDirectoryEl);
                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    GetData(objClsUsuarioActiveDirectoryEl, Convert.ToInt32(ClsCryptographyBl.DecodeString(HttpContext.Current.Server.HtmlDecode(Request.QueryString["id"]))));
                }
            }

            if (Request.Form.Count > 0 &&
                !string.IsNullOrEmpty(hidId.Value) &&
                !string.IsNullOrEmpty(hidPagado.Value) &&
                ddlColaborador.SelectedIndex > 0 &&
                !string.IsNullOrEmpty(txtMontoBono.Text) &&
                !string.IsNullOrEmpty(txtAnio.Text))
            {
                SetData(objClsUsuarioActiveDirectoryEl);
            }
        }

        private  void FillDdl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl)
        {
            //ddlColaboradores
            var objClsCollaboratorBl = new ClsCollaboratorBl(objClsUsuarioActiveDirectoryEl);
            var lstListItem = objClsCollaboratorBl.GetCollaborators(0, "").Where(itm => itm.BolActivo.Equals(true))
                                                                          .OrderBy(itm => itm.StrNombreColaborador)
                                                                          .Select(itm => new ListItem() { Value = itm.IntId.ToString(), Text = itm.StrNombreColaborador }).ToList();

            lstListItem.Insert(0, new ListItem() { Value = "0", Text = "Selecciona" });
            ddlColaborador.Items.AddRange(lstListItem.ToArray());
        }

        private void GetData(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl, int intId)
        {
            var objClsBonusesBl = new ClsBonusesBl(objClsUsuarioActiveDirectoryEl);
            var objClsTblBonosEl = objClsBonusesBl.GetBonuses(intId, 0).FirstOrDefault();

            hidId.Value = objClsTblBonosEl?.IntId.ToString();
            hidPagado.Value = objClsTblBonosEl?.BolPagado.ToString();
            ddlColaborador.SelectedValue = objClsTblBonosEl?.IntIdColaborador.ToString();
            txtMontoBono.Text = $"{objClsTblBonosEl?.DecMontoBono:c}";
            txtAnio.Text = objClsTblBonosEl?.IntAnio.ToString();
        }

        private void SetData(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl)
        {
            var intId = Convert.ToInt32(hidId.Value);
            var bolPagado = Convert.ToBoolean(hidPagado.Value);
            var intIdColaborador = Convert.ToInt32(ddlColaborador.SelectedValue);
            var decMontoBono = decimal.Parse(txtMontoBono.Text, NumberStyles.AllowCurrencySymbol | NumberStyles.Number);
            var intAnio = Convert.ToInt32(txtAnio.Text);

            var objClsBonusesBl = new ClsBonusesBl(objClsUsuarioActiveDirectoryEl);
            var objClsSimpleResultEl = objClsBonusesBl.SetBonus(intId, intIdColaborador, decMontoBono, intAnio, bolPagado);
            Session["objClsResultEl"] = new ClsResultEl() { StrId = objClsSimpleResultEl.IntId.ToString(), BolSuccess = objClsSimpleResultEl.BolSuccess, StrMessage = objClsSimpleResultEl.StrMessage };
            Response.Redirect("FrmAdminBonos.aspx", true);
        }
    }
}