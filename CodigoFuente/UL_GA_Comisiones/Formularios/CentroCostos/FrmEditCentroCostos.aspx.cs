﻿using System;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL_GA_Comisiones;
using EL_GA_Comisiones;
using Utility_GA_Comisiones;

namespace UL_GA_Comisiones.Formularios.CentroCostos
{
    public partial class FrmEditCentroCostos : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["objClsResultEl"] != null)
            {
                var objClsResultEl = (ClsResultEl)Session["objClsResultEl"];
                Session["objClsResultEl"] = null;
                Page.Controls.Add(new LiteralControl($"<script>fnMessage(\" {(objClsResultEl.BolSuccess ? "alert-success" : "alert-danger")} \", \"{ objClsResultEl.StrMessage }\", true);</script>"));
            }

            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)Context.User.Identity).Ticket.UserData);

            if (Request.Form.Count == 0)
            {
                FillDdl(objClsUsuarioActiveDirectoryEl);
                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    GetData(objClsUsuarioActiveDirectoryEl, Convert.ToInt32(ClsCryptographyBl.DecodeString(HttpContext.Current.Server.HtmlDecode(Request.QueryString["id"]))));
                }
            }

            if (Request.Form.Count > 0 &&
                !string.IsNullOrEmpty(hidId.Value) &&
                !string.IsNullOrEmpty(txtFechaPayBack.Text) &&
                !string.IsNullOrEmpty(txtIngreso.Text) &&
                !string.IsNullOrEmpty(txtEgreso.Text) &&
                !string.IsNullOrEmpty(txtMubContratado.Text) &&
                !string.IsNullOrEmpty(txtObservacion.Text) &&
                ddlUnStaff.SelectedIndex > 0 &&
                ddlGerentes.SelectedIndex > 0 &&
                ddlAnoCreacion.SelectedIndex > 0 )
            {
                SetData(objClsUsuarioActiveDirectoryEl);
            }
        }

        private void FillDdl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl)
        {
            //ddlGerentes
            var objClsCollaboratorBl = new ClsCollaboratorBl(objClsUsuarioActiveDirectoryEl);
            var objClsRubroPagoBl = new ClsRubroPagoBl();
            var lstListItem = objClsCollaboratorBl.GetCollaborators(0, "").Where(itm => itm.BolActivo.Equals(true) && itm.IntIdPuesto.Equals(1))
                                                                          .OrderBy(itm => itm.StrNombreColaborador)
                                                                          .Select(itm => new ListItem { Value = itm.IntId.ToString(), Text = itm.StrNombreColaborador }).ToList();

            lstListItem.Insert(0, new ListItem { Value = "0", Text = "Selecciona" });
            ddlGerentes.Items.AddRange(lstListItem.ToArray());
            ddlGerneteMubModal.Items.AddRange(lstListItem.ToArray());
            ddlGerenteModalRubrosColaborador.Items.AddRange(lstListItem.ToArray());
            //ddlUnStaff
            lstListItem = objClsCollaboratorBl.GetCollaborators(0, "").Where(itm => itm.BolActivo.Equals(true) && itm.IntIdPuesto.Equals(2))
                                                                      .OrderBy(itm => itm.StrNombreColaborador)
                                                                      .Select(itm => new ListItem { Value = itm.IntId.ToString(), Text = itm.StrNombreColaborador }).ToList();
            lstListItem.Insert(0, new ListItem { Value = "0", Text = "Selecciona" });
            ddlUnStaff.Items.AddRange(lstListItem.ToArray());
            ddlDirectorMubModal.Items.AddRange(lstListItem.ToArray());
            //ddlRubros
            lstListItem = objClsRubroPagoBl.GetRubroPago(0, 0).Where(itm => itm.Activo.Equals(1))
                                                                      .OrderBy(itm => itm.Nombre)
                                                                      .Select(itm => new ListItem { Value = itm.IdRubro.ToString(), Text = itm.Nombre }).ToList();
            lstListItem.Insert(0, new ListItem { Value = "0", Text = "Selecciona" });
            ddlRubrosPago.Items.AddRange(lstListItem.ToArray());
            //ddlAños
            ddlAnoCreacion.Items.Add(new ListItem { Value = "0", Text = "Selecciona" });
            foreach (var itm in StrucQuerysEl.Anios.Split(','))
            {
                ddlAnoCreacion.Items.Add(new ListItem { Value = itm, Text = itm });
            }
            //ddlEstaus
            ddlEstaus.Items.Add(new ListItem { Value = "0", Text = "Selecciona" });
            ddlEstaus.Items.Add(new ListItem { Value = "1", Text = "Activo" });
            ddlEstaus.Items.Add(new ListItem { Value = "2", Text = "Inactivo" });
        }

        private void GetData(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl, int intId)
        {
            var objClsCenterCostsBl = new ClsCenterCostsBl(objClsUsuarioActiveDirectoryEl);
            var objClsCatCentroCostosEl = objClsCenterCostsBl.GetCenterCosts(intId, "").FirstOrDefault();
            if (objClsCatCentroCostosEl != null)
            {
                hidId.Value = objClsCatCentroCostosEl.IntId.ToString();
                txtId.Text = objClsCatCentroCostosEl.StrId;
                txtId.ToolTip = objClsCatCentroCostosEl.StrId;
                txtCentroCostos.Text = objClsCatCentroCostosEl.StrNombre;
                txtCentroCostos.ToolTip = objClsCatCentroCostosEl.StrNombre;
                txtEstatus.Text = objClsCatCentroCostosEl.StrEstatus;
                txtEstatus.ToolTip = objClsCatCentroCostosEl.StrEstatus;
                txtEmpresa.Text = objClsCatCentroCostosEl.StrEmpresa;
                txtEmpresa.ToolTip = objClsCatCentroCostosEl.StrEmpresa;
                txtUNegocio.Text = objClsCatCentroCostosEl.StrUnidadNegocio;
                txtUNegocio.ToolTip = objClsCatCentroCostosEl.StrUnidadNegocio;
                txtArea.Text = objClsCatCentroCostosEl.StrArea;
                txtArea.ToolTip = objClsCatCentroCostosEl.StrArea;
                txtIdHomologado.Text = objClsCatCentroCostosEl.StrZIdHomologado;
                txtIdHomologado.ToolTip = objClsCatCentroCostosEl.StrZIdHomologado;
                txtCentroCostosHomologado.Text = objClsCatCentroCostosEl.StrZNombreHomologado;
                txtCentroCostosHomologado.ToolTip = objClsCatCentroCostosEl.StrZNombreHomologado;
                txtClasificacionAlterna.Text = objClsCatCentroCostosEl.StrClasificacionAlterna;
                txtClasificacionAlterna.ToolTip = objClsCatCentroCostosEl.StrClasificacionAlterna;
                txtFechaInicio.Text = objClsCatCentroCostosEl.DtFechaInicio?.ToString("dd/MM/yyyy") ?? "";
                txtFechaInicio.ToolTip = objClsCatCentroCostosEl.DtFechaInicio?.ToString("dd/MM/yyyy") ?? "";
                txtFechaCierre.Text = objClsCatCentroCostosEl.DtFechaCierreAdmin?.ToString("dd/MM/yyyy") ?? "";
                txtFechaCierre.ToolTip = objClsCatCentroCostosEl.DtFechaCierreAdmin?.ToString("dd/MM/yyyy") ?? "";
                txtDuracionMeses.Text = objClsCatCentroCostosEl.IntDuracionMeses?.ToString() ?? "";
                txtDuracionMeses.ToolTip = objClsCatCentroCostosEl.IntDuracionMeses?.ToString() ?? "";
                ddlUnStaff.SelectedValue = objClsCatCentroCostosEl.IntIdDirector?.ToString() ?? "0";
                ddlGerentes.SelectedValue = objClsCatCentroCostosEl.IntIdGerente?.ToString() ?? "0";
                txtFechaPayBack.Text = objClsCatCentroCostosEl.DtFechaPayBack?.ToString("dd/MM/yyyy") ?? "";
                txtFechaPayBack.ToolTip = objClsCatCentroCostosEl.DtFechaPayBack?.ToString("dd/MM/yyyy") ?? "";
                txtIngreso.Text = objClsCatCentroCostosEl.DecIngresosContrato.HasValue ? $"{objClsCatCentroCostosEl.DecIngresosContrato:c}" : "";
                txtIngreso.ToolTip = objClsCatCentroCostosEl.DecIngresosContrato.HasValue ? $"{objClsCatCentroCostosEl.DecIngresosContrato:c}" : "";
                txtEgreso.Text = objClsCatCentroCostosEl.DecEgresoContrato.HasValue ? $"{objClsCatCentroCostosEl.DecEgresoContrato:c}" : "";
                txtEgreso.ToolTip = objClsCatCentroCostosEl.DecEgresoContrato.HasValue ? $"{objClsCatCentroCostosEl.DecEgresoContrato:c}" : "";
                txtMubContratado.Text = objClsCatCentroCostosEl.DecMubContrato.HasValue ? $"{objClsCatCentroCostosEl.DecMubContrato:c}" : "";
                txtMubContratado.ToolTip = objClsCatCentroCostosEl.DecMubContrato.HasValue ? $"{objClsCatCentroCostosEl.DecMubContrato:c}" : "";
                ddlAnoCreacion.SelectedValue = objClsCatCentroCostosEl.IntAnoCreacion?.ToString() ?? "";
                txtObservacion.Text = objClsCatCentroCostosEl.StrObservaciones;
                txtObservacion.ToolTip = objClsCatCentroCostosEl.StrObservaciones;
                ddlEstaus.SelectedValue = objClsCatCentroCostosEl.BolActivo ? "1" : "2";
            }
        }

        private void SetData(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl)
        {
            var intId = Convert.ToInt32(hidId.Value);
            var dtFechaPayBack = Convert.ToDateTime(txtFechaPayBack.Text);
            var decIngreso = decimal.Parse(txtIngreso.Text, NumberStyles.AllowCurrencySymbol | NumberStyles.Number);
            var decEgreso = decimal.Parse(txtEgreso.Text, NumberStyles.AllowCurrencySymbol | NumberStyles.Number);
            var decMubContratado = decimal.Parse(txtMubContratado.Text, NumberStyles.AllowCurrencySymbol | NumberStyles.Number);
            var strObservacion = txtObservacion.Text;
            var intIdDirector = Convert.ToInt32(ddlUnStaff.SelectedValue);
            var intIdGerente = Convert.ToInt32(ddlGerentes.SelectedValue);
            var intAnoCreacion = Convert.ToInt32(ddlAnoCreacion.SelectedValue);
            var bolActivo = ddlEstaus.SelectedValue == "1";

            var objClsCenterCostsBl = new ClsCenterCostsBl(objClsUsuarioActiveDirectoryEl);
            var objClsSimpleResultEl = objClsCenterCostsBl.ComplementCenterCost(intId, dtFechaPayBack, decIngreso, decEgreso, decMubContratado, strObservacion, intIdDirector, intIdGerente, intAnoCreacion, bolActivo);
            Session["objClsResultEl"] = new ClsResultEl() { StrId = objClsSimpleResultEl.IntId.ToString(), BolSuccess = objClsSimpleResultEl.BolSuccess, StrMessage = objClsSimpleResultEl.StrMessage };
            if (objClsSimpleResultEl.BolSuccess)
            {
                Response.Redirect("~/Formularios/CentroCostos/FrmAdminCentroCostos.aspx", true);
            }
        }

    }
}