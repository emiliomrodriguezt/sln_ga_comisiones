﻿using System;
using System.Web.UI;
using EL_GA_Comisiones;

namespace UL_GA_Comisiones.Formularios.CentroCostos
{
    public partial class FrmAdminCentroCostos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["objClsResultEl"] != null)
            {
                var objClsResultEl = (ClsResultEl)Session["objClsResultEl"];
                Session["objClsResultEl"] = null;
                Page.Controls.Add(new LiteralControl($"<script>fnMessage(\" {(objClsResultEl.BolSuccess ? "alert-success" : "alert-danger")} \", \"{ objClsResultEl.StrMessage }\", true);</script>"));
            }
        }
    }
}