﻿using System;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using BL_GA_Comisiones;
using EL_GA_Comisiones;
using System.Web;
using Utility_GA_Comisiones;

namespace UL_GA_Comisiones.Formularios.Resultados
{
    public partial class FrmResumenResultados : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["objClsResultEl"] != null)
            {
                var objClsResultEl = (ClsResultEl) Session["objClsResultEl"];
                Session["objClsResultEl"] = null;
                Page.Controls.Add(
                    new LiteralControl(
                        $"<script>fnMessage(\" {(objClsResultEl.BolSuccess ? "alert-success" : "alert-danger")} \", \"{objClsResultEl.StrMessage}\", true);</script>"));
            }

            var objClsUsuarioActiveDirectoryEl =
                ClsUserBl.UserLogged(((FormsIdentity) Context.User.Identity).Ticket.UserData);

            if (Request.Form.Count == 0)
            {

                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    var intIdComison =
                        Convert.ToInt32(
                            ClsCryptographyBl.DecodeString(
                                HttpContext.Current.Server.HtmlDecode(Request.QueryString["id"])));
                    if (intIdComison > 0)
                    {
                        GetData(objClsUsuarioActiveDirectoryEl, intIdComison);
                    }
                    else
                    {
                        Page.Controls.Add(
                            new LiteralControl(
                                $"<script>fnMessage(\"alert-success\", \"Ocurrió un error al procesar el centro de costos, inténtelo mas tarde.\", true);</script>"));
                    }

                }
            }

            if (Request.Form.Count > 0 &&
                !string.IsNullOrEmpty(txtContraloria.Text) &&
                !string.IsNullOrEmpty(txtDirectorUn.Text) &&
                !string.IsNullOrEmpty(txtDirectorGrl.Text) &&
                !string.IsNullOrEmpty(txtPresidente.Text) &&
                !string.IsNullOrEmpty(hidId.Value) &&
                !string.IsNullOrEmpty(HidIsPdf.Value))
            {
                GetExportReport(objClsUsuarioActiveDirectoryEl, Convert.ToInt32(hidId.Value), txtContraloria.Text, txtDirectorUn.Text, txtDirectorGrl.Text, txtPresidente.Text, HidIsPdf.Value == "1");
            }
        }

        private void GetData(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl, int intId)
        {
            var objClsCommissionCalculationBl = new ClsCommissionCalculationBl(objClsUsuarioActiveDirectoryEl);
            var objClsResumenResultadosBl = new ClsResumenResultadosBl(objClsUsuarioActiveDirectoryEl);

            var objClsTblCalculoComisionEl = objClsCommissionCalculationBl.GetCommissionCalculation(intId, 0, 0).FirstOrDefault();
            if (objClsTblCalculoComisionEl != null)
            {
                var strOtrosPagos = objClsResumenResultadosBl.GetOtherPay(objClsTblCalculoComisionEl.IdComision);
                hidTotAPagar.Value = $"{objClsTblCalculoComisionEl.TotalAPagar:c}";
                HidTotComison.Value = $"{objClsTblCalculoComisionEl.ComisionTotal:c}";
                HidAnticipos.Value = strOtrosPagos.Item1;
                HidSas.Value = strOtrosPagos.Item2;
                HidBono.Value = strOtrosPagos.Item3;
                hidId.Value = objClsTblCalculoComisionEl.IdComision.ToString();
                hidIColaborador.Value = objClsTblCalculoComisionEl.IdColaborador.ToString();
                hidAno.Value = objClsTblCalculoComisionEl.Anio.ToString();
                lblColaborador.Text = objClsTblCalculoComisionEl.ObjClsTblColaboradorEl.StrNombreColaborador;
                lblFechaingreso.Text = objClsTblCalculoComisionEl.ObjClsTblColaboradorEl.DtFechaAlta.ToString("dd-MM-yyyy");
                lblUnidadNegocio.Text = objClsTblCalculoComisionEl.ObjClsTblColaboradorEl.ObjClsCatCentroCostosEl.StrNombre;
                txtContraloria.Text = objClsTblCalculoComisionEl.FirmaContraloria;
                txtDirectorUn.Text = objClsTblCalculoComisionEl.FirmaDirectorUn;
                txtDirectorGrl.Text = objClsTblCalculoComisionEl.FirmaDirectorGrl;
                txtPresidente.Text = objClsTblCalculoComisionEl.FirmaPresidente;
                tblMubContratadoAno.DataSource = objClsResumenResultadosBl.GetFormatReportSasCollaborator(objClsTblCalculoComisionEl.IdColaborador, objClsTblCalculoComisionEl.Anio, objClsTblCalculoComisionEl.SASVisibles, objClsTblCalculoComisionEl.ObjClsTblColaboradorEl.DtFechaAlta);
                tblMubContratadoAno.DataBind();
                tblMubContratado.DataSource = objClsResumenResultadosBl.GetFormatReportCenterCostCollaboratorSas(objClsTblCalculoComisionEl.IdColaborador, objClsTblCalculoComisionEl.SASVisibles, objClsTblCalculoComisionEl.ObjClsTblColaboradorEl.DtFechaAlta, objClsTblCalculoComisionEl.Anio);
                tblMubContratado.DataBind();
                tblComisionCentroCostos.DataSource = objClsResumenResultadosBl.GetCenterCostComisones(objClsTblCalculoComisionEl.IdComision, objClsTblCalculoComisionEl.Anio);
                tblComisionCentroCostos.DataBind();
            }
        }

        private void GetExportReport(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl, int intId, string strFirmaContraloria, string strFirmaDirectorUn, string strFirmaDirectorGrl, string strFirmaPresidente, bool isPdf)
        {
            var objClsResumenResultadosBl = new ClsResumenResultadosBl(objClsUsuarioActiveDirectoryEl);
            var arrBytDocument = objClsResumenResultadosBl.GetExportReport(intId, isPdf, strFirmaContraloria, strFirmaDirectorUn, strFirmaDirectorGrl, strFirmaPresidente).ToArray();
            var objClsCommissionCalculationBl = new ClsCommissionCalculationBl(objClsUsuarioActiveDirectoryEl);
            var objClsSimpleResultEl = objClsCommissionCalculationBl.UpdateSigning(intId, strFirmaContraloria, strFirmaDirectorUn, strFirmaDirectorGrl, strFirmaPresidente, arrBytDocument);
            if (objClsSimpleResultEl.BolSuccess)
            {
                Response.Clear();
                Response.ContentType = isPdf ? "application/pdf" : "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=\"Autorizacion." + (isPdf ? "pdf" : "xls") +  "\"");
                Response.BinaryWrite(arrBytDocument);
                Response.Flush();
                Response.End();
            }
        }
    }
}