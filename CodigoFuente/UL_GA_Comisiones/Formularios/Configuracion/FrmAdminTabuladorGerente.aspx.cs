﻿using System;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL_GA_Comisiones;
using EL_GA_Comisiones;
using Microsoft.Ajax.Utilities;

namespace UL_GA_Comisiones.Formularios.Configuracion
{
    public partial class FrmAdminTabuladores : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["objClsResultEl"] != null)
            {
                var objClsResultEl = (ClsResultEl)Session["objClsResultEl"];
                Session["objClsResultEl"] = null;
                Page.Controls.Add(new LiteralControl($"<script>fnMessage(\" {(objClsResultEl.BolSuccess ? "alert-success" : "alert-danger")} \", \"{ objClsResultEl.StrMessage }\", true);</script>"));
            }

            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)Context.User.Identity).Ticket.UserData);

            if (Session["objClsResultEl"] != null)
            {
                var objClsResultEl = (ClsResultEl)Session["objClsResultEl"];
                Session["objClsResultEl"] = null;
                Page.Controls.Add(new LiteralControl($"<script>fnMessage(\" {(objClsResultEl.BolSuccess ? "alert-success" : "alert-danger")} \", \"{ objClsResultEl.StrMessage }\", true);</script>"));
            }

            if (Request.Form.Count == 0)
            {
                FillDdl(objClsUsuarioActiveDirectoryEl);
            }
        }

        private void FillDdl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl)
        {
            //ddlAñoOrigen
            var objClsTabulatorManagerBl = new ClsTabulatorManagerBl(objClsUsuarioActiveDirectoryEl);
            var lstListItem = objClsTabulatorManagerBl.GetTabulatorManager(0,0,0).Where(itm => itm.BolActivo.Equals(true))
                                                                                 .OrderBy(itm => itm.IntAnoVigencia)
                                                                                 .ThenBy(itm=> itm.DecCostoMensualGaMinimo)
                                                                                 .DistinctBy( itm => itm.IntAnoVigencia)
                                                                                 .Select(itm => new ListItem { Value = itm.IntAnoVigencia.ToString(), Text = itm.IntAnoVigencia.ToString() }).ToList();
            lstListItem.Insert(0, new ListItem { Value = "0", Text = "Selecciona" });
            ddlAnoOrigenModalCopyTabulador.Items.AddRange(lstListItem.ToArray());
            ddlAno.Items.AddRange(lstListItem.ToArray());

            //ddlTipoTabulador
            var objClsTypeTabulatorBl = new ClsTypeTabulatorBl(objClsUsuarioActiveDirectoryEl);
            lstListItem = objClsTypeTabulatorBl.GetTabulatorManager(0,"").Where(itm => itm.BolActivo.Equals(true))
                                                                         .OrderBy(itm => itm.StrNombre)
                                                                         .Select(itm => new ListItem { Value = itm.IntId.ToString(), Text = itm.StrNombre }).ToList();
            lstListItem.Insert(0, new ListItem { Value = "0", Text = "Selecciona" });
            ddlTipoTabulador.Items.AddRange(lstListItem.ToArray());

            //ddlAñoDestino
            ddlAnoDestinoModalCopyTabulador.Items.Add(new ListItem { Value = "0", Text = "Selecciona" });
            int[] arrIntAnos = { DateTime.Now.Year, DateTime.Now.Year + 1, DateTime.Now.Year + 2 };
            foreach (var itm in arrIntAnos)
            {
                ddlAnoDestinoModalCopyTabulador.Items.Add(new ListItem { Value = itm.ToString(), Text = itm.ToString() });
            }
        }
    }
}