﻿using System;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL_GA_Comisiones;
using EL_GA_Comisiones;
using Utility_GA_Comisiones;

namespace UL_GA_Comisiones.Formularios.Configuracion
{
    public partial class FrmEditTabuladorGerente : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["objClsResultEl"] != null)
            {
                var objClsResultEl = (ClsResultEl)Session["objClsResultEl"];
                Session["objClsResultEl"] = null;
                Page.Controls.Add(new LiteralControl($"<script>fnMessage(\" {(objClsResultEl.BolSuccess ? "alert-success" : "alert-danger")} \", \"{ objClsResultEl.StrMessage }\", true);</script>"));
            }

            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)Context.User.Identity).Ticket.UserData);

            if (Request.Form.Count == 0)
            {
                FillDdl(objClsUsuarioActiveDirectoryEl);
                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    GetData(objClsUsuarioActiveDirectoryEl, Convert.ToInt32(ClsCryptographyBl.DecodeString(HttpContext.Current.Server.HtmlDecode(Request.QueryString["id"]))));
                }
            }

            if (Request.Form.Count > 0 &&
                !string.IsNullOrEmpty(hidId.Value) &&
                !string.IsNullOrEmpty(txtCosMenMin.Text) &&
                !string.IsNullOrEmpty(txtCosMenMax.Text) &&
                !string.IsNullOrEmpty(txtMub.Text) &&
                !string.IsNullOrEmpty(txtComision.Text) &&
                ddlTipoTabulador.SelectedIndex > 0 &&
                ddlAno.SelectedIndex > 0)
            {
                SetData(objClsUsuarioActiveDirectoryEl);
            }
        }

        private void FillDdl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl)
        {
            //ddlTipoTabulador
            var objClsTypeTabulatorBl = new ClsTypeTabulatorBl(objClsUsuarioActiveDirectoryEl);
            var lstListItem = objClsTypeTabulatorBl.GetTabulatorManager(0, "").Where(itm => itm.BolActivo.Equals(true))
                                                                              .OrderBy(itm => itm.StrNombre)
                                                                              .Select(itm => new ListItem { Value = itm.IntId.ToString(), Text = itm.StrNombre }).ToList();
            lstListItem.Insert(0, new ListItem { Value = "0", Text = "Selecciona" });
            ddlTipoTabulador.Items.AddRange(lstListItem.ToArray());

            //ddlAñoDestino
            ddlAno.Items.Add(new ListItem { Value = "0", Text = "Selecciona" });

            foreach (var itm in StrucQuerysEl.Anios.Split(','))
            {
                ddlAno.Items.Add(new ListItem { Value = itm, Text = itm });
            }
        }

        private void GetData(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl, int intId)
        {
            var objClsTabulatorManagerBl = new ClsTabulatorManagerBl(objClsUsuarioActiveDirectoryEl);
            var objClsCatTabuladorGerentesEl = objClsTabulatorManagerBl.GetTabulatorManager(intId, 0, 0).FirstOrDefault();
            if (objClsCatTabuladorGerentesEl != null)
            {
                hidId.Value = objClsCatTabuladorGerentesEl.IntId.ToString();
                ddlTipoTabulador.SelectedValue = objClsCatTabuladorGerentesEl.IntIdTipoTabulador.ToString();
                txtCosMenMin.Text = $"{objClsCatTabuladorGerentesEl.DecCostoMensualGaMinimo:c}";
                txtCosMenMax.Text = $"{objClsCatTabuladorGerentesEl.DecCostoMensualGaMaximo:c}";
                txtMub.Text = $"{objClsCatTabuladorGerentesEl.DecMubMinimo:c}";
                txtComision.Text = $"{objClsCatTabuladorGerentesEl.DecComision:c}";
                ddlAno.SelectedValue = objClsCatTabuladorGerentesEl.IntAnoVigencia.ToString();
            }
        }

        private void SetData(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl)
        {
            var objClsTabulatorManagerBl = new ClsTabulatorManagerBl(objClsUsuarioActiveDirectoryEl);

            var intId = Convert.ToInt32(hidId.Value);
            var intIdTipoTabulador = Convert.ToInt32(ddlTipoTabulador.SelectedValue);
            var decCostMenMin = decimal.Parse(txtCosMenMin.Text, NumberStyles.AllowCurrencySymbol | NumberStyles.Number);
            var decCostMenMax = decimal.Parse(txtCosMenMax.Text, NumberStyles.AllowCurrencySymbol | NumberStyles.Number);
            var decMub = decimal.Parse(txtMub.Text, NumberStyles.AllowCurrencySymbol | NumberStyles.Number);
            var decComision = decimal.Parse(txtComision.Text, NumberStyles.AllowCurrencySymbol | NumberStyles.Number);
            var intAno = Convert.ToInt32(ddlAno.SelectedValue);

            var objClsSimpleResultEl = objClsTabulatorManagerBl.SetTabulatorManager(intId, intIdTipoTabulador, decCostMenMin, decCostMenMax, decMub, decComision, intAno);
            Session["objClsResultEl"] = new ClsResultEl() { StrId = objClsSimpleResultEl.IntId.ToString(), BolSuccess = objClsSimpleResultEl.BolSuccess, StrMessage = objClsSimpleResultEl.StrMessage };
            if (objClsSimpleResultEl.BolSuccess)
            {
                Response.Redirect("~/Formularios/Configuracion/FrmAdminTabuladorGerente.aspx", true);
            }
        }
    }
}