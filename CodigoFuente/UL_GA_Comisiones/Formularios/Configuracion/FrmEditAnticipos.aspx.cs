﻿using System;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL_GA_Comisiones;
using EL_GA_Comisiones;
using Utility_GA_Comisiones;

namespace UL_GA_Comisiones.Formularios.Configuracion
{
    public partial class FrmEditAnticipos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["objClsResultEl"] != null)
            {
                var objClsResultEl = (ClsResultEl)Session["objClsResultEl"];
                Session["objClsResultEl"] = null;
                Page.Controls.Add(new LiteralControl($"<script>fnMessage(\" {(objClsResultEl.BolSuccess ? "alert-success" : "alert-danger")} \", \"{ objClsResultEl.StrMessage }\", true);</script>"));
            }

            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)Context.User.Identity).Ticket.UserData);

            if (Request.Form.Count == 0)
            {
                FillDdl(objClsUsuarioActiveDirectoryEl);
                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    GetData(objClsUsuarioActiveDirectoryEl, Convert.ToInt32(ClsCryptographyBl.DecodeString(HttpContext.Current.Server.HtmlDecode(Request.QueryString["id"]))));
                }
            }

            if (Request.Form.Count > 0 &&
                !string.IsNullOrEmpty(hidId.Value) &&
                ddlColaborador.SelectedIndex > 0 &&
                ddlCentroCostos.SelectedIndex > 0 &&
                !string.IsNullOrEmpty(txtImporteNeto.Text) &&
                !string.IsNullOrEmpty(txtImporteBruto.Text) &&
                !string.IsNullOrEmpty(txtCheque.Text) &&
                !string.IsNullOrEmpty(txtPoliza.Text) &&
                ddlEmpresas.SelectedIndex > 0 &&
                !string.IsNullOrEmpty(txtFecha.Text))
            {
                SetData(objClsUsuarioActiveDirectoryEl);
            }
        }

        private  void FillDdl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl)
        {
            //ddlColaboradores
            var objClsCollaboratorBl = new ClsCollaboratorBl(objClsUsuarioActiveDirectoryEl);
            var lstListItem = objClsCollaboratorBl.GetCollaborators(0, "").Where(itm => itm.BolActivo.Equals(true))
                                                                          .OrderBy(itm => itm.StrNombreColaborador)
                                                                          .Select(itm => new ListItem() { Value = itm.IntId.ToString(), Text = itm.StrNombreColaborador }).ToList();

            lstListItem.Insert(0, new ListItem() { Value = "0", Text = "Selecciona" });
            ddlColaborador.Items.AddRange(lstListItem.ToArray());
            //ddlCentroCostos
            var objClsCenterCostsBl = new ClsCenterCostsBl(objClsUsuarioActiveDirectoryEl);
            lstListItem = objClsCenterCostsBl.GetCenterCosts(0, "").Where(itm => itm.BolActivo.Equals(true))
                                                                   .OrderBy(itm => itm.StrNombre)
                                                                   .Select(itm => new ListItem() { Value = itm.IntId.ToString(), Text = itm.StrNombre }).ToList();
            lstListItem.Insert(0, new ListItem() { Value = "0", Text = "Selecciona" });
            ddlCentroCostos.Items.AddRange(lstListItem.ToArray());
            //ddlEmpresas
            var objClsCompanyBl = new ClsCompanyBl(objClsUsuarioActiveDirectoryEl);
            lstListItem = objClsCompanyBl.GetCompanies("").Where(itm => itm.StrStatus.ToUpper().Contains("ABIERTO"))
                                                                   .OrderBy(itm => itm.StrNombre)
                                                                   .Select(itm => new ListItem() { Value = itm.StrNo, Text = itm.StrNombre }).ToList();
            lstListItem.Insert(0, new ListItem() { Value = "0", Text = "Selecciona" });
            ddlEmpresas.Items.AddRange(lstListItem.ToArray());
        }

        private void GetData(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl, int intId)
        {
            var objClsAdvancesBl = new ClsAdvancesBl(objClsUsuarioActiveDirectoryEl);
            var objClsTblAnticiposEl = objClsAdvancesBl.GetAdvances(intId, 0, 0).FirstOrDefault();

            hidId.Value = objClsTblAnticiposEl?.IntId.ToString();
            ddlColaborador.SelectedValue = objClsTblAnticiposEl?.IntIdColaborador.ToString();
            ddlCentroCostos.SelectedValue = objClsTblAnticiposEl?.IntIdCentroCostos.ToString();
            txtImporteNeto.Text = $"{objClsTblAnticiposEl?.DecImporteNeto:c}";
            txtImporteBruto.Text = $"{objClsTblAnticiposEl?.DecImporteBruto:c}";
            txtCheque.Text = objClsTblAnticiposEl?.StrNoChequeTransferencia;
            txtPoliza.Text = objClsTblAnticiposEl?.StrNoPolizaContable;
            ddlEmpresas.SelectedValue = objClsTblAnticiposEl?.StrNoEmpresa;
            txtFecha.Text = objClsTblAnticiposEl?.DtFechaPago.ToString("dd/MM/yyyy");
        }

        private void SetData(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl)
        {
            var intId = Convert.ToInt32(hidId.Value);
            var intIdColaborador = Convert.ToInt32(ddlColaborador.SelectedValue);
            var intIdCentroCostos = Convert.ToInt32(ddlCentroCostos.SelectedValue);
            var decImporteNeto = decimal.Parse(txtImporteNeto.Text, NumberStyles.AllowCurrencySymbol | NumberStyles.Number);
            var decImporteBruto = decimal.Parse(txtImporteBruto.Text, NumberStyles.AllowCurrencySymbol | NumberStyles.Number);
            var strCheque = txtCheque.Text;
            var strPoliza = txtPoliza.Text;
            var strNoEmpresa = ddlEmpresas.SelectedValue;
            var dtFecha = Convert.ToDateTime(txtFecha.Text);

            var objClsAdvancesBl = new ClsAdvancesBl(objClsUsuarioActiveDirectoryEl);
            var objClsSimpleResultEl = objClsAdvancesBl.SetAdvance(intId, intIdColaborador, intIdCentroCostos, decImporteBruto, decImporteNeto, strNoEmpresa, strCheque, dtFecha, strPoliza, 0);
            Session["objClsResultEl"] = new ClsResultEl() { StrId = objClsSimpleResultEl.IntId.ToString(), BolSuccess = objClsSimpleResultEl.BolSuccess, StrMessage = objClsSimpleResultEl.StrMessage };
            Response.Redirect("FrmAdminAnticipos.aspx", true);
        }
    }
}