﻿using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using BL_GA_Comisiones;
using EL_GA_Comisiones;
using Utility_GA_Comisiones;

namespace UL_GA_Comisiones.Formularios.Administracion
{
    public partial class EditRol : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["objClsResultEl"] != null)
            {
                var objClsResultEl = (ClsResultEl)Session["objClsResultEl"];
                Session["objClsResultEl"] = null;
                Page.Controls.Add(new LiteralControl($"<script>fnMessage(\" {(objClsResultEl.BolSuccess ? "alert-success" : "alert-danger")} \", \"{ objClsResultEl.StrMessage }\", true);</script>"));
            }

            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)Context.User.Identity).Ticket.UserData);

            if (Request.Form.Count == 0)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    GetData(objClsUsuarioActiveDirectoryEl, Convert.ToInt32(ClsCryptographyBl.DecodeString(HttpContext.Current.Server.HtmlDecode(Request.QueryString["id"]))));
                }
            }

            if (Request.Form.Count > 0 && !string.IsNullOrEmpty(txtNombreRol.Text))
            {
                SetData(objClsUsuarioActiveDirectoryEl);
            }

        }

        private void GetData(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl, int intId)
        {
            var objClsRolBl = new ClsRolBl(objClsUsuarioActiveDirectoryEl);
            var objClsCatRolesEl = objClsRolBl.GetRols(intId, "").FirstOrDefault();
            hidId.Value = objClsCatRolesEl?.IntId.ToString() ?? "";
            txtNombreRol.Text = objClsCatRolesEl?.StrNombre ?? "";
        }

        private void SetData(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl)
        {
            var objClsRolBl = new ClsRolBl(objClsUsuarioActiveDirectoryEl);
            var intId = Convert.ToInt32(hidId.Value);
            var strNommbre = txtNombreRol.Text;
            var objClsSimpleResultEl = objClsRolBl.SetRol(intId, strNommbre);
            Session["objClsResultEl"] = new ClsResultEl(){ StrId = objClsSimpleResultEl.IntId.ToString(), BolSuccess = objClsSimpleResultEl.BolSuccess, StrMessage = objClsSimpleResultEl.StrMessage};
            if (objClsSimpleResultEl.BolSuccess)
            {
                Response.Redirect("AdminRoles.aspx", true);
            }
        }
    }
}