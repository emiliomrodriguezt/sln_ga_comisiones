﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminRoles.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.Administracion.AdminRoles" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
    <i class="fa fa-sitemap" aria-hidden="true"></i> Administración / Roles
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <div class="row">
        <div class="col-md-12 text-right">
            <a href="EditRol.aspx" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Agregar rol</a>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped" id="dtResult"></table>
        </div>
    </div>
    <script type="text/javascript">
        var fnFillTable = function() {
            fnGetTable($("#dtResult"),
                {
                    BolEdit: jsonPermits.Edit,
                    BolDelete: true,
                    BolConsult: true,
                    StrPathEdit: $.cookie("strApplicationPath") + "/Formularios/Administracion/EditRol.aspx",
                    StrPathDelete: $.cookie("strApplicationPath") + "/api/Roles/DelRol/"
                },
                [
                    { "sTitle": "Nombre" },
                    { "sTitle": "Activo", "sClass": "text-center", "bSortable": false, "bSearchable": false },
                    { "sTitle": "Acciones", "sClass": "text-center", "bSortable": false, "bSearchable": false }
                ],
                $.cookie("strApplicationPath") + "/api/Roles/GetRoles/");
        }
        $(document).ready(function () {
            fnFillTable();
        });
    </script>
</asp:Content>
