﻿using System;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL_GA_Comisiones;
using EL_GA_Comisiones;

namespace UL_GA_Comisiones.Formularios.Administracion
{
    public partial class Permisos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["objClsResultEl"] != null)
            {
                var objClsResultEl = (ClsResultEl)Session["objClsResultEl"];
                Session["objClsResultEl"] = null;
                Page.Controls.Add(new LiteralControl($"<script>fnMessage(\" {(objClsResultEl.BolSuccess ? "alert-success" : "alert-danger")} \", \"{ objClsResultEl.StrMessage }\", true);</script>"));
            }

            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)Context.User.Identity).Ticket.UserData);

            if (Request.Form.Count == 0)
            {
                FillDdl(objClsUsuarioActiveDirectoryEl);
            }
        }

        private void FillDdl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl)
        {
            var objClsRolBl = new ClsRolBl(objClsUsuarioActiveDirectoryEl);
            var lstListItem = objClsRolBl.GetRols(0, "").Where(itm => itm.BolActivo.Equals(true))
                                                        .OrderBy(itm => itm.StrNombre)
                                                        .Select(itm => new ListItem() { Value = itm.IntId.ToString(), Text = itm.StrNombre }).ToList();

            lstListItem.Insert(0, new ListItem() { Value = "0", Text = "Selecciona" });
            ddlRoles.Items.AddRange(lstListItem.ToArray());
        }
    }
}