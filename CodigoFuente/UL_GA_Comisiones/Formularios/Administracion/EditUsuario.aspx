﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditUsuario.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.Administracion.EditUsuario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
    <i class="fa fa-sitemap" aria-hidden="true"></i> Administración / Roles / <label id="bredCrub">Nuevo Registro</label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <form runat="server" ClientIDMode="Static" ID="frmUser" class="form-striped" action="EditUsuario.aspx" method="post">
        <asp:HiddenField runat="server" ClientIDMode="Static" id="hidId" Value="0"/>
        <div class="row">
            <div class="col-md-6">
                <label for="txtUser">Rol*:</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-angle-down"></i></span>
                    <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlRoles" data-validar="requerido" CssClass="form-control"/>    
                </div>
                <small class="form-text-error"></small>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-6">
                <label for="txtName">Nombre*:</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                    <asp:TextBox runat="server" ClientIDMode="Static" ID="txtName" data-validar="requerido" placeholder="Nombre" CssClass="form-control"></asp:TextBox>
                 </div>
                <small class="form-text-error"></small>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-6">
                <label for="txtUser">Usuario*:</label>
                <div class="input-group input-group-lg mb15">
                   <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                    <asp:TextBox runat="server" ClientIDMode="Static" ID="txtUser" data-validar="requerido" placeholder="Usuario" CssClass="form-control"></asp:TextBox>
                 </div>
                <small class="form-text-error"></small>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-6 text-right">
                <a href="/Formularios/Administracion/AdminUsuarios.aspx" class="btn btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Regresar</a>
                <button type="reset" class="btn btn-darkblue"><i class="fa fa-eraser" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Limpiar</button>
                <button type="submit" data-form="frmUser" class="btn btn-primary validaForm"><i class="fa fa-save" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Guardar</button>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {
            if (fnGetQuery()["edit"] === "1") {
                fnDiSabledForm("frmUser");
                $("#bredCrub").html("Consulta");
            } else if (fnGetQuery()["id"] != null) {
                $("#bredCrub").html("Actualiza");
            }

            $("#txtUser").attr("readonly", true);

            $("#txtName").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "GET",
                        url: $.cookie("strApplicationPath") + "/api/Usuario/GetUserDirectory/" + request.term,
                        dataType: "json",
                        success: function (data) {
                            response(data);
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    $("#txtName").val(ui.item.label);
                    $("#txtUser").val(ui.item.value);
                    event.preventDefault();
                }
            });
        });
    </script>
</asp:Content>
