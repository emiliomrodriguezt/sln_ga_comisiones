﻿using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL_GA_Comisiones;
using EL_GA_Comisiones;
using Utility_GA_Comisiones;

namespace UL_GA_Comisiones.Formularios.Administracion
{
    public partial class EditUsuario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["objClsResultEl"] != null)
            {
                var objClsResultEl = (ClsResultEl)Session["objClsResultEl"];
                Session["objClsResultEl"] = null;
                Page.Controls.Add(new LiteralControl($"<script>fnMessage(\" {(objClsResultEl.BolSuccess ? "alert-success" : "alert-danger")} \", \"{ objClsResultEl.StrMessage }\", true);</script>"));
            }

            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)Context.User.Identity).Ticket.UserData);

            if (Request.Form.Count == 0)
            {
                FillDdl(objClsUsuarioActiveDirectoryEl);
                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    GetData(objClsUsuarioActiveDirectoryEl, Convert.ToInt32(ClsCryptographyBl.DecodeString(HttpContext.Current.Server.HtmlDecode(Request.QueryString["id"]))));
                }
            }

            if (Request.Form.Count > 0 &&
                ddlRoles.SelectedIndex > 0 &&
                !string.IsNullOrEmpty(txtName.Text) &&
                !string.IsNullOrEmpty(txtUser.Text))
            {
                SetData(objClsUsuarioActiveDirectoryEl);
            }
        }

        private void FillDdl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl)
        {
            var objClsRolBl = new ClsRolBl(objClsUsuarioActiveDirectoryEl);
            var lstListItem = objClsRolBl.GetRols(0, "").Where(itm => itm.BolActivo.Equals(true))
                                                        .OrderBy(itm => itm.StrNombre)
                                                        .Select(itm => new ListItem() { Value = itm.IntId.ToString(), Text = itm.StrNombre }).ToList();

            lstListItem.Insert(0, new ListItem() { Value = "0", Text = "Selecciona" });
            ddlRoles.Items.AddRange(lstListItem.ToArray());
        }

        private void GetData(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl, int intId)
        {
            var objClsUserBl = new ClsUserBl(objClsUsuarioActiveDirectoryEl);
            var objClsCatUsuarioEl = objClsUserBl.GetUsers(intId, "", "").FirstOrDefault();
            hidId.Value = intId.ToString();
            ddlRoles.SelectedValue = objClsCatUsuarioEl?.IntIdRol.ToString();
            txtName.Text = objClsCatUsuarioEl?.StrNombre;
            txtUser.Text = objClsCatUsuarioEl?.StrUser;
        }

        private void SetData(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl)
        {
            var objClsUserBl = new ClsUserBl(objClsUsuarioActiveDirectoryEl);
            var intId = Convert.ToInt32(hidId.Value);
            var intIdRol = Convert.ToInt32(ddlRoles.SelectedValue);
            var strName = txtName.Text;
            var strUser = txtUser.Text;
            var objClsSimpleResultEl = objClsUserBl.SetUser(intId, intIdRol, strName, strUser);
            Session["objClsResultEl"] = new ClsResultEl() { StrId = objClsSimpleResultEl.IntId.ToString(), BolSuccess = objClsSimpleResultEl.BolSuccess, StrMessage = objClsSimpleResultEl.StrMessage };
            if (objClsSimpleResultEl.BolSuccess)
            {
                Response.Redirect("AdminUsuarios.aspx", true);
            }
        }
    }
}