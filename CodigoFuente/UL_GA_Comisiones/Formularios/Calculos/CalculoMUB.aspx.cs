﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EL_GA_Comisiones;
using BL_GA_Comisiones;
using System.Web.Security;
using System.Reflection;

namespace UL_GA_Comisiones.Formularios.Calculos
{
    public partial class CalculoMUB : System.Web.UI.Page
    {
        private int usuario;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LlenaPeriodos();
                resultados.Visible = false;
            }

            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)Context.User.Identity).Ticket.UserData);
            usuario = objClsUsuarioActiveDirectoryEl.IntId;
        }

        protected void LlenaPeriodos()
        {
            foreach (var itm in StrucQuerysEl.Anios.Split(','))
            {
                ddlAnio.Items.Add(new ListItem { Value = itm, Text = itm });
            }
        }

        protected void btnCalcular_Click(object sender, EventArgs e)
        {
            try
            {
                ClsOperacionesBl objBl = new ClsOperacionesBl();
                ClsRespuestaSP resultado = objBl.ExecCalculoMUB(Convert.ToInt32(ddlAnio.SelectedValue), Convert.ToInt32(ddlPeriodo.SelectedValue), usuario);

                if (resultado.Resultado == 0)
                {
                    List<ClsTblMUBCalculadoProyectoEl> listado = objBl.GetResultadoMUB(Convert.ToInt32(ddlAnio.SelectedValue));
                    gvResultados.DataSource = listado;
                    gvResultados.DataBind();

                    if (gvResultados.HeaderRow != null)
                        gvResultados.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                resultados.Visible = true;
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = string.Empty,
                    DtFecha = DateTime.Now
                });
            }
        }
    }
}