﻿using BL_GA_Comisiones;
using EL_GA_Comisiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UL_GA_Comisiones.Formularios.Calculos
{
    public partial class ConsultarComision : System.Web.UI.Page
    {
        private int usuario;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CargaCatalogos();
            }

            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)Context.User.Identity).Ticket.UserData);
            usuario = objClsUsuarioActiveDirectoryEl.IntId;

        }

        protected void CargaCatalogos()
        {
            ClsTblEmpleadoBl objBl = new ClsTblEmpleadoBl();
            List<ClsTblEmpleadoEl> empleados = objBl.GetColaborador(0, 0, 1);
            ddlGerente.DataSource = empleados;
            ddlGerente.DataTextField = "NombreColaborador";
            ddlGerente.DataValueField = "IdEmp";
            ddlGerente.DataBind();
            
        }

        protected void btnBuscar_Click(Object sender, EventArgs e)
        {
            try
            {
                ClsOperacionesBl objBl = new ClsOperacionesBl();
                int gerente = Convert.ToInt32(ddlGerente.SelectedValue);
                int anio = Convert.ToInt32(string.IsNullOrEmpty(txtAnio.Text) ? "0" : txtAnio.Text);
                List<ClsComisionesPagadas> comisiones = objBl.GetComisionesPagadas(gerente, anio);

                gvComisiones.DataSource = comisiones;
                gvComisiones.DataBind();

                
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = string.Empty,
                    DtFecha = DateTime.Now
                });
            }
        }
    }
}