﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EL_GA_Comisiones;
using BL_GA_Comisiones;
using System.Data;
using System.Web.Security;
using System.Reflection;

namespace UL_GA_Comisiones.Formularios.Calculos
{
    public partial class Comision : System.Web.UI.Page
    {
        private int usuario;

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                CargaCatalogos();
                resultados.Visible = false;
            }
            litScripts.Text = string.Empty;
            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)Context.User.Identity).Ticket.UserData);
            usuario = objClsUsuarioActiveDirectoryEl.IntId;
        }

        protected void CargaCatalogos()
        {
            ClsTblEmpleadoBl objBl = new ClsTblEmpleadoBl();
            List<ClsTblEmpleadoEl> empleados = objBl.GetColaborador(0, 1, 1);
            ddlGerente.DataSource = empleados;
            ddlGerente.DataTextField = "NombreColaborador";
            ddlGerente.DataValueField = "IdEmp";
            ddlGerente.DataBind();

            foreach (var itm in StrucQuerysEl.Anios.Split(','))
            {
                ddlPeriodo.Items.Add(new ListItem { Value = itm, Text = itm });
            }

            for(int i = 1; i <= 10; i++)
                ddlCuotas.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }

        protected void btnBuscar_Click(Object sender, EventArgs e)
        {
            try
            {
                ClsOperacionesBl objBl = new ClsOperacionesBl();
                List<ClsProyectosComisionEl> proyectos = objBl.GetProyectosComision(usuario, Convert.ToInt32(ddlGerente.SelectedValue), Convert.ToInt32(ddlPeriodo.SelectedValue), 12);

                gvProyectos.DataSource = proyectos;
                gvProyectos.DataBind();

                List<ClsOtrosPagosComisionEl> otrosPagos = objBl.GetOtrosPagosComision(usuario, Convert.ToInt32(ddlGerente.SelectedValue), Convert.ToInt32(ddlPeriodo.SelectedValue));
                gvOtros.DataSource = otrosPagos;
                gvOtros.DataBind();

                lblUltimoMUB.Text = objBl.GetUltimoCalculoMUB(Convert.ToInt32(ddlPeriodo.SelectedValue), usuario);

                resultados.Visible = true;
            }
            catch(Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = string.Empty,
                    DtFecha = DateTime.Now
                });
            }
        }

        protected void btnGuardar_Click(Object sender, EventArgs e)
        {
            try
            {
                ClsTblCalculoComisionEl comision = new ClsTblCalculoComisionEl()
                    {
                        IdColaborador = Convert.ToInt32(ddlGerente.SelectedValue),
                        Anio = Convert.ToInt32(ddlPeriodo.SelectedValue),
                        Periodo = 12,
                        SASVisibles = Convert.ToInt32(ddlCuotas.SelectedValue),
                        IdUsuarioCreacion = usuario,
                        ComisionTotal = 0,
                        FechaCreacion = DateTime.Now,
                        IdComision = 0,
                        MontoSAS = 0,
                        OtrosPagos = 0,
                        Pagado = false,
                        TotalAPagar = 0
                    };

                ClsOperacionesBl objBl = new ClsOperacionesBl();
                ClsRespuestaSP respuesta = new ClsRespuestaSP();

                List<ClsTblCentroCostosComision> proyectos = new List<ClsTblCentroCostosComision>();
                ClsTblCentroCostosComision nvoProyecto = null;

                foreach(GridViewRow fila in gvProyectos.Rows)
                {
                    if (!((CheckBox)fila.FindControl("chkProy")).Checked)
                        continue;

                    nvoProyecto = new ClsTblCentroCostosComision()
                    {
                        ComisionTotal = 0,
                        IdComision = 0,
                        IdTabla = 0,
                        MontoPagado = 0,
                        Porcentaje = 0,
                        MUBComision = 0,
                        IdCentroCostos = Convert.ToInt32(gvProyectos.DataKeys[fila.RowIndex]["IdCC"])
                    };
                    proyectos.Add(nvoProyecto);
                }

                List<ClsTblOtrosPagosComisionPrevEl> otrosPagos = new List<ClsTblOtrosPagosComisionPrevEl>();
                ClsTblOtrosPagosComisionPrevEl pago = null;
                decimal monto = 0;
                string strMonto = string.Empty;
                decimal importe = 0;

                foreach (GridViewRow fila in gvOtros.Rows)
                {
                    if (!((CheckBox)fila.FindControl("chkOtro")).Checked)
                        continue;
                    
                    strMonto = ((TextBox)fila.FindControl("txtOtro")).Text;
                    strMonto = strMonto.Replace("$", "").Replace(" ", "");
                    if(!decimal.TryParse(strMonto, out monto))
                    {
                        lblRespuesta.Text = "Existen valores inválidos en la sección de otros importes";
                        litScripts.Text = "<script>muestraError();</script>";
                        return;
                    }

                    importe = Convert.ToDecimal(gvOtros.DataKeys[fila.RowIndex]["Importe"]);
                    if(importe < 0 && (monto < importe || monto >= 0))
                    {
                        lblRespuesta.Text = "Existen valores fuera de rango en la sección de otros importes";
                        litScripts.Text = "<script>muestraError();</script>";
                        return;
                    }

                    if(importe >= 0 && (monto > importe || monto < 0))
                    {
                        lblRespuesta.Text = "Existen valores fuera de rango en la sección de otros importes";
                        litScripts.Text = "<script>muestraError();</script>";
                        return;
                    }

                    pago = new ClsTblOtrosPagosComisionPrevEl()
                    {
                        Anio =0,
                        IdOtroPago = 0,
                        Pagado = false,
                        ImporteTotal = 0,
                        IdComision = 0,
                        ImporteAPagar = monto,
                        IdPago =Convert.ToInt32(gvOtros.DataKeys[fila.RowIndex]["IdOtro"]),
                        IdTipoPago= Convert.ToInt32(gvOtros.DataKeys[fila.RowIndex]["IdTipo"]),
                    };
                    otrosPagos.Add(pago);
                }

                respuesta = objBl.SetNvaComision(comision, proyectos, otrosPagos);

                if (respuesta.Resultado != 0)
                {
                    lblRespuesta.Text = respuesta.Mensaje;
                    litScripts.Text = "<script>muestraError();</script>";
                    return;
                }

                string id = HttpContext.Current.Server.UrlEncode(Utility_GA_Comisiones.ClsCryptographyBl.EncodeString(respuesta.Valor.ToString()));
                Response.Redirect("/Formularios/Resultados/FrmResumenResultados.aspx?id=" + id);
            }
            catch(System.Threading.ThreadAbortException e1)
            {
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch(Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = string.Empty,
                    DtFecha = DateTime.Now
                });
            }
        }

        protected void gvProyectos_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow)
                return;

            ClsProyectosComisionEl rowView = (ClsProyectosComisionEl)e.Row.DataItem;

            switch(rowView.Orden)
            {
                case 1:
                    e.Row.BackColor = System.Drawing.Color.LimeGreen;
                    break;
                case 2:
                    e.Row.BackColor = System.Drawing.Color.Yellow;
                    break;
                default:
                    e.Row.BackColor = System.Drawing.Color.Orange;
                    break;
            }
        }
    }
}