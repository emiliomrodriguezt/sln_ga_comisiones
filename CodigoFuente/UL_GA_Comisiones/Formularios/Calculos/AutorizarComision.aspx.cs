﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EL_GA_Comisiones;
using BL_GA_Comisiones;
using Utility_GA_Comisiones;
using System.Reflection;
using System.Web.Security;

namespace UL_GA_Comisiones.Formularios.Calculos
{
    public partial class AutorizarComision : System.Web.UI.Page
    {
        private int usuario;
        protected void Page_Load(object sender, EventArgs e)
        {
            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)Context.User.Identity).Ticket.UserData);
            usuario = objClsUsuarioActiveDirectoryEl.IntId;
            

            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                //string id = ClsCryptographyBl.DecodeString(HttpContext.Current.Server.HtmlDecode(Request.QueryString["id"]));
                string id = Request.QueryString["id"];
                if (!AutorizaComision(Convert.ToInt32(id)))
                    return;
            }
                
            LlenaTabla();
            
        }

        protected bool AutorizaComision(int id)
        {
            ClsOperacionesBl objBl = new ClsOperacionesBl();
            ClsRespuestaSP respuesta;

            try
            {
                respuesta = objBl.ActorizarComision(id, usuario);
            }
            catch(Exception ex)
            {
                return false;
            }

            return respuesta.Resultado == 0 ? true : false;
        }

        protected void LlenaTabla()
        {
            try
            {
                ClsOperacionesBl objBl = new ClsOperacionesBl();
                List<ClsComisionSinAutorizarEl> comisiones = objBl.GetComisionesSinAutorizar();

                gvComisiones.DataSource = comisiones;
                gvComisiones.DataBind();

                if (gvComisiones.HeaderRow != null)
                    gvComisiones.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            catch(Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = string.Empty,
                    DtFecha = DateTime.Now
                });
            }
        }
    }
}