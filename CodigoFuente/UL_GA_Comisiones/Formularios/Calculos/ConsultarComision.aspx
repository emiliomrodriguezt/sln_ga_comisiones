﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ConsultarComision.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.Calculos.ConsultarComision" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
    <i class="fa fa-sitemap" aria-hidden="true"></i> Operaciones / Consultar Comisión
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <main class="page">
        <form id="form1" runat="server">
            <div class="modal fade" id="modalRespuesta" role="dialog" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-lg" role="document" id="modalDialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button id="btnCloseModal" aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                            <h4 class="modal-title">Consultar comisión</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-8">
                                    <asp:Label ID="lblRespuesta" runat="server" CssClass="heading"></asp:Label>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar">Cerrar</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <div>
                <div class="row">
                    <div class="col-md-12 tituloPagina">
                        <asp:Label ID="lblTitulo" runat="server">Consulta de comisiones</asp:Label>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-1" style="text-align:right;">
                        <label for="ddlGerente" style="text-align:right">Gerente:</label>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <asp:DropDownList ID="ddlGerente" runat="server" style="text-align:center;text-align-last:center;" CssClass="form-control" AppendDataBoundItems="true">
                                <asp:ListItem Text="--- Seleccione ---" Value="0" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-1" style="text-align:right;">
                        <label for="txtAnio" style="text-align:right">Año:</label>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <asp:TextBox ID="txtAnio" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <asp:LinkButton  ID="btnBuscar" runat="server" CssClass="btn btn-primary validaForm" data-form="form1" OnClick="btnBuscar_Click"><i class="fa fa-search" aria-hidden="true"></i> Buscar</asp:LinkButton>
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-8">
                        <asp:GridView ID="gvComisiones" runat="server" AutoGenerateColumns="false" CssClass="table table-striped"
                            ShowHeader="true" EmptyDataText="No se encontraron resultados" DataKeyNames="IdComision">
                            <Columns>
                                <asp:BoundField HeaderText="ID" DataField="IdComision" />
                                <asp:BoundField HeaderText="Nombre del colaborador" DataField="Colaborador" />
                                <asp:BoundField HeaderText="Unidad de negocio" DataField="UnidadNegocio" />
                                <asp:BoundField HeaderText="Año" DataField="Anio" />
                                <asp:BoundField HeaderText="Monto pagado" DataField="MontoPagado" DataFormatString="{0:c}" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <a href="/Formularios/Resultados/FrmResumenResultados.aspx?id=<%#(HttpContext.Current.Server.UrlEncode(Utility_GA_Comisiones.ClsCryptographyBl.EncodeString(Eval("IdComision").ToString()))) %>">Ver</a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </form>
    </main>
</asp:Content>
