﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using Newtonsoft.Json;
using EL_GA_Comisiones;
using BL_GA_Comisiones;
using System.Web.Security;
using Utility_GA_Comisiones;
using System.Globalization;

namespace UL_GA_Comisiones.Formularios.Rubros
{
    public partial class EditRubroPago : System.Web.UI.Page
    {
        private int usuario;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (String.IsNullOrEmpty(Request.QueryString["id"]))
                    Response.Redirect("~/Home.aspx");

                string id = ClsCryptographyBl.DecodeString(HttpContext.Current.Server.HtmlDecode(Request.QueryString["id"]));
                CargaRubro(Convert.ToInt32(id));
            }

            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)Context.User.Identity).Ticket.UserData);
            usuario = objClsUsuarioActiveDirectoryEl.IntId;
        }

        protected void CargaRubro(int id)
        {
            try
            {
                ClsRubroPagoBl objBl = new ClsRubroPagoBl();
                ClsRubroPagoEl rubro = objBl.GetRubroPago(id, 0).FirstOrDefault();
                if (rubro == null)
                    Response.Redirect("~/Home.aspx");

                hdnId.Value = rubro.IdRubro.ToString();
                txtConcepto.Text = rubro.Nombre;
                txtPorcentaje.Text = (rubro.Porcentaje * 100).ToString();
                ddlEstatus.SelectedValue = rubro.Activo.ToString();
            }
            catch (Exception e)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = e.Message,
                    StrPilaEventos = e.StackTrace,
                    StrMensajeInterno = e.InnerException?.Message ?? "",
                    StrPilaEventosInterno = e.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { id }),
                    DtFecha = DateTime.Now
                });
            }
        }

        protected ClsRubroPagoEl LlenaEntidad()
        {
            ClsRubroPagoEl rubro = null;
            try
            {
                ClsRubroPagoBl objBl = new ClsRubroPagoBl();
                int id = Convert.ToInt32(hdnId.Value);

                rubro = objBl.GetRubroPago(id, 0).FirstOrDefault();

                rubro.Activo = Convert.ToInt32(ddlEstatus.SelectedValue);
                rubro.IdUsuarioActulizacion = usuario;
            }
            catch (Exception e)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = e.Message,
                    StrPilaEventos = e.StackTrace,
                    StrMensajeInterno = e.InnerException?.Message ?? "",
                    StrPilaEventosInterno = e.InnerException?.StackTrace ?? "",
                    StrEntrada = string.Empty,
                    DtFecha = DateTime.Now
                });
            }

            return rubro;
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                ClsRubroPagoEl rubro = LlenaEntidad();
                ClsRubroPagoBl objBl = new ClsRubroPagoBl();
                ClsRespuestaSP respuesta = objBl.UpdateRubroPago(rubro);

                if (respuesta.Resultado == 0)
                {
                    lblRespuesta.Text = "El registro se guardó con éxito";
                    litScripts.Text = "<script>muestraExito();</script>";
                }
                else
                {
                    respuesta.Mensaje = respuesta.Mensaje.Replace("Error al ejecutar consulta:", string.Empty);
                    lblRespuesta.Text = respuesta.Mensaje;
                    litScripts.Text = "<script>muestraError();</script>";
                }
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = string.Empty,
                    DtFecha = DateTime.Now
                });
            }
        }
    }
}