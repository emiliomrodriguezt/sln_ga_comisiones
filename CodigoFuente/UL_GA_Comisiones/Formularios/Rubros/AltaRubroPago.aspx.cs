﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using EL_GA_Comisiones;
using BL_GA_Comisiones;
using System.Web.Security;

namespace UL_GA_Comisiones.Formularios.Rubros
{
    public partial class AltaRubroPago : System.Web.UI.Page
    {
        private int usuario;
        protected void Page_Load(object sender, EventArgs e)
        {
            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)Context.User.Identity).Ticket.UserData);
            usuario = objClsUsuarioActiveDirectoryEl.IntId;
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                ClsRubroPagoEl rubro = LlenaEntidad();
                ClsRubroPagoBl objBl = new ClsRubroPagoBl();
                ClsRespuestaSP respuesta = objBl.AddRubroPago(rubro);

                if (respuesta.Resultado == 0)
                {
                    lblRespuesta.Text = "El registro se guardó con éxito";
                    litScripts.Text = "<script>muestraExito();</script>";
                }
                else
                {
                    respuesta.Mensaje = respuesta.Mensaje.Replace("Error al ejecutar consulta:", string.Empty);
                    lblRespuesta.Text = respuesta.Mensaje;
                    litScripts.Text = "<script>muestraError();</script>";
                }
            }
            catch(Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = string.Empty,
                    DtFecha = DateTime.Now
                });
            }
        }

        protected ClsRubroPagoEl LlenaEntidad()
        {
            ClsRubroPagoEl rubro = new ClsRubroPagoEl();
            try
            {
                rubro.Nombre = txtConcepto.Text;
                rubro.Porcentaje = Convert.ToDecimal(txtPorcentaje.Text);
                rubro.Activo = Convert.ToInt32(ddlEstatus.SelectedValue);
                rubro.IdUsuarioCreacion = 1;
            }
            catch (Exception e)
            {
            }

            return rubro;
        }
    }
}