﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Empleados.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.Colaboradores.Empleados" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
    <i class="fa fa-sitemap" aria-hidden="true"></i> Configuración / Empleados
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <main class="page">
        <form id="form1" runat="server">
            <div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group pull-right">
                            <asp:LinkButton ID="btnNuevo" runat="server" CssClass="btn btn-primary" OnClientClick="javascript: window.location.href='AltaEmpleado.aspx'; return false;" ><i class="fa fa-plus"> Nuevo Registro</i></asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-12" style="overflow:auto;">
                        <asp:GridView ID="gvEmpleados" runat="server" AutoGenerateColumns="false" CssClass="table table-striped dataTables" ShowHeader="true">
                            <Columns>
                                <asp:BoundField HeaderText="No. de colaborador" DataField="noColaborador" />
                                <asp:BoundField HeaderText="Nombre" DataField="nombreColaborador" />
                                <asp:BoundField HeaderText="Estatus" DataField="estatus" />
                                <asp:BoundField HeaderText="Fecha alta" DataField="fechaAlta" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField HeaderText="Fecha baja" DataField="fechaBaja" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField HeaderText="Puesto" DataField="puesto" />
                                <asp:BoundField HeaderText="C.C." DataField="idCentroCostos" />
                                <asp:BoundField HeaderText="Nombre C.C." DataField="centroCostos" />
                                <asp:BoundField HeaderText="Empresa" DataField="empresa" />
                                <asp:BoundField HeaderText="Sueldo por nómina" DataField="sueldoNomina" DataFormatString="{0:c}" />
                                <asp:BoundField HeaderText="Honorarios" DataField="sueldoHonorarios" DataFormatString="{0:c}" />
                                <asp:BoundField HeaderText="Sueldo total" DataField="sueldoMensual" DataFormatString="{0:c}" />
                                <asp:TemplateField HeaderText="Acciones">
                                    <HeaderStyle CssClass="sorting_disabled">
                                        
                                    </HeaderStyle>
                                    <ItemTemplate>
                                        <%if (editar)
                                            { %>
                                        <a href='EditEmpleado.aspx?id=<%#(HttpContext.Current.Server.UrlEncode(Utility_GA_Comisiones.ClsCryptographyBl.EncodeString(Eval("idEmp").ToString()))) %>'><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        <%} %>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="text-center" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </form>
    </main>
</asp:Content>
