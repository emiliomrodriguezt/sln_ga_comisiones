﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web.Security;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EL_GA_Comisiones;
using BL_GA_Comisiones;
using Newtonsoft.Json;
using Utility_GA_Comisiones;

namespace UL_GA_Comisiones.Formularios.Colaboradores
{
    public partial class EditEmpleado : System.Web.UI.Page
    {
        private int usuario;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (String.IsNullOrEmpty(Request.QueryString["id"]))
                    Response.Redirect("~/Home.aspx");

                CargaCatalogos();
                string id = ClsCryptographyBl.DecodeString(HttpContext.Current.Server.HtmlDecode(Request.QueryString["id"]));
                CargaEmpleado(Convert.ToInt32(id));
            }
            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)Context.User.Identity).Ticket.UserData);
            usuario = objClsUsuarioActiveDirectoryEl.IntId;
        }

        protected void CargaCatalogos()
        {
            ClsCatalogosBl objBl = new ClsCatalogosBl();
            List<ClsItemCatalogoEl> puestos = objBl.GetPuestos();
            ddlPuesto.DataSource = puestos;
            ddlPuesto.DataTextField = "descripcion";
            ddlPuesto.DataValueField = "id";
            ddlPuesto.DataBind();

            List<ClsItemCatalogoEl> centros = objBl.GetUNs();
            ddlCentroCosto.DataSource = centros;
            ddlCentroCosto.DataTextField = "descripcion";
            ddlCentroCosto.DataValueField = "id";
            ddlCentroCosto.DataBind();

            ClsCompanyBl objComp = new ClsCompanyBl();
            List<ClsEmpresasEl> empresas = objComp.GetCompanies(string.Empty);
            ddlEmpresa.DataSource = empresas;
            ddlEmpresa.DataTextField = "StrNombre";
            ddlEmpresa.DataValueField = "StrNo";
            ddlEmpresa.DataBind();
        }

        protected void CargaEmpleado(int id)
        {
            try
            {
                ClsTblEmpleadoBl objBl = new ClsTblEmpleadoBl();
                ClsTblEmpleadoEl empleado = objBl.GetColaborador(id, 0, 0).First();
                if (empleado == null)
                    Response.Redirect("~/Home.aspx");

                hdnId.Value = empleado.IdEmp.ToString();
                txtNoEmpleado.Text = empleado.NoColaborador;
                txtNombre.Text = empleado.NombreColaborador;
                ddlEstatus.SelectedValue = empleado.Activo.ToString();
                ddlPuesto.SelectedValue = empleado.IdPuesto.ToString();
                txtFechaAlta.Text = empleado.FechaAlta.ToString("dd/MM/yyyy");
                txtFechaBaja.Text = empleado.FechaBaja?.ToString("dd/MM/yyyy");
                ddlCentroCosto.SelectedValue = empleado.IdCentroCostos.ToString();
                ddlEmpresa.SelectedValue = empleado.NoEmpresa;
                txtNomina.Text = $"{empleado.SueldoNomina:c}";
                txtHonorarios.Text = $"{empleado.SueldoHonorarios:c}";
                txtMensual.Text = $"{empleado.SueldoMensual:c}";
            }
            catch (Exception e)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = e.Message,
                    StrPilaEventos = e.StackTrace,
                    StrMensajeInterno = e.InnerException?.Message ?? "",
                    StrPilaEventosInterno = e.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new {id}),
                    DtFecha = DateTime.Now
                });
            }
        }

        protected ClsTblEmpleadoEl LlenaEntidad()
        {
            ClsTblEmpleadoEl colaborador = null;
            try
            {
                var decHonorarios = decimal.Parse(txtHonorarios.Text, NumberStyles.AllowCurrencySymbol | NumberStyles.Number);
                var decNomina = decimal.Parse(txtNomina.Text, NumberStyles.AllowCurrencySymbol | NumberStyles.Number);
                ClsTblEmpleadoBl objBl = new ClsTblEmpleadoBl();
                int id = Convert.ToInt32(hdnId.Value);

                colaborador = objBl.GetColaborador(id, 0, 0).First();

                colaborador.Activo = Convert.ToInt32(ddlEstatus.SelectedValue);
                colaborador.FechaAlta = DateTime.Parse(txtFechaAlta.Text);
                colaborador.FechaBaja = null;
                if (!string.IsNullOrEmpty(txtFechaBaja.Text))
                    colaborador.FechaBaja = DateTime.Parse(txtFechaBaja.Text);
                colaborador.IdCentroCostos = Convert.ToInt32(ddlCentroCosto.SelectedValue);
                colaborador.IdPuesto = Convert.ToInt32(ddlPuesto.SelectedValue);
                colaborador.IdUsuarioActualizacion = usuario;
                colaborador.NoColaborador = txtNoEmpleado.Text;
                colaborador.NoEmpresa = ddlEmpresa.SelectedValue;
                colaborador.Empresa = ddlEmpresa.Text;
                colaborador.NombreColaborador = txtNombre.Text.ToUpper();
                colaborador.SueldoNomina = decNomina;
                colaborador.SueldoHonorarios = decHonorarios;
                colaborador.SueldoMensual = decNomina + decHonorarios;
            }
            catch (Exception e)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = e.Message,
                    StrPilaEventos = e.StackTrace,
                    StrMensajeInterno = e.InnerException?.Message ?? "",
                    StrPilaEventosInterno = e.InnerException?.StackTrace ?? "",
                    StrEntrada = string.Empty,
                    DtFecha = DateTime.Now
                });
            }

            return colaborador;
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                ClsTblEmpleadoEl colaborador = LlenaEntidad();
                ClsTblEmpleadoBl objBl = new ClsTblEmpleadoBl();
                ClsRespuestaSP respuesta = objBl.UpdateColaborador(colaborador);

                if (respuesta.Resultado == 0)
                {
                    lblRespuesta.Text = "El registro se guardó con éxito";
                    litScripts.Text = "<script>muestraExito();</script>";
                }
                else
                {
                    respuesta.Mensaje = respuesta.Mensaje.Replace("Error al ejecutar consulta:", string.Empty);
                    lblRespuesta.Text = respuesta.Mensaje;
                    litScripts.Text = "<script>muestraError();</script>";
                }
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = string.Empty,
                    DtFecha = DateTime.Now
                });
            }
        }
    }
}