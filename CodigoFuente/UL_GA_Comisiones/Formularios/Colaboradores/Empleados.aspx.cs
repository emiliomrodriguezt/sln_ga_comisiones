﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EL_GA_Comisiones;
using BL_GA_Comisiones;
using System.Web.Security;

namespace UL_GA_Comisiones.Formularios.Colaboradores
{
    public partial class Empleados : System.Web.UI.Page
    {
        protected bool editar;
        protected void Page_Load(object sender, EventArgs e)
        {
            LlenaTabla();

            editar = false;
            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)Context.User.Identity).Ticket.UserData);
            var objClsAsocRolPaginaEl = ClsPermitsBl.Getpermission(objClsUsuarioActiveDirectoryEl.IntIdRol, Request.Url.LocalPath);

            if (objClsAsocRolPaginaEl != null && objClsAsocRolPaginaEl.BolEditar)
            {
                editar = true;
            }
        }

        protected void LlenaTabla()
        {
            ClsTblEmpleadoBl objBl = new ClsTblEmpleadoBl();
            List<ClsTblEmpleadoEl> empleados = objBl.GetColaborador(0, 0, 0);
            gvEmpleados.DataSource = empleados;
            gvEmpleados.DataBind();

            if(gvEmpleados.HeaderRow != null)
                gvEmpleados.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
}