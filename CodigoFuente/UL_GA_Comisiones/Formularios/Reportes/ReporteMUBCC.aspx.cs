﻿using BL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utility_GA_Comisiones;

namespace UL_GA_Comisiones.Formularios.Reportes
{
    public partial class ReporteMUBCC : System.Web.UI.Page
    {
        private int usuario;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CargaCatalogos();
            }

            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)Context.User.Identity).Ticket.UserData);
            usuario = objClsUsuarioActiveDirectoryEl.IntId;
            
        }

        protected void CargaCatalogos()
        {
            ClsTblEmpleadoBl objBl = new ClsTblEmpleadoBl();
            List<ClsTblEmpleadoEl> empleados = objBl.GetColaborador(0, 0, 1);
            ddlGerente.DataSource = empleados;
            ddlGerente.DataTextField = "NombreColaborador";
            ddlGerente.DataValueField = "IdEmp";
            ddlGerente.DataBind();

            ClsCatalogosBl objBl2 = new ClsCatalogosBl();
            List<ClsItemCatalogoEl> centros = objBl2.GetCentrosCostosHomologados(0, 0, 0);
            ddlProyecto.DataSource = centros;
            ddlProyecto.DataTextField = "Descripcion";
            ddlProyecto.DataValueField = "Id";
            ddlProyecto.DataBind();
            
        }

        protected void LlenaGrid()
        {
            ClsReportesBl objBl = new ClsReportesBl();
            List<ClsReporteMUBCCEl> reporte = objBl.GetReporteMUBCC(usuario, Convert.ToInt32(ddlGerente.SelectedValue), Convert.ToInt32(ddlProyecto.SelectedValue));
            gvProyectos.DataSource = reporte;
            gvProyectos.DataBind();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                LlenaGrid();
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = string.Empty,
                    DtFecha = DateTime.Now
                });
            }
        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            try
            {
                ClsReportesBl objBl = new ClsReportesBl();

                byte[] archivo = objBl.GetReporteMUBCCXls(usuario, Convert.ToInt32(ddlGerente.SelectedValue), Convert.ToInt32(ddlProyecto.SelectedValue));

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Buffer = true;
                HttpContext.Current.Response.Charset = "";
                HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=ReporteMUBGerente.xlsx");
                HttpContext.Current.Response.BinaryWrite(archivo);
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();
            }
            catch (System.Threading.ThreadAbortException e1)
            {
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = string.Empty,
                    DtFecha = DateTime.Now
                });
            }
        }

        protected void ddlGerente_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClsCatalogosBl objBl = new ClsCatalogosBl();
            List<ClsItemCatalogoEl> centros = objBl.GetCentrosCostosHomologados(0, 0, Convert.ToInt32(ddlGerente.SelectedValue));
            ddlProyecto.Items.Clear();
            ddlProyecto.DataSource = centros;
            ddlProyecto.DataTextField = "Descripcion";
            ddlProyecto.DataValueField = "Id";
            ddlProyecto.DataBind();
            ddlProyecto.Items.Insert(0, new ListItem("--- Todos ---", "0"));
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            int id = 0;
            decimal interes = 0;
            decimal mub = 0;

            try
            {
                id = Convert.ToInt32(hdnIdMUB.Value);
                interes = Convert.ToDecimal(txtInteres.Text);
                mub = Convert.ToDecimal(txtMUB.Text);

                ClsReportesBl objBl = new ClsReportesBl();
                ClsRespuestaSP respuesta = objBl.UpdateInteresesMUB(usuario, id, interes, mub);

                if(respuesta.Resultado == 0)
                {
                    LlenaGrid();
                    respuesta.Mensaje = "La actualización se realizó de forma correcta.";
                }

                Page.Controls.Add(new LiteralControl($"<script>fnMessage(\" {(respuesta.Resultado == 0 ? "alert-success" : "alert-danger")} \", \"{ respuesta.Mensaje }\", true);</script>"));
            }
            catch(Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { usuario, id, interes, mub }),
                    DtFecha = DateTime.Now
                });
            }
        }

        protected string EsVisible(object item)
        {
            string display = "style='display:";
            if (Convert.ToBoolean(item))
                display += "none'";
            else
                display += "block'";

            return display;
        }
    }

    
}