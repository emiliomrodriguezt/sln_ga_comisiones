﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReporteEdoCuenta.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.Reportes.ReporteEdoCuenta" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
    <i class="fa fa-sitemap" aria-hidden="true"></i> Reportes / Estado de cuenta
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <main class="page">
        <form id="form1" runat="server">
            <div class="modal fade" id="modalRespuesta" role="dialog" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-lg" role="document" id="modalDialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button id="btnCloseModal" aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                            <h4 class="modal-title">Estado de cuenta</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-8">
                                    <asp:Label ID="lblRespuesta" runat="server" CssClass="heading"></asp:Label>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar">Cerrar</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <div>
                <div class="row">
                    <div class="col-md-12 tituloPagina">
                        <asp:Label ID="lblTitulo" runat="server">Estado de cuenta</asp:Label>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-1" style="text-align:right;">
                        <label for="ddlGerente" style="text-align:right">Gerente:</label>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <asp:DropDownList ID="ddlGerente" runat="server" style="text-align:center;text-align-last:center;"
                                CssClass="form-control" AppendDataBoundItems="true" data-validar="requerido" >
                                <asp:ListItem Text="--- Seleccione ---" Value="0" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <small class="form-text-error"></small>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <asp:LinkButton  ID="btnBuscar" runat="server" CssClass="btn btn-primary validaForm" data-form="form1" OnClick="btnBuscar_Click"><i class="fa fa-search" aria-hidden="true"></i> Buscar</asp:LinkButton>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <asp:LinkButton  ID="btnExportar" runat="server" CssClass="btn btn-success validaForm" data-form="form1" OnClick="btnExportar_Click"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Exportar</asp:LinkButton>
                        </div>
                    </div>
                    <div class="col-md-5"></div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10" style="overflow:auto;">
                        <asp:GridView ID="gvResultados" runat="server" AutoGenerateColumns="false" CssClass="table table-striped ">
                            <Columns>
                                <asp:BoundField HeaderText="UN" DataField="UnidadNegocio" />
                                <asp:BoundField HeaderText="No. C.C." DataField="ClaveCC" />
                                <asp:BoundField HeaderText="Nombre C.C." DataField="NombreCC" />
                                <asp:BoundField HeaderText="Concepto" DataField="Concepto" />
                                <asp:BoundField HeaderText="Importes pagados" DataField="Importe" DataFormatString="{0:c}" />
                                <asp:BoundField HeaderText="Total" DataField="Acumulado" DataFormatString="{0:c}" />
                                <asp:BoundField HeaderText="Año" DataField="Anio" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </form>
    </main>
    <script type="text/javascript">
        $(document).ready(function () { 
            $('#<%=gvResultados.ClientID%>').dataTable({
                "bFilter": true,
                "bPaginate": true,
                "bInfo": true,
                "sPaginationType": "full_numbers",
                "bAutoWidth": false,
                "serverSide": true,
                "bDestroy": true,
                "iDisplayLength": 10,
                "oLanguage": {
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "&Uacute;ltimo",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente",
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente"
                    },
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sEmptyTable": "No hay informaci&oacute;n disponible",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sLoadingRecords": "Cargando...",
                    "sZeroRecords": "No se encontraron registros.",
                    "sProcessing": "Espere, por favor...",
                    "sSearch": "B&uacute;squeda R&aacute;pida:",
                    "sLengthMenu": "<select class=\"form-control input-sm mb15\">" +
                    "<option value=\"-1\">Todos</option>" +
                    "<option value=\"10\">10</option>" +
                    "<option value=\"25\">25</option>" +
                    "<option value=\"50\">50</option>" +
                    "<option value=\"100\">100</option>" +
                    "</select>"
                },
                "aaSorting": []
            });
        });
    </script>
    
</asp:Content>
