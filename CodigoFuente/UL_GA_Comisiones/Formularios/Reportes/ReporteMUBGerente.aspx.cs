﻿using BL_GA_Comisiones;
using EL_GA_Comisiones;
using Utility_GA_Comisiones;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UL_GA_Comisiones.Formularios.Reportes
{
    public partial class ReporteMUBGerente : System.Web.UI.Page
    {
        private int usuario;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CargaCatalogos();
            }

            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)Context.User.Identity).Ticket.UserData);
            usuario = objClsUsuarioActiveDirectoryEl.IntId;
            
        }

        protected void CargaCatalogos()
        {
            ClsTblEmpleadoBl objBl = new ClsTblEmpleadoBl();
            List<ClsTblEmpleadoEl> empleados = objBl.GetColaborador(0, 0, 1);
            ddlGerente.DataSource = empleados;
            ddlGerente.DataTextField = "NombreColaborador";
            ddlGerente.DataValueField = "IdEmp";
            ddlGerente.DataBind();

            ddlPeriodo.Items.Add(new ListItem("--- Seleccione ---", "0"));
            ddlPeriodo.Items.Add(new ListItem("Enero", "1"));
            ddlPeriodo.Items.Add(new ListItem("Febrero", "2"));
            ddlPeriodo.Items.Add(new ListItem("Marzo", "3"));
            ddlPeriodo.Items.Add(new ListItem("Abril", "4"));
            ddlPeriodo.Items.Add(new ListItem("Mayo", "5"));
            ddlPeriodo.Items.Add(new ListItem("Junio", "6"));
            ddlPeriodo.Items.Add(new ListItem("Julio", "7"));
            ddlPeriodo.Items.Add(new ListItem("Agosto", "8"));
            ddlPeriodo.Items.Add(new ListItem("Septiembre", "9"));
            ddlPeriodo.Items.Add(new ListItem("Octubre", "10"));
            ddlPeriodo.Items.Add(new ListItem("Noviembre", "11"));
            ddlPeriodo.Items.Add(new ListItem("Diciembre", "12"));

            foreach (var itm in StrucQuerysEl.Anios.Split(','))
            {
                ddlAnios.Items.Add(new ListItem { Value = itm, Text = itm });
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                ClsReportesBl objBl = new ClsReportesBl();
                int gerente = Convert.ToInt32(ddlGerente.SelectedValue);
                int anio = Convert.ToInt32(ddlAnios.SelectedValue);
                int periodo = Convert.ToInt32(ddlPeriodo.SelectedValue);

                DataTable reporte = objBl.GetReporteMUBGerente(usuario, gerente, anio, periodo);

                LlenaTabla(reporte);
            }catch(Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = string.Empty,
                    DtFecha = DateTime.Now
                });
            }
        }

        protected void LlenaTabla(DataTable tabla)
        {
            if (tabla == null || tabla.Rows.Count == 0 || tabla.Columns.Count < 18)
                return;

            int anioFinal = DateTime.Now.Year;
            int anioInicial = anioFinal - ((tabla.Columns.Count - 14) / 4) + 1;
            int anioActual = anioInicial;

            TableRow fila = new TableRow();
            System.Drawing.Color color = System.Drawing.Color.FromArgb(119, 136, 153);

            fila.Cells.Add(new TableCell() { Text = "zIdH", CssClass="tdReporte", BackColor = color });
            fila.Cells.Add(new TableCell() { Text = "No. de UN", CssClass = "tdReporte", BackColor = color });
            fila.Cells.Add(new TableCell() { Text = "Nombre UN", CssClass = "tdReporte", BackColor = color });
            fila.Cells.Add(new TableCell() { Text = "Director de área", CssClass = "tdReporte", BackColor = color });
            fila.Cells.Add(new TableCell() { Text = "Gerente de venta", CssClass = "tdReporte", BackColor = color });
            fila.Cells.Add(new TableCell() { Text = "No. C.C.", CssClass = "tdReporte", BackColor = color });
            fila.Cells.Add(new TableCell() { Text = "Nombre C.C.", CssClass = "tdReporte", BackColor = color });
            fila.Cells.Add(new TableCell() { Text = "Estatus", CssClass = "tdReporte", BackColor = color });
            fila.Cells.Add(new TableCell() { Text = "Fecha de inicio", CssClass = "tdReporte", BackColor = color });
            fila.Cells.Add(new TableCell() { Text = "Fecha payback", CssClass = "tdReporte", BackColor = color });
            fila.Cells.Add(new TableCell() { Text = "Fecha de término", CssClass = "tdReporte",  BackColor = color });

            for (anioActual = anioInicial; anioActual <= anioFinal; anioActual++)
            {
                if(anioActual % 2==0)
                    color = System.Drawing.Color.FromArgb(95, 158, 160);
                else
                    color = System.Drawing.Color.FromArgb(0, 139, 139);

                fila.Cells.Add(new TableCell() { Text = "Ingresos cobrados " + anioActual.ToString(), CssClass = "tdReporte", BackColor = color });
                fila.Cells.Add(new TableCell() { Text = "Egresos " + anioActual.ToString(), CssClass = "tdReporte", BackColor = color });
                fila.Cells.Add(new TableCell() { Text = "Intereses " + anioActual.ToString(), CssClass = "tdReporte", BackColor = color });
                fila.Cells.Add(new TableCell() { Text = "MUB " + anioActual.ToString(), CssClass = "tdReporte", BackColor = color });
            }
            color = System.Drawing.Color.FromArgb(80, 80, 148);
            fila.Cells.Add(new TableCell() { Text = "Ingresos cobrados acumulados a " + anioFinal.ToString(), CssClass = "tdReporte", BackColor = color });
            fila.Cells.Add(new TableCell() { Text = "Egresos acumulados a " + anioFinal.ToString(), CssClass = "tdReporte", BackColor = color });
            fila.Cells.Add(new TableCell() { Text = "Intereses acumulados a " + anioFinal.ToString(), CssClass = "tdReporte", BackColor = color });
            fila.Cells.Add(new TableCell() { Text = "MUB acumulado a " + anioFinal.ToString(), CssClass = "tdReporte", BackColor = color });

            fila.TableSection = TableRowSection.TableHeader;
            fila.Cells[0].Visible = false;
            tblResultados.Rows.Add(fila);

            int i;
            foreach(DataRow row in tabla.Rows)
            {
                fila = new TableRow();
                
                for(i=0; i<8; i++)
                    fila.Cells.Add(new TableCell() { Text = row[i].ToString(), CssClass = "tdReporteFila" });

                fila.Cells.Add(new TableCell() { Text = String.Format("{0:dd/MM/yyyy}", row[8]), CssClass = "tdReporteFila" });
                fila.Cells.Add(new TableCell() { Text = row[9].ToString(), CssClass = "tdReporteFila" });
                fila.Cells.Add(new TableCell() { Text = String.Format("{0:dd/MM/yyyy}", row[10]), CssClass = "tdReporteFila" });

                i = 11;
                for (anioActual = anioInicial; anioActual <= anioFinal; anioActual++, i+=4)
                {
                    fila.Cells.Add(new TableCell() { Text = string.Format("{0:c}", row[i]), CssClass = "tdReporteFila" });
                    fila.Cells.Add(new TableCell() { Text = string.Format("{0:c}", row[i + 1]), CssClass = "tdReporteFila" });
                    fila.Cells.Add(new TableCell() { Text = string.Format("{0:c}", row[i + 2]), CssClass = "tdReporteFila" });
                    fila.Cells.Add(new TableCell() { Text = string.Format("{0:c}", row[i + 3]), CssClass = "tdReporteFila" });
                }
                fila.Cells.Add(new TableCell() { Text = string.Format("{0:c}", row[i]), CssClass = "tdReporteFila" });
                fila.Cells.Add(new TableCell() { Text = string.Format("{0:c}", row[i + 1]), CssClass = "tdReporteFila" });
                fila.Cells.Add(new TableCell() { Text = string.Format("{0:c}", row[i + 2]), CssClass = "tdReporteFila" });
                fila.Cells.Add(new TableCell() { Text = string.Format("{0:c}", row[i + 3]), CssClass = "tdReporteFila" });

                fila.Cells[0].Visible = false;
                tblResultados.Rows.Add(fila);
            }

            tblResultados.DataBind();
            
        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            try
            {
                ClsReportesBl objBl = new ClsReportesBl();
                int gerente = Convert.ToInt32(ddlGerente.SelectedValue);
                int anio = Convert.ToInt32(ddlAnios.SelectedValue);
                int periodo = Convert.ToInt32(ddlPeriodo.SelectedValue);

                DataTable reporte = objBl.GetReporteMUBGerente(usuario, gerente, anio, periodo);
                reporte.Columns.Remove("id");

                string titulo = "Reporte de MUB por gerente";
                int total = reporte.Columns.Count;
                String[] columnas = new string[total];

                int anioFinal = Convert.ToInt32(ddlAnios.SelectedValue);
                int anioInicial = anioFinal - ((total - 14) / 4) + 1;
                int anioActual = anioInicial;

                columnas[0] = "No. de UN";
                columnas[1] = "Nombre UN";
                columnas[2] = "Director de área";
                columnas[3] = "Gerente de venta";
                columnas[4] = "No. C.C.";
                columnas[5] = "Nombre C.C.";
                columnas[6] = "Estatus";
                columnas[7] = "Fecha de inicio";
                columnas[8] = "Fecha payback";
                columnas[9] = "Fecha de término";

                int i = 10;
                for (anioActual = anioInicial; anioActual <= anioFinal; anioActual++, i+=4)
                {
                    columnas[i] = "Ingresos cobrados " + anioActual.ToString();
                    columnas[i + 1] = "Egresos " + anioActual.ToString();
                    columnas[i + 2] = "Intereses " + anioActual.ToString();
                    columnas[i + 3] = "MUB " + anioActual.ToString();
                }

                columnas[i] = "Ingresos cobrados acumulados a " + anioFinal.ToString();
                columnas[i + 1] = "Egresos acumulados a " + anioFinal.ToString();
                columnas[i + 2] = "Intereses acumulados a " + anioFinal.ToString();
                columnas[i + 3] = "MUB acumulado a " + anioFinal.ToString();

                byte[] archivo = ClsDataTableToExcelBl.ExportExcel(reporte, titulo, true, columnas);

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Buffer = true;
                HttpContext.Current.Response.Charset = "";
                HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=ReporteMUBGerente.xlsx");
                HttpContext.Current.Response.BinaryWrite(archivo);
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();
            }
            catch(System.Threading.ThreadAbortException e1)
            {
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = string.Empty,
                    DtFecha = DateTime.Now
                });
            }
        }
    }
}