﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReporteMUBGerente.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.Reportes.ReporteMUBGerente" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
    <i class="fa fa-sitemap" aria-hidden="true"></i> Reportes / MUB por gerente
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <main class="page">
        <form id="form1" runat="server">
            <div class="modal fade" id="modalRespuesta" role="dialog" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-lg" role="document" id="modalDialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button id="btnCloseModal" aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                            <h4 class="modal-title">MUB por gerente</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-8">
                                    <asp:Label ID="lblRespuesta" runat="server" CssClass="heading"></asp:Label>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar">Cerrar</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <div>
                <div class="row">
                    <div class="col-md-12 tituloPagina">
                        <asp:Label ID="lblTitulo" runat="server">Reporte de MUB por gerente</asp:Label>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-1" style="text-align:right;">
                        <label for="ddlGerente" style="text-align:right">Gerente:</label>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <asp:DropDownList ID="ddlGerente" runat="server" style="text-align:center;text-align-last:center;"
                                CssClass="form-control" AppendDataBoundItems="true">
                                <asp:ListItem Text="--- Todos ---" Value="0" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-1" style="text-align:right;">
                        <label for="ddlPeriodo" style="text-align:right">Hasta*:</label>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <asp:DropDownList ID="ddlAnios" runat="server" style="text-align:center;text-align-last:center;"
                                CssClass="form-control" AppendDataBoundItems="true" data-validar="requerido">                                
                            </asp:DropDownList>
                        </div>
                        <small class="form-text-error"></small>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <asp:DropDownList ID="ddlPeriodo" runat="server" style="text-align:center;text-align-last:center;"
                                CssClass="form-control" AppendDataBoundItems="true" data-validar="requerido">                                
                            </asp:DropDownList>
                        </div>
                        <small class="form-text-error"></small>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <asp:LinkButton  ID="btnBuscar" runat="server" CssClass="btn btn-primary validaForm" data-form="form1" OnClick="btnBuscar_Click"><i class="fa fa-search" aria-hidden="true"></i> Buscar</asp:LinkButton>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <asp:LinkButton  ID="btnExportar" runat="server" CssClass="btn btn-success validaForm" data-form="form1" OnClick="btnExportar_Click"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Exportar</asp:LinkButton>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10" style="overflow:auto;">
                        <asp:Table ID="tblResultados" runat="server" CssClass="table table-striped"></asp:Table>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </form>
    </main>
</asp:Content>
