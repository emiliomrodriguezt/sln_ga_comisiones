﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EL_GA_Comisiones;
using BL_GA_Comisiones;
using System.Reflection;
using System.Web.Security;

namespace UL_GA_Comisiones.Formularios.Reportes
{
    public partial class ReporteEdoCuenta : System.Web.UI.Page
    {
        private int usuario;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CargaCatalogos();
            }

            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity)Context.User.Identity).Ticket.UserData);
            usuario = objClsUsuarioActiveDirectoryEl.IntId;
            
        }

        protected void CargaCatalogos()
        {
            ClsTblEmpleadoBl objBl = new ClsTblEmpleadoBl();
            List<ClsTblEmpleadoEl> empleados = objBl.GetColaborador(0, 0, 1);
            ddlGerente.DataSource = empleados;
            ddlGerente.DataTextField = "NombreColaborador";
            ddlGerente.DataValueField = "IdEmp";
            ddlGerente.DataBind();

        }

        protected void LlenaGrid()
        {
            ClsReportesBl objBl = new ClsReportesBl();
            List<ClsReporteEdoCuentaEl> reporte = objBl.GetReporteEdoCuenta(usuario, Convert.ToInt32(ddlGerente.SelectedValue));

            gvResultados.DataSource = reporte;
            gvResultados.DataBind();

            if (gvResultados.HeaderRow != null)
                gvResultados.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                LlenaGrid();
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = string.Empty,
                    DtFecha = DateTime.Now
                });
            }
        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            try
            {
                ClsReportesBl objBl = new ClsReportesBl();

                byte[] archivo = objBl.GetReporteEdoCuentaXls(Convert.ToInt32(ddlGerente.SelectedValue));

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Buffer = true;
                HttpContext.Current.Response.Charset = "";
                HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=ReporteMUBGerente.xlsx");
                HttpContext.Current.Response.BinaryWrite(archivo);
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();
            }
            catch (System.Threading.ThreadAbortException e1)
            {
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception ex)
            {
                ClsLogBl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = string.Empty,
                    DtFecha = DateTime.Now
                });
            }
        }

    }
}