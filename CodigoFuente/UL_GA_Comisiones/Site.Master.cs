﻿using System;
using System.Web;
using System.Web.Security;
using BL_GA_Comisiones;

namespace UL_GA_Comisiones
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Context.User.Identity.IsAuthenticated)
            {
                Response.Redirect("SignIn.aspx");
            }

            var objClsUsuarioActiveDirectoryEl = ClsUserBl.UserLogged(((FormsIdentity) Context.User.Identity).Ticket.UserData);

            var objClsAsocRolPaginaEl = ClsPermitsBl.Getpermission(objClsUsuarioActiveDirectoryEl.IntIdRol, Request.Url.LocalPath);

            if (objClsAsocRolPaginaEl == null)
            {
                Response.Redirect("~/Home.aspx?option=1");
            }

            var objHttpCookie = new HttpCookie("Permits")
            {
                ["Edit"] = objClsAsocRolPaginaEl?.BolEditar.ToString(),
                ["Delete"] = objClsAsocRolPaginaEl?.BolEditar.ToString(),
                Expires = DateTime.Now.AddDays(1d)
            };

            Response.Cookies.Add(objHttpCookie);
            lblNombre.Text = objClsUsuarioActiveDirectoryEl.StrDisplayName;

            DivMenuPrincipal.InnerHtml = ClsMenuBl.GetMenu(((FormsIdentity)Context.User.Identity).Ticket.UserData, Request.Url.LocalPath);
        }
    }
}