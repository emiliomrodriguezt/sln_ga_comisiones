﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.UI;

namespace UL_GA_Comisiones
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = false;

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                        "~/Scripts/jquery-1.10.2.min.js",
                        "~/Scripts/jquery-migrate-1.2.1.min.js",
                        "~/Scripts/jquery-ui-1.10.3.min.js",
                        "~/Scripts/bootstrap.min.js",
                        "~/Scripts/modernizr.min.js",
                        "~/Scripts/jquery.sparkline.min.js",
                        "~/Scripts/toggles.min.js",
                        "~/Scripts/retina.min.js",
                        "~/Scripts/jquery.cookies.js",
                        "~/Scripts/flot/flot.min.js",
                        "~/Scripts/flot/flot.resize.min.js",
                        "~/Scripts/morris.min.js",
                        "~/Scripts/raphael-2.1.0.min.js",
                        "~/Scripts/jquery.datatables.min.js",
                        "~/Scripts/chosen.jquery.min.js",
                        "~/Scripts/jquery.maskMoney.js",
                        "~/Scripts/accounting.js",
                        "~/Scripts/jquery.mask.js",
                        "~/Scripts/custom.js"
                        ));
        }
    }
}