﻿using System;
using System.Web;
using System.Web.Security;

namespace UL_GA_Comisiones
{
    public partial class SingOut : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Context.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.SignOut();
            }

            if (Request.Cookies["Permits"] != null)
            {
                var objHttpCookie = new HttpCookie("Permits")
                {
                    Expires = DateTime.Now.AddDays(-1d)
                };
                Response.Cookies.Add(objHttpCookie);
            }

            if (Request.Cookies["strApplicationPath"] != null)
            {
                var objHttpCookie = new HttpCookie("strApplicationPath")
                {
                    Expires = DateTime.Now.AddDays(-1d)
                };
                Response.Cookies.Add(objHttpCookie);
            }

            Response.Redirect("Login.aspx");

        }
    }
}