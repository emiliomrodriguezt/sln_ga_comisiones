﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using BL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace UL_GA_Comisiones
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.Form["strUser"]) && !string.IsNullOrEmpty(Request.Form["strPassword"]))
            {
                var strUser = Request.Form["strUser"].Trim();
                var strPassword = Request.Form["strPassword"].Trim();

                var objClsLoginBl = new ClsLoginBl(strUser, strPassword);
                var obj = objClsLoginBl.Login();

                if (obj.Item2.IntId > 0)
                {
                    IniciaSession(obj.Item2);
                }

                ShowMessage("alert-danger", obj.Item1);
            }
        }

        private void IniciaSession(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl)
        {
            var objFormsAuthenticationTicket = new FormsAuthenticationTicket(1, objClsUsuarioActiveDirectoryEl.StrUser, DateTime.Now, DateTime.Now.AddMinutes(30), true, JsonConvert.SerializeObject(objClsUsuarioActiveDirectoryEl));
            var objHttpCookie = new HttpCookie(FormsAuthentication.FormsCookieName,FormsAuthentication.Encrypt(objFormsAuthenticationTicket))
            {
                Expires = objFormsAuthenticationTicket.Expiration,
                Path = FormsAuthentication.FormsCookiePath
            };
            Response.Cookies.Add(objHttpCookie);

            var objHttpCookieHost = new HttpCookie("strApplicationPath", $"{Request.Url.Scheme}://{Request.Url.Host}:{Request.Url.Port}")
            {
                Expires = DateTime.Now.AddDays(1d)
            };
            Response.Cookies.Add(objHttpCookieHost);

            var strRedirect = Request["ReturnUrl"] ?? "Home.aspx";

            Response.Redirect(strRedirect, true);
        }

        private void ShowMessage(string strType, string strMessage)
        {
            Page.Controls.Add(new LiteralControl($"<script>fnMessage(\"{strType}\", \"{strMessage}\", true);</script>"));
        }
    }
}