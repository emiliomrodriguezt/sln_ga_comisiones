﻿using System;
using System.Web.UI;
using EL_GA_Comisiones;

namespace UL_GA_Comisiones
{
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["option"]))
            {
                ShowMessage("alert-danger", StrucMessagesEl.WithoutPermits);
            }
        }

        private void ShowMessage(string strType, string strMessage)
        {
            Page.Controls.Add(new LiteralControl($"<script>fnMessage(\"{strType}\", \"{strMessage}\", true);</script>"));
        }
    }
}