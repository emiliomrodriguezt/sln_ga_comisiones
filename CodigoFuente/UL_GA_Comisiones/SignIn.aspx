﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SignIn.aspx.cs" Inherits="UL_GA_Comisiones.Login" %>
<!DOCTYPE html>
<html lang="es">
    <head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Grupo Altavista - SCV</title>
        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/bundles/js") %>
        </asp:PlaceHolder>
        <webopt:bundlereference runat="server" path="~/Content/css" />
        <link href="~/images/GA.png" rel="shortcut icon" type="image/x-icon" />
    </head>
    <body class="signin">
        <div id="preloader">
            <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
        </div>
        <section>
            <div class="signinpanel">
                <div class="row">
                    <div class="col-md-12">
                        <div id="divMensaje" class="alert alert-danger cssClsHidden">
                             <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                             <span id="strMensaje"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <div class="signin-info">
                            <div class="logopanel">
                                <img alt="GRUPO ALTAVISTA" src="/images/LogoGA.png" />
                            </div>
                            <div class="mb20"></div>
                            <h5><strong>Bienvenido al sistema de cálculo de compensación variable.</strong></h5>
                            <ul>
                                <li><i class="fa fa-arrow-circle-o-right mr5"></i> Diseño totalmente responsivo</li>
                                <li><i class="fa fa-arrow-circle-o-right mr5"></i> HTML5/CSS3 </li>
                                <li><i class="fa fa-arrow-circle-o-right mr5"></i> MultiNavegador</li>
                                <li><i class="fa fa-arrow-circle-o-right mr5"></i> Disponible las 24 horas los 365 días del año. </li>
                            </ul>
                            <div class="mb20"></div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <form id="frmLogin" method="post" action="SignIn.aspx">
                            <h4 class="nomargin">Inicia Sesión.</h4>
                            <p class="mt5 mb20">Entre para acceder a su cuenta.</p>
                            <asp:Label runat="server" ID="lblMsg" Visible="False" CssClass="text-danger FontSmall"></asp:Label>
                            <div>
                                <input type="text" placeholder="Usuario" id="user" name="strUser" data-validar="requerido" class="form-control"/>
                            </div>
                            <small class="form-text-error"></small>
                            <div>
                                <input type="password" placeholder="Contraseña" id="pwd" name="strPassword" data-validar="requerido" class="form-control"/>
                            </div>
                            <small class="form-text-error"></small>
                            <button type="submit" data-form="frmLogin" class="btn btn-success btn-block validaForm"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Acceder</button>
                        </form>
                    </div>
                </div>
                <div class="signup-footer">
                    <div class="pull-left">
                        &copy; 2017. Todos los derechos reservados. Grupo Altavista.
                    </div>
                    <div class="pull-right">
                        Created By: <a href="http://www.grupoaltavista.com/" target="_blank">Grupo Altavista</a>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>