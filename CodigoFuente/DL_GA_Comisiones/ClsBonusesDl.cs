﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace DL_GA_Comisiones
{
    public class ClsBonusesDl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsBonusesDl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsTblBonosEl> GetBonuses(int intId, int intIdColaborador)
        {
            var lstClsTblBonosEl = new List<ClsTblBonosEl>();
            var objClsCollaboratorDl = new ClsCollaboratorDl(ObjClsUsuarioActiveDirectoryEl);

            try
            {
                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_TBL_BONOS]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", intId),
                        new SqlParameter("@p_idColaborador", intIdColaborador)
                    });

                lstClsTblBonosEl = (from a in dtResult.DtResult.AsEnumerable()
                                        select new ClsTblBonosEl()
                                        {
                                            IntId = (int)a["id"],
                                            IntIdColaborador = (int)a["idColaborador"],
                                            DecMontoBono = (decimal)a["montoBono"],
                                            IntAnio = (int)a["anio"],
                                            BolPagado = (bool)a["pagado"],
                                            BolActivo = (bool)a["activo"],
                                            IntIdUsuarioCrecion = (int)a["idUsuarioCreacion"],
                                            IntIdUsuarioActulizacion = a["idUsuarioActulizacion"] == DBNull.Value ? null : (int?)a["idUsuarioActulizacion"],
                                            DtFechaCreacion = (DateTime)a["fechaCreacion"],
                                            DtFechaActulizacion = a["fechaActulizacion"] == DBNull.Value ? null : (DateTime?)a["fechaActulizacion"],
                                            ObjClsTblColaboradorEl = objClsCollaboratorDl.GetCollaborators((int)a["idColaborador"], "").FirstOrDefault()
                                        }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdColaborador }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsTblBonosEl;
        }

        public ClsSimpleResultEl SetBonus(ClsTblBonosEl objClsTblBonosEl, bool isDelete)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();

            try
            {
                objClsSimpleResultEl = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_SET_TBL_BONOS]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", objClsTblBonosEl.IntId),
                        new SqlParameter("@p_idColaborador", objClsTblBonosEl.IntIdColaborador),
                        new SqlParameter("@p_montoBono", objClsTblBonosEl.DecMontoBono),
                        new SqlParameter("@p_anio", objClsTblBonosEl.IntAnio),
                        new SqlParameter("@p_pagado", objClsTblBonosEl.BolPagado),
                        new SqlParameter("@p_activo", objClsTblBonosEl.BolActivo),
                        new SqlParameter("@p_idUsuarioCreacion", objClsTblBonosEl.IntIdUsuarioCrecion),
                        new SqlParameter("@p_idUsuarioActulizacion", objClsTblBonosEl.IntIdUsuarioActulizacion),
                        new SqlParameter("@p_fechaCreacion", objClsTblBonosEl.DtFechaCreacion),
                        new SqlParameter("@p_fechaActulizacion", objClsTblBonosEl.DtFechaActulizacion),
                        new SqlParameter("@p_isDelete", isDelete)
                    });
                objClsSimpleResultEl.BolSuccess = true;
                objClsSimpleResultEl.StrMessage = objClsSimpleResultEl.IntId == 0 ? StrucMessagesEl.GenericoInsertOk : StrucMessagesEl.GenericoUpdateOk;
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(objClsTblBonosEl),
                    DtFecha = DateTime.Now
                });
                objClsSimpleResultEl.BolSuccess = false;
                objClsSimpleResultEl.StrMessage = StrucMessagesEl.GenericoError;
            }

            return objClsSimpleResultEl;
        }
    }
}
