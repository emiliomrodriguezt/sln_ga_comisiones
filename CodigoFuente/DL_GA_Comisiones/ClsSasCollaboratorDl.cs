﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace DL_GA_Comisiones
{
    public class ClsSasCollaboratorDl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsSasCollaboratorDl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsTblSASCalculadoColaboradorEl> GetSasCollaborator(int intId, int intIdColaborador, int intAnio)
        {
            var lstClsTblSasCalculadoColaboradorEl = new List<ClsTblSASCalculadoColaboradorEl>();

            try
            {
                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_TBL_SASCALCULADOCOLABORADOR]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", intId),
                        new SqlParameter("@p_idColaborador", intIdColaborador),
                        new SqlParameter("@p_ano", intAnio)
                    });

                lstClsTblSasCalculadoColaboradorEl = (from a in dtResult.DtResult.AsEnumerable()
                                                      select new ClsTblSASCalculadoColaboradorEl()
                                                      {
                                                          IdSAS = (int)a["id"],
                                                          IdColaborador = (int)a["idColaborador"],
                                                          DuracionMeses = (int)a["duracionMeses"],
                                                          Anio = (int)a["ano"],
                                                          MUBMinimo = (decimal)a["MUBMinimo"],
                                                          MUBContratado = (decimal)a["MUBContratado"],
                                                          SueldoMUBContratado = (decimal)a["sueldoMUBContratado"],
                                                          Sueldo = (decimal)a["sueldo"],
                                                          Pagado = (bool)a["pagado"],
                                                          CuotaSAS = (decimal)a["cuotaSAS"],
                                                          MontoPagado = (decimal)a["montoPagado"],
                                                          IdUsuarioCreacion = (int)a["idUsuarioCreacion"],
                                                          FechaCreacion = (DateTime)a["fechaCreacion"] 
                                                      }).ToList();
            }                                         
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdColaborador, intAnio }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsTblSasCalculadoColaboradorEl;
        }

        public DataTable GetFormatReportSasCollaborator(int intIdColaborador, int intAnioInicio, int intAnioFinal)
        {
            var dtResult = new DataTable();

            try
            {
                dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_TBL_SASCALCULADOCOLABORADORREPORTE]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_idColaborador", intIdColaborador),
                        new SqlParameter("@p_anoInicio", intAnioInicio),
                        new SqlParameter("@p_anoFinal", intAnioFinal)
                    }).DtResult;
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intIdColaborador, intAnioInicio, intAnioFinal }),
                    DtFecha = DateTime.Now
                });
            }

            return dtResult;
        }
    }
}
