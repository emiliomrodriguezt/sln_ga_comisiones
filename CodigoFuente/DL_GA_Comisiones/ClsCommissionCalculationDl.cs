﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace DL_GA_Comisiones
{
    public class ClsCommissionCalculationDl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsCommissionCalculationDl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsTblCalculoComisionEl> GetCommissionCalculation(int intId, int intIdColaborador, int intAnio)
        {
            var lstClsTblCalculoComisionEl = new List<ClsTblCalculoComisionEl>();

            try
            {
                var objClsCollaboratorDl = new ClsCollaboratorDl(ObjClsUsuarioActiveDirectoryEl);

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_TBL_CALCULOCOMISION]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", intId),
                        new SqlParameter("@p_idColaborador", intIdColaborador),
                        new SqlParameter("@p_anio", intAnio)
                    });

                lstClsTblCalculoComisionEl = (from a in dtResult.DtResult.AsEnumerable()
                                              select new ClsTblCalculoComisionEl()
                                              {
                                                  IdComision = (int)a["id"],
                                                  IdColaborador = (int)a["idColaborador"],
                                                  Anio = (int)a["anio"],
                                                  Periodo = (int)a["periodo"],
                                                  SASVisibles = (int)a["sasVisibles"],
                                                  ComisionTotal = a["comisionTotal"] == DBNull.Value ? null : (decimal?)a["comisionTotal"],
                                                  MontoSAS = a["montoSAS"] == DBNull.Value ? null : (decimal?)a["montoSAS"],
                                                  OtrosPagos = a["otrosPagos"] == DBNull.Value ? null : (decimal?)a["otrosPagos"],
                                                  TotalAPagar = a["totalAPagar"] == DBNull.Value ? null : (decimal?)a["totalAPagar"],
                                                  Pagado = (bool)a["pagado"],
                                                  FirmaContraloria = a["firmaContraloria"].ToString(),
                                                  FirmaDirectorUn = a["firmaDirectorUn"].ToString(),
                                                  FirmaPresidente = a["firmaPresidente"].ToString(),
                                                  FirmaDirectorGrl = a["firmaDirectorGrl"].ToString(),
                                                  DocGenerado = a["docGenerado"] == DBNull.Value ? new byte[0] : (byte[])a["docGenerado"],
                                                  IdUsuarioCreacion = (int)a["idUsuarioCreacion"],
                                                  FechaCreacion = (DateTime)a["fechaCreacion"],
                                                  ObjClsTblColaboradorEl = objClsCollaboratorDl.GetCollaborators((int)a["idColaborador"], "").FirstOrDefault()
                                              }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdColaborador, intAnio }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsTblCalculoComisionEl;
        }

        public ClsSimpleResultEl SetCommissionCalculation(ClsTblCalculoComisionEl objClsTblCalculoComisionEl, bool isDelete)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();

            try
            {
                objClsSimpleResultEl = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_SET_TBL_CALCULOCOMISION]", new List<SqlParameter>()
                {
                    new SqlParameter("@p_id", objClsTblCalculoComisionEl.IdComision),
                    new SqlParameter("@p_idColaborador", objClsTblCalculoComisionEl.IdColaborador),
                    new SqlParameter("@p_anio", objClsTblCalculoComisionEl.Anio),
                    new SqlParameter("@p_periodo", objClsTblCalculoComisionEl.Periodo),
                    new SqlParameter("@p_sasVisibles", objClsTblCalculoComisionEl.SASVisibles),
                    new SqlParameter("@p_comisionTotal", objClsTblCalculoComisionEl.ComisionTotal),
                    new SqlParameter("@p_montoSAS", objClsTblCalculoComisionEl.MontoSAS),
                    new SqlParameter("@p_otrosPagos", objClsTblCalculoComisionEl.OtrosPagos),
                    new SqlParameter("@p_totalAPagar", objClsTblCalculoComisionEl.TotalAPagar),
                    new SqlParameter("@p_pagado", objClsTblCalculoComisionEl.Pagado),
                    new SqlParameter("@p_idUsuarioCreacion", objClsTblCalculoComisionEl.IdUsuarioCreacion),
                    new SqlParameter("@p_fechaCreacion", objClsTblCalculoComisionEl.FechaCreacion),
                    new SqlParameter("@p_isDelete", isDelete)
                });

                objClsSimpleResultEl.BolSuccess = true;
                objClsSimpleResultEl.StrMessage = objClsSimpleResultEl.IntId == 0 ? StrucMessagesEl.GenericoInsertOk : StrucMessagesEl.GenericoUpdateOk;
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(objClsTblCalculoComisionEl),
                    DtFecha = DateTime.Now
                });
                objClsSimpleResultEl.BolSuccess = false;
                objClsSimpleResultEl.StrMessage = StrucMessagesEl.GenericoError;
            }

            return objClsSimpleResultEl;
        }

        public ClsSimpleResultEl UpdateSigning(int intId, string strFirmaContraloria, string strFirmaDirectorUn, string strFirmaDirectorGrl, string strFirmaPresidente, byte[] arrBytDocument)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();

            try
            {
                objClsSimpleResultEl = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_UPDATE_TBL_CALCULOCOMISION]", new List<SqlParameter>()
                {
                    new SqlParameter("@p_id", intId),
                    new SqlParameter("@p_firmaContraloria", strFirmaContraloria),
                    new SqlParameter("@p_firmaDirectorUn", strFirmaDirectorUn),
                    new SqlParameter("@p_firmaDirectorGrl", strFirmaDirectorGrl),
                    new SqlParameter("@p_firmaPresidente", strFirmaPresidente),
                    new SqlParameter("@p_docGenerado", arrBytDocument)
                });

                objClsSimpleResultEl.BolSuccess = true;
                objClsSimpleResultEl.StrMessage = objClsSimpleResultEl.IntId == 0 ? StrucMessagesEl.GenericoInsertOk : StrucMessagesEl.GenericoUpdateOk;
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, strFirmaContraloria, strFirmaDirectorUn, strFirmaDirectorGrl, strFirmaPresidente }),
                    DtFecha = DateTime.Now
                });
                objClsSimpleResultEl.BolSuccess = false;
                objClsSimpleResultEl.StrMessage = StrucMessagesEl.GenericoError;
            }

            return objClsSimpleResultEl;
        }
    }
}
