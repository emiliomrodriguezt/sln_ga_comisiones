﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace DL_GA_Comisiones
{
    public class ClsOtherPayDl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsOtherPayDl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsTblOtrosPagosComisionEl> GetsOtherPay(int intId, int intIdComision, int intIdTipoPago, int intIdPago)
        {
            var lstClsTblOtrosPagosComisionEl = new List<ClsTblOtrosPagosComisionEl>();

            try
            {
                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_TBL_OTROSPAGOS_COMISION]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", intId),
                        new SqlParameter("@p_idComision", intIdComision),
                        new SqlParameter("@p_idTipoPago", intIdTipoPago),
                        new SqlParameter("@p_idPago", intIdPago)
                    });

                var objClsAdvancesDl = new ClsAdvancesDl(ObjClsUsuarioActiveDirectoryEl);

                lstClsTblOtrosPagosComisionEl = (from a in dtResult.DtResult.AsEnumerable()
                                                 select new ClsTblOtrosPagosComisionEl()
                                                 {
                                                     IntId = (int)a["id"],
                                                     IntIdComision = (int)a["idComision"],
                                                     IntIdTipoPago = (int)a["idTipoPago"],
                                                     IntIdPago = (int)a["idPago"],
                                                     DecImporteAPagar = (decimal)a["importeAPagar"],
                                                     DecImporteTotal = (decimal)a["importeTotal"],
                                                     IntAno = (int)a["anio"],
                                                     BolPagado = (bool)a["pagado"],
                                                     ObjClsTblAnticiposEl = (int)a["idTipoPago"] == 2 ? objClsAdvancesDl.GetAdvances(Convert.ToInt32(a["idPago"]), 0, 0).FirstOrDefault() : new ClsTblAnticiposEl()
                                                 }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdComision, intIdTipoPago, intIdPago }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsTblOtrosPagosComisionEl;
        }

    }
}
