﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace DL_GA_Comisiones
{
    public class ClsMenunDl
    {
        public static List<ClsCatPaginaEl> GetMenu(int intIdRol)
        {
            var lstClsCatPaginaEl = new List<ClsCatPaginaEl>();

            try
            {
                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_Menu]", new List<SqlParameter>()
                    {
                        new SqlParameter("@p_idRol", intIdRol)
                    });

                lstClsCatPaginaEl = (from a in dtResult.DtResult.AsEnumerable()
                                     select new ClsCatPaginaEl()
                                     {
                                         IntId = (int) a["id"],
                                         IntIdPagina = a["idPagina"] == DBNull.Value ? null : (int?) a["idPagina"],
                                         StrNombre = a["nombre"].ToString(),
                                         StrUrl = a["url"].ToString(),
                                         StrIcono = a["icono"].ToString(),
                                         IntOrden = (int) a["orden"],
                                         StrDescripcion = a["descripcion"].ToString(),
                                         BolActivo = (bool) a["activo"],
                                         IntIdUusarioCreacion = a["idUsuarioCreacion"] == DBNull.Value ? null : (int?) a["idUsuarioCreacion"],
                                         IntIdUsuarioActualizacion = a["idUsuarioActulizacion"] == DBNull.Value ? null : (int?) a["idUsuarioActulizacion"],
                                         DtFechaCreacion = a["fechaCreacion"] == DBNull.Value ? null : (DateTime?) a["fechaCreacion"],
                                         DtActualizacion = a["fechaActulizacion"] == DBNull.Value ? null : (DateTime?) a["fechaActulizacion"]
                                     }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intIdRol }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsCatPaginaEl;
        }
    }
}
