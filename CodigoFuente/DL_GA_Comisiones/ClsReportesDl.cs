﻿using EL_GA_Comisiones;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DL_GA_Comisiones
{
    public class ClsReportesDl
    {
        public DataTable GetReporteMUBGerente(int idCol, int anio, int mes)
        {
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@idCol", idCol),
                    new SqlParameter("@anio", anio),
                    new SqlParameter("@mes", mes)
                };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_ReporteMUBGerente]",
                    parametros);

                return dtResult.DtResult;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public DataTable GetCostosPorAnio(int usuario, int anio, int mes)
        {
            try
            {
                string consultaCostos = @"SELECT [C.C ERP P&L] zIDH, año anio, ISNULL(SUM(monto), 0) costo 
                                        FROM [BDM_GLFI].[dbo].[IDM_Presupuestos_All_Vi]
                                        WHERE TIPO='Real' AND DESCRIPCION_PL='COSTO'
                                          AND ([AÑO] < " + anio + " OR ([AÑO]="+anio+ " AND MONTH([PERIODO CONTABLE])<="+mes.ToString()+@"))
                                          GROUP BY [C.C ERP P&L], [año]
                                          ORDER BY 1";

                List<SqlParameter> parametros = new List<SqlParameter>();

                ClsSimpleResultEl dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER16", consultaCostos,
                        parametros, false);

                return dtResult.DtResult;
            }
            catch (Exception e)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = e.Message,
                    StrPilaEventos = e.StackTrace,
                    StrMensajeInterno = e.InnerException?.Message ?? "",
                    StrPilaEventosInterno = e.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { usuario }),
                    DtFecha = DateTime.Now
                });
            }

            return null;
        }

        public DataTable GetReporteMUBCC(int idCol, int idCC)
        {
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@idCol", idCol),
                    new SqlParameter("@idCC", idCC)
                };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_ReporteMUBCC]",
                    parametros);

                return dtResult.DtResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ClsRespuestaSP UpdateInteresesMUB(int idUsuario, int idMUB, decimal intereses, decimal mub)
        {
            ClsRespuestaSP respuesta = new ClsRespuestaSP();
            respuesta.Resultado = 0;
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@idMub", idMUB),
                    new SqlParameter("@interes", intereses),
                    new SqlParameter("@mub", mub),
                    new SqlParameter("@idUsuario", idUsuario),
                    new SqlParameter("@resultado", SqlDbType.Int)
                        {Direction=ParameterDirection.Output }
                };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_UPDATE_InteresesMUB]",
                    parametros);
            }
            catch (Exception e)
            {
                respuesta.Resultado = -1;
                respuesta.Mensaje = e.Message;
            }
            return respuesta;
        }

        public DataTable GetReporteEdoCuenta(int idCol)
        {
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@idCol", idCol)
                };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_ReporteEdoCuenta]",
                    parametros);

                return dtResult.DtResult;
            }
            catch (Exception ex)
            {

            }
            return null;
        }
    }
}
