﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EL_GA_Comisiones;

namespace DL_GA_Comisiones
{
    public class ClsRubroPagoDl
    {
        public List<ClsRubroPagoEl> GetRuboPago(int id, int activo)
        {
            var respuesta = new List<ClsRubroPagoEl>();
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@idRubro", id),
                    new SqlParameter("@activo", activo)
                };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_RubroPago]",
                    parametros);

                respuesta = (from a in dtResult.DtResult.AsEnumerable()
                             select new ClsRubroPagoEl()
                             {
                                 IdRubro = (int)a["idRubro"],
                                 Nombre = a["nombre"].ToString(),
                                 Porcentaje = (decimal)a["porcentaje"],
                                 Activo = (bool)a["activo"] == true ? 1 : 0,
                                 IdUsuarioCreacion = (int)a["idUsuarioCreacion"],
                                 IdUsuarioActulizacion = a["idUsuarioActulizacion"] == DBNull.Value ? null : (int?)a["idUsuarioActulizacion"],
                                 FechaCreacion = (DateTime)a["fechaCreacion"],
                                 FechaActulizacion = a["fechaActulizacion"] == DBNull.Value ? null : (DateTime?)a["fechaActulizacion"],
                                 Estatus = (bool)a["activo"] == true ? "Activo" : "Inactivo"
                             }).ToList();
            }
            catch (Exception e)
            {

            }
            return respuesta;
        }

        public ClsRespuestaSP SetRubroPago(ClsRubroPagoEl rubro)
        {
            ClsRespuestaSP respuesta = new ClsRespuestaSP();
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@nombre", rubro.Nombre),
                    new SqlParameter("@porcentaje", rubro.Porcentaje),
                    new SqlParameter("@activo", rubro.Activo),
                    new SqlParameter("@idUsuarioCreacion", rubro.IdUsuarioCreacion),
                    new SqlParameter("@resultado", SqlDbType.Int)
                        {Direction=ParameterDirection.Output }
                };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_SET_RubroPago]",
                    parametros);

                respuesta.Valor = dtResult.IntId.Value;
            }
            catch (Exception e)
            {
                respuesta.Resultado = -1;
                respuesta.Mensaje = e.Message;
            }
            return respuesta;
        }

        public ClsRespuestaSP UpdateRubroPago(ClsRubroPagoEl rubro)
        {
            ClsRespuestaSP respuesta = new ClsRespuestaSP();
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@idRubro", rubro.IdRubro),
                    new SqlParameter("@nombre", rubro.Nombre),
                    new SqlParameter("@porcentaje", rubro.Porcentaje),
                    new SqlParameter("@activo", rubro.Activo),
                    new SqlParameter("@idUsuarioActualizacion", rubro.IdUsuarioActulizacion),
                    new SqlParameter("@resultado", SqlDbType.Int)
                        {Direction=ParameterDirection.Output }
                };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_UPDATE_RubroPago]",
                    parametros);

                respuesta.Resultado = 0;
            }
            catch (Exception e)
            {
                respuesta.Resultado = -1;
                respuesta.Mensaje = e.Message;
            }
            return respuesta;
        }
    }
}
