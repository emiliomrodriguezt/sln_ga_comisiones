﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace DL_GA_Comisiones
{
    public class ClsCenterCostCollaboratorSasDl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsCenterCostCollaboratorSasDl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsTblCentoCostosColaboradorSasEl> GetCenterCostCollaboratorSas(int intId, int intIdColaborador, int intIdDirector, int intIdCentroCostos, int intAno)
        {
            var lstClsTblCentoCostosColaboradorSasEl = new List<ClsTblCentoCostosColaboradorSasEl>();
            var objClsCollaboratorDl = new ClsCollaboratorDl();
            var objClsCenterCostsDl = new ClsCenterCostsDl(ObjClsUsuarioActiveDirectoryEl);

            try
            {
                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_TBL_CENTROCOSTOS_COLABORADOR_SAS]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", intId),
                        new SqlParameter("@p_idColaborador", intIdColaborador),
                        new SqlParameter("@p_idDirector", intIdDirector),
                        new SqlParameter("@p_idCentroCostos", intIdCentroCostos),
                        new SqlParameter("@p_ano", intAno)
                    });

                lstClsTblCentoCostosColaboradorSasEl = (from a in dtResult.DtResult.AsEnumerable()
                                                        select new ClsTblCentoCostosColaboradorSasEl()
                                                        {
                                                            IntId = (int)a["id"],
                                                            IntIdColaborador = (int)a["idColaborador"],
                                                            IntIdDirector = (int)a["idDirector"],
                                                            IntIdCentroCostos = (int)a["idCentroCostos"],
                                                            IntAno = (int)a["ano"],
                                                            DecPorcentajeParticipacion = (decimal)a["porcentajeParticipacion"],
                                                            DecMubContratado = (decimal)a["mubContratado"],
                                                            DecMonto = (decimal)a["monto"],
                                                            BolPagado = (bool)a["pagado"],
                                                            IntIdUsuarioCreacion = (int)a["idUsuarioCreacion"],
                                                            DtFechaCreacion = (DateTime)a["fechaCreacion"],
                                                            ObjClsTblColaboradorEl = objClsCollaboratorDl.GetCollaborators((int)a["idColaborador"], "").FirstOrDefault(),
                                                            ObjClsTblColaboradorDirectorEl = objClsCollaboratorDl.GetCollaborators((int)a["idDirector"], "").FirstOrDefault(),
                                                            ObjClsCatCentroCostosEl = objClsCenterCostsDl.GetCenterCosts((int)a["idCentroCostos"], "").FirstOrDefault()
                                                        }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdColaborador, intIdCentroCostos }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsTblCentoCostosColaboradorSasEl;
        }
    }
}
