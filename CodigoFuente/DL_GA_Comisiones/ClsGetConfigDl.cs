﻿using System;
using System.IO;
using System.Xml;
using System.Reflection;
using EL_GA_Comisiones;
using Utility_GA_Comisiones;

namespace DL_GA_Comisiones
{
    public static class ClsGetConfigDl
    {
        public static ClsDataConectionEl GetDatosConeccion(string strNameConecction)
        {
            try
            {
                var strPathConfig = $"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase)}\\DataConfig.xml".Replace("file:\\", "");

                var xmlFilConfig = new XmlDocument();
                xmlFilConfig.Load(strPathConfig);
                var xnoConfig = xmlFilConfig.SelectSingleNode($"configuration/SQLSERVER/{strNameConecction}");
                if (xnoConfig == null)
                {
                    throw new Exception($"No se encontro la coneccion con nombre [{strNameConecction}]");
                }
                if (xnoConfig.Attributes == null)
                {
                    throw new Exception($"El nodo [{strNameConecction}] no contiene attributos");

                }

                if (string.IsNullOrEmpty(xnoConfig.Attributes["StrHostName"].Value))
                {
                    throw new Exception($"El nodo [{strNameConecction}] no contiene attributos");
                }
                if (string.IsNullOrEmpty(xnoConfig.Attributes["StrDataBaseName"].Value))
                {
                    throw new Exception("El atributo [StrDataBaseName] no se encontro");
                }
                if (string.IsNullOrEmpty(xnoConfig.Attributes["StrUser"].Value))
                {
                    throw new Exception("El atributo [StrUser] no se encontro");
                }
                if (string.IsNullOrEmpty(xnoConfig.Attributes["StrPassword"].Value))
                {
                    throw new Exception("El atributo [StrPassword] no se encontro");
                }

                return new ClsDataConectionEl()
                {
                    StrHostName = ClsCryptographyBl.DecodeString(xnoConfig.Attributes["StrHostName"].Value),
                    StrInstance = ClsCryptographyBl.DecodeString(xnoConfig.Attributes["StrInstance"].Value),
                    StrPort = ClsCryptographyBl.DecodeString(xnoConfig.Attributes["StrPort"].Value),
                    StrDataBaseName = ClsCryptographyBl.DecodeString(xnoConfig.Attributes["StrDataBaseName"].Value),
                    StrUser = ClsCryptographyBl.DecodeString(xnoConfig.Attributes["StrUser"].Value),
                    StrPassword = ClsCryptographyBl.DecodeString(xnoConfig.Attributes["StrPassword"].Value)
                };
            }
            catch (Exception ex)
            {
                throw new Exception($"Erro al recuperar info de archivo config [{ex.Message}]");
            }
        }

        public static string GetKeyConfig(string strNameConfig, string strKeyName)
        {
            try
            {
                var strPathConfig = $"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase)}\\DataConfig.xml".Replace("file:\\", "");

                if (!File.Exists(strPathConfig))
                {
                    throw new Exception($"No se encontro el archvo de configuracion en la ruta [{strPathConfig}]");
                }

                var xmlFilConfig = new XmlDocument();
                xmlFilConfig.Load(strPathConfig);
                var xnoConfig = xmlFilConfig.SelectSingleNode($"configuration/APPCONFIG/{strNameConfig}");
                if (xnoConfig == null)
                {
                    throw new Exception($"No se encontro la coneccion con nombre [{strNameConfig}]");
                }

                if (xnoConfig.Attributes == null)
                {
                    throw new Exception($"El nodo [{strNameConfig}] no contiene attributos");
                }

                if (xnoConfig.Attributes[strKeyName] == null)
                {
                    throw new Exception($"no se encontro el atributo [{strKeyName}] en el nodo [{strNameConfig}]");
                }

                return ClsCryptographyBl.DecodeString(xnoConfig.Attributes[strKeyName].Value);
            }
            catch (Exception ex)
            {
                throw new Exception($"Erro al recuperar info de archivo config [{ex.Message}]");
            }
        }
    }
}
