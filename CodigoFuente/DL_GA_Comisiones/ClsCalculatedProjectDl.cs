using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace DL_GA_Comisiones
{
    public class ClsMubCalculatedProjectDl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsMubCalculatedProjectDl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsTblMubCalculadoProyecto> GetMubCalculatedProject(int intId, int intIdCentroCostos, int intAno)
        {
            var lstClsTblMubCalculadoProyectoEl = new List<ClsTblMubCalculadoProyecto>();
            var objClsCenterCostsDl = new ClsCenterCostsDl(ObjClsUsuarioActiveDirectoryEl);

            try
            {
                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_TBL_MUBCALCULADOPROYECTO]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", intId),
                        new SqlParameter("@p_idCentroCostos", intIdCentroCostos),
                        new SqlParameter("@p_ano", intAno)
                    });

                lstClsTblMubCalculadoProyectoEl = (from a in dtResult.DtResult.AsEnumerable()
                    select new ClsTblMubCalculadoProyecto()
                    {
                        IntId= (int)a["id"],
                        IntIdCentroCostos= (int)a["idCentroCostos"],
                        IntPeriodo = (int)a["periodo"],
                        IntAno = (int)a["ano"],
                        DecTotalIngresos = (decimal)a["totalIngresos"],
                        DecTotalEgresos = (decimal)a["totalEgresos"],
                        DecTotalIntereses = (decimal)a["totalIntereses"],
                        DecMubCalculado = (decimal)a["MUBCalculado"],
                        DecMubComision = (decimal)a["MUBComision"],
                        BolCongelado = (bool)a["congelado"],
                        IntIdUsuarioCreacion = a["idUsuarioCreacion"] == DBNull.Value ? null : (int?)a["idUsuarioCreacion"],
                        IntIdUsuarioActualizacion = a["idUsuarioActualizacion"] == DBNull.Value ? null : (int?)a["idUsuarioActualizacion"],
                        DtFechaCreacion = a["fechaCreacion"] == DBNull.Value ? null : (DateTime?)a["fechaCreacion"],
                        DtFechaActualizacion = a["fechaActualizacion"] == DBNull.Value ? null : (DateTime?)a["fechaActualizacion"],
                        ObjClsCatCentroCostosEl = objClsCenterCostsDl.GetCenterCosts((int)a["idCentroCostos"], "").FirstOrDefault()
                    }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdCentroCostos, intAno }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsTblMubCalculadoProyectoEl;
        }
    }
}