﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace DL_GA_Comisiones
{
    public class ClsTblEmpleadoDl
    {
        public ClsRespuestaSP SetEmpleado(ClsTblEmpleadoEl colaborador)
        {
            ClsRespuestaSP respuesta = new ClsRespuestaSP();
            respuesta.Resultado = 0;
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@noColaborador", colaborador.NoColaborador),
                    new SqlParameter("@nombreColaborador", colaborador.NombreColaborador),
                    new SqlParameter("@fechaAlta", colaborador.FechaAlta),
                    new SqlParameter("@fechaBaja", colaborador.FechaBaja),
                    new SqlParameter("@idPuesto", colaborador.IdPuesto),
                    new SqlParameter("@idCentroCostos", colaborador.IdCentroCostos),
                    new SqlParameter("@NoEmpresa", colaborador.NoEmpresa),
                    new SqlParameter("@Empresa", colaborador.Empresa),
                    new SqlParameter("@sueldoNomina", colaborador.SueldoNomina),
                    new SqlParameter("@sueldoHonorarios", colaborador.SueldoHonorarios),
                    new SqlParameter("@activo", colaborador.Activo),
                    new SqlParameter("@idUsuarioCreacion", colaborador.IdUsuarioCreacion),
                    new SqlParameter("@resultado", SqlDbType.Int)
                        {Direction=ParameterDirection.Output }
                };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_SET_Colaborador]",
                    parametros);

                respuesta.Valor = dtResult.IntId.Value;
            }
            catch (Exception e)
            {
                respuesta.Resultado = -1;
                respuesta.Mensaje = e.Message;
            }
            return respuesta;
        }

        public List<ClsTblEmpleadoEl> GetEmpleado(int id, int activo, int puesto)
        {
            var respuesta = new List<ClsTblEmpleadoEl>();
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@idEmp", id),
                    new SqlParameter("@activo", activo),
                    new SqlParameter("@idPuesto", puesto)
                };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_Colaborador]",
                    parametros);

                respuesta = (from a in dtResult.DtResult.AsEnumerable()
                             select new ClsTblEmpleadoEl()
                             {
                                 IdEmp = (int)a["idEmp"],
                                 NoColaborador = a["noColaborador"].ToString(),
                                 NombreColaborador = a["nombreColaborador"].ToString(),
                                 FechaAlta = (DateTime)a["fechaAlta"],
                                 FechaBaja = a["fechaBaja"] == DBNull.Value ? null : (DateTime?)a["fechaBaja"],
                                 IdPuesto = (int)a["idPuesto"],
                                 Puesto = a["puesto"].ToString(),
                                 IdCentroCostos = (int)a["idCentroCostos"],
                                 CentroCostos = a["centroCostos"].ToString(),
                                 NoEmpresa = a["noEmpresa"].ToString(),
                                 Empresa = a["empresa"].ToString(),
                                 SueldoNomina = (decimal)a["sueldoNomina"],
                                 SueldoHonorarios = (decimal)a["sueldoHonorarios"],
                                 SueldoMensual = (decimal)a["sueldoMensual"],
                                 Activo = (bool)a["activo"] == true ? 1 : 0,
                                 Estatus = (bool)a["activo"] == true ? "Activo" : "Baja",
                                 IdUsuarioCreacion = (int)a["idUsuarioCreacion"],
                                 IdUsuarioActualizacion = a["idUsuarioActualizacion"] == DBNull.Value ? null : (int?)a["idUsuarioActualizacion"],
                                 FechaCreacion = (DateTime)a["fechaCreacion"],
                                 FechaActualizacion = a["fechaActualizacion"] == DBNull.Value ? null : (DateTime?)a["fechaActualizacion"]
                             }).ToList();
            }
            catch (Exception e)
            {

            }
            return respuesta;
        }

        public ClsRespuestaSP UpdateEmpleado(ClsTblEmpleadoEl colaborador)
        {
            ClsRespuestaSP respuesta = new ClsRespuestaSP();
            respuesta.Resultado = 0;
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@idEmp", colaborador.IdEmp),
                    new SqlParameter("@noColaborador", colaborador.NoColaborador),
                    new SqlParameter("@nombreColaborador", colaborador.NombreColaborador),
                    new SqlParameter("@fechaAlta", colaborador.FechaAlta),
                    new SqlParameter("@fechaBaja", colaborador.FechaBaja),
                    new SqlParameter("@idPuesto", colaborador.IdPuesto),
                    new SqlParameter("@idCentroCostos", colaborador.IdCentroCostos),
                    new SqlParameter("@NoEmpresa", colaborador.NoEmpresa),
                    new SqlParameter("@Empresa", colaborador.Empresa),
                    new SqlParameter("@sueldoNomina", colaborador.SueldoNomina),
                    new SqlParameter("@sueldoHonorarios", colaborador.SueldoHonorarios),
                    new SqlParameter("@activo", colaborador.Activo),
                    new SqlParameter("@idUsuarioActualizacion", colaborador.IdUsuarioActualizacion),
                    new SqlParameter("@resultado", SqlDbType.Int)
                        {Direction=ParameterDirection.Output }
                };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_UPDATE_Colaborador]",
                    parametros);
            }
            catch (Exception e)
            {
                respuesta.Resultado = -1;
                respuesta.Mensaje = e.Message;
            }
            return respuesta;
        }
    }
}
