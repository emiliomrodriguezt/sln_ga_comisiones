﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace DL_GA_Comisiones
{
    public class ClsItemsCenterCostDl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsItemsCenterCostDl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsAsocRubrosCentroCostosEl> GetItemsCenterCosts(int intId, int intIdRubroPago, int intIdCentroCostos)
        {
            var lstClsAsocRubrosCentroCostosEl = new List<ClsAsocRubrosCentroCostosEl>();
            var objClsCenterCostsDl = new ClsCenterCostsDl(ObjClsUsuarioActiveDirectoryEl);
            var objClsRubroPagoDl = new ClsRubroPagoDl();


            try
            {
                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_ASOC_RUBROS_CENTROCOSTOS]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", intId),
                        new SqlParameter("@p_idRubroPago", intIdRubroPago),
                        new SqlParameter("@p_idCentroCostos", intIdCentroCostos)
                    });

                lstClsAsocRubrosCentroCostosEl = (from a in dtResult.DtResult.AsEnumerable()
                                                  select new ClsAsocRubrosCentroCostosEl()
                                                  {
                                                      IntId = Convert.ToInt32(a["id"]),
                                                      IntIdRubroPago = Convert.ToInt32(a["idRubroPago"]),
                                                      IntIdCentroCostos = Convert.ToInt32(a["idCentroCostos"]),
                                                      IntIdUsuarioCreacion = a["idUsuarioCreacion"] == DBNull.Value ? null : (int?)a["idUsuarioCreacion"],
                                                      DtFechaCreacion = a["fechaCreacion"] == DBNull.Value ? null : (DateTime?)a["fechaCreacion"],
                                                      ObjClsCatCentroCostosEl = objClsCenterCostsDl.GetCenterCosts(Convert.ToInt32(a["idRubroPago"]), "").FirstOrDefault(),
                                                      ObjClsRubroPagoEl = objClsRubroPagoDl.GetRuboPago(Convert.ToInt32(a["idRubroPago"]), 0).FirstOrDefault()
                                                  }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdRubroPago, intIdCentroCostos }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsAsocRubrosCentroCostosEl;
        }

        public ClsSimpleResultEl SetItemsCenterCost(ClsAsocRubrosCentroCostosEl objClsAsocRubrosCentroCostosEl, bool isDelete)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();

            try
            {
                objClsSimpleResultEl = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_SET_ASOC_RUBROS_CENTROCOSTOS]", new List<SqlParameter>()
                {
                    new SqlParameter("@p_id", objClsAsocRubrosCentroCostosEl.IntId),
                    new SqlParameter("@p_idRubroPago", objClsAsocRubrosCentroCostosEl.IntIdRubroPago),
                    new SqlParameter("@p_idCentroCostos", objClsAsocRubrosCentroCostosEl.IntIdCentroCostos),
                    new SqlParameter("@p_idUsuarioCreacion", objClsAsocRubrosCentroCostosEl.IntIdUsuarioCreacion),
                    new SqlParameter("@p_fechaCreacion", objClsAsocRubrosCentroCostosEl.DtFechaCreacion),
                    new SqlParameter("@p_isDelete", isDelete)
                });

                objClsSimpleResultEl.BolSuccess = true;
                objClsSimpleResultEl.StrMessage = objClsSimpleResultEl.IntId == 0 ? StrucMessagesEl.GenericoInsertOk : StrucMessagesEl.GenericoUpdateOk;
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(objClsAsocRubrosCentroCostosEl),
                    DtFecha = DateTime.Now
                });
                objClsSimpleResultEl.BolSuccess = false;
                objClsSimpleResultEl.StrMessage = StrucMessagesEl.GenericoError;
            }

            return objClsSimpleResultEl;
        }
    }
}
