﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace DL_GA_Comisiones
{
    public class ClsAdvancesDl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsAdvancesDl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsTblAnticiposEl> GetAdvances(int intId, int intIdColaborador, int intIdCentroCostos)
        {
            var lstClsTblAnticiposEl = new List<ClsTblAnticiposEl>();
            var objClsCollaboratorDl = new ClsCollaboratorDl();
            var objClsCenterCostsDl = new ClsCenterCostsDl(ObjClsUsuarioActiveDirectoryEl);

            try
            {
                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_Anticipos]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", intId),
                        new SqlParameter("@p_idColaborador", intIdColaborador),
                        new SqlParameter("@p_idCentroCostos", intIdCentroCostos)
                    });

                lstClsTblAnticiposEl = (from a in dtResult.DtResult.AsEnumerable()
                                        select new ClsTblAnticiposEl()
                                        {
                                            IntId = (int)a["id"],
                                            IntIdColaborador = (int)a["idColaborador"],
                                            IntIdCentroCostos = (int)a["idCentroCostos"],
                                            DecImporteBruto = (decimal)a["importeBruto"],
                                            DecImporteNeto = (decimal)a["importeNeto"],
                                            StrNoEmpresa = a["NoEmpresa"].ToString(),
                                            StrNoChequeTransferencia = a["noChequeTransferencia"].ToString(),
                                            DtFechaPago = (DateTime)a["fechaPago"],
                                            StrNoPolizaContable =  a["noPolizaContable"].ToString(),
                                            DecAcomulado = (decimal)a["acomulado"],
                                            BolActivo = (bool)a["activo"],
                                            IntIdUsuarioCreacion = a["idUsuarioCreacion"] == DBNull.Value ? null : (int?)a["idUsuarioCreacion"],
                                            IntIdUsuarioActualizacion = a["idUsuarioActulizacion"] == DBNull.Value ? null : (int?)a["idUsuarioActulizacion"],
                                            DtFechaCreacion = a["fechaCreacion"] == DBNull.Value ? null : (DateTime?)a["fechaCreacion"],
                                            DtFechaActualizacion = a["fechaActulizacion"] == DBNull.Value ? null : (DateTime?)a["fechaActulizacion"],
                                            ObjClsCatCentroCostosEl = objClsCenterCostsDl.GetCenterCosts((int)a["idCentroCostos"], "").FirstOrDefault(),
                                            ObjClsTblColaboradorEl = objClsCollaboratorDl.GetCollaborators((int)a["idColaborador"], "").FirstOrDefault()
                                        }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdColaborador, intIdCentroCostos }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsTblAnticiposEl;
        }

        public ClsSimpleResultEl SetAdvance(ClsTblAnticiposEl objClsTblAnticiposEl, bool isDelete)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();

            try
            {
                objClsSimpleResultEl = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_SET_ANTICIPO]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", objClsTblAnticiposEl.IntId),
                        new SqlParameter("@p_idColaborador", objClsTblAnticiposEl.IntIdColaborador),
                        new SqlParameter("@p_idCentroCostos", objClsTblAnticiposEl.IntIdCentroCostos),
                        new SqlParameter("@p_importeBruto", objClsTblAnticiposEl.DecImporteBruto),
                        new SqlParameter("@p_importeNeto", objClsTblAnticiposEl.DecImporteNeto),
                        new SqlParameter("@p_NoEmpresa", objClsTblAnticiposEl.StrNoEmpresa),
                        new SqlParameter("@p_noChequeTransferencia", objClsTblAnticiposEl.StrNoChequeTransferencia),
                        new SqlParameter("@p_fechaPago", objClsTblAnticiposEl.DtFechaPago),
                        new SqlParameter("@p_noPolizaContable", objClsTblAnticiposEl.StrNoPolizaContable),
                        new SqlParameter("@p_acomulado", objClsTblAnticiposEl.DecAcomulado),
                        new SqlParameter("@p_activo", objClsTblAnticiposEl.BolActivo),
                        new SqlParameter("@p_idUsuarioCreacion", objClsTblAnticiposEl.IntIdUsuarioCreacion),
                        new SqlParameter("@p_idUsuarioActulizacion", objClsTblAnticiposEl.IntIdUsuarioActualizacion),
                        new SqlParameter("@p_fechaCreacion", objClsTblAnticiposEl.DtFechaCreacion),
                        new SqlParameter("@p_fechaActulizacion", objClsTblAnticiposEl.DtFechaActualizacion),
                        new SqlParameter("@p_isDelete", isDelete)
                    });
                objClsSimpleResultEl.BolSuccess = true;
                objClsSimpleResultEl.StrMessage = objClsSimpleResultEl.IntId == 0 ? StrucMessagesEl.GenericoInsertOk : StrucMessagesEl.GenericoUpdateOk;
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(objClsTblAnticiposEl),
                    DtFecha = DateTime.Now
                });
                objClsSimpleResultEl.BolSuccess = false;
                objClsSimpleResultEl.StrMessage = StrucMessagesEl.GenericoError;
            }

            return objClsSimpleResultEl;
        }
    }
}
