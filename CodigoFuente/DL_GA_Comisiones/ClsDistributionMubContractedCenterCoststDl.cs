﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace DL_GA_Comisiones
{
    public class ClsDistributionMubContractedCenterCoststDl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsDistributionMubContractedCenterCoststDl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsAsocCenterCostosColaboradorMubEl> GetMubCenterCostColaborador(int intId, int intIdCentroCostos, int intIdGerente, int intIdDirector)
        {
            var lstClsAsocCentrocostosColaboradorMubEl = new List<ClsAsocCenterCostosColaboradorMubEl>();
            var objClsCenterCostsDl = new ClsCenterCostsDl();
            var objClsCollaboratorDl = new ClsCollaboratorDl();


            try
            {
                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_ASOC_CENTROCOSTOS_COLABRADOR_MUB]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", intId),
                        new SqlParameter("@p_idCentroCostos", intIdCentroCostos),
                        new SqlParameter("@p_idGerente", intIdGerente),
                        new SqlParameter("@p_idDirector", intIdDirector)
                    });

                lstClsAsocCentrocostosColaboradorMubEl = (from a in dtResult.DtResult.AsEnumerable()
                                                          select new ClsAsocCenterCostosColaboradorMubEl()
                                                          {
                                                              IntId = Convert.ToInt32(a["id"]),
                                                              IntIdCentroCostos = Convert.ToInt32(a["idCentroCostos"]),
                                                              IntIdGerente = Convert.ToInt32(a["idGerente"]),
                                                              IntIdDirector = Convert.ToInt32(a["idDirector"]),
                                                              DecPorcentaje = Convert.ToDecimal(a["porcentaje"]),
                                                              IntIdUsuarioCreacion = a["idUsuarioCreacion"] == DBNull.Value ? null : (int?)a["idUsuarioCreacion"],
                                                              DtFechaCreacion = a["fechaCreacion"] == DBNull.Value ? null : (DateTime?)a["fechaCreacion"],
                                                              ObjClsCatCentroCostosEl = objClsCenterCostsDl.GetCenterCosts(Convert.ToInt32(a["idCentroCostos"]), "").FirstOrDefault(),
                                                              ObjGerente = objClsCollaboratorDl.GetCollaborators(Convert.ToInt32(a["idGerente"]), "").FirstOrDefault(),
                                                              ObjDirector = objClsCollaboratorDl.GetCollaborators(Convert.ToInt32(a["idDirector"]), "").FirstOrDefault(),
                                                          }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdCentroCostos, intIdGerente, intIdDirector }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsAsocCentrocostosColaboradorMubEl;
        }

        public ClsSimpleResultEl SetMubCenterCostColaborador(ClsAsocCenterCostosColaboradorMubEl objClsAsocCentrocostosColaboradorMubEl, bool isDelete)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();

            try
            {
                objClsSimpleResultEl = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_SET_ASOC_CENTROCOSTOS_COLABRADOR_MUB]", new List<SqlParameter>()
                {
                    new SqlParameter("@p_id", objClsAsocCentrocostosColaboradorMubEl.IntId),
                    new SqlParameter("@p_idCentroCostos", objClsAsocCentrocostosColaboradorMubEl.IntIdCentroCostos),
                    new SqlParameter("@p_idGerente", objClsAsocCentrocostosColaboradorMubEl.IntIdGerente),
                    new SqlParameter("@p_idDirector", objClsAsocCentrocostosColaboradorMubEl.IntIdDirector),
                    new SqlParameter("@p_porcentaje", objClsAsocCentrocostosColaboradorMubEl.DecPorcentaje),
                    new SqlParameter("@p_idUsuarioCreacion", objClsAsocCentrocostosColaboradorMubEl.IntIdUsuarioCreacion),
                    new SqlParameter("@p_fechaCreacion", objClsAsocCentrocostosColaboradorMubEl.DtFechaCreacion),
                    new SqlParameter("@p_isDelete", isDelete)
                });

                objClsSimpleResultEl.BolSuccess = true;
                objClsSimpleResultEl.StrMessage = objClsSimpleResultEl.IntId == 0 ? StrucMessagesEl.GenericoInsertOk : StrucMessagesEl.GenericoUpdateOk;
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(objClsAsocCentrocostosColaboradorMubEl),
                    DtFecha = DateTime.Now
                });
                objClsSimpleResultEl.BolSuccess = false;
                objClsSimpleResultEl.StrMessage = StrucMessagesEl.GenericoError;
            }

            return objClsSimpleResultEl;
        }
    }
}
