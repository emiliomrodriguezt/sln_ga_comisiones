﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace DL_GA_Comisiones
{
    public class ClsCommissionCenterCostDl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsCommissionCenterCostDl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsTblCentroCostosComisonEl> GetCommissionCenterCost(int intId, int intIdComision, int intIdCentroCostos)
        {
            var lstClsTblCentroCostosComisonEl = new List<ClsTblCentroCostosComisonEl>();
            var objClsCommissionCalculationBl = new ClsCommissionCalculationDl(ObjClsUsuarioActiveDirectoryEl);
            var objClsCenterCostsDl = new ClsCenterCostsDl(ObjClsUsuarioActiveDirectoryEl);

            try
            {
                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_TBL_CENTROCOSTOS_COMISION]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", intId),
                        new SqlParameter("@p_idComision", intIdComision),
                        new SqlParameter("@p_idCentroCostos", intIdCentroCostos)
                    });

                lstClsTblCentroCostosComisonEl = (from a in dtResult.DtResult.AsEnumerable()
                                                  select new ClsTblCentroCostosComisonEl()
                                                  {
                                                      IntId = (int)a["id"],
                                                      IntIdComision = (int)a["idComision"],
                                                      IntIdCentroCostos = (int)a["idCentroCostos"],
                                                      DecMubComision = (decimal)a["MUBComision"],
                                                      DecPorcentaje = (decimal)a["porcentaje"],
                                                      DecMontoPagado = (decimal)a["montoPagado"],
                                                      DecComisionTotal = (decimal)a["comisionTotal"],
                                                      StrObservaciones = a["observaciones"].ToString(),
                                                      ObjClsCatCentroCostosEl = objClsCenterCostsDl.GetCenterCosts((int)a["idCentroCostos"], "").FirstOrDefault(),
                                                      ObjClsTblCalculoComisionEl = objClsCommissionCalculationBl.GetCommissionCalculation((int)a["idComision"], 0, 0).FirstOrDefault()
                                                  }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdComision, intIdCentroCostos }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsTblCentroCostosComisonEl;
        }

        public ClsSimpleResultEl UpdateObservaciones(int intId, string strObservaciones)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();

            try
            {
                objClsSimpleResultEl = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_UPDATE_TBL_CENTROCOSTOS_COMISION]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", intId),
                        new SqlParameter("@p_observaciones", strObservaciones),
                    });

                objClsSimpleResultEl.StrMessage = objClsSimpleResultEl.IntId == 0 ? StrucMessagesEl.GenericoInsertOk : StrucMessagesEl.GenericoUpdateOk;
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, strObservaciones }),
                    DtFecha = DateTime.Now
                });
                objClsSimpleResultEl.StrMessage = StrucMessagesEl.GenericoError;
            }

            return objClsSimpleResultEl;
        }

    }
}
