﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;
using EL_GA_Comisiones;

namespace DL_GA_Comisiones
{
    public class ClsCatalogosDl
    {
        public List<ClsItemCatalogoEl> GetPuestos()
        {
            var respuesta = new List<ClsItemCatalogoEl>();
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_Puestos]",
                    parametros);

                respuesta = (from a in dtResult.DtResult.AsEnumerable()
                             select new ClsItemCatalogoEl()
                             {
                                 Id = (int)a["idPuesto"],
                                 Descripcion = a["puesto"].ToString()
                             }).ToList();
            }
            catch (Exception e)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 1,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = e.Message,
                    StrPilaEventos = e.StackTrace,
                    StrMensajeInterno = e.InnerException?.Message ?? "",
                    StrPilaEventosInterno = e.InnerException?.StackTrace ?? "",
                    StrEntrada = string.Empty,
                    DtFecha = DateTime.Now
                });
            }
            return respuesta;
        }

        public List<ClsItemCatalogoEl> GetCentrosCosto(int idCC, int activo, int idGerente)
        {
            var respuesta = new List<ClsItemCatalogoEl>();
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                    {
                        new SqlParameter("@idCC", idCC),
                        new SqlParameter("@activo", activo),
                        new SqlParameter("@idGerente", idGerente)
                    };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_CentroCostos]",
                    parametros);

                respuesta = (from a in dtResult.DtResult.AsEnumerable()
                             where a["clasificacionAlterna"].ToString() == "PROYECTOS" || a["clasificacionAlterna"].ToString() == "OPORTUNIDADES"
                             select new ClsItemCatalogoEl()
                             {
                                 Id = (int)a["id"],
                                 Descripcion = a["nombre"].ToString()
                             }).ToList();
            }
            catch (Exception e)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 1,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = e.Message,
                    StrPilaEventos = e.StackTrace,
                    StrMensajeInterno = e.InnerException?.Message ?? "",
                    StrPilaEventosInterno = e.InnerException?.StackTrace ?? "",
                    StrEntrada = string.Empty,
                    DtFecha = DateTime.Now
                });
            }
            return respuesta;
        }

        public List<ClsItemCatalogoEl> GetUNs()
        {
            var respuesta = new List<ClsItemCatalogoEl>();
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                    {
                        new SqlParameter("@idCC", 0),
                        new SqlParameter("@activo", 1),
                        new SqlParameter("@idGerente", 0)
                    };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_CentroCostos]",
                    parametros);

                respuesta = (from a in dtResult.DtResult.AsEnumerable()
                             where a["clasificacionAlterna"].ToString() == "UN´S"
                             select new ClsItemCatalogoEl()
                             {
                                 Id = (int)a["id"],
                                 Descripcion = a["nombre"].ToString()
                             }).ToList();
            }
            catch (Exception e)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 1,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = e.Message,
                    StrPilaEventos = e.StackTrace,
                    StrMensajeInterno = e.InnerException?.Message ?? "",
                    StrPilaEventosInterno = e.InnerException?.StackTrace ?? "",
                    StrEntrada = string.Empty,
                    DtFecha = DateTime.Now
                });
            }
            return respuesta;
        }

        public List<ClsItemCatalogoEl> GetCentrosCostoHomologados(int idCC, int activo, int idGerente)
        {
            var respuesta = new List<ClsItemCatalogoEl>();
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                    {
                        new SqlParameter("@idCC", idCC),
                        new SqlParameter("@activo", activo),
                        new SqlParameter("@idGerente", idGerente)
                    };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_CentroCostosHomologados]",
                    parametros);

                respuesta = (from a in dtResult.DtResult.AsEnumerable()
                             select new ClsItemCatalogoEl()
                             {
                                 Id = (int)a["id"],
                                 Descripcion = a["nombre"].ToString()
                             }).ToList();
            }
            catch (Exception e)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 1,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = e.Message,
                    StrPilaEventos = e.StackTrace,
                    StrMensajeInterno = e.InnerException?.Message ?? "",
                    StrPilaEventosInterno = e.InnerException?.StackTrace ?? "",
                    StrEntrada = string.Empty,
                    DtFecha = DateTime.Now
                });
            }
            return respuesta;
        }
    }
}
