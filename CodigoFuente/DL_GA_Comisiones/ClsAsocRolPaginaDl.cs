﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Reflection;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace DL_GA_Comisiones
{
    public class ClsAsocRolPaginaDl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsAsocRolPaginaDl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public ClsSimpleResultEl SetRol(ClsAsocRolPaginaEl objClsAsocRolPaginaEl, bool isDelete)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();

            try
            {
                objClsSimpleResultEl = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_SET_ROLPAGINA]", new List<SqlParameter>()
                {
                    new SqlParameter("@p_id", objClsAsocRolPaginaEl.IntId),
                    new SqlParameter("@p_idRol", objClsAsocRolPaginaEl.IntIdRol),
                    new SqlParameter("@p_idPagina", objClsAsocRolPaginaEl.IntIdPagina),
                    new SqlParameter("@p_editar", objClsAsocRolPaginaEl.BolEditar),
                    new SqlParameter("@p_idUsuarioCreacion", objClsAsocRolPaginaEl.IntIdUsuarioCreacion),
                    new SqlParameter("@p_fechaCreacion", objClsAsocRolPaginaEl.DtFechaCreacion),
                    new SqlParameter("@p_isDelete", isDelete)
                });

                objClsSimpleResultEl.BolSuccess = true;
                objClsSimpleResultEl.StrMessage = objClsSimpleResultEl.IntId == 0 ? StrucMessagesEl.GenericoInsertOk : StrucMessagesEl.GenericoUpdateOk;
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(objClsAsocRolPaginaEl),
                    DtFecha = DateTime.Now
                });
                objClsSimpleResultEl.BolSuccess = false;
                objClsSimpleResultEl.StrMessage = StrucMessagesEl.GenericoError;
            }

            return objClsSimpleResultEl;
        }
    }
}
