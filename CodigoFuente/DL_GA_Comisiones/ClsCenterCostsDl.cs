﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using EL_GA_Comisiones;
using Newtonsoft.Json;
using Utility_GA_Comisiones;

namespace DL_GA_Comisiones
{
    public class ClsCenterCostsDl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsCenterCostsDl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsCatCentroCostosEl> GetCenterCosts(int intId, string strNombre)
        {
            var lstClsCatCentroCostosEl = new List<ClsCatCentroCostosEl>();

            try
            {
                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_CENTROCOSTO]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", intId),
                        new SqlParameter("@p_nombre", strNombre)
                    });

                lstClsCatCentroCostosEl = (from a in dtResult.DtResult.AsEnumerable()
                                           select new ClsCatCentroCostosEl()
                                           {
                                               IntId = (int)a["id"],
                                               StrId = a["idStr"] == DBNull.Value ? null : a["idStr"].ToString(),
                                               IntIdDirector = a["idDirector"] == DBNull.Value ? null : (int?)a["idDirector"],
                                               IntIdGerente = a["idGerente"] == DBNull.Value ? null : (int?)a["idGerente"],
                                               StrNoEmpresa = a["NoEmpresa"] == DBNull.Value ? null : a["NoEmpresa"].ToString(),
                                               StrEmpresa = a["empresa"] == DBNull.Value ? null : a["empresa"].ToString(),
                                               DtFechaInicio = a["fechaInicio"] == DBNull.Value ? null : (DateTime?)a["fechaInicio"],
                                               StrEstatus = a["Estatus"] == DBNull.Value ? null : a["Estatus"].ToString(),
                                               StrNombre = a["nombre"] == DBNull.Value ? null : a["nombre"].ToString(),
                                               DtFechaCierreAdmin = a["fechaCierreAdministrativo"] == DBNull.Value ? null : (DateTime?)a["fechaCierreAdministrativo"],
                                               StrUnidadNegocio = a["unidadNegocio"] == DBNull.Value ? null : a["unidadNegocio"].ToString(),
                                               StrClasificacion = a["clasificacion"] == DBNull.Value ? null : a["clasificacion"].ToString(),
                                               StrArea = a["area"] == DBNull.Value ? null : a["area"].ToString(),
                                               StrSubArea = a["subArea"] == DBNull.Value ? null : a["subArea"].ToString(),
                                               StrZIdHomologado = a["zIdHomologado"] == DBNull.Value ? null : a["zIdHomologado"].ToString(),
                                               StrZNombreHomologado = a["zNombreHomlogado"] == DBNull.Value ? null : a["zNombreHomlogado"].ToString(),
                                               StrAreaAlterna = a["areaAlterna"] == DBNull.Value ? null : a["areaAlterna"].ToString(),
                                               StrSubAreaAlterna = a["subAreaAlterna"] == DBNull.Value ? null : a["subAreaAlterna"].ToString(),
                                               StrClasificacionAlterna = a["clasificacionAlterna"] == DBNull.Value ? null : a["clasificacionAlterna"].ToString(),
                                               StrZSource = a["zSource"] == DBNull.Value ? null : a["zSource"].ToString(),
                                               BolZActive = a["zActive"] == DBNull.Value ? null : (bool?)a["zActive"],
                                               StrZComentario = a["zComentario"] == DBNull.Value ? null : a["zComentario"].ToString(),
                                               StrZFuenteHomologada = a["zFuenteHomologada"] == DBNull.Value ? null : a["zFuenteHomologada"].ToString(),
                                               DtFechaPayBack = a["fechaPayback"] == DBNull.Value ? null : (DateTime?)a["fechaPayback"],
                                               IntDuracionMeses = a["duracionMeses"] == DBNull.Value ? null : (int?)a["duracionMeses"],
                                               DecIngresosContrato = a["IngresosContrato"] == DBNull.Value ? null : (decimal?)a["IngresosContrato"],
                                               DecEgresoContrato = a["EgresoContrato"] == DBNull.Value ? null : (decimal?)a["EgresoContrato"],
                                               DecMubContrato = a["MUBContrato"] == DBNull.Value ? null : (decimal?)a["MUBContrato"],
                                               StrObservaciones = a["Obsevacion"] == DBNull.Value ? null : a["Obsevacion"].ToString(),
                                               IntAnoCreacion = a["anoCreacion"] == DBNull.Value ? null : (int?)a["anoCreacion"],
                                               BolActivo = (bool)a["activo"],
                                               IntIdUsuarioCreacion = a["idUsuarioCreacion"] == DBNull.Value ? null : (int?)a["idUsuarioCreacion"],
                                               IntIdUsuarioActualizacion = a["idUsuarioActualizacion"] == DBNull.Value ? null : (int?)a["idUsuarioActualizacion"],
                                               DtFechaCreacion = a["fechaCreacion"] == DBNull.Value ? null : (DateTime?)a["fechaCreacion"],
                                               DtFechaActualizacion = a["fechaActualizacion"] == DBNull.Value ? null : (DateTime?)a["fechaActualizacion"]
                                           }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, strNombre }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsCatCentroCostosEl;
        }

        public ClsSimpleResultEl ComplementCenterCost(ClsCatCentroCostosEl objClsCatCentroCostosEl)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();

            try
            {
                objClsSimpleResultEl = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_COMPLEMENT_CENTROCOSTOS]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", objClsCatCentroCostosEl.IntId),
                        new SqlParameter("@p_idDirector", objClsCatCentroCostosEl.IntIdDirector),
                        new SqlParameter("@p_idGerente", objClsCatCentroCostosEl.IntIdGerente),
                        new SqlParameter("@p_fechaPayback", objClsCatCentroCostosEl.DtFechaPayBack),
                        new SqlParameter("@p_IngresosContrato", objClsCatCentroCostosEl.DecIngresosContrato),
                        new SqlParameter("@p_EgresoContrato", objClsCatCentroCostosEl.DecEgresoContrato),
                        new SqlParameter("@p_MUBContrato", objClsCatCentroCostosEl.DecMubContrato),
                        new SqlParameter("@p_Obsevacion", objClsCatCentroCostosEl.StrObservaciones),
                        new SqlParameter("@p_anoCreacion", objClsCatCentroCostosEl.IntAnoCreacion),
                        new SqlParameter("@p_activo", objClsCatCentroCostosEl.BolActivo),
                        new SqlParameter("@p_idUsuarioActualizacion", objClsCatCentroCostosEl.IntIdUsuarioActualizacion),
                        new SqlParameter("@p_fechaActualizacion", objClsCatCentroCostosEl.DtFechaActualizacion)
                    });
                objClsSimpleResultEl.StrMessage = objClsSimpleResultEl.IntId == 0 ? StrucMessagesEl.GenericoInsertOk : StrucMessagesEl.GenericoUpdateOk;
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(objClsCatCentroCostosEl),
                    DtFecha = DateTime.Now
                });
                objClsSimpleResultEl.StrMessage = StrucMessagesEl.GenericoError;
            }

            return objClsSimpleResultEl;
        }

        public List<ClsCentercostInsertEl> GetCenterCostsInternal()
        {
            var lstClsCentercostInsertEl = new List<ClsCentercostInsertEl>();

            try
            {
                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_CENTROCOSTO]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", 0),
                        new SqlParameter("@p_nombre", "")
                    });

                lstClsCentercostInsertEl = (from a in dtResult.DtResult.AsEnumerable()
                                            select new ClsCentercostInsertEl()
                                            {
                                                IntId = (int)a["id"],
                                                BolActivo = (bool)a["activo"],
                                                IntIdUsuarioCreacion = a["idUsuarioCreacion"] == DBNull.Value ? null : (int?)a["idUsuarioCreacion"],
                                                IntIdUsuarioActualizacion = a["idUsuarioActualizacion"] == DBNull.Value ? null : (int?)a["idUsuarioActualizacion"],
                                                DtFechaCreacion = a["fechaCreacion"] == DBNull.Value ? null : (DateTime?)a["fechaCreacion"],
                                                DtFechaActualizacion = a["fechaActualizacion"] == DBNull.Value ? null : (DateTime?)a["fechaActualizacion"],
                                                ObjClsCenterCostOrgEl = new ClsCenterCostOrgEl()
                                                { 
                                                    StrId = a["idStr"] == DBNull.Value ? null : a["idStr"].ToString(),
                                                    StrEmpresa = a["empresa"] == DBNull.Value ? null : a["empresa"].ToString(),
                                                    DtFechaInicio = a["fechaInicio"] == DBNull.Value ? null : (DateTime?)a["fechaInicio"],
                                                    StrEstatus = a["Estatus"] == DBNull.Value ? null : a["Estatus"].ToString(),
                                                    StrNombre = a["nombre"] == DBNull.Value ? null : a["nombre"].ToString(),
                                                    DtFechaCierreAdmin = a["fechaCierreAdministrativo"] == DBNull.Value ? null : (DateTime?)a["fechaCierreAdministrativo"],
                                                    StrUnidadNegocio = a["unidadNegocio"] == DBNull.Value ? null : a["unidadNegocio"].ToString(),
                                                    StrClasificacion = a["clasificacion"] == DBNull.Value ? null : a["clasificacion"].ToString(),
                                                    StrArea = a["area"] == DBNull.Value ? null : a["area"].ToString(),
                                                    StrSubArea = a["subArea"] == DBNull.Value ? null : a["subArea"].ToString(),
                                                    StrZIdHomologado = a["zIdHomologado"] == DBNull.Value ? null : a["zIdHomologado"].ToString(),
                                                    StrZNombreHomologado = a["zNombreHomlogado"] == DBNull.Value ? null : a["zNombreHomlogado"].ToString(),
                                                    StrAreaAlterna = a["areaAlterna"] == DBNull.Value ? null : a["areaAlterna"].ToString(),
                                                    StrSubAreaAlterna = a["subAreaAlterna"] == DBNull.Value ? null : a["subAreaAlterna"].ToString(),
                                                    StrClasificacionAlterna = a["clasificacionAlterna"] == DBNull.Value ? null : a["clasificacionAlterna"].ToString(),
                                                    StrZSource = a["zSource"] == DBNull.Value ? null : a["zSource"].ToString(),
                                                    BolZActive = a["zActive"] == DBNull.Value ? null : (bool?)a["zActive"],
                                                    StrZComentario = a["zComentario"] == DBNull.Value ? null : a["zComentario"].ToString(),
                                                    StrZFuenteHomologada = a["zFuenteHomologada"] == DBNull.Value ? null : a["zFuenteHomologada"].ToString()
                                                }
                                            }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = "",
                    DtFecha = DateTime.Now
                });
            }

            return lstClsCentercostInsertEl;
        }

        public List<ClsCenterCostOrgEl> GetCenterCostsExternal()
        {
            var lstClsCenterCostOrgEl = new List<ClsCenterCostOrgEl>();

            try
            {
                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER16", StrucQuerysEl.QryGetCostCenter, new List<SqlParameter>(), false);

                lstClsCenterCostOrgEl = (from a in dtResult.DtResult.AsEnumerable()
                                         select new ClsCenterCostOrgEl()
                                         {
                                             StrId = a["ID"] == DBNull.Value ? null : a["ID"].ToString(),
                                             StrNombre = a["Centro_De_Costo"] == DBNull.Value ? null : a["Centro_De_Costo"].ToString(),
                                             DtFechaInicio = ClsConvertFechaBl.Fecha(a["Fecha_Inicio"]),
                                             DtFechaCierreAdmin = ClsConvertFechaBl.Fecha(a["Fecha_Cierre_Administrativo"]),
                                             StrEmpresa = a["Empresa_Ganadora"] == DBNull.Value ? null : a["Empresa_Ganadora"].ToString(),                                           
                                             StrEstatus = a["Estatus"] == DBNull.Value ? null : a["Estatus"].ToString(),
                                             StrUnidadNegocio = a["U_Negocio"] == DBNull.Value ? null : a["U_Negocio"].ToString(),
                                             StrClasificacion = a["Clasificacion"] == DBNull.Value ? null : a["Clasificacion"].ToString(),
                                             StrArea = a["Area"] == DBNull.Value ? null : a["Area"].ToString(),
                                             StrSubArea = a["Sub_Area"] == DBNull.Value ? null : a["Sub_Area"].ToString(),
                                             StrClasificacionAlterna = a["clasificacion_alterna_planeacion"] == DBNull.Value ? null : a["clasificacion_alterna_planeacion"].ToString(),
                                             StrAreaAlterna = a["area_alterna_planeacion"] == DBNull.Value ? null : a["area_alterna_planeacion"].ToString(),
                                             StrSubAreaAlterna = a["subarea_alterna_planeacion"] == DBNull.Value ? null : a["subarea_alterna_planeacion"].ToString(),
                                             StrZSource = a["zSource"] == DBNull.Value ? null : a["zSource"].ToString(),
                                             BolZActive = a["zActive"] == DBNull.Value ? null : (bool?)a["zActive"],
                                             StrZComentario = a["zComentario"] == DBNull.Value ? null : a["zComentario"].ToString(),
                                             StrZIdHomologado = a["zID_Homologado"] == DBNull.Value ? null : a["zID_Homologado"].ToString(),
                                             StrZNombreHomologado = a["zCentro_de_Costo_Homologado"] == DBNull.Value ? null : a["zCentro_de_Costo_Homologado"].ToString(),
                                             StrZFuenteHomologada = a["zFuente_Homologada"] == DBNull.Value ? null : a["zFuente_Homologada"].ToString(),
                                         }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = "",
                    DtFecha = DateTime.Now
                });
            }

            return lstClsCenterCostOrgEl;
        }

        public ClsSimpleResultEl SetCenterCost(ClsCentercostInsertEl objClsCentercostInsertEl)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();

            try
            {
                objClsSimpleResultEl = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_SET_CENTROCOSTOS]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", objClsCentercostInsertEl.IntId),
                        new SqlParameter("@p_strId", objClsCentercostInsertEl.ObjClsCenterCostOrgEl.StrId),
                        new SqlParameter("@p_empresa", objClsCentercostInsertEl.ObjClsCenterCostOrgEl.StrEmpresa),
                        new SqlParameter("@p_fechaInicio", objClsCentercostInsertEl.ObjClsCenterCostOrgEl.DtFechaInicio),
                        new SqlParameter("@p_Estatus", objClsCentercostInsertEl.ObjClsCenterCostOrgEl.StrEstatus),
                        new SqlParameter("@p_nombre", objClsCentercostInsertEl.ObjClsCenterCostOrgEl.StrNombre),
                        new SqlParameter("@p_fechaCierreAdministrativo", objClsCentercostInsertEl.ObjClsCenterCostOrgEl.DtFechaCierreAdmin),
                        new SqlParameter("@p_unidadNegocio", objClsCentercostInsertEl.ObjClsCenterCostOrgEl.StrUnidadNegocio),
                        new SqlParameter("@p_clasificacion", objClsCentercostInsertEl.ObjClsCenterCostOrgEl.StrClasificacion),
                        new SqlParameter("@p_area", objClsCentercostInsertEl.ObjClsCenterCostOrgEl.StrArea),
                        new SqlParameter("@p_subArea", objClsCentercostInsertEl.ObjClsCenterCostOrgEl.StrSubArea),
                        new SqlParameter("@p_zIdHomologado", objClsCentercostInsertEl.ObjClsCenterCostOrgEl.StrZIdHomologado),
                        new SqlParameter("@p_zNombreHomlogado", objClsCentercostInsertEl.ObjClsCenterCostOrgEl.StrZNombreHomologado),
                        new SqlParameter("@p_areaAlterna", objClsCentercostInsertEl.ObjClsCenterCostOrgEl.StrAreaAlterna),
                        new SqlParameter("@p_subAreaAlterna", objClsCentercostInsertEl.ObjClsCenterCostOrgEl.StrSubAreaAlterna),
                        new SqlParameter("@p_clasificacionAlterna", objClsCentercostInsertEl.ObjClsCenterCostOrgEl.StrClasificacionAlterna),
                        new SqlParameter("@p_zSource", objClsCentercostInsertEl.ObjClsCenterCostOrgEl.StrZSource),
                        new SqlParameter("@p_zActive", objClsCentercostInsertEl.ObjClsCenterCostOrgEl.BolZActive),
                        new SqlParameter("@p_zComentario", objClsCentercostInsertEl.ObjClsCenterCostOrgEl.StrZComentario),
                        new SqlParameter("@p_zFuenteHomologada", objClsCentercostInsertEl.ObjClsCenterCostOrgEl.StrZFuenteHomologada),
                        new SqlParameter("@p_activo", objClsCentercostInsertEl.BolActivo),
                        new SqlParameter("@p_idUsuarioCreacion", objClsCentercostInsertEl.IntIdUsuarioCreacion),
                        new SqlParameter("@p_idUsuarioActualizacion", objClsCentercostInsertEl.IntIdUsuarioActualizacion),
                        new SqlParameter("@p_fechaCreacion", objClsCentercostInsertEl.DtFechaCreacion),
                        new SqlParameter("@p_fechaActualizacion", objClsCentercostInsertEl.DtFechaActualizacion)
                    });
                objClsSimpleResultEl.StrMessage = objClsSimpleResultEl.IntId == 0 ? StrucMessagesEl.GenericoInsertOk : StrucMessagesEl.GenericoUpdateOk;
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(objClsCentercostInsertEl),
                    DtFecha = DateTime.Now
                });
                objClsSimpleResultEl.StrMessage = StrucMessagesEl.GenericoError;
            }

            return objClsSimpleResultEl;
        }
    }
}
