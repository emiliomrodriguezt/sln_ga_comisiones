﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using EL_GA_Comisiones;

namespace DL_GA_Comisiones
{
    public static class ClsSqlExecuteDl
    {
        public static ClsSimpleResultEl ExecuteSentence(string strNameConfig, string strSqlQuery, IEnumerable<SqlParameter> lstSqlParameter, bool bolSp = true)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();

            try
            {
                using (var objSqlConnection = new SqlConnection(CreateConectionString(strNameConfig)))
                {
                    using (var objSqlCommand = CreateSqlCommand(lstSqlParameter, strSqlQuery, bolSp))
                    {
                        objSqlConnection.Open();
                        objSqlCommand.Connection = objSqlConnection;
                        objClsSimpleResultEl.DtResult.Load(objSqlCommand.ExecuteReader());
                        objClsSimpleResultEl.BolSuccess = true;

                        if (objClsSimpleResultEl.DtResult.Rows.Count == 0)
                        {
                            if (!string.IsNullOrEmpty(objSqlCommand.Parameters["p_message"]?.Value.ToString().Trim()))
                            {
                                throw new Exception($"Error al ejecutar consulta: {objSqlCommand.Parameters["p_message"].Value.ToString().Trim()}");
                            }

                            if (!string.IsNullOrEmpty(objSqlCommand.Parameters["p_identity"]?.Value.ToString().Trim()))
                            {
                                objClsSimpleResultEl.IntId = Convert.ToInt32(objSqlCommand.Parameters["p_identity"].Value);
                            }
                        }
                        objSqlConnection.Close();
                        objSqlCommand.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                objClsSimpleResultEl.BolSuccess = false;
                throw new Exception($"Error al ejecutar consulta: {ex.Message}");
            }

            return objClsSimpleResultEl;
        }

        private static string CreateConectionString(string strNameConfig)
        {
            var objClsDataConectionEl = ClsGetConfigDl.GetDatosConeccion(strNameConfig);
            var strResult = new StringBuilder();

            try
            {
                strResult.Append($"Data Source={objClsDataConectionEl.StrHostName}");
                strResult.Append(string.IsNullOrEmpty(objClsDataConectionEl.StrInstance) ? "" : $"\\{objClsDataConectionEl.StrInstance}");
                strResult.Append(string.IsNullOrEmpty(objClsDataConectionEl.StrPort) ? ";" : $",{objClsDataConectionEl.StrPort};");
                strResult.Append($"Initial Catalog={objClsDataConectionEl.StrDataBaseName};");
                strResult.Append("Persist Security Info=True;");
                strResult.Append($"User ID={objClsDataConectionEl.StrUser};");
                strResult.Append($"Password={objClsDataConectionEl.StrPassword}");
            }
            catch (Exception ex)
            {
                throw new Exception($"Error al crear cadena de conexion: [{ex.Message}]");
            }

            return strResult.ToString();
        }
        
        private static SqlCommand CreateSqlCommand(IEnumerable<SqlParameter> lstSqlParameter, string strSqlQuery, bool bolSp = true)
        {
            try
            {
                var scResutl = new SqlCommand
                {
                    CommandText = strSqlQuery,
                    CommandType = bolSp ? CommandType.StoredProcedure : CommandType.Text,
                    CommandTimeout = 0
                };

                scResutl.Parameters.Add(new SqlParameter("p_identity", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                });

                scResutl.Parameters.Add(new SqlParameter("p_message", SqlDbType.VarChar, 500)
                {
                    Direction = ParameterDirection.Output
                });

                foreach (var itm in lstSqlParameter)
                {
                    scResutl.Parameters.Add(itm);
                }

                return scResutl;
            }
            catch (Exception ex)
            {
                throw new Exception($"Error al generar SqlCommand para sp [{ex.Message}]");
            }
        }
    }
}