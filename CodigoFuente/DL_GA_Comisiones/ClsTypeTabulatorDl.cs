﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace DL_GA_Comisiones
{
    public class ClsTypeTabulatorDl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsTypeTabulatorDl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsCatTipoTabuladorEl> GetTypeTabulator(int intId, string strNombre)
        {
            var lstClsCatTipoTabuladorEl = new List<ClsCatTipoTabuladorEl>();

            try
            {
                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_CAT_TIPOTABULADOR]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", intId),
                        new SqlParameter("@p_nombre", strNombre)
                    });

                lstClsCatTipoTabuladorEl = (from a in dtResult.DtResult.AsEnumerable()
                                              select new ClsCatTipoTabuladorEl()
                                              {
                                                  IntId = (int)a["id"],
                                                  StrNombre = a["nombre"].ToString(),
                                                  BolActivo = (bool)a["activo"],
                                                  IntIdUsuarioCreacion = a["idUsuarioCreacion"] == DBNull.Value ? null : (int?)a["idUsuarioCreacion"],
                                                  IntIdUsuarioActulizacion = a["idUsuarioActulizacion"] == DBNull.Value ? null : (int?)a["idUsuarioActulizacion"],
                                                  DtFechaCreacion = a["fechaCreacion"] == DBNull.Value ? null : (DateTime?)a["fechaCreacion"],
                                                  DtFechaActualizacion = a["fechaActulizacion"] == DBNull.Value ? null : (DateTime?)a["fechaActulizacion"]
                                              }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, strNombre }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsCatTipoTabuladorEl;
        }

        public ClsSimpleResultEl SetTypeTabulator(ClsCatTipoTabuladorEl objClsCatTipoTabuladorEl, bool isDelete)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();

            try
            {
                objClsSimpleResultEl = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_SET_CAT_TIPOTABULADOR]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", objClsCatTipoTabuladorEl.IntId),
                        new SqlParameter("@p_nombre", objClsCatTipoTabuladorEl.IntId),
                        new SqlParameter("@p_activo", objClsCatTipoTabuladorEl.BolActivo),
                        new SqlParameter("@p_idUsuarioCreacion", objClsCatTipoTabuladorEl.IntIdUsuarioCreacion),
                        new SqlParameter("@p_idUsuarioActulizacion", objClsCatTipoTabuladorEl.IntIdUsuarioActulizacion),
                        new SqlParameter("@p_fechaCreacion", objClsCatTipoTabuladorEl.DtFechaCreacion),
                        new SqlParameter("@p_fechaActulizacion", objClsCatTipoTabuladorEl.DtFechaActualizacion),
                        new SqlParameter("p_isDelete", isDelete)
                    });
                objClsSimpleResultEl.BolSuccess = true;
                objClsSimpleResultEl.StrMessage = objClsSimpleResultEl.IntId == 0 ? StrucMessagesEl.GenericoInsertOk : StrucMessagesEl.GenericoUpdateOk;
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(objClsCatTipoTabuladorEl),
                    DtFecha = DateTime.Now
                });
                objClsSimpleResultEl.BolSuccess = false;
                objClsSimpleResultEl.StrMessage = StrucMessagesEl.GenericoError;
            }

            return objClsSimpleResultEl;
        }
    }
}
