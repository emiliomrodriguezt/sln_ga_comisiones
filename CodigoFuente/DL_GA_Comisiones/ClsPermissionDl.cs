﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace DL_GA_Comisiones
{
    public class ClsPermissionDl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsPermissionDl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsPermissionEl> GetPermits(int intIdRol)
        {
            var lstClsPermisosEl = new List<ClsPermissionEl>();

            try
            {
                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_Permisos]", new List<SqlParameter>()
                {
                    new SqlParameter("@p_idRol", intIdRol)
                });

                lstClsPermisosEl = (from a in dtResult.DtResult.AsEnumerable()
                                    select new ClsPermissionEl()
                                    {
                                        IntIdPemriso = (int)a["idPemiso"],
                                        IntIdPagina = (int)a["id"],
                                        StrNombrePagina = a["nombre"].ToString(),
                                        StrDescripcionPage = a["descripcion"].ToString(),
                                        BolEdit = (int)a["editar"] == 1
                                    }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intIdRol }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsPermisosEl;
        }
    }
}
