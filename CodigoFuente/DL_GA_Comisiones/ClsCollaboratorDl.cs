﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace DL_GA_Comisiones
{
    public class ClsCollaboratorDl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsCollaboratorDl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsTblColaboradorEl> GetCollaborators(int intId, string strNombre)
        {
            var lstClsTblColaboradorEl = new List<ClsTblColaboradorEl>();

            try
            {
                var objClsCenterCostsDl = new ClsCenterCostsDl(ObjClsUsuarioActiveDirectoryEl);
                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_Colaborador]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@idEmp", intId),
                        new SqlParameter("@activo", false),
                        new SqlParameter("@idPuesto", strNombre)
                    });

                lstClsTblColaboradorEl = (from a in dtResult.DtResult.AsEnumerable()
                                         select new ClsTblColaboradorEl()
                                         {
                                             IntId = (int)a["idEmp"],
                                             IntIdPuesto = (int)a["idPuesto"],
                                             IntIdCentroCostos = (int)a["idCentroCostos"],
                                             StrNoColaborador = a["noColaborador"].ToString(),
                                             StrNombreColaborador = a["nombreColaborador"].ToString(),
                                             DtFechaAlta = (DateTime)a["fechaAlta"],
                                             DtFechaBaja = a["fechaBaja"] == DBNull.Value ? null : (DateTime?)a["fechaBaja"],
                                             StrNoEmpresa = a["NoEmpresa"].ToString(),
                                             StrEmpresa = a["empresa"].ToString(),
                                             DecSueldoNomina = (decimal)a["sueldoNomina"],
                                             DecSueldoHonorarios = (decimal)a["sueldoHonorarios"],
                                             BolActivo = (bool)a["activo"],
                                             IntIdUsuarioCreacion = a["idUsuarioCreacion"] == DBNull.Value ? null : (int?)a["idUsuarioCreacion"],
                                             IntIdUsuarioActualizacion = a["idUsuarioActualizacion"] == DBNull.Value ? null : (int?)a["idUsuarioActualizacion"],
                                             DtFechaCreacion = a["fechaCreacion"] == DBNull.Value ? null : (DateTime?)a["fechaCreacion"],
                                             DtFechaActualizacion = a["fechaActualizacion"] == DBNull.Value ? null : (DateTime?)a["fechaActualizacion"],
                                             ObjClsCatCentroCostosEl = objClsCenterCostsDl.GetCenterCosts((int)a["idCentroCostos"], "").FirstOrDefault()
                                         }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, strNombre }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsTblColaboradorEl;
        }
    }
}
