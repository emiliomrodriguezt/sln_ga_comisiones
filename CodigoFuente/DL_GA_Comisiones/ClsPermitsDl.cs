﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace DL_GA_Comisiones
{
    public class ClsPermitsDl
    {
        public static List<ClsAsocRolPaginaEl> GetPermisos(int intId, int intIdRol, string strUrl)
        {
            var lstClsAsocRolPaginaEl = new List<ClsAsocRolPaginaEl>();

            try
            {
                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_Accesos]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", intId),
                        new SqlParameter("@p_idRol", intIdRol),
                        new SqlParameter("@p_url", strUrl)
                    });

                lstClsAsocRolPaginaEl = (from a in dtResult.DtResult.AsEnumerable()
                                         select new ClsAsocRolPaginaEl()
                                         {
                                            IntId = (int) a["id"],
                                            IntIdRol = (int) a["idRol"],
                                            IntIdPagina = (int) a["idPagina"],
                                            BolEditar = (bool) a["editar"],
                                            IntIdUsuarioCreacion = a["idUsuarioCreacion"] == DBNull.Value ? null : (int?) a["idUsuarioCreacion"],
                                            DtFechaCreacion = a["fechaCreacion"] == DBNull.Value ? null : (DateTime?) a["fechaCreacion"]
                                         }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { strUrl }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsAsocRolPaginaEl;
        }
    }
}
