﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace DL_GA_Comisiones
{
    public class ClsCompanyDl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsCompanyDl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsEmpresasEl> GetCompanies(string strName)
        {
            var lstClsEmpresasEl = new List<ClsEmpresasEl>();

            try
            {
                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER16", StrucQuerysEl.QryGetCompany, new List<SqlParameter>(), false);

                lstClsEmpresasEl = (from a in dtResult.DtResult.AsEnumerable()
                                    select new ClsEmpresasEl()
                                    {
                                        StrTamanoNombre = a["TamanoNombre"] == DBNull.Value ? "" : a["TamanoNombre"].ToString(),
                                        StrNo = a["No"] == DBNull.Value ? "" : a["No"].ToString(),
                                        StrNombre = a["Nombre"] == DBNull.Value ? "" : a["Nombre"].ToString(),
                                        StrInterciaErp = a["InterciaErp"] == DBNull.Value ? "" : a["InterciaErp"].ToString(),
                                        StrConsolidacionErp = a["ConsolidacionErp"] == DBNull.Value ? "" : a["ConsolidacionErp"].ToString(),
                                        StrInterciaCoi = a["InterciaCoi"] == DBNull.Value ? "" : a["InterciaCoi"].ToString(),
                                        StrConsolidacionCoi = a["ConsolidacionCoi"] == DBNull.Value ? "" : a["ConsolidacionCoi"].ToString(),
                                        StrCuentaCostoGasto = a["CuentaCostoGasto"] == DBNull.Value ? "" : a["CuentaCostoGasto"].ToString(),
                                        StrStatus = a["Status"] == DBNull.Value ? "" : a["Status"].ToString(),
                                        StrClasificacion = a["Clasificacion"] == DBNull.Value ? null : a["Clasificacion"].ToString(),
                                        StrArea = a["Area"] == DBNull.Value ? "" : a["Area"].ToString(),
                                        StrSubArea = a["SubArea"] == DBNull.Value ? "" : a["SubArea"].ToString(),
                                        StrFuente = a["Fuente"] == DBNull.Value ? "" : a["Fuente"].ToString()
                                    }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { strName }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsEmpresasEl;
        }
    }
}
