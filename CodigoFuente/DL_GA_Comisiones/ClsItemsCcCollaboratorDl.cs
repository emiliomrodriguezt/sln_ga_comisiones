﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace DL_GA_Comisiones
{
    public class ClsItemsCcCollaboratorDl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsItemsCcCollaboratorDl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsAsocRubosCcColaboradorEl> GetItemsCcCollaborator(int intId, int intIdRubroCc, int intIdColaborador)
        {
            var lstClsAsocRubosCcColaboradorEl = new List<ClsAsocRubosCcColaboradorEl>();
            var objClsItemsCenterCostDl = new ClsItemsCenterCostDl(ObjClsUsuarioActiveDirectoryEl);
            var objClsCollaboratorDl = new ClsCollaboratorDl(ObjClsUsuarioActiveDirectoryEl);

            try
            {
                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_ASOC_RUBROSCC_COLABORADOR]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", intId),
                        new SqlParameter("@p_idRubroCc", intIdRubroCc),
                        new SqlParameter("@p_idColaborador", intIdColaborador)
                    });

                lstClsAsocRubosCcColaboradorEl = (from a in dtResult.DtResult.AsEnumerable()
                                                  select new ClsAsocRubosCcColaboradorEl()
                                                  {
                                                      IntId = Convert.ToInt32(a["id"]),
                                                      IntIdRubroCc = Convert.ToInt32(a["idRubroCc"]),
                                                      IntIdColaborador = Convert.ToInt32(a["idColaborador"]),
                                                      DecPorcentaje = Convert.ToDecimal(a["porcentaje"]),
                                                      IntIdUsuarioCreacion = a["idUsuarioCreacion"] == DBNull.Value ? null : (int?)a["idUsuarioCreacion"],
                                                      DtFechaCreacion = a["fechaCreacion"] == DBNull.Value ? null : (DateTime?)a["fechaCreacion"],
                                                      ObjClsTblColaboradorEl = objClsCollaboratorDl.GetCollaborators(Convert.ToInt32(a["idColaborador"]), "").FirstOrDefault(),
                                                      ObjClsAsocRubrosCentroCostosEl = objClsItemsCenterCostDl.GetItemsCenterCosts(Convert.ToInt32(a["idRubroCc"]), 0, 0).FirstOrDefault()

                                                  }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdRubroCc, intIdColaborador }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsAsocRubosCcColaboradorEl;
        }

        public ClsSimpleResultEl SetItemsCcCollaborator(ClsAsocRubosCcColaboradorEl objClsAsocRubosCcColaboradorEl, bool isDelete)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();

            try
            {
                objClsSimpleResultEl = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_SET_ASOC_RUBROSCC_COLABORADOR]", new List<SqlParameter>()
                {
                    new SqlParameter("@p_id", objClsAsocRubosCcColaboradorEl.IntId),
                    new SqlParameter("@p_idRubroCc", objClsAsocRubosCcColaboradorEl.IntIdRubroCc),
                    new SqlParameter("@p_idColaborador", objClsAsocRubosCcColaboradorEl.IntIdColaborador),
                    new SqlParameter("p_porcentaje", objClsAsocRubosCcColaboradorEl.DecPorcentaje),
                    new SqlParameter("@idUsuarioCreacion", objClsAsocRubosCcColaboradorEl.IntIdUsuarioCreacion),
                    new SqlParameter("@p_fechaCreacion", objClsAsocRubosCcColaboradorEl.DtFechaCreacion),
                    new SqlParameter("@p_isDelete", isDelete)
                });

                objClsSimpleResultEl.BolSuccess = true;
                objClsSimpleResultEl.StrMessage = objClsSimpleResultEl.IntId == 0 ? StrucMessagesEl.GenericoInsertOk : StrucMessagesEl.GenericoUpdateOk;
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(objClsAsocRubosCcColaboradorEl),
                    DtFecha = DateTime.Now
                });
                objClsSimpleResultEl.BolSuccess = false;
                objClsSimpleResultEl.StrMessage = StrucMessagesEl.GenericoError;
            }

            return objClsSimpleResultEl;
        }
    }
}
