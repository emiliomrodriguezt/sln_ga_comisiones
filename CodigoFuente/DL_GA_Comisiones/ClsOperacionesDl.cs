﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EL_GA_Comisiones;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using Newtonsoft.Json;

namespace DL_GA_Comisiones
{
    public class ClsOperacionesDl
    {
        public List<ClsTblMUBCalculadoProyectoEl> GetResultadoMUB(int anio)
        {
            var resultados = new List<ClsTblMUBCalculadoProyectoEl>();

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@anio", anio)
                };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_ResultadosMUB]",
                    parametros);

                resultados = (from a in dtResult.DtResult.AsEnumerable()
                            select new ClsTblMUBCalculadoProyectoEl()
                            {
                                IdMUB = (int)a["id"],
                                IdCentroCostos = (int)a["idCentroCostos"],
                                IdCC = a["clavecc"].ToString(),
                                NombreCC = a["nombrecc"].ToString(),
                                Periodo = (int)a["periodo"],
                                Anio = (int)a["ano"],
                                TotalIngresos = (decimal)a["totalIngresos"],
                                TotalEgresos = (decimal)a["totalEgresos"],
                                TotalIntereses = (decimal)a["totalIntereses"],
                                MUBCalculado = (decimal)a["MUBCalculado"],
                                MUBComision = (decimal)a["MUBComision"],
                                IdUsuarioCreacion = (int)a["idUsuarioCreacion"],
                                IdUsuarioActualizacion = a["idUsuarioActualizacion"] == DBNull.Value ? null : (int?)a["idUsuarioActuualizacion"],
                                FechaCreacion = (DateTime)a["fechaCreacion"],
                                FechaActualizacion = a["fechaActualizacion"] == DBNull.Value ? null : (DateTime?)a["fechaActualizacion"],
                                Congelado = (bool)a["congelado"]
                             }).ToList();

            }
            catch(Exception e)
            {

            }
            return resultados;
        }

        public ClsRespuestaSP ClearResultadoMUB(int anio, int periodo, int usuario)
        {
            ClsRespuestaSP resultado = new ClsRespuestaSP();

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@anio", anio),
                    new SqlParameter("@periodo", periodo),
                    new SqlParameter("@resultado", DbType.Int32)
                        {
                            Direction =ParameterDirection.Output
                        }
                };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_CLEAR_MUBCentroCosto]",
                    parametros);

                if(!string.IsNullOrEmpty(dtResult.StrMessage))
                {
                    resultado.Mensaje = dtResult.StrMessage;
                    resultado.Resultado = 1;
                }

            }
            catch (Exception e)
            {
                resultado.Mensaje = e.Message;
                resultado.Resultado = 2;

                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = e.Message,
                    StrPilaEventos = e.StackTrace,
                    StrMensajeInterno = e.InnerException?.Message ?? "",
                    StrPilaEventosInterno = e.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { anio, periodo, usuario }),
                    DtFecha = DateTime.Now
                });
            }
            return resultado;
        }

        public ClsRespuestaSP ExecCalculoMUB(int anio, int periodo, int usuario)
        {
            var resultados = new ClsRespuestaSP()
            {
                Resultado = 0,
                Mensaje = string.Empty,
                Valor = 0
            };

            try
            {
                ClsCenterCostsDl objCC = new ClsCenterCostsDl();
                List<ClsCatCentroCostosEl> listaCentros = objCC.GetCenterCosts(0, string.Empty);

                /*string consultaCostos = @"SELECT [C.C ERP P&L] zIDH, ISNULL(SUM(monto), 0) costo
                                        FROM [BDM_GLFI].[dbo].[IDM_Presupuestos_All_Vi]
                                        WHERE TIPO='Real' AND DESCRIPCION_PL='COSTO'
                                          AND (año<@anio OR (año=@anio AND month([PERIODO CONTABLE])<=@periodo))
                                          GROUP BY [C.C ERP P&L]
                                          ORDER BY 1";

                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@anio", anio),
                    new SqlParameter("@periodo", periodo)
                };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER16", consultaCostos,
                    parametros, false);*/

                List<SqlParameter> parametros = null;
                ClsSimpleResultEl respuesta = null;

                decimal costo = 0;
                string calculaMUB = "[dbo].[SP_GET_MUBCentroCosto]";
                int contador = 0;
                //DataRow fila = null;

                foreach(ClsCatCentroCostosEl centro in listaCentros)
                {
                    if (centro.DtFechaInicio == null || centro.DtFechaInicio?.Year > anio)
                        continue;

                    //fila = dtResult.DtResult.Select("zIDH='" + centro.StrZIdHomologado + "'").FirstOrDefault();

                    //costo = fila == null ? 0 : Convert.ToDecimal(fila["costo"]);
                    costo = 0;

                    parametros = new List<SqlParameter>()
                    {
                        new SqlParameter("@claveCC", string.IsNullOrEmpty(centro.StrZIdHomologado) ? "0" : centro.StrZIdHomologado),
                        new SqlParameter("@anio", anio),
                        new SqlParameter("@periodo", periodo),
                        new SqlParameter("@costo", costo),
                        new SqlParameter("@usuario", usuario),
                        new SqlParameter("@resultado", DbType.Int32)
                        {
                            Direction =ParameterDirection.Output
                        }
                    };

                    respuesta = ClsSqlExecuteDl.ExecuteSentence("SERVER60", calculaMUB, parametros);

                    if (respuesta.StrMessage != null && respuesta.StrMessage != string.Empty)
                        contador++;
                }

                if(contador > 0)
                {
                    ClsLogDl.InserLog(new ClsTblLogEl()
                    {
                        IntIdUsuario = usuario,
                        StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                        StrMetodo = MethodBase.GetCurrentMethod().Name,
                        StrMensaje = "Cálculo MUB ejecutado con " + contador.ToString() + " errores",
                        StrPilaEventos = string.Empty,
                        StrMensajeInterno = string.Empty,
                        StrPilaEventosInterno = string.Empty,
                        StrEntrada = JsonConvert.SerializeObject(new { anio, periodo, usuario }),
                        DtFecha = DateTime.Now
                    });
                }
            }
            catch (Exception e)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = e.Message,
                    StrPilaEventos = e.StackTrace,
                    StrMensajeInterno = e.InnerException?.Message ?? "",
                    StrPilaEventosInterno = e.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { anio, periodo, usuario }),
                    DtFecha = DateTime.Now
                });
                resultados.Resultado = -1;
                resultados.Mensaje = e.Message;
            }
            return resultados;
        }

        public List<ClsTblSASCalculadoColaboradorEl> GetResultadoSAS(int anio)
        {
            var resultados = new List<ClsTblSASCalculadoColaboradorEl>();

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@anio", anio)
                };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_ResultadosSAS]",
                    parametros);

                resultados = (from a in dtResult.DtResult.AsEnumerable()
                              select new ClsTblSASCalculadoColaboradorEl()
                              {
                                    IdSAS = (int)a["idSAS"],
                                    IdColaborador = (int)a["idColaborador"],
                                    NoColaborador = a["noColaborador"].ToString(),
                                    Colaborador = a["colaborador"].ToString(),
                                    DuracionMeses = (int)a["duracion"],
                                    Anio = (int)a["anio"],
                                    MUBMinimo = (decimal)a["MUBminimo"],
                                    MUBContratado = (decimal)a["MUBcontratado"],
                                    SueldoMUBContratado = (decimal)a["sueldoContratado"],
                                    Sueldo = (decimal)a["sueldo"],
                                    Pagado = (bool)a["pagado"],
                                    CuotaSAS = (decimal)a["cuotaSAS"],
                                    IdUsuarioCreacion = (int)a["usuario"],
                                    FechaCreacion = (DateTime)a["fecha"]
                              }).ToList();
            }
            catch (Exception e)
            {

            }
            return resultados;
        }

        public ClsRespuestaSP ClearResultadoSAS(int anio, int usuario)
        {
            ClsRespuestaSP resultado = new ClsRespuestaSP();

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@anio", anio),
                    new SqlParameter("@resultado", DbType.Int32)
                        {
                            Direction =ParameterDirection.Output
                        }
                };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_CLEAR_SASCalculado]",
                    parametros);

                if (!string.IsNullOrEmpty(dtResult.StrMessage))
                {
                    resultado.Mensaje = dtResult.StrMessage;
                    resultado.Resultado = 1;
                }

            }
            catch (Exception e)
            {
                resultado.Mensaje = e.Message;
                resultado.Resultado = 2;

                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = e.Message,
                    StrPilaEventos = e.StackTrace,
                    StrMensajeInterno = e.InnerException?.Message ?? "",
                    StrPilaEventosInterno = e.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { anio, usuario }),
                    DtFecha = DateTime.Now
                });
            }
            return resultado;
        }

        public ClsRespuestaSP ExecCalculoSAS(int anio, int usuario)
        {
            var resultados = new ClsRespuestaSP()
            {
                Resultado = 0,
                Mensaje = string.Empty,
                Valor = 0
            };

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@anio", anio),
                    new SqlParameter("@usuario", usuario),
                    new SqlParameter("@resultado", DbType.Int32)
                    {
                        Direction =ParameterDirection.Output
                    }
                };

                ClsSimpleResultEl respuesta = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_SASColaborador]", parametros);

                if (!string.IsNullOrEmpty(respuesta.StrMessage))
                {
                    ClsLogDl.InserLog(new ClsTblLogEl()
                    {
                        IntIdUsuario = usuario,
                        StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                        StrMetodo = MethodBase.GetCurrentMethod().Name,
                        StrMensaje = "Cálculo SAS ejecutado con errores",
                        StrPilaEventos = string.Empty,
                        StrMensajeInterno = string.Empty,
                        StrPilaEventosInterno = string.Empty,
                        StrEntrada = JsonConvert.SerializeObject(new { anio, usuario }),
                        DtFecha = DateTime.Now
                    });
                }
            }
            catch (Exception e)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = e.Message,
                    StrPilaEventos = e.StackTrace,
                    StrMensajeInterno = e.InnerException?.Message ?? "",
                    StrPilaEventosInterno = e.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { anio, usuario }),
                    DtFecha = DateTime.Now
                });
                resultados.Resultado = -1;
                resultados.Mensaje = e.Message;
            }
            return resultados;
        }

        public List<ClsProyectosComisionEl> GetProyectosComision(int usuario, int idCol, int anio, int mes)
        {
            List<ClsProyectosComisionEl> listado = null;

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@idCol", idCol),
                    new SqlParameter("@anio", anio),
                    new SqlParameter("@mes", mes)
                };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "SELECT * FROM dbo.FN_GET_ProyectosComision(@idCol, @anio, @mes) ORDER BY orden",
                    parametros, false);

                listado = (from a in dtResult.DtResult.AsEnumerable()
                            select new ClsProyectosComisionEl()
                            {
                                IdCC = (int)a["idCC"],
                                StrCC = a["strCC"].ToString(),
                                NombreCC = a["nombreCC"].ToString(),
                                StrUN = a["strUN"].ToString(),
                                Payback = a["payback"] == DBNull.Value ? null: (int?)a["payback"],
                                MUBcomision = (decimal)a["mubComision"],
                                Porcentaje = (decimal)a["porcentaje"],
                                MontoPagado = (decimal)a["montoPagado"],
                                Orden = (int)a["orden"],
                                Seleccionado = (bool)a["seleccionado"]
                            }).ToList();
            }
            catch(Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { usuario, idCol, anio, mes }),
                    DtFecha = DateTime.Now
                });
            }

            return listado;
        }

        public List<ClsOtrosPagosComisionEl> GetOtrosPagosComision(int usuario, int idCol)
        {
            List<ClsOtrosPagosComisionEl> listado = null;

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@idCol", idCol)
                };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "SELECT * FROM dbo.FN_GET_OtrosPagosComision(@idCol)",
                    parametros, false);

                listado = (from a in dtResult.DtResult.AsEnumerable()
                            select new ClsOtrosPagosComisionEl()
                            {
                                IdTipo = (int)a["idTipo"],
                                IdOtro = (int)a["idOtro"],
                                StrCC = a["strCC"].ToString(),
                                NombreCC = a["nombreCC"].ToString(),
                                Concepto = a["concepto"].ToString(),
                                Importe = (decimal)a["importe"],
                                ImportePagar = (decimal)a["importePagar"],
                                Anio = (int)a["anio"],
                                Seleccionado = (bool)a["seleccionado"]
                            }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { usuario, idCol }),
                    DtFecha = DateTime.Now
                });
            }

            return listado;
        }

        public ClsRespuestaSP SetNuevaComision(ClsTblCalculoComisionEl comision)
        {
            ClsRespuestaSP respuesta = new ClsRespuestaSP()
            {
                Resultado = 0
            };

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@idCol", comision.IdColaborador),
                    new SqlParameter("@anio", comision.Anio),
                    new SqlParameter("@periodo", comision.Periodo),
                    new SqlParameter("@nSAS", comision.SASVisibles),
                    new SqlParameter("@usuario", comision.IdUsuarioCreacion),
                    new SqlParameter("@resultado", SqlDbType.Int)
                        {Direction=ParameterDirection.Output }
                };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_SET_NvoCalculoComision]",
                    parametros);

                respuesta.Valor = dtResult.IntId.Value;
            }
            catch (Exception e)
            {
                respuesta.Resultado = -1;
                respuesta.Mensaje = e.Message;
            }
            return respuesta;
        }

        public ClsRespuestaSP SetProyectoComision(int idCol, int anio, ClsTblCentroCostosComision proyecto)
        {
            ClsRespuestaSP respuesta = new ClsRespuestaSP()
            {
                Resultado = 0
            };

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@idCol", idCol),
                    new SqlParameter("@idComision", proyecto.IdComision),
                    new SqlParameter("@idCC", proyecto.IdCentroCostos),
                    new SqlParameter("@anio", anio),
                    new SqlParameter("@resultado", SqlDbType.Int)
                        {Direction=ParameterDirection.Output }
                };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_SET_CentroCostoComision]",
                    parametros);

                respuesta.Valor = dtResult.IntId.Value;
            }
            catch (Exception e)
            {
                respuesta.Resultado = -1;
                respuesta.Mensaje = e.Message;
            }
            return respuesta;
        }

        public ClsRespuestaSP SetOtroPagoComision(ClsTblOtrosPagosComisionPrevEl pago)
        {
            ClsRespuestaSP respuesta = new ClsRespuestaSP()
            {
                Resultado = 0
            };

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@idComision", pago.IdComision),
                    new SqlParameter("@idTipoPago", pago.IdTipoPago),
                    new SqlParameter("@idPago", pago.IdPago),
                    new SqlParameter("@importePagar", pago.ImporteAPagar),
                    new SqlParameter("@resultado", SqlDbType.Int)
                        {Direction=ParameterDirection.Output }
                };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_SET_OtroPagoComision]",
                    parametros);

                respuesta.Valor = dtResult.IntId.Value;
            }
            catch (Exception e)
            {
                respuesta.Resultado = -1;
                respuesta.Mensaje = e.Message;
            }
            return respuesta;
        }

        public ClsRespuestaSP UpdateMontosComision(int idComision)
        {
            ClsRespuestaSP respuesta = new ClsRespuestaSP();
            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@idComision", idComision),
                    new SqlParameter("@resultado", SqlDbType.Int)
                        {Direction=ParameterDirection.Output }
                };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_UPDATE_MontosComision]",
                    parametros);

                respuesta.Resultado = 0;
            }
            catch (Exception e)
            {
                respuesta.Resultado = -1;
                respuesta.Mensaje = e.Message;
            }
            return respuesta;
        }

        public List<ClsComisionSinAutorizarEl> GetComisionesSinAutorizar()
        {
            var resultados = new List<ClsComisionSinAutorizarEl>();

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>();

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_ComisionesSinAutorizar]",
                    parametros);

                resultados = (from a in dtResult.DtResult.AsEnumerable()
                              select new ClsComisionSinAutorizarEl()
                              {
                                  IdComision = (int)a["idComision"],
                                  Colaborador = a["colaborador"].ToString(),
                                  UnidadNegocio = a["unidadNegocio"].ToString(),
                                  Anio = (int)a["anio"],
                                  MontoAPagar = (decimal)a["montoAPagar"]
                              }).ToList();

            }
            catch (Exception e)
            {

            }
            return resultados;
        }

        public ClsRespuestaSP AutorizarComision(int idComision, int usuario)
        {
            ClsRespuestaSP respuesta = new ClsRespuestaSP()
            {
                Resultado = 0
            };

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@idComision", idComision),
                    new SqlParameter("@usuario", usuario),
                    new SqlParameter("@resultado", SqlDbType.Int)
                        {Direction=ParameterDirection.Output }
                };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_SET_AutorizacionComision]",
                    parametros);
            }
            catch (Exception e)
            {
                respuesta.Resultado = -1;
                respuesta.Mensaje = e.Message;
            }
            return respuesta;
        }

        public List<ClsComisionesPagadas> GetComisionesPagadas(int idCol, int anio)
        {
            var resultados = new List<ClsComisionesPagadas>();

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@idCol", idCol),
                    new SqlParameter("@anio", anio)
                };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_ComisionesPagadas]",
                    parametros);

                resultados = (from a in dtResult.DtResult.AsEnumerable()
                              select new ClsComisionesPagadas()
                              {
                                  IdComision = (int)a["idComision"],
                                  Colaborador = a["colaborador"].ToString(),
                                  UnidadNegocio = a["unidadNegocio"].ToString(),
                                  Anio = (int)a["anio"],
                                  MontoPagado = (decimal)a["montoPagado"]
                              }).ToList();

            }
            catch (Exception e)
            {

            }
            return resultados;
        }

        public string GetUltimoCalculoMUB(int anio, int usuario)
        {
            string respuesta = string.Empty;

            try
            {
                List<SqlParameter> parametros = new List<SqlParameter>()
                {
                    new SqlParameter("@anio", anio)
                };

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_UltimoMUB]",
                    parametros);

                if (dtResult.DtResult != null && dtResult.DtResult.Rows.Count > 0)
                {
                    respuesta = dtResult.DtResult.Rows[0][0].ToString();
                }

            }
            catch (Exception e)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = e.Message,
                    StrPilaEventos = e.StackTrace,
                    StrMensajeInterno = e.InnerException?.Message ?? "",
                    StrPilaEventosInterno = e.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { anio, usuario }),
                    DtFecha = DateTime.Now
                });
            }
            return respuesta;
        }
    }
}
