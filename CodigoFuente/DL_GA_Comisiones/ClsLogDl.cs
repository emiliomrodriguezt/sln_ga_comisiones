﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace DL_GA_Comisiones
{
    public static class ClsLogDl
    {
        public static  void InserLog(ClsTblLogEl objClsCatLogEl)
        {
            try
            {
                ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_SET_LOG]", new List<SqlParameter>()
                {
                    new SqlParameter("@p_idUsuario", objClsCatLogEl.IntIdUsuario),
                    new SqlParameter("@p_clase", objClsCatLogEl.StrClase),
                    new SqlParameter("@p_metodo", objClsCatLogEl.StrMetodo),
                    new SqlParameter("@p_mensajeError", objClsCatLogEl.StrMensaje),
                    new SqlParameter("@p_pilaEventos", objClsCatLogEl.StrPilaEventos),
                    new SqlParameter("@p_mensajeErroInterno", objClsCatLogEl.StrMensajeInterno),
                    new SqlParameter("@p_pilaEventosInterno", objClsCatLogEl.StrPilaEventosInterno),
                    new SqlParameter("@p_entrada", objClsCatLogEl.StrEntrada),
                    new SqlParameter("@p_fecha", objClsCatLogEl.DtFecha)
                });
            }
            catch (Exception ex)
            {
                InsertLogTxt(objClsCatLogEl, ex);
            }
        }

        public static void InsertLogTxt(ClsTblLogEl objClsCatLogEl, Exception ex)
        {
            try
            {
                var dtDateExcepcion = DateTime.Now;
                var strPathDirectoryLog = Path.Combine(new[]
                {
                    ClsGetConfigDl.GetKeyConfig("LOGTXT", "strPathLog"),
                    dtDateExcepcion.Year.ToString(),
                    dtDateExcepcion.Month.ToString()
                });

                if (!Directory.Exists(strPathDirectoryLog))
                {
                    Directory.CreateDirectory(strPathDirectoryLog);
                }

                var strPathFileLog = Path.Combine(strPathDirectoryLog,
                    $"{dtDateExcepcion.DayOfWeek}_{dtDateExcepcion.Day}/{dtDateExcepcion.Month}/{dtDateExcepcion.Year}.log");

                if (!File.Exists(strPathFileLog))
                {
                    File.Create(strPathFileLog);
                }

                using (var objStreamWriter = File.AppendText(strPathFileLog))
                {
                    objStreamWriter.WriteLine(
                        $"Error al intenatar escribir el log [{JsonConvert.SerializeObject(objClsCatLogEl)}], Error: [{ex.Message}]");
                }
            }
            catch (Exception)
            {
                //;(
            }
}
    }
}
