﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace DL_GA_Comisiones
{
    public class ClsTabulatorManagerDl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsTabulatorManagerDl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsCatTabuladorGerentesEl> GetTabulatorManager(int intId, int intAnoVigencia, int intTipoTabulador)
        {
            var objClsTypeTabulatorDl = new ClsTypeTabulatorDl(ObjClsUsuarioActiveDirectoryEl);
            var lstClsCatTabuladorGerentes = new List<ClsCatTabuladorGerentesEl>();

            try
            {
                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_CAT_TABULADORGERENTES]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", intId),
                        new SqlParameter("@p_anoVigencia", intAnoVigencia),
                        new SqlParameter("@p_idTipoTabulador", intTipoTabulador)
                    });

                lstClsCatTabuladorGerentes = (from a in dtResult.DtResult.AsEnumerable()
                                              select new ClsCatTabuladorGerentesEl()
                                              {
                                                  IntId = (int)a["id"],
                                                  IntIdTipoTabulador = (int)a["idTipoTabulador"],
                                                  DecCostoMensualGaMinimo = (decimal)a["costoMensualGAminimo"],
                                                  DecCostoMensualGaMaximo = (decimal)a["costoMensualGAmaximo"],
                                                  DecMubMinimo = (decimal)a["MUBminimo"],
                                                  DecComision = (decimal)a["comision"],
                                                  BolActivo = (bool)a["activo"],
                                                  IntAnoVigencia = (int)a["anoVigencia"],
                                                  IntIdUsuarioCreacion = a["idUsuarioCreacion"] == DBNull.Value ? null : (int?)a["idUsuarioCreacion"],
                                                  IntIdUsuarioActulizacion = a["idUsuarioActualizacion"] == DBNull.Value ? null : (int?)a["idUsuarioActualizacion"],
                                                  DtFechaCreacion = a["fechaCreacion"] == DBNull.Value ? null : (DateTime?)a["fechaCreacion"],
                                                  DtFechaActualizacion = a["fechaActualizacion"] == DBNull.Value ? null : (DateTime?)a["fechaActualizacion"],
                                                  ObjClsCatTipoTabuladorEl = objClsTypeTabulatorDl.GetTypeTabulator((int)a["idTipoTabulador"], "").FirstOrDefault()
                                              }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intAnoVigencia }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsCatTabuladorGerentes;
        }

        public ClsSimpleResultEl SetTabulatorManager(ClsCatTabuladorGerentesEl objClsCatTabuladorGerentes, bool isDelete)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();

            try
            {
                objClsSimpleResultEl = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_SET_CAT_TABULADORGERENTES]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", objClsCatTabuladorGerentes.IntId),
                        new SqlParameter("@p_idTipoTabulador", objClsCatTabuladorGerentes.IntIdTipoTabulador),
                        new SqlParameter("@p_costoMensualGAminimo", objClsCatTabuladorGerentes.DecCostoMensualGaMinimo),
                        new SqlParameter("@p_costoMensualGAmaximo", objClsCatTabuladorGerentes.DecCostoMensualGaMaximo),
                        new SqlParameter("@p_MUBminimo", objClsCatTabuladorGerentes.DecMubMinimo),
                        new SqlParameter("@p_comison", objClsCatTabuladorGerentes.DecComision),
                        new SqlParameter("@p_activo", objClsCatTabuladorGerentes.BolActivo),
                        new SqlParameter("@p_anoVigencia", objClsCatTabuladorGerentes.IntAnoVigencia),
                        new SqlParameter("@p_idUsuarioCreacion", objClsCatTabuladorGerentes.IntIdUsuarioCreacion),
                        new SqlParameter("@p_idUsuarioActualizacion", objClsCatTabuladorGerentes.IntIdUsuarioActulizacion),
                        new SqlParameter("@p_fechaCreacion", objClsCatTabuladorGerentes.DtFechaCreacion),
                        new SqlParameter("@p_fechaActualizacion", objClsCatTabuladorGerentes.DtFechaActualizacion),
                        new SqlParameter("@p_isDelete", isDelete)
                    });
                objClsSimpleResultEl.BolSuccess = true;
                objClsSimpleResultEl.StrMessage = objClsSimpleResultEl.IntId == 0 ? StrucMessagesEl.GenericoInsertOk : StrucMessagesEl.GenericoUpdateOk;
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(objClsCatTabuladorGerentes),
                    DtFecha = DateTime.Now
                });
                objClsSimpleResultEl.BolSuccess = false;
                objClsSimpleResultEl.StrMessage = StrucMessagesEl.GenericoError;
            }

            return objClsSimpleResultEl;
        }

        public ClsSimpleResultEl CopyTabulatorManager(int anoOld, int anoNew, int intIdUsuario, DateTime dtFecha)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();

            try
            {
                objClsSimpleResultEl = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_COPY_CAT_TABULADORGERENTES]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_anoOld", anoOld),
                        new SqlParameter("@p_anoNew", anoNew),
                        new SqlParameter("@p_idUsuarioCreacion", intIdUsuario),
                        new SqlParameter("@p_fechaCreacion", dtFecha)
                    });
                objClsSimpleResultEl.BolSuccess = true;
                objClsSimpleResultEl.StrMessage = objClsSimpleResultEl.IntId == 0 ? StrucMessagesEl.GenericoInsertOk : StrucMessagesEl.GenericoUpdateOk;
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { anoOld, anoNew, intIdUsuario, dtFecha }),
                    DtFecha = DateTime.Now
                });
                objClsSimpleResultEl.BolSuccess = false;
                objClsSimpleResultEl.StrMessage = StrucMessagesEl.GenericoError;
            }

            return objClsSimpleResultEl;
        }
    }
}
