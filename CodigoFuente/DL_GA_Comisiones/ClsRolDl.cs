﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace DL_GA_Comisiones
{
    public class ClsRolDl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsRolDl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public static List<ClsCatRolesEl> GetRols(int intId, string strNombre)
        {
            var lstClsCatRolesEl = new List<ClsCatRolesEl>();

            try
            {
                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_Rol]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", intId),
                        new SqlParameter("@p_nombre", strNombre)
                    });

                    lstClsCatRolesEl = (from a in dtResult.DtResult.AsEnumerable()
                                        select new ClsCatRolesEl()
                                        {
                                            IntId = (int)a["id"],
                                            StrNombre = a["nombre"].ToString(),
                                            BolActivo = (bool)a["activo"],
                                            IntIdUsuarioCreacion = a["idUsuarioCreacion"] == DBNull.Value ? null : (int?)a["idUsuarioCreacion"],
                                            IntIdUsuarioActualizacion = a["idUsuarioActulizacion"] == DBNull.Value ? null : (int?)a["idUsuarioActulizacion"],
                                            DtFechaCreacion = a["fechaCreacion"] == DBNull.Value ? null : (DateTime?)a["fechaCreacion"],
                                            DtFechaActualizacion = a["fechaActulizacion"] == DBNull.Value ? null : (DateTime?)a["fechaActulizacion"]
                                        }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, strNombre }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsCatRolesEl;
        }

        public ClsSimpleResultEl SetRol(ClsCatRolesEl objClsCatRolesEl, bool isDelete)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();

            try
            {
                objClsSimpleResultEl = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_SET_ROL]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", objClsCatRolesEl.IntId),
                        new SqlParameter("@p_nombre", objClsCatRolesEl.StrNombre),
                        new SqlParameter("@p_activo", objClsCatRolesEl.BolActivo),
                        new SqlParameter("@p_idUsuarioCreacion", objClsCatRolesEl.IntIdUsuarioCreacion),
                        new SqlParameter("@p_idUsuarioActulizacion", objClsCatRolesEl.IntIdUsuarioActualizacion),
                        new SqlParameter("@p_fechaCreacion", objClsCatRolesEl.DtFechaCreacion),
                        new SqlParameter("@p_fechaActulizacion", objClsCatRolesEl.DtFechaActualizacion),
                        new SqlParameter("p_isDelete", isDelete)
                    });
                objClsSimpleResultEl.BolSuccess = true;
                objClsSimpleResultEl.StrMessage = objClsSimpleResultEl.IntId == 0 ? StrucMessagesEl.GenericoInsertOk : StrucMessagesEl.GenericoUpdateOk;
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(objClsCatRolesEl),
                    DtFecha = DateTime.Now
                });
                objClsSimpleResultEl.BolSuccess = false;
                objClsSimpleResultEl.StrMessage = StrucMessagesEl.GenericoError;
            }

            return objClsSimpleResultEl;
        }
    }
}
