﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace DL_GA_Comisiones
{
    public class ClsUserDl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsUserDl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public static List<ClsCatUsuarioEl> GetUsers(int intId, string strUser)
        {
            var lstClsCatUsuarioEl = new List<ClsCatUsuarioEl>();

            try
            {

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_Usuario]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", intId),
                        new SqlParameter("@p_usuario", strUser)
                    });

                lstClsCatUsuarioEl = (from a in dtResult.DtResult.AsEnumerable()
                    select new ClsCatUsuarioEl()
                    {
                        IntId = (int) a["id"],
                        IntIdRol = (int) a["idRol"],
                        StrNombre = a["nombre"].ToString(),
                        StrUser = a["usuario"].ToString(),
                        BolActivo = (bool) a["activo"],
                        IntIdCreateuser = a["idUsuarioCreacion"] == DBNull.Value ? null : (int?) a["idUsuarioCreacion"],
                        IntIdUpdateUser = a["idUsuarioActulizacion"] == DBNull.Value ? null : (int?) a["idUsuarioActulizacion"],
                        DtCreateDate = a["fechaCreacion"] == DBNull.Value ? null : (DateTime?) a["fechaCreacion"],
                        DtUpdateDate = a["fechaActulizacion"] == DBNull.Value ? null : (DateTime?) a["fechaActulizacion"]
                    }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, strUser }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsCatUsuarioEl;
        }

        public List<ClsCatUsuarioEl> GetUsers(int intId, string strName, string strUser)
        {
            var lstClsCatUsuarioEl = new List<ClsCatUsuarioEl>();

            try
            {

                var dtResult = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_GET_Usuario]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", intId),
                        new SqlParameter("@p_usuario", strUser),
                         new SqlParameter("@p_nombre", strName)
                    });

                lstClsCatUsuarioEl = (from a in dtResult.DtResult.AsEnumerable()
                                      select new ClsCatUsuarioEl()
                                      {
                                          IntId = (int)a["id"],
                                          IntIdRol = (int)a["idRol"],
                                          StrNombre = a["nombre"].ToString(),
                                          StrUser = a["usuario"].ToString(),
                                          BolActivo = (bool)a["activo"],
                                          IntIdCreateuser = a["idUsuarioCreacion"] == DBNull.Value ? null : (int?)a["idUsuarioCreacion"],
                                          IntIdUpdateUser = a["idUsuarioActulizacion"] == DBNull.Value ? null : (int?)a["idUsuarioActulizacion"],
                                          DtCreateDate = a["fechaCreacion"] == DBNull.Value ? null : (DateTime?)a["fechaCreacion"],
                                          DtUpdateDate = a["fechaActulizacion"] == DBNull.Value ? null : (DateTime?)a["fechaActulizacion"]
                                      }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, strUser }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsCatUsuarioEl;
        }

        public ClsSimpleResultEl SetUser(ClsCatUsuarioEl objClsCatUsuarioEl, bool isDelete)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();

            try
            {
                objClsSimpleResultEl = ClsSqlExecuteDl.ExecuteSentence("SERVER60", "[dbo].[SP_SET_USUARIO]",
                    new List<SqlParameter>()
                    {
                        new SqlParameter("@p_id", objClsCatUsuarioEl.IntId),
                        new SqlParameter("@p_idRol", objClsCatUsuarioEl.IntIdRol),
                        new SqlParameter("@p_nombre", objClsCatUsuarioEl.StrNombre),
                        new SqlParameter("@p_usuario", objClsCatUsuarioEl.StrUser),
                        new SqlParameter("p_activo", objClsCatUsuarioEl.BolActivo),
                        new SqlParameter("@p_idUsuarioCreacion", objClsCatUsuarioEl.IntIdCreateuser),
                        new SqlParameter("@p_idUsuarioActulizacion", objClsCatUsuarioEl.IntIdUpdateUser),
                        new SqlParameter("@p_fechaCreacion", objClsCatUsuarioEl.DtCreateDate),
                        new SqlParameter("@p_fechaActulizacion", objClsCatUsuarioEl.DtUpdateDate),
                        new SqlParameter("p_isDelete", isDelete)
                    });
                objClsSimpleResultEl.BolSuccess = true;
                objClsSimpleResultEl.StrMessage = objClsSimpleResultEl.IntId == 0 ? StrucMessagesEl.GenericoInsertOk : StrucMessagesEl.GenericoUpdateOk;
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(objClsCatUsuarioEl),
                    DtFecha = DateTime.Now
                });
                objClsSimpleResultEl.BolSuccess = false;
                objClsSimpleResultEl.StrMessage = StrucMessagesEl.GenericoError;
            }

            return objClsSimpleResultEl;
        }
    }
}
