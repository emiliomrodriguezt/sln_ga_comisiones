﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using CrystalDecisions.Shared;
using DL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;
using Utility_GA_Comisiones;
using BL_GA_Comisiones.Reports.plantilla;

namespace BL_GA_Comisiones
{
    public class ClsResumenResultadosBl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsResumenResultadosBl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public DataTable GetFormatReportSasCollaborator(int intIdColaborador, int intAnio, int intSas, DateTime dtFechaEntrada)
        {
            var dt = new DataTable();

            try
            {
                var objClsSasCollaboratorDl = new ClsSasCollaboratorDl(ObjClsUsuarioActiveDirectoryEl);
                return objClsSasCollaboratorDl.GetFormatReportSasCollaborator(intIdColaborador, AnioSas(intAnio, intSas, dtFechaEntrada), intAnio).GenerateTransposedTable();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intIdColaborador, intAnio }),
                    DtFecha = DateTime.Now
                });
            }

            return dt;
        }

        public dynamic GetFormatReportCenterCostCollaboratorSas(int intIdColaborador, int intNumSas, DateTime dtFechaIngreso, int intAno)
        {
            try
            {
                var objClsCenterCostCollaboratorSasBl = new ClsCenterCostCollaboratorSasBl(ObjClsUsuarioActiveDirectoryEl);
                return objClsCenterCostCollaboratorSasBl.GetCenterCostCollaboratorSas(0, intIdColaborador, 0, 0, 0)
                                                        .Where(itm => itm.IntAno >= AnioSas(intAno, intNumSas, dtFechaIngreso) && itm.IntAno <= intAno)
                                                        .Select(itm => new
                                                        {
                                                            itm.IntAno,
                                                            itm.ObjClsCatCentroCostosEl.StrNombre,
                                                            itm.ObjClsCatCentroCostosEl.StrId,
                                                            Mub = $"{itm.DecMonto:c}"
                                                        });
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intIdColaborador, intNumSas, dtFechaIngreso, intAno }),
                    DtFecha = DateTime.Now
                });
            }

            return "";
        }

        public dynamic GetCenterCostComisones(int intIdComision, int intAno)
        {
            try
            {
                var objClsCommissionCenterCostBl = new ClsCommissionCenterCostBl(ObjClsUsuarioActiveDirectoryEl);
                var objClsCommissionCalculationBl = new ClsCommissionCalculationBl(ObjClsUsuarioActiveDirectoryEl);
                var objClsMubCalculatedProjectBl = new ClsMubCalculatedProjectBl(ObjClsUsuarioActiveDirectoryEl);

                return (from a in objClsCommissionCenterCostBl.GetCommissionCenterCost(0, 0, 0)
                        join b in objClsCommissionCalculationBl.GetCommissionCalculation(0, 0, 0) on a.IntIdComision equals b.IdComision
                        where a.IntIdComision.Equals(intIdComision) && b.Anio.Equals(intAno)
                        select new
                        {
                            a.IntId,
                            a.ObjClsCatCentroCostosEl.StrZIdHomologado,
                            a.ObjClsCatCentroCostosEl.StrNombre,
                            a.ObjClsCatCentroCostosEl.StrEstatus,
                            Ano = a.ObjClsCatCentroCostosEl.DtFechaInicio?.ToString("yyyy") ?? "",
                            PayBack = a.ObjClsCatCentroCostosEl.DtFechaPayBack?.ToString("yyyy") ?? "",
                            Termino = a.ObjClsCatCentroCostosEl.DtFechaCierreAdmin?.ToString("yyyy") ?? "",
                            MubAcomulado = $"{objClsMubCalculatedProjectBl.GetMubCalculatedProject(0, a.IntIdCentroCostos, b.Anio).Select(itm => itm.DecMubCalculado).FirstOrDefault():c}",
                            MubComisiones = $"{a.DecMubComision:c}",
                            PorcentajeComision = $"{a.DecPorcentaje:p}",
                            ComisionesA = $"{((a.DecPorcentaje * a.DecMubComision)):c}",
                            DecMontoPagado = $"{a.DecMontoPagado:c}",
                            DecComisionTotal = $"{a.DecComisionTotal:c}",
                            a.StrObservaciones,
                        }).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intIdComision, intAno }),
                    DtFecha = DateTime.Now
                });
            }

            return "";
        }

        public Tuple<string, string, string> GetOtherPay(int intIdComision)
        {
            var strJsonAnticipo = string.Empty;
            var strJsonSas = string.Empty;
            var strJsonBono = string.Empty;

            try
            {
                var objClsOtherPayBl = new ClsOtherPayBl(ObjClsUsuarioActiveDirectoryEl);
                var lstClsTblOtrosPagosComisionEl = objClsOtherPayBl.GetsOtherPay(0, intIdComision, 0, 0);

                strJsonSas = JsonConvert.SerializeObject(lstClsTblOtrosPagosComisionEl.Where(itm => itm.IntIdTipoPago.Equals(1))
                                                                                      .OrderBy(itm => itm.IntAno)
                                                                                      .Select(itm => new
                                                                                       {
                                                                                           importe = $"{itm.DecImporteAPagar:c}",
                                                                                           tipo = "SAS - " + itm.IntAno.ToString()
                                                                                       }));

                strJsonAnticipo = JsonConvert.SerializeObject(lstClsTblOtrosPagosComisionEl.Where(itm => itm.IntIdTipoPago.Equals(2))
                                                                                           .OrderBy(itm => itm.IntAno)
                                                                                           .Select(itm => new
                                                                                           {
                                                                                               importe = $"{itm.DecImporteAPagar:c}",
                                                                                               tipo = "Anticipo - " + itm.IntAno.ToString()
                                                                                           }));

                strJsonBono = JsonConvert.SerializeObject(lstClsTblOtrosPagosComisionEl.Where(itm => itm.IntIdTipoPago.Equals(3))
                                                                                           .OrderBy(itm => itm.IntAno)
                                                                                           .Select(itm => new
                                                                                           {
                                                                                               importe = $"{itm.DecImporteAPagar:c}",
                                                                                               tipo = "Bono - " + itm.IntAno.ToString()
                                                                                           }));
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intIdComision }),
                    DtFecha = DateTime.Now
                });
            }

            return new Tuple<string, string, string>(strJsonAnticipo, strJsonSas, strJsonBono);
        }

        public MemoryStream GetExportReport(int intIdComsion, bool bolIsPdf, string strFirmaContraloria, string strFirmaDirUn, string strFirmaDirGrl, string strFirmaPdte)
        {
            var objClsCommissionCalculationDl = new ClsCommissionCalculationDl(ObjClsUsuarioActiveDirectoryEl);
            var objClsSasCollaboratorDl = new ClsSasCollaboratorDl(ObjClsUsuarioActiveDirectoryEl);
            var objClsCenterCostCollaboratorSasDl = new ClsCenterCostCollaboratorSasDl(ObjClsUsuarioActiveDirectoryEl);
            var objClsCommissionCenterCostDl = new ClsCommissionCenterCostDl(ObjClsUsuarioActiveDirectoryEl);
            var objClsMubCalculatedProjectDl = new ClsMubCalculatedProjectDl(ObjClsUsuarioActiveDirectoryEl);
            var objClsOtherPayDl = new ClsOtherPayDl(ObjClsUsuarioActiveDirectoryEl);

            var objClsTblCalculoComisionEl = objClsCommissionCalculationDl.GetCommissionCalculation(intIdComsion, 0, 0).FirstOrDefault();
            if (objClsTblCalculoComisionEl == null)
            {
                return new MemoryStream();
            }

            var dtGeneral = objClsCommissionCalculationDl.GetCommissionCalculation(objClsTblCalculoComisionEl.IdComision, 0, 0).Select(itm => new
            {
                IntAnoGeneral = itm.Anio,
                StrCentroCostos  = itm.ObjClsTblColaboradorEl.ObjClsCatCentroCostosEl.StrNombre,
                StrGerente = itm.ObjClsTblColaboradorEl.StrNombreColaborador,
                DtFechaIngreso = itm.ObjClsTblColaboradorEl.DtFechaAlta,
                StrFirmaContraloria = strFirmaContraloria,
                StrFirmaUn = strFirmaDirUn,
                StrFirmaGrl = strFirmaDirGrl,
                StrPresidencia = strFirmaPdte,
                DecMonto = itm.TotalAPagar ?? 0m
            }).ToList().ToDataTable();
            dtGeneral.TableName = "tblGeneral";

            var intAno = AnioSas(objClsTblCalculoComisionEl.Anio,
                                 objClsTblCalculoComisionEl.SASVisibles,
                                 objClsTblCalculoComisionEl.ObjClsTblColaboradorEl.DtFechaAlta);

            var dtTblSasCalCol = objClsSasCollaboratorDl.GetSasCollaborator(0, 
                                                                            objClsTblCalculoComisionEl.IdColaborador,
                                                                            0)
                                 .Select(itm => new
                                 {
                                     intAnoSasCol = itm.Anio,
                                     decSueldo = itm.Sueldo,
                                     desCuotaVsSas = itm.MUBMinimo,
                                     decCuotaCon = itm.MUBContratado,
                                     decCuotaAlc = itm.MUBMinimo != 0 ? (itm.MUBContratado / itm.MUBMinimo) * 100 : 0M,
                                     decSueldoMub = itm.SueldoMUBContratado,
                                     decSueldoPagNom = itm.Sueldo,
                                     decDifPagar = itm.SueldoMUBContratado - itm.Sueldo,
                                     intMesesAno = itm.DuracionMeses,
                                     decCuotaSasPorPagar = itm.CuotaSAS
                                 })
                                 .Where(itm => itm.intAnoSasCol >= intAno && itm.intAnoSasCol <= objClsTblCalculoComisionEl.Anio).ToList().ToDataTable();
            dtTblSasCalCol.TableName = "tblSasCalCol";


            var dtTblCentroCostosSas = objClsCenterCostCollaboratorSasDl.GetCenterCostCollaboratorSas(0, 
                                                                                                      objClsTblCalculoComisionEl.IdColaborador, 
                                                                                                      0, 
                                                                                                      0, 
                                                                                                      0)
                                       .Where(itm => itm.IntAno >= intAno && itm.IntAno  <= objClsTblCalculoComisionEl.Anio)
                                       .Select(itm => new
                                       {
                                           intAnoSas = itm.IntAno,
                                           strNoCcSas = itm.ObjClsCatCentroCostosEl.StrNombre,
                                           strNombreCcSas = itm.ObjClsCatCentroCostosEl.StrZIdHomologado,
                                           decMubContratadoSas = itm.DecMonto
                                       }).ToList().ToDataTable();
            dtTblCentroCostosSas.TableName = "tblCentroCostosSas";

            var dttblDetalleComision = (from a in objClsCommissionCenterCostDl.GetCommissionCenterCost(0, 0, 0)
                                        join b in objClsCommissionCalculationDl.GetCommissionCalculation(0, 0, 0) on a.IntIdComision equals b.IdComision
                                        where a.IntIdComision.Equals(objClsTblCalculoComisionEl.IdComision) && b.Anio.Equals(objClsTblCalculoComisionEl.Anio)
                                        select new
                                        {
                                            strNoCc = a.ObjClsCatCentroCostosEl.StrZIdHomologado,
                                            strNombreCc = a.ObjClsCatCentroCostosEl.StrNombre,
                                            strEstatus = a.ObjClsCatCentroCostosEl.StrEstatus,
                                            intAnoInicio = a.ObjClsCatCentroCostosEl.DtFechaInicio?.Year ?? 0,
                                            intAnoPayBack = a.ObjClsCatCentroCostosEl.DtFechaPayBack?.Year ?? 0,
                                            intAnoTemrino = a.ObjClsCatCentroCostosEl.DtFechaCierreAdmin?.Year ?? 0,
                                            decTotalMubAcomuladoA = a.DecMubComision,
                                            decTotalDeMubA = objClsMubCalculatedProjectDl.GetMubCalculatedProject(0, a.IntIdCentroCostos, b.Anio).Select(itm => itm.DecMubComision).FirstOrDefault(),
                                            decPorComision = a.DecPorcentaje,
                                            decComisionesA = ((a.DecPorcentaje * a.DecMubComision)),
                                            decImportePagadoADicDe = a.DecMontoPagado,
                                            decComisionAPagar = a.DecComisionTotal,
                                            strObservaciobes = a.StrObservaciones,
                                            intAnoReporte = objClsTblCalculoComisionEl.Anio
                                        }).ToList().ToDataTable();
            dttblDetalleComision.TableName = "tblDetalleComision";

            var dtTblOtrosPagos = objClsOtherPayDl.GetsOtherPay(0, 
                                                                objClsTblCalculoComisionEl.IdComision, 
                                                                0, 
                                                                0)
                                                .Select(itm => new
                                                {
                                                    strConcepto = itm.IntIdTipoPago == 1 ? $"SAS - {itm.IntAno}" : itm.IntIdTipoPago == 2 ? $"Anticipo - {itm.IntAno}  {itm.ObjClsTblAnticiposEl.ObjClsCatCentroCostosEl.StrZIdHomologado}" : $"BONO - {itm.IntAno}",
                                                    decMontoPago = itm.IntIdTipoPago == 2 ? itm.DecImporteAPagar * -1 : itm.DecImporteAPagar
                                                }).ToList().ToDataTable();


            var row = dtTblOtrosPagos.NewRow();
            row[0] = "Total comisiones";
            row[1] = objClsTblCalculoComisionEl.ComisionTotal;
            dtTblOtrosPagos.Rows.InsertAt(row, 0);

            row = dtTblOtrosPagos.NewRow();
            row[0] = "Neto a pagar";
            row[1] = objClsTblCalculoComisionEl.TotalAPagar;
            dtTblOtrosPagos.Rows.Add(row);
            dtTblOtrosPagos.TableName = "tblOtrosPagos";

            var objDataSet = new DataSet
            {
                DataSetName = "Resultados"
            };
            objDataSet.Tables.Add(dtGeneral);
            objDataSet.Tables.Add(dtTblSasCalCol);
            objDataSet.Tables.Add(dtTblCentroCostosSas);
            objDataSet.Tables.Add(dttblDetalleComision);
            objDataSet.Tables.Add(dtTblOtrosPagos);


            var objRptSasColaborador = new RptResultados();
            objRptSasColaborador.Load();
            objRptSasColaborador.SetDataSource(objDataSet);
            var objStream = objRptSasColaborador.ExportToStream((bolIsPdf) ? ExportFormatType.PortableDocFormat : ExportFormatType.Excel );
            objRptSasColaborador.Close();
            objRptSasColaborador.Dispose();

            return objStream.ReadFully();

        }

        public int AnioSas(int intAnioComision, int intNumSas, DateTime dtFechaIngreso)
        {
            var intAnio = intAnioComision;

            if (intNumSas > 1)
            {
                intAnio = intAnio - (intNumSas - 1);
            }

            if (intAnio < dtFechaIngreso.Year)
            {
                intAnio = dtFechaIngreso.Year;
            }

            return intAnio;
        }
    }
}
