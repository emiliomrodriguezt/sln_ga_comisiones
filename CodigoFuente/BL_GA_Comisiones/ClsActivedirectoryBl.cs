﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Reflection;
using EL_GA_Comisiones;
using DL_GA_Comisiones;
using Newtonsoft.Json;

namespace BL_GA_Comisiones
{
    public static class ClsActiveDirectoryBl
    {
        public static ClsUsuarioActiveDirectoryEl ValidUserActiveDirectory(string strUser, string strPwd)
        {
            var objClsUsuarioActiveDirectoryEl = new ClsUsuarioActiveDirectoryEl();

            try
            {
                var objDirectoryEntry = new DirectoryEntry(ClsGetConfigDl.GetKeyConfig("ACTIVEDIRECTORY", "strHost"),
                    strUser,
                    strPwd,
                    AuthenticationTypes.Secure);

                var objDirectorySearcher = new DirectorySearcher(objDirectoryEntry)
                {
                    Filter = $"(&(objectCategory=person)(anr={strUser}))"
                };

                objDirectorySearcher.PropertiesToLoad.Add("userAccountControl");
                objDirectorySearcher.PropertiesToLoad.Add("samaccountname");
                objDirectorySearcher.PropertiesToLoad.Add("displayname");

                foreach (SearchResult itm in objDirectorySearcher.FindAll())
                {
                    objClsUsuarioActiveDirectoryEl.StrUserAccountControl = itm.Properties["userAccountControl"][0].ToString();
                    objClsUsuarioActiveDirectoryEl.StrSamAccountName = itm.Properties["samaccountname"][0].ToString();
                    objClsUsuarioActiveDirectoryEl.StrDisplayName = itm.Properties["displayname"][0].ToString();
                }
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { strUser, strPwd }),
                    DtFecha = DateTime.Now
                });
            }

            return objClsUsuarioActiveDirectoryEl;
        }

        public static List<ClsUsuarioActiveDirectoryEl> GetUserActiveDirectory()
        {
            var lstClsUsuarioActiveDirectoryEl = new List<ClsUsuarioActiveDirectoryEl>();

            try
            {
                var objDirectoryEntry = new DirectoryEntry(ClsGetConfigDl.GetKeyConfig("ACTIVEDIRECTORY", "strHost"),
                                                           ClsGetConfigDl.GetKeyConfig("ACTIVEDIRECTORY", "strUserAdmin"),
                                                           ClsGetConfigDl.GetKeyConfig("ACTIVEDIRECTORY", "strPasswordAdmin"), 
                                                           AuthenticationTypes.Secure);

                var objDirectorySearcher = new DirectorySearcher(objDirectoryEntry)
                {
                    Filter = "(&(objectCategory=person))"
                };

                foreach (SearchResult itm in objDirectorySearcher.FindAll())
                {
                    var deUser = itm.GetDirectoryEntry();
                    if (deUser.Properties.Contains("userAccountControl") && 
                        deUser.Properties.Contains("sAMAccountName") && 
                        deUser.Properties.Contains("displayName"))
                    {
                        lstClsUsuarioActiveDirectoryEl.Add(new ClsUsuarioActiveDirectoryEl()
                        {
                            StrUserAccountControl = deUser.Properties["userAccountControl"].Value.ToString(),
                            StrSamAccountName = deUser.Properties["sAMAccountName"].Value.ToString(),
                            StrDisplayName = deUser.Properties["displayName"].Value.ToString()
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = "",
                    DtFecha = DateTime.Now
                });
            }

            return lstClsUsuarioActiveDirectoryEl;
        }
    }
}
