﻿using System.Web;
using EL_GA_Comisiones;
using Utility_GA_Comisiones;

namespace BL_GA_Comisiones
{
    public static class ClsGetLinksBl
    {
        public static string GetLinkAccion(ClsDataTableEl objClsDataTableEl, int intId, bool bolActivo, bool bolCentroCostos = false)
        {
            string strResult = string.Empty;

            if (objClsDataTableEl.BolConsult)
            {
                strResult += "&nbsp;&nbsp;&nbsp;<a class=\"tooltips\" data-toggle=\"tooltip\" title =\"Consultar\"" +
                             $" href=\"{objClsDataTableEl.StrPathEdit}?edit=1&id={HttpContext.Current.Server.UrlEncode(ClsCryptographyBl.EncodeString(intId.ToString()))}\" " +
                             "><i class=\"fa fa-bars\" aria-hidden=\"true\"></i></a>";
            }
            if (objClsDataTableEl.BolEdit && bolActivo)
            {
                strResult += "&nbsp;&nbsp;&nbsp;<a class=\"tooltips\" data-toggle=\"tooltip\" title =\"Editar\"" +
                             $" href=\"{objClsDataTableEl.StrPathEdit}?edit=0&id={HttpContext.Current.Server.UrlEncode(ClsCryptographyBl.EncodeString(intId.ToString()))}\" " +
                             "title=\"EDITA\"><i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\"></i></span></a>";

            }else if (bolCentroCostos)
            {
                strResult += "&nbsp;&nbsp;&nbsp;<a class=\"tooltips\" data-toggle=\"tooltip\" title =\"Editar\"" +
                             $" href=\"{objClsDataTableEl.StrPathEdit}?edit=0&id={HttpContext.Current.Server.UrlEncode(ClsCryptographyBl.EncodeString(intId.ToString()))}\" " +
                             "title=\"EDITA\"><i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\"></i></span></a>";
            }
            if (objClsDataTableEl.BolDelete && bolActivo)
            {
                strResult += "&nbsp;&nbsp;&nbsp;<a class=\"tooltips\" data-toggle=\"tooltip\" title =\"Eliminar\"" +
                             $"href=\"#\" onclick=\"fnConfirDelete('{intId}','{objClsDataTableEl.StrPathDelete}');\" " +
                             "title=\"ELIMINA\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a>&nbsp;&nbsp;&nbsp;";
            }

            

            return new HtmlString(strResult).ToString();
        }

        public static string BolResult(bool bolValue)
        {
            var strHtmlResult = bolValue
                ? "<i class=\"fa fa-check text-success\" aria-hidde=\"true\"></i>"
                : "<i class=\"fa fa-times text-danger\" aria-hidde=\"true\"></i>";
            return new HtmlString(strHtmlResult).ToString();
        }
    }
}
