﻿using System;
using System.Collections.Generic;
using System.Reflection;
using DL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace BL_GA_Comisiones
{
    public class ClsRolBl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsRolBl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsCatRolesEl> GetRols(int intId, string strNombre)
        {
            var lstClsCatRolesEl = new List<ClsCatRolesEl>();

            try
            {
                lstClsCatRolesEl = ClsRolDl.GetRols(intId, strNombre);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId , strNombre }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsCatRolesEl;
        }

        public ClsSimpleResultEl SetRol(int intId, string strNombre)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();
            try
            {
                var objClsCatRolesEl = new ClsCatRolesEl()
                {
                    IntId = intId,
                    StrNombre = strNombre,
                    BolActivo = true,
                    IntIdUsuarioCreacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                    IntIdUsuarioActualizacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                    DtFechaCreacion = DateTime.Now,
                    DtFechaActualizacion = DateTime.Now,
                };

                var objClsRolBl = new ClsRolDl(ObjClsUsuarioActiveDirectoryEl);
                return objClsRolBl.SetRol(objClsCatRolesEl, false);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, strNombre }),
                    DtFecha = DateTime.Now
                });
            }

            return objClsSimpleResultEl;
        }

        public ClsSimpleResultEl DelRol(int intId)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();
            try
            {
                var objClsCatRolesEl = new ClsCatRolesEl()
                {
                    IntId = intId,
                    StrNombre = "",
                    BolActivo = false,
                    IntIdUsuarioCreacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                    IntIdUsuarioActualizacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                    DtFechaCreacion = DateTime.Now,
                    DtFechaActualizacion = DateTime.Now,
                };

                var objClsRolBl = new ClsRolDl(ObjClsUsuarioActiveDirectoryEl);
                return objClsRolBl.SetRol(objClsCatRolesEl, true);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId }),
                    DtFecha = DateTime.Now
                });
            }

            return objClsSimpleResultEl;
        }
    }
}
