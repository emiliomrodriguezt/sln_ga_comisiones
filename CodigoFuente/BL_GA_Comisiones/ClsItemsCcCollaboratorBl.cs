﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using DL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace BL_GA_Comisiones
{
    public class ClsItemsCcCollaboratorBl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsItemsCcCollaboratorBl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsAsocRubosCcColaboradorEl> GetItemsCcCollaborator(int intId, int intIdRubroCc, int intIdColaborador)
        {
            var lstClsAsocRubosCcColaboradorEl = new List<ClsAsocRubosCcColaboradorEl>();
            var objClsItemsCcCollaboratorDl = new ClsItemsCcCollaboratorDl(ObjClsUsuarioActiveDirectoryEl);

            try
            {

                lstClsAsocRubosCcColaboradorEl = objClsItemsCcCollaboratorDl.GetItemsCcCollaborator(intId, intIdRubroCc, intIdColaborador);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdRubroCc, intIdColaborador }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsAsocRubosCcColaboradorEl;
        }

        public ClsSimpleResultEl SetItemsCcCollaborator(int intId, int intIdRubroCc, int intIdColaborador, decimal decPorcentaje)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();
            var objClsItemsCcCollaboratorDl = new ClsItemsCcCollaboratorDl(ObjClsUsuarioActiveDirectoryEl);
            try
            {
                var decPorAsig = objClsItemsCcCollaboratorDl.GetItemsCcCollaborator(0, intIdRubroCc, 0).Sum(itm => itm.DecPorcentaje);
                var decRestante = 1M - decPorAsig;
                var strRestante = $"{decRestante:p2}";
                if ((decPorcentaje / 100) > decRestante)
                {
                    return new ClsSimpleResultEl() {
                        BolSuccess = false,
                        StrMessage = String.Format(StrucMessagesEl.MaxPorcentaje, strRestante)                        
                    };
                }
                
                var id = objClsItemsCcCollaboratorDl.GetItemsCcCollaborator(0, intIdRubroCc, intIdColaborador).Select(itm => itm.IntId).FirstOrDefault();

                if (id > 0)
                {
                    return new ClsSimpleResultEl()
                    {
                        BolSuccess = false,
                        StrMessage = StrucMessagesEl.ErrorDuplicadoColaboradorARubro
                    };
                }

                var objClsAsocRubosCcColaboradorEl = new ClsAsocRubosCcColaboradorEl()
                {
                    IntId = intId,
                    IntIdRubroCc = intIdRubroCc,
                    IntIdColaborador = intIdColaborador,
                    DecPorcentaje = decPorcentaje / 100,
                    IntIdUsuarioCreacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                    DtFechaCreacion = DateTime.Now
                };

                return objClsItemsCcCollaboratorDl.SetItemsCcCollaborator(objClsAsocRubosCcColaboradorEl, false);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdRubroCc, intIdColaborador, decPorcentaje }),
                    DtFecha = DateTime.Now
                });
            }

            return objClsSimpleResultEl;
        }

        public ClsSimpleResultEl DelItemsCcCollaborator(int intId)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();
            var objClsItemsCcCollaboratorDl = new ClsItemsCcCollaboratorDl(ObjClsUsuarioActiveDirectoryEl);
            try
            {
                var objClsAsocRubosCcColaboradorEl = new ClsAsocRubosCcColaboradorEl()
                {
                    IntId = intId,
                    IntIdRubroCc = 0,
                    IntIdColaborador = 0,
                    DecPorcentaje = 0M,
                    IntIdUsuarioCreacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                    DtFechaCreacion = DateTime.Now
                };

                return objClsItemsCcCollaboratorDl.SetItemsCcCollaborator(objClsAsocRubosCcColaboradorEl, true);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId }),
                    DtFecha = DateTime.Now
                });
            }

            return objClsSimpleResultEl;
        }
    }
}
