﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using EL_GA_Comisiones;
using DL_GA_Comisiones;
using Newtonsoft.Json;

namespace BL_GA_Comisiones
{
    public class ClsCatalogosBl
    {
        public List<ClsItemCatalogoEl> GetPuestos()
        {
            List<ClsItemCatalogoEl> listado = null;
            try
            {
                ClsCatalogosDl objDl = new ClsCatalogosDl();
                listado = objDl.GetPuestos();
            }
            catch (Exception e)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 1,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = e.Message,
                    StrPilaEventos = e.StackTrace,
                    StrMensajeInterno = e.InnerException?.Message ?? "",
                    StrPilaEventosInterno = e.InnerException?.StackTrace ?? "",
                    StrEntrada = string.Empty,
                    DtFecha = DateTime.Now
                });
            }

            return listado;
        }

        public List<ClsItemCatalogoEl> GetCentrosCostos(int idCC, int activo, int idGerente)
        {
            List<ClsItemCatalogoEl> listado = null;
            try
            {
                ClsCatalogosDl objDl = new ClsCatalogosDl();
                listado = objDl.GetCentrosCosto(idCC, activo, idGerente);
            }
            catch (Exception e)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 1,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = e.Message,
                    StrPilaEventos = e.StackTrace,
                    StrMensajeInterno = e.InnerException?.Message ?? "",
                    StrPilaEventosInterno = e.InnerException?.StackTrace ?? "",
                    StrEntrada = string.Empty,
                    DtFecha = DateTime.Now
                });
            }

            return listado;
        }

        public List<ClsItemCatalogoEl> GetUNs()
        {
            List<ClsItemCatalogoEl> listado = null;
            
            try
            {
                ClsCatalogosDl objDl = new ClsCatalogosDl();
                listado = objDl.GetUNs();
            }
            catch (Exception e)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 1,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = e.Message,
                    StrPilaEventos = e.StackTrace,
                    StrMensajeInterno = e.InnerException?.Message ?? "",
                    StrPilaEventosInterno = e.InnerException?.StackTrace ?? "",
                    StrEntrada = string.Empty,
                    DtFecha = DateTime.Now
                });
            }

            return listado;
        }

        public List<ClsItemCatalogoEl> GetCentrosCostosHomologados(int idCC, int activo, int idGerente)
        {
            List<ClsItemCatalogoEl> listado = null;
            try
            {
                ClsCatalogosDl objDl = new ClsCatalogosDl();
                listado = objDl.GetCentrosCostoHomologados(idCC, activo, idGerente);
            }
            catch (Exception e)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 1,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = e.Message,
                    StrPilaEventos = e.StackTrace,
                    StrMensajeInterno = e.InnerException?.Message ?? "",
                    StrPilaEventosInterno = e.InnerException?.StackTrace ?? "",
                    StrEntrada = string.Empty,
                    DtFecha = DateTime.Now
                });
            }

            return listado;
        }
    }
}
