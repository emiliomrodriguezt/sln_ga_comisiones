﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace BL_GA_Comisiones
{
    public class ClsCenterCostCollaboratorSasBl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsCenterCostCollaboratorSasBl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsTblCentoCostosColaboradorSasEl> GetCenterCostCollaboratorSas(int intId, int intIdColaborador, int intIdDirector, int intIdCentroCostos, int intAno)
        {
            var lstClsTblCentoCostosColaboradorSasEl = new List<ClsTblCentoCostosColaboradorSasEl>();

            try
            {
                var objClsCenterCostCollaboratorSasDl = new ClsCenterCostCollaboratorSasDl(ObjClsUsuarioActiveDirectoryEl);
                lstClsTblCentoCostosColaboradorSasEl = objClsCenterCostCollaboratorSasDl.GetCenterCostCollaboratorSas(intId, intIdColaborador, intIdDirector, intIdCentroCostos, intAno);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdColaborador, intIdCentroCostos }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsTblCentoCostosColaboradorSasEl;
        }
    }
}
