﻿using System;
using System.Collections.Generic;
using System.Reflection;
using DL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace BL_GA_Comisiones
{
    public class ClsOtherPayBl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsOtherPayBl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsTblOtrosPagosComisionEl> GetsOtherPay(int intId, int intIdComision, int intIdTipoPago, int intIdPago)
        {
            var lstClsTblOtrosPagosComisionEl = new List<ClsTblOtrosPagosComisionEl>();

            try
            {
                var objClsOtherPayDl = new ClsOtherPayDl(ObjClsUsuarioActiveDirectoryEl);
                lstClsTblOtrosPagosComisionEl = objClsOtherPayDl.GetsOtherPay(intId, intIdComision, intIdTipoPago, intIdPago);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdComision, intIdTipoPago, intIdPago }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsTblOtrosPagosComisionEl;
        }
    }
}
