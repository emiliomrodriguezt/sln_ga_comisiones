﻿using System;
using System.Collections.Generic;
using System.Reflection;
using DL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace BL_GA_Comisiones
{
    public class ClsTblEmpleadoBl
    {
        public ClsRespuestaSP AddColaborador(ClsTblEmpleadoEl colaborador)
        {
            ClsRespuestaSP respuesta = new ClsRespuestaSP();

            try
            {
                ClsTblEmpleadoDl objDl = new ClsTblEmpleadoDl();
                respuesta = objDl.SetEmpleado(colaborador);
            }
            catch (Exception e)
            {
                respuesta.Resultado = -1;
                respuesta.Mensaje = e.Message;
            }
            return respuesta;
        }

        public List<ClsTblEmpleadoEl> GetColaborador(int id, int activo, int puesto)
        {
            List<ClsTblEmpleadoEl> listado = null;
            try
            {
                ClsTblEmpleadoDl objDl = new ClsTblEmpleadoDl();
                listado = objDl.GetEmpleado(id, activo, puesto);
            }
            catch (Exception e)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = e.Message,
                    StrPilaEventos = e.StackTrace,
                    StrMensajeInterno = e.InnerException?.Message ?? "",
                    StrPilaEventosInterno = e.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { id, activo, puesto }),
                    DtFecha = DateTime.Now
                });
            }

            return listado;
        }

        public ClsRespuestaSP UpdateColaborador(ClsTblEmpleadoEl colaborador)
        {
            ClsRespuestaSP respuesta = new ClsRespuestaSP();

            try
            {
                ClsTblEmpleadoDl objDl = new ClsTblEmpleadoDl();
                respuesta = objDl.UpdateEmpleado(colaborador);
            }
            catch (Exception e)
            {
                respuesta.Resultado = -1;
                respuesta.Mensaje = e.Message;
            }
            return respuesta;
        }
    }
}
