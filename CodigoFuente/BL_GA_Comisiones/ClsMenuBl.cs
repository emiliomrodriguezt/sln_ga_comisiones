﻿using System;
using System.Linq;
using System.Reflection;
using System.Text;
using DL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace BL_GA_Comisiones
{
    public static class ClsMenuBl
    {
        public static string GetMenu(string strDataSession, string strLocalPath)
        {
            var sbMenu = new StringBuilder();
            try
            {
                var objClsUserEl = JsonConvert.DeserializeObject<ClsUsuarioActiveDirectoryEl>(strDataSession);

                var intIdPageSelect = 0;
                var intIdPageChildrenSelect = 0;

                var objClsCatPaginaEl = ClsPageDl.GetPages(0, strLocalPath).FirstOrDefault();
                if (objClsCatPaginaEl != null)
                {
                    intIdPageSelect = objClsCatPaginaEl.IntIdPagina ?? objClsCatPaginaEl.IntId;
                    intIdPageChildrenSelect = objClsCatPaginaEl.IntIdPagina.HasValue ? objClsCatPaginaEl.IntId : 0;
                }

                var objClsCatRolEl = ClsRolDl.GetRols(objClsUserEl.IntIdRol, "").FirstOrDefault();
                var lstPage = ClsMenunDl.GetMenu(objClsUserEl.IntIdRol);


                sbMenu.Append("<div class=\"visible-xs hidden-sm hidden-md hidden-lg\">");
                sbMenu.Append("<div class=\"media userlogged\">");
                sbMenu.Append("<img alt=\"\" src=\"/images/photos/user.png\" class=\"media-object\">");
                sbMenu.Append("<div class=\"media-body\">");
                sbMenu.Append($"<h4>{objClsUserEl.StrNombre}</h4>");
                sbMenu.Append($"<span>\"{objClsCatRolEl?.StrNombre ?? ""}\"</span>");
                sbMenu.Append("</div>");
                sbMenu.Append("</div>");
                sbMenu.Append("<h5 class=\"sidebartitle\">Cuenta</h5>");
                sbMenu.Append("<ul class=\"nav nav-pills nav-stacked nav-bracket mb30\">");
                sbMenu.Append("<li><a href=\"SingOut.aspx\"><i class=\"fa fa-sign-out\"></i> <span>Salir</span></a></li>");
                sbMenu.Append("</ul>");
                sbMenu.Append("</div>");
                sbMenu.Append("<h5 class=\"sidebartitle\">MENU</h5>");
                sbMenu.Append("<ul class=\"nav nav-pills nav-stacked nav-bracket\">");
                foreach (var itm in lstPage.Where(x => x.IntIdPagina.Equals(null)).OrderBy(x => x.IntOrden))
                {
                    var lstChildrenPage = lstPage.Where(x => x.IntIdPagina.Equals(itm.IntId)).OrderBy(x => x.IntOrden);
                    if (lstChildrenPage.Any())
                    {
                        sbMenu.Append($"<li class=\"nav-parent tooltips {(intIdPageSelect == itm.IntId ? " active" : "")}\" data-toggle=\"tooltip\" title=\"{itm.StrDescripcion}\" >");
                        sbMenu.Append($"<a href=\"{itm.StrUrl}\">");
                        sbMenu.Append($"<i class=\"{itm.StrIcono}\"></i> <span>{itm.StrNombre}</span>");
                        sbMenu.Append("</a>");
                        sbMenu.Append("<ul class=\"children\">");
                        foreach (var itmx in lstChildrenPage)
                        {
                            sbMenu.Append($"<li class=\"tooltips {(intIdPageChildrenSelect == itmx.IntId ? " active" : "")}\" >");
                            sbMenu.Append($"<a href =\"{itmx.StrUrl}\" >");
                            sbMenu.Append($"<i class=\"{itmx.StrIcono}\"></i>{itmx.StrNombre}");
                            sbMenu.Append("</a>");
                            sbMenu.Append("</li>");
                        }
                        sbMenu.Append("</ul>");
                        sbMenu.Append("</li>");
                    }
                    else
                    {
                        sbMenu.Append($"<li class=\"tooltips {(intIdPageSelect == itm.IntId ? " active" : "")}\" data-toggle=\"tooltip\" title=\"{itm.StrDescripcion}\" >");
                        sbMenu.Append($"<a href=\"{itm.StrUrl}\">");
                        sbMenu.Append($"<i class=\"{itm.StrIcono}\"></i> <span>{itm.StrNombre}</span>");
                        sbMenu.Append("</a>");
                        sbMenu.Append("</li>");
                    }
                }
                sbMenu.Append("</ul>");
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { strDataSession, strLocalPath }),
                    DtFecha = DateTime.Now
                });
            }

            return sbMenu.ToString();
        }
    }
}
