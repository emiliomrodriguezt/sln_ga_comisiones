﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace BL_GA_Comisiones
{
    public class ClsBonusesBl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsBonusesBl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsTblBonosEl> GetBonuses(int intId, int intIdColaborador)
        {
            var lstClsTblBonosEl = new List<ClsTblBonosEl>();

            try
            {
                var objClsBonusesDl = new ClsBonusesDl(ObjClsUsuarioActiveDirectoryEl);
                lstClsTblBonosEl = objClsBonusesDl.GetBonuses(intId, intIdColaborador);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdColaborador }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsTblBonosEl;
        }

        public ClsSimpleResultEl SetBonus(int intId, int intIdColaborador, decimal decMontoBono, int intAnio, bool bolPagado)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();
            try
            {
                if (!bolPagado)
                {
                    var objClsTblAnticiposEl = new ClsTblBonosEl()
                    {
                        IntId = intId,
                        IntIdColaborador = intIdColaborador,
                        DecMontoBono = decMontoBono,
                        IntAnio = intAnio,
                        BolPagado = false,
                        BolActivo = true,
                        IntIdUsuarioCrecion = ObjClsUsuarioActiveDirectoryEl.IntId,
                        IntIdUsuarioActulizacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                        DtFechaCreacion = DateTime.Now,
                        DtFechaActulizacion = DateTime.Now
                    };

                    var objClsBonusesDl = new ClsBonusesDl(ObjClsUsuarioActiveDirectoryEl);
                    return objClsBonusesDl.SetBonus(objClsTblAnticiposEl, false);
                }

                return new ClsSimpleResultEl()
                {
                    BolSuccess = false,
                    StrMessage = StrucMessagesEl.BonoUpdatePagado
                };
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new
                    {
                        intId,
                        intIdColaborador,
                        decMontoBono,
                        intAnio,
                        bolPagado
                    }),
                    DtFecha = DateTime.Now
                });
            }

            return objClsSimpleResultEl;
        }

        public ClsSimpleResultEl DelBonus(int intId)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();
            try
            {
                var objClsTblAnticiposEl = new ClsTblBonosEl()
                {
                    IntId = intId,
                    IntIdColaborador = 0,
                    DecMontoBono = 0M,
                    IntAnio = 0,
                    BolPagado = false,
                    BolActivo = false,
                    IntIdUsuarioCrecion = ObjClsUsuarioActiveDirectoryEl.IntId,
                    IntIdUsuarioActulizacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                    DtFechaCreacion = DateTime.Now,
                    DtFechaActulizacion = DateTime.Now
                };

                var objClsBonusesDl = new ClsBonusesDl(ObjClsUsuarioActiveDirectoryEl);
                var objClsTblBonosEl = objClsBonusesDl.GetBonuses(intId, 0).FirstOrDefault();

                if (objClsTblBonosEl != null && !objClsTblBonosEl.BolPagado)
                {
                    return objClsBonusesDl.SetBonus(objClsTblAnticiposEl, true);
                }

                return new ClsSimpleResultEl()
                {
                    BolSuccess = false,
                    StrMessage = StrucMessagesEl.BonoDeletePagado
                };
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId }),
                    DtFecha = DateTime.Now
                });
            }

            return objClsSimpleResultEl;
        }
    }
}
