﻿using System;
using System.Collections.Generic;
using System.Reflection;
using DL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace BL_GA_Comisiones
{
    public class ClsCommissionCalculationBl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsCommissionCalculationBl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsTblCalculoComisionEl> GetCommissionCalculation(int intId, int intIdColaborador, int intAnio)
        {
            var lstClsTblCalculoComisionEl = new List<ClsTblCalculoComisionEl>();

            try
            {
                var objClsCommissionCalculationDl = new ClsCommissionCalculationDl(ObjClsUsuarioActiveDirectoryEl);
                lstClsTblCalculoComisionEl = objClsCommissionCalculationDl.GetCommissionCalculation(intId, intIdColaborador, intAnio);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdColaborador, intAnio }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsTblCalculoComisionEl;
        }

        public ClsSimpleResultEl UpdateSigning(int intId, string strFirmaContraloria, string strFirmaDirectorUn, string strFirmaDirectorGrl, string strFirmaPresidente, byte[] arrBytDocument)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();
            try
            {
                var objClsCommissionCalculationDl = new ClsCommissionCalculationDl(ObjClsUsuarioActiveDirectoryEl);
                return objClsCommissionCalculationDl.UpdateSigning(intId, strFirmaContraloria, strFirmaDirectorUn, strFirmaDirectorGrl, strFirmaPresidente, arrBytDocument);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new
                    {
                        intId,
                        strFirmaContraloria,
                        strFirmaDirectorUn,
                        strFirmaDirectorGrl,
                        strFirmaPresidente
                    }),
                    DtFecha = DateTime.Now
                });
            }

            return objClsSimpleResultEl;
        }

    }
}
