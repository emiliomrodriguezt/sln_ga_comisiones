﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace BL_GA_Comisiones
{
    public class ClsAdvancesBl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsAdvancesBl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsTblAnticiposEl> GetAdvances(int intId, int intIdColaborador, int intIdCentroCostos)
        {
            var lstClsTblAnticiposEl = new List<ClsTblAnticiposEl>();

            try
            {
                var objClsAdvancesDl = new ClsAdvancesDl(ObjClsUsuarioActiveDirectoryEl);
                lstClsTblAnticiposEl = objClsAdvancesDl.GetAdvances(intId, intIdColaborador, intIdCentroCostos);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdColaborador, intIdCentroCostos }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsTblAnticiposEl;
        }

        public ClsSimpleResultEl SetAdvance(int intId, int intIdColaborador, int intIdCentroCostos, decimal decImporteBruto, decimal decImporteNeto,
                                            string strNoEmpresa, string strNoChequeTransferencia, DateTime dtFechaPago, string strNoPolizaContable, decimal decAcomulado)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();
            try
            {
                var objClsAdvancesDl = new ClsAdvancesDl(ObjClsUsuarioActiveDirectoryEl);

                if (intId > 0)
                {
                    var objClsTblAnticiposEl1 = objClsAdvancesDl.GetAdvances(intId, 0, 0).FirstOrDefault();
                    if (objClsTblAnticiposEl1 != null && objClsTblAnticiposEl1.IntId > 0)
                    {
                        if (objClsTblAnticiposEl1.DecAcomulado > 0)
                        {
                            return new ClsSimpleResultEl()
                            {
                                BolSuccess = false,
                                StrMessage = StrucMessagesEl.AnticipoUpdatePagado
                            };
                        }
                    }

                }

                var objClsTblAnticiposEl = new ClsTblAnticiposEl()
                {
                    IntId = intId,
                    IntIdColaborador = intIdColaborador,
                    IntIdCentroCostos = intIdCentroCostos,
                    DecImporteBruto = decImporteBruto,
                    DecImporteNeto = decImporteNeto,
                    StrNoEmpresa = strNoEmpresa,
                    StrNoChequeTransferencia = strNoChequeTransferencia,
                    DtFechaPago = dtFechaPago,
                    StrNoPolizaContable = strNoPolizaContable,
                    DecAcomulado = decAcomulado,
                    BolActivo = true,
                    IntIdUsuarioCreacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                    IntIdUsuarioActualizacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                    DtFechaCreacion = DateTime.Now,
                    DtFechaActualizacion = DateTime.Now
                };

                
                return objClsAdvancesDl.SetAdvance(objClsTblAnticiposEl, false);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new {
                        intId,
                        intIdColaborador,
                        intIdCentroCostos,
                        decImporteBruto,
                        decImporteNeto,
                        strNoEmpresa,
                        strNoChequeTransferencia,
                        dtFechaPago,
                        strNoPolizaContable,
                        decAcomulado
                    }),
                    DtFecha = DateTime.Now
                });
            }

            return objClsSimpleResultEl;
        }

        public ClsSimpleResultEl DelAdvance(int intId)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();
            try
            {
                var objClsAdvancesDl = new ClsAdvancesDl(ObjClsUsuarioActiveDirectoryEl);

                if (intId > 0)
                {
                    var objClsTblAnticiposEl1 = objClsAdvancesDl.GetAdvances(intId, 0, 0).FirstOrDefault();
                    if (objClsTblAnticiposEl1 != null && objClsTblAnticiposEl1.IntId > 0)
                    {
                        if (objClsTblAnticiposEl1.DecAcomulado > 0)
                        {
                            return new ClsSimpleResultEl()
                            {
                                BolSuccess = false,
                                StrMessage = StrucMessagesEl.AnticipoDeletePagado
                            };
                        }
                    }

                }

                var objClsTblAnticiposEl = new ClsTblAnticiposEl()
                {
                    IntId = intId,
                    IntIdColaborador = 0,
                    IntIdCentroCostos = 0,
                    DecImporteBruto = 0,
                    DecImporteNeto = 0,
                    StrNoEmpresa = string.Empty,
                    StrNoChequeTransferencia = string.Empty,
                    DtFechaPago = DateTime.Now,
                    StrNoPolizaContable = string.Empty,
                    DecAcomulado = 0,
                    BolActivo = false,
                    IntIdUsuarioCreacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                    IntIdUsuarioActualizacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                    DtFechaCreacion = DateTime.Now,
                    DtFechaActualizacion = DateTime.Now
                };

                return objClsAdvancesDl.SetAdvance(objClsTblAnticiposEl, true);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId }),
                    DtFecha = DateTime.Now
                });
            }

            return objClsSimpleResultEl;
        }
    }
}
