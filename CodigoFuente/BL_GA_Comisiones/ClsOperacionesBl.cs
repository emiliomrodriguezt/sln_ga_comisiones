﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EL_GA_Comisiones;
using DL_GA_Comisiones;
using System.Reflection;
using Newtonsoft.Json;

namespace BL_GA_Comisiones
{
    public class ClsOperacionesBl
    {
        public List<ClsTblMUBCalculadoProyectoEl> GetResultadoMUB(int anio)
        {
            var resultados = new List<ClsTblMUBCalculadoProyectoEl>();
            try
            {
                ClsOperacionesDl objDl = new ClsOperacionesDl();
                resultados = objDl.GetResultadoMUB(anio);

            }catch(Exception e)
            {

            }

            return resultados;
        }

        public ClsRespuestaSP ExecCalculoMUB(int anio, int periodo, int usuario)
        {
            ClsRespuestaSP respuesta = new ClsRespuestaSP();
            try
            {
                ClsOperacionesDl objDl = new ClsOperacionesDl();
                respuesta = objDl.ClearResultadoMUB(anio, periodo, usuario);
                respuesta = objDl.ExecCalculoMUB(anio, periodo, usuario);
            }catch(Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { anio, periodo, usuario }),
                    DtFecha = DateTime.Now
                });

                respuesta.Resultado = -1;
                respuesta.Mensaje = ex.Message;
            }

            return respuesta;
        }

        public List<ClsTblSASCalculadoColaboradorEl> GetResultadoSAS(int anio)
        {
            var resultados = new List<ClsTblSASCalculadoColaboradorEl>();
            try
            {
                ClsOperacionesDl objDl = new ClsOperacionesDl();
                resultados = objDl.GetResultadoSAS(anio);

            }
            catch (Exception e)
            {

            }

            return resultados;
        }

        public ClsRespuestaSP ExecCalculoSAS(int anio, int usuario)
        {
            ClsRespuestaSP respuesta = new ClsRespuestaSP();
            try
            {
                ClsOperacionesDl objDl = new ClsOperacionesDl();
                respuesta = objDl.ClearResultadoSAS(anio, usuario);
                respuesta = objDl.ExecCalculoSAS(anio, usuario);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { anio, usuario }),
                    DtFecha = DateTime.Now
                });

                respuesta.Resultado = -1;
                respuesta.Mensaje = ex.Message;
            }

            return respuesta;
        }

        public List<ClsProyectosComisionEl> GetProyectosComision(int usuario, int idCol, int anio, int mes)
        {
            List<ClsProyectosComisionEl> listado = null;

            try
            {
                ClsOperacionesDl objDl = new ClsOperacionesDl();
                listado = objDl.GetProyectosComision(usuario, idCol, anio, mes);
            }catch(Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { usuario, idCol, anio, mes }),
                    DtFecha = DateTime.Now
                });
            }

            return listado;
        }

        public List<ClsOtrosPagosComisionEl> GetOtrosPagosComision(int usuario, int idCol, int anio)
        {
            List<ClsOtrosPagosComisionEl> listado = null;

            try
            {
                ClsOperacionesDl objDl = new ClsOperacionesDl();
                listado = objDl.GetOtrosPagosComision(usuario, idCol).Where(x=>x.Anio<=anio).ToList();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { usuario, idCol, anio }),
                    DtFecha = DateTime.Now
                });
            }

            return listado;
        }

        public ClsRespuestaSP SetNvaComision(ClsTblCalculoComisionEl comision, List<ClsTblCentroCostosComision> proyectos, List<ClsTblOtrosPagosComisionPrevEl> otrosPagos)
        {
            ClsRespuestaSP respuesta = new ClsRespuestaSP();

            try
            {
                ClsOperacionesDl objDl = new ClsOperacionesDl();
                respuesta = objDl.SetNuevaComision(comision);

                if(respuesta.Resultado != 0)
                {
                    respuesta.Mensaje = respuesta.Mensaje.Replace("Error al ejecutar consulta: ", string.Empty);
                    return respuesta;
                }

                int idComision = respuesta.Valor;

                foreach(ClsTblCentroCostosComision proy in proyectos)
                {
                    proy.IdComision = idComision;
                    respuesta = objDl.SetProyectoComision(comision.IdColaborador, comision.Anio, proy);

                    if (respuesta.Resultado != 0)
                    {
                        respuesta.Mensaje = "Ocurrió un error al guardar el proyecto";
                        return respuesta;
                    }
                }

                foreach(ClsTblOtrosPagosComisionPrevEl pago in otrosPagos)
                {
                    pago.IdComision = idComision;
                    respuesta = objDl.SetOtroPagoComision(pago);

                    if(respuesta.Resultado != 0)
                    {
                        respuesta.Mensaje = "Ocurrió un error al guardar el pago adicional";
                        return respuesta;
                    }
                }

                respuesta = objDl.UpdateMontosComision(idComision);
                respuesta.Valor = idComision;
            }
            catch (Exception e)
            {
                respuesta.Resultado = -1;
                respuesta.Mensaje = e.Message;
            }
            return respuesta;
        }

        public List<ClsComisionSinAutorizarEl> GetComisionesSinAutorizar()
        {
            var resultados = new List<ClsComisionSinAutorizarEl>();
            try
            {
                ClsOperacionesDl objDl = new ClsOperacionesDl();
                resultados = objDl.GetComisionesSinAutorizar();

            }
            catch (Exception e)
            {

            }

            return resultados;
        }

        public ClsRespuestaSP ActorizarComision(int idComision, int usuario)
        {
            ClsRespuestaSP respuesta = new ClsRespuestaSP()
            {
                Resultado = 0
            };

            try
            {
                ClsOperacionesDl objDl = new ClsOperacionesDl();
                respuesta = objDl.AutorizarComision(idComision, usuario);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { idComision, usuario }),
                    DtFecha = DateTime.Now
                });

                respuesta.Resultado = -1;
                respuesta.Mensaje = ex.Message;
            }

            return respuesta;
        }

        public List<ClsComisionesPagadas> GetComisionesPagadas(int idCol, int anio)
        {
            var resultados = new List<ClsComisionesPagadas>();
            try
            {
                ClsOperacionesDl objDl = new ClsOperacionesDl();
                resultados = objDl.GetComisionesPagadas(idCol, anio);

            }
            catch (Exception e)
            {

            }

            return resultados;
        }

        public string GetUltimoCalculoMUB(int anio, int usuario)
        {
            string respuesta = string.Empty;
            try
            {
                ClsOperacionesDl objDl = new ClsOperacionesDl();
                respuesta = objDl.GetUltimoCalculoMUB(anio, usuario);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { anio, usuario }),
                    DtFecha = DateTime.Now
                });
            }

            return respuesta;
        }
    }
}
