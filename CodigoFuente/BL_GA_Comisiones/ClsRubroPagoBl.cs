﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EL_GA_Comisiones;
using DL_GA_Comisiones;

namespace BL_GA_Comisiones
{
    public class ClsRubroPagoBl
    {
        public List<ClsRubroPagoEl> GetRubroPago(int id, int activo)
        {
            List<ClsRubroPagoEl> listado = null;
            try
            {
                ClsRubroPagoDl objDl = new ClsRubroPagoDl();
                listado = objDl.GetRuboPago(id, activo);
            }
            catch (Exception e)
            {

            }

            return listado;
        }

        public ClsRespuestaSP AddRubroPago(ClsRubroPagoEl rubro)
        {
            ClsRespuestaSP respuesta = new ClsRespuestaSP();

            try
            {
                ClsRubroPagoDl objDl = new ClsRubroPagoDl();
                respuesta = objDl.SetRubroPago(rubro);
            }
            catch (Exception e)
            {
                respuesta.Resultado = -1;
                respuesta.Mensaje = e.Message;
            }
            return respuesta;
        }

        public ClsRespuestaSP UpdateRubroPago(ClsRubroPagoEl rubro)
        {
            ClsRespuestaSP respuesta = new ClsRespuestaSP();

            try
            {
                ClsRubroPagoDl objDl = new ClsRubroPagoDl();
                respuesta = objDl.UpdateRubroPago(rubro);
            }
            catch (Exception e)
            {
                respuesta.Resultado = -1;
                respuesta.Mensaje = e.Message;
            }
            return respuesta;
        }
    }
}
