﻿using System;
using System.Collections.Generic;
using System.Reflection;
using DL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace BL_GA_Comisiones
{
    public class ClsCommissionCenterCostBl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsCommissionCenterCostBl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsTblCentroCostosComisonEl> GetCommissionCenterCost(int intId, int intIdComision, int intIdCentroCostos)
        {
            var lstClsTblCentroCostosComisonEl = new List<ClsTblCentroCostosComisonEl>();

            try
            {
                var objClsCommissionCenterCostDl = new ClsCommissionCenterCostDl(ObjClsUsuarioActiveDirectoryEl);
                lstClsTblCentroCostosComisonEl = objClsCommissionCenterCostDl.GetCommissionCenterCost(intId, intIdComision, intIdCentroCostos);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdComision, intIdCentroCostos }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsTblCentroCostosComisonEl;
        }

        public ClsSimpleResultEl UpdateObservaciones(int intId, string strObservaciones)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();

            try
            {
                var objClsCommissionCenterCostDl = new ClsCommissionCenterCostDl(ObjClsUsuarioActiveDirectoryEl);
                objClsSimpleResultEl = objClsCommissionCenterCostDl.UpdateObservaciones(intId, strObservaciones);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, strObservaciones }),
                    DtFecha = DateTime.Now
                });
            }

            return objClsSimpleResultEl;
        }
    }
}
