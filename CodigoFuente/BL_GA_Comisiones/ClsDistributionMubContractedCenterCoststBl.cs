﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using DL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace BL_GA_Comisiones
{
    public class ClsDistributionMubContractedCenterCoststBl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsDistributionMubContractedCenterCoststBl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsAsocCenterCostosColaboradorMubEl> GetMubCenterCostColaborador(int intId, int intIdCentroCostos, int intIdGerente, int intIdDirector)
        {
            var lstClsAsocCenterCostosColaboradorMubEl = new List<ClsAsocCenterCostosColaboradorMubEl>();
            var objClsDistributionMubContractedCenterCoststDl = new ClsDistributionMubContractedCenterCoststDl(ObjClsUsuarioActiveDirectoryEl);

            try
            {

                lstClsAsocCenterCostosColaboradorMubEl = objClsDistributionMubContractedCenterCoststDl.GetMubCenterCostColaborador(intId, intIdCentroCostos, intIdGerente, intIdDirector);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdCentroCostos, intIdGerente, intIdDirector }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsAsocCenterCostosColaboradorMubEl;
        }

        public ClsSimpleResultEl SetMubCenterCostColaborador(int intId, int intIdCentroCostos, int intIdGerente, int intIdDirector, decimal decPorcentaje)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();
            var objClsDistributionMubContractedCenterCoststDl = new ClsDistributionMubContractedCenterCoststDl(ObjClsUsuarioActiveDirectoryEl);
            try
            {
                var decPorAsig = objClsDistributionMubContractedCenterCoststDl.GetMubCenterCostColaborador(0, intIdCentroCostos, 0, 0).Sum(itm => itm.DecPorcentaje);
                var decRestante = 1M - decPorAsig;
                var strRestante = $"{decRestante:p2}";
                if ((decPorcentaje / 100) > decRestante)
                {
                    return new ClsSimpleResultEl()
                    {
                        BolSuccess = false,
                        StrMessage = String.Format(StrucMessagesEl.MaxPorcentaje, strRestante)
                    };
                }

                var id = objClsDistributionMubContractedCenterCoststDl.GetMubCenterCostColaborador(0, intIdCentroCostos, intIdGerente, 0).Select(itm => itm.IntId).FirstOrDefault();

                if (id > 0)
                {
                    return new ClsSimpleResultEl()
                    {
                        BolSuccess = false,
                        StrMessage = StrucMessagesEl.ErrorDuplicadoMubSas
                    };
                }

                var clsClsAsocCenterCostosColaboradorMubEl = new ClsAsocCenterCostosColaboradorMubEl()
                {
                    IntId = intId,
                    IntIdCentroCostos = intIdCentroCostos,
                    IntIdGerente = intIdGerente,
                    IntIdDirector = intIdDirector,
                    DecPorcentaje = decPorcentaje / 100,
                    IntIdUsuarioCreacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                    DtFechaCreacion = DateTime.Now
                };

                return objClsDistributionMubContractedCenterCoststDl.SetMubCenterCostColaborador(clsClsAsocCenterCostosColaboradorMubEl, false);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdCentroCostos, intIdGerente, intIdDirector, decPorcentaje }),
                    DtFecha = DateTime.Now
                });
            }

            return objClsSimpleResultEl;
        }

        public ClsSimpleResultEl DelMubCenterCostColaborador(int intId)
        {
            var objClsDistributionMubContractedCenterCoststDl = new ClsDistributionMubContractedCenterCoststDl(ObjClsUsuarioActiveDirectoryEl);
            var objClsSimpleResultEl = new ClsSimpleResultEl();
            try
            {
                var clsClsAsocCenterCostosColaboradorMubEl = new ClsAsocCenterCostosColaboradorMubEl()
                {
                    IntId = intId,
                    IntIdCentroCostos = 0,
                    IntIdGerente = 0,
                    IntIdDirector = 0,
                    DecPorcentaje = 0M,
                    IntIdUsuarioCreacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                    DtFechaCreacion = DateTime.Now
                };

                return objClsDistributionMubContractedCenterCoststDl.SetMubCenterCostColaborador(clsClsAsocCenterCostosColaboradorMubEl, true);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId }),
                    DtFecha = DateTime.Now
                });
            }

            return objClsSimpleResultEl;
        }
    }
}
