﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using DL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace BL_GA_Comisiones
{
    public class ClsSasCollaboratorBl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsSasCollaboratorBl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsTblSASCalculadoColaboradorEl> GetSasCollaborator(int intId, int intIdColaborador, int intAnio)
        {
            var lstClsTblSasCalculadoColaboradorEl = new List<ClsTblSASCalculadoColaboradorEl>();

            try
            {
                var objClsSasCollaboratorDl = new ClsSasCollaboratorDl(ObjClsUsuarioActiveDirectoryEl);
                lstClsTblSasCalculadoColaboradorEl = objClsSasCollaboratorDl.GetSasCollaborator(intId, intIdColaborador, intAnio);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdColaborador, intAnio }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsTblSasCalculadoColaboradorEl;
        }
    }
}
