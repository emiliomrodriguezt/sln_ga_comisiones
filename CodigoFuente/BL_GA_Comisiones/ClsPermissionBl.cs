﻿using System;
using System.Collections.Generic;
using System.Reflection;
using DL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace BL_GA_Comisiones
{
    public class ClsPermissionBl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsPermissionBl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsPermissionEl> GetPermits(int intIdRol)
        {
            var lstClsPermissionEl = new List<ClsPermissionEl>();

            try
            {
                var objClsPermissionDl = new ClsPermissionDl();
                return objClsPermissionDl.GetPermits(intIdRol);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intIdRol }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsPermissionEl;

            
        }

        public List<ClsSimpleResultEl> SetPermits(List<ClsAddPermissionEl> lstClsAddPermissionEl)
        {
            var lstClsSimpleResultEl = new List<ClsSimpleResultEl>();
            var objClsAsocRolPaginaDl = new ClsAsocRolPaginaDl(ObjClsUsuarioActiveDirectoryEl);

            try
            {
                foreach (var itm in lstClsAddPermissionEl)
                {
                    if (itm.BolAcces)
                    {
                        var objClsSimpleResultEl = objClsAsocRolPaginaDl.SetRol(new ClsAsocRolPaginaEl()
                        {
                            IntId = itm.IntIdPermiso,
                            IntIdRol = itm.IntIdRol,
                            IntIdPagina = itm.IntIdPagina,
                            BolEditar = itm.BolEdit,
                            IntIdUsuarioCreacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                            DtFechaCreacion = DateTime.Now
                        }, false);
                        objClsSimpleResultEl.IntId = itm.IntIdPagina;
                        lstClsSimpleResultEl.Add(objClsSimpleResultEl);
                    }
                    else
                    {
                        if (itm.IntIdPermiso > 0)
                        {
                            var objClsSimpleResultEl = objClsAsocRolPaginaDl.SetRol(new ClsAsocRolPaginaEl()
                            {
                                IntId = itm.IntIdPermiso,
                                IntIdRol = itm.IntIdRol,
                                IntIdPagina = itm.IntIdPagina,
                                BolEditar = itm.BolEdit,
                                IntIdUsuarioCreacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                                DtFechaCreacion = DateTime.Now
                            }, true);

                            objClsSimpleResultEl.IntId = itm.IntIdPagina;
                            lstClsSimpleResultEl.Add(objClsSimpleResultEl);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = ObjClsUsuarioActiveDirectoryEl.IntId,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(lstClsAddPermissionEl),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsSimpleResultEl;
        }
    }
}
