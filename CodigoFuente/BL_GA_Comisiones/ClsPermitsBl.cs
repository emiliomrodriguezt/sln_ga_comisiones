﻿using System;
using System.Linq;
using System.Reflection;
using DL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace BL_GA_Comisiones
{
    public class ClsPermitsBl
    {
        public static ClsAsocRolPaginaEl Getpermission(int intIdRol, string strUrl)
        {
            var objClsAsocRolPaginaEl = new ClsAsocRolPaginaEl();

            try
            {
                objClsAsocRolPaginaEl = ClsPermitsDl.GetPermisos(0, intIdRol, strUrl).FirstOrDefault();
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { strUrl }),
                    DtFecha = DateTime.Now
                });
            }

            return objClsAsocRolPaginaEl;
        }
    }
}
