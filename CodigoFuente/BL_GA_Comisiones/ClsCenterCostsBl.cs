﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using DL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;
using Utility_GA_Comisiones;

namespace BL_GA_Comisiones
{
    public class ClsCenterCostsBl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsCenterCostsBl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsCatCentroCostosEl> GetCenterCosts(int intId, string strName)
        {
            var lstClsCatCentroCostosEl = new List<ClsCatCentroCostosEl>();

            try
            {
                var obClsCenterCostsDl = new ClsCenterCostsDl(ObjClsUsuarioActiveDirectoryEl);
                lstClsCatCentroCostosEl = obClsCenterCostsDl.GetCenterCosts(intId, strName);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, strName }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsCatCentroCostosEl;
        }

        public ClsSimpleResultEl ComplementCenterCost(int intId, DateTime dtFechaPayBack, decimal decIngreso, decimal decEgreso, decimal decMubContratado,
                                                      string strObservacion, int intIdDirector, int intIdGerente, int intAnoCreacion, bool bolActivo)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();
            try
            {
                var objClsCatCentroCostosEl = new ClsCatCentroCostosEl()
                {
                    IntId = intId,
                    IntIdDirector = intIdDirector,
                    IntIdGerente = intIdGerente,
                    DtFechaPayBack = dtFechaPayBack,
                    DecIngresosContrato = decIngreso,
                    DecEgresoContrato = decEgreso,
                    DecMubContrato = decMubContratado,
                    StrObservaciones = strObservacion,
                    IntAnoCreacion = intAnoCreacion,
                    BolActivo = bolActivo,
                    IntIdUsuarioCreacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                    IntIdUsuarioActualizacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                    DtFechaCreacion = DateTime.Now,
                    DtFechaActualizacion = DateTime.Now,
                };

                var objClsCenterCostsDl = new ClsCenterCostsDl(ObjClsUsuarioActiveDirectoryEl);
                return objClsCenterCostsDl.ComplementCenterCost(objClsCatCentroCostosEl);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new
                    {
                        intId,
                        dtFechaPayBack,
                        decIngreso,
                        decEgreso,
                        decMubContratado,
                        strObservacion,
                        intIdDirector,
                        intIdGerente,
                        intAnoCreacion
                    }),
                    DtFecha = DateTime.Now
                });
            }

            return objClsSimpleResultEl;
        }

        public MemoryStream GetCenterCostsExport(int intId, string strName)
        {
            var objMemoryStream = new MemoryStream();

            try
            {
                var obClsCenterCostsDl = new ClsCenterCostsDl(ObjClsUsuarioActiveDirectoryEl);
                objMemoryStream = ClsDataTableToExcelBl.ExportToExcel(obClsCenterCostsDl.GetCenterCosts(intId, strName));
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, strName }),
                    DtFecha = DateTime.Now
                });
            }

            return objMemoryStream;
        }

        public List<ClsResultEl> UpdateCenterCosts()
        {
            var lstClsResultUpdateCenterCostsEl = new List<ClsResultEl>();
            var lstIdFind = new List<int>();
            var lstStrIdInsert = new List<string>();

            try
            {
                var objClsCenterCostsDl = new ClsCenterCostsDl(ObjClsUsuarioActiveDirectoryEl);
                var lstClsCenterCostOrgEl = objClsCenterCostsDl.GetCenterCostsExternal();
                var lstClsCentercostInsertEl = objClsCenterCostsDl.GetCenterCostsInternal();

                //Se recorreo todos los centros de costos obtenidos de la fuente original
                foreach (var itm in lstClsCenterCostOrgEl)
                {
                    //Se busca por id externo que el elemento se encuentre en la lista de de centro de costos originales

                    var objClsCentercostInsertEl = lstClsCentercostInsertEl.Find(x => x.ObjClsCenterCostOrgEl.StrId.Equals(itm.StrId));

                    if (objClsCentercostInsertEl != null)
                    {
                        //Existe Lo marcamos para no borrarlo
                        if (objClsCentercostInsertEl.IntId > 0)
                        {
                            lstIdFind.Add(objClsCentercostInsertEl.IntId);
                        }

                        //Existe, comparamos los objetos
                        if (!EqualsObjet(itm, objClsCentercostInsertEl.ObjClsCenterCostOrgEl))
                        {
                            //No son iguales se actuliza centro de costos
                            var objClsSimpleResult = objClsCenterCostsDl.SetCenterCost(new ClsCentercostInsertEl()
                            {
                                IntId = objClsCentercostInsertEl.IntId,
                                BolActivo = itm.StrEstatus.ToUpper().Contains("ABIERTO"),
                                IntIdUsuarioCreacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                                DtFechaCreacion = DateTime.Now,
                                IntIdUsuarioActualizacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                                DtFechaActualizacion = DateTime.Now,
                                ObjClsCenterCostOrgEl = itm
                            });

                            lstClsResultUpdateCenterCostsEl.Add(new ClsResultEl()
                            {
                                BolSuccess = objClsSimpleResult.BolSuccess,
                                StrMessage = objClsSimpleResult.BolSuccess ? StrucMessagesEl.UpdateCc : StrucMessagesEl.ErrorCc,
                                StrId = itm.StrId,
                            });
                        }
                        else
                        {
                            lstClsResultUpdateCenterCostsEl.Add(new ClsResultEl()
                            {
                                BolSuccess = true,
                                StrMessage = StrucMessagesEl.IgnorateCc,
                                StrId = itm.StrId,
                            });
                        }
                    }
                    else
                    {
                        //No existe el centro de costos, se crea
                        //Se asegura que ese id no haya sido poreceda en este bloque de actulizacion
                        if (!lstStrIdInsert.Contains(itm.StrId))
                        {
                            var objClsSimpleResult = objClsCenterCostsDl.SetCenterCost(new ClsCentercostInsertEl()
                            {
                                IntId = 0,
                                BolActivo = itm.StrEstatus.ToUpper().Contains("ABIERTO"),
                                IntIdUsuarioCreacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                                DtFechaCreacion = DateTime.Now,
                                IntIdUsuarioActualizacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                                DtFechaActualizacion = DateTime.Now,
                                ObjClsCenterCostOrgEl = itm,
                            });
                            //Lo marco como procesado pro si existe duplpicado en la base origen no se duplique en la base destino
                            if (objClsSimpleResult.BolSuccess)
                            {
                                lstStrIdInsert.Add(itm.StrId);
                            }
                            lstClsResultUpdateCenterCostsEl.Add(new ClsResultEl()
                            {
                                BolSuccess = objClsSimpleResult.BolSuccess,
                                StrMessage = objClsSimpleResult.BolSuccess ? StrucMessagesEl.AddCc : StrucMessagesEl.ErrorCc,
                                StrId = itm.StrId,
                            });
                        }
                        else
                        {
                            lstClsResultUpdateCenterCostsEl.Add(new ClsResultEl()
                            {
                                BolSuccess = false,
                                StrMessage = StrucMessagesEl.DuplicateCc,
                                StrId = itm.StrId,
                            });
                        }
                    }
                }

                foreach (var itm in lstClsCentercostInsertEl)
                {
                    //si no se encontro en la ista de Debe marcarse como borrado
                    if (!lstIdFind.Contains(itm.IntId))
                    {
                        var objClsSimpleResult = objClsCenterCostsDl.SetCenterCost(new ClsCentercostInsertEl()
                        {
                            IntId = itm.IntId,
                            BolActivo = false,
                            IntIdUsuarioCreacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                            DtFechaCreacion = DateTime.Now,
                            IntIdUsuarioActualizacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                            DtFechaActualizacion = DateTime.Now,
                            ObjClsCenterCostOrgEl = itm.ObjClsCenterCostOrgEl,
                        });

                        lstClsResultUpdateCenterCostsEl.Add(new ClsResultEl()
                        {
                            BolSuccess = objClsSimpleResult.BolSuccess,
                            StrMessage = objClsSimpleResult.BolSuccess ? StrucMessagesEl.DeleteCc : StrucMessagesEl.ErrorCc,
                            StrId = itm.ObjClsCenterCostOrgEl.StrId,
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = "",
                    DtFecha = DateTime.Now
                });
            }

            return lstClsResultUpdateCenterCostsEl;
        }

        private static bool EqualsObjet(ClsCenterCostOrgEl objClsCenterCostOrgEl, ClsCenterCostOrgEl objClsCenterCostOrgEl1)
        {
            return  objClsCenterCostOrgEl.StrId == objClsCenterCostOrgEl1.StrId &&
                    objClsCenterCostOrgEl.StrNombre == objClsCenterCostOrgEl1.StrNombre &&
                    objClsCenterCostOrgEl.DtFechaInicio == objClsCenterCostOrgEl1.DtFechaInicio &&
                    objClsCenterCostOrgEl.DtFechaCierreAdmin == objClsCenterCostOrgEl1.DtFechaCierreAdmin &&
                    objClsCenterCostOrgEl.StrEmpresa == objClsCenterCostOrgEl1.StrEmpresa &&
                    objClsCenterCostOrgEl.StrEstatus == objClsCenterCostOrgEl1.StrEstatus &&
                    objClsCenterCostOrgEl.StrUnidadNegocio == objClsCenterCostOrgEl1.StrUnidadNegocio &&
                    objClsCenterCostOrgEl.StrClasificacion == objClsCenterCostOrgEl1.StrClasificacion &&
                    objClsCenterCostOrgEl.StrArea == objClsCenterCostOrgEl1.StrArea &&
                    objClsCenterCostOrgEl.StrSubArea == objClsCenterCostOrgEl1.StrSubArea &&
                    objClsCenterCostOrgEl.StrClasificacionAlterna == objClsCenterCostOrgEl1.StrClasificacionAlterna &&
                    objClsCenterCostOrgEl.StrAreaAlterna == objClsCenterCostOrgEl1.StrAreaAlterna &&
                    objClsCenterCostOrgEl.StrSubAreaAlterna == objClsCenterCostOrgEl1.StrSubAreaAlterna &&
                    objClsCenterCostOrgEl.StrZSource == objClsCenterCostOrgEl1.StrZSource &&
                    objClsCenterCostOrgEl.BolZActive == objClsCenterCostOrgEl1.BolZActive &&
                    objClsCenterCostOrgEl.StrZComentario == objClsCenterCostOrgEl1.StrZComentario &&
                    objClsCenterCostOrgEl.StrZIdHomologado == objClsCenterCostOrgEl1.StrZIdHomologado &&
                    objClsCenterCostOrgEl.StrZNombreHomologado == objClsCenterCostOrgEl1.StrZNombreHomologado &&
                    objClsCenterCostOrgEl.StrZFuenteHomologada == objClsCenterCostOrgEl1.StrZFuenteHomologada;
        }
    }
}
