using System;
using System.Collections.Generic;
using System.Reflection;
using DL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace BL_GA_Comisiones
{
    public class ClsMubCalculatedProjectBl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsMubCalculatedProjectBl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsTblMubCalculadoProyecto> GetMubCalculatedProject(int intId, int intIdCentroCostos, int intAno)
        {
            var lstClsTblMubCalculadoProyecto = new List<ClsTblMubCalculadoProyecto>();

            try
            {
                var objClsMubCalculatedProjectDl = new ClsMubCalculatedProjectDl(ObjClsUsuarioActiveDirectoryEl);
                lstClsTblMubCalculadoProyecto = objClsMubCalculatedProjectDl.GetMubCalculatedProject(intId, intIdCentroCostos, intAno);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdCentroCostos, intAno }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsTblMubCalculadoProyecto;
        }
    }
}