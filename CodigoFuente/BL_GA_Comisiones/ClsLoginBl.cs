using System;
using System.Linq;
using System.Reflection;
using DL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace BL_GA_Comisiones
{
    public class ClsLoginBl
    {
        public string StrUser { get; set; }
        public string StrPassword { get; set; }

        public ClsLoginBl(string strUser, string strPassword)
        {
            StrUser = strUser;
            StrPassword = strPassword;
        }

        public Tuple<string, ClsUsuarioActiveDirectoryEl> Login()
        {
            var objClsUsuarioActiveDirectoryEl = ClsActiveDirectoryBl.ValidUserActiveDirectory(StrUser, StrPassword);
            var strError = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(objClsUsuarioActiveDirectoryEl.StrSamAccountName))
                {
                    strError = StrucMessagesEl.LoginPassIncorrect;
                }
                else
                {

                    switch (objClsUsuarioActiveDirectoryEl.StrUserAccountControl ?? "")
                    {
                        case "":
                            strError = StrucMessagesEl.LoginPassIncorrect;
                            break;
                        case "2":
                            strError = StrucMessagesEl.LoginDesactivada;
                            break;
                        case "16":
                            strError = StrucMessagesEl.LoginBloq;
                            break;
                        default:
                            var objClsCatUsuarioEl = ClsUserDl.GetUsers(0, StrUser).FirstOrDefault();

                            if (objClsCatUsuarioEl != null)
                            {
                                objClsUsuarioActiveDirectoryEl.IntId = objClsCatUsuarioEl.IntId;
                                objClsUsuarioActiveDirectoryEl.IntIdRol = objClsCatUsuarioEl.IntIdRol;
                                objClsUsuarioActiveDirectoryEl.StrNombre = objClsCatUsuarioEl.StrNombre;
                                objClsUsuarioActiveDirectoryEl.StrUser = objClsCatUsuarioEl.StrUser;
                                objClsUsuarioActiveDirectoryEl.BolActivo = objClsCatUsuarioEl.BolActivo;
                                objClsUsuarioActiveDirectoryEl.IntIdCreateuser = objClsCatUsuarioEl.IntIdCreateuser;
                                objClsUsuarioActiveDirectoryEl.DtCreateDate = objClsCatUsuarioEl.DtCreateDate;
                                objClsUsuarioActiveDirectoryEl.IntIdUpdateUser = objClsCatUsuarioEl.IntIdUpdateUser;
                                objClsUsuarioActiveDirectoryEl.DtUpdateDate = objClsCatUsuarioEl.DtUpdateDate;
                            }
                            else
                            {
                                strError = StrucMessagesEl.LoginUnRegister;
                            }

                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { StrUser, StrPassword }),
                    DtFecha = DateTime.Now
                });
            }

            return new Tuple<string, ClsUsuarioActiveDirectoryEl>(strError, objClsUsuarioActiveDirectoryEl);
        }
    }
}