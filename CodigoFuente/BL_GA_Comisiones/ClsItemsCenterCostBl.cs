﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace BL_GA_Comisiones
{
    public class ClsItemsCenterCostBl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsItemsCenterCostBl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsAsocRubrosCentroCostosEl> GetItemsCenterCosts(int intId, int intIdRubroPago, int intIdCentroCostos)
        {
            var lstClsAsocRubrosCentroCostosEl = new List<ClsAsocRubrosCentroCostosEl>();
            var objClsItemsCenterCostDl = new ClsItemsCenterCostDl(ObjClsUsuarioActiveDirectoryEl);

            try
            {

                lstClsAsocRubrosCentroCostosEl = objClsItemsCenterCostDl.GetItemsCenterCosts(intId, intIdRubroPago, intIdCentroCostos);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdRubroPago, intIdCentroCostos }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsAsocRubrosCentroCostosEl;
        }

        public ClsSimpleResultEl SetItemsCenterCost(int intId, int intIdRubroPago, int intIdCentroCostos)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();
            var objClsItemsCenterCostDl = new ClsItemsCenterCostDl(ObjClsUsuarioActiveDirectoryEl);
            try
            {
                var clsClsAsocRubrosCentroCostosEl = new ClsAsocRubrosCentroCostosEl()
                {
                    IntId = intId,
                    IntIdRubroPago = intIdRubroPago,
                    IntIdCentroCostos = intIdCentroCostos,
                    IntIdUsuarioCreacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                    DtFechaCreacion = DateTime.Now
                };

                var id = objClsItemsCenterCostDl.GetItemsCenterCosts(0, intIdRubroPago, intIdCentroCostos).Select(itm => itm.IntId).FirstOrDefault();

                if (id > 0)
                {
                    return new ClsSimpleResultEl()
                    {
                        BolSuccess = false,
                        StrMessage = StrucMessagesEl.ErrorDuplicadoRubro,
                    };
                }

                return objClsItemsCenterCostDl.SetItemsCenterCost(clsClsAsocRubrosCentroCostosEl, false);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdRubroPago, intIdCentroCostos }),
                    DtFecha = DateTime.Now
                });
            }

            return objClsSimpleResultEl;
        }

        public ClsSimpleResultEl DelItemsCenterCost(int intId)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();
            var objClsItemsCenterCostDl = new ClsItemsCenterCostDl(ObjClsUsuarioActiveDirectoryEl);
            var objClsItemsCcCollaboratorBl = new ClsItemsCcCollaboratorBl(ObjClsUsuarioActiveDirectoryEl);
            try
            {
                if (objClsItemsCcCollaboratorBl.GetItemsCcCollaborator(0, intId, 0).Count == 0)
                {
                    var clsClsAsocRubrosCentroCostosEl = new ClsAsocRubrosCentroCostosEl()
                    {
                        IntId = intId,
                        IntIdRubroPago = 0,
                        IntIdCentroCostos = 0,
                        IntIdUsuarioCreacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                        DtFechaCreacion = DateTime.Now
                    };

                    return objClsItemsCenterCostDl.SetItemsCenterCost(clsClsAsocRubrosCentroCostosEl, true);
                }

                objClsSimpleResultEl = new ClsSimpleResultEl()
                {
                    BolSuccess = false,
                    StrMessage = StrucMessagesEl.ErrorEliminarRubro
                };
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId }),
                    DtFecha = DateTime.Now
                });
            }

            return objClsSimpleResultEl;
        }
    }
}
