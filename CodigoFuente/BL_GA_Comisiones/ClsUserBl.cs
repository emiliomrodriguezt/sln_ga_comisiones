﻿using System;
using System.Collections.Generic;
using System.Reflection;
using DL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace BL_GA_Comisiones
{
    public class ClsUserBl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsUserBl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public static ClsUsuarioActiveDirectoryEl UserLogged(string strDataSession)
        {
            var objClsUsuarioActiveDirectoryEl = new ClsUsuarioActiveDirectoryEl();
            try
            {
                objClsUsuarioActiveDirectoryEl = JsonConvert.DeserializeObject<ClsUsuarioActiveDirectoryEl>(strDataSession);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = strDataSession,
                    DtFecha = DateTime.Now
                });
            }

            return objClsUsuarioActiveDirectoryEl;
        }

        public List<ClsCatUsuarioEl> GetUsers(int intId, string strName, string strUser)
        {
            var lstClsCatUsuarioEl = new List<ClsCatUsuarioEl>();

            try
            {
                var objClsUserDl = new ClsUserDl(ObjClsUsuarioActiveDirectoryEl);
                lstClsCatUsuarioEl = objClsUserDl.GetUsers(intId, strName, strUser);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, strName, strUser }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsCatUsuarioEl;
        }

        public ClsSimpleResultEl SetUser(int intId, int intIdRol, string strName, string strUser)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();
            try
            {
                var objClsCatUsuarioEl = new ClsCatUsuarioEl()
                {
                    IntId = intId,
                    IntIdRol = intIdRol,
                    StrNombre = strName,
                    StrUser = strUser,
                    BolActivo = true,
                    IntIdCreateuser = ObjClsUsuarioActiveDirectoryEl.IntId,
                    IntIdUpdateUser = ObjClsUsuarioActiveDirectoryEl.IntId,
                    DtCreateDate = DateTime.Now,
                    DtUpdateDate = DateTime.Now
                };

                var objClsUserDl = new ClsUserDl(ObjClsUsuarioActiveDirectoryEl);
                return objClsUserDl.SetUser(objClsCatUsuarioEl, false);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intIdRol, strName, strUser }),
                    DtFecha = DateTime.Now
                });
            }

            return objClsSimpleResultEl;
        }

        public ClsSimpleResultEl DelUser(int intId)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();
            try
            {
                var objClsCatUsuarioEl = new ClsCatUsuarioEl()
                {
                    IntId = intId,
                    IntIdRol = 0,
                    StrNombre = "",
                    StrUser = "",
                    BolActivo = false,
                    IntIdCreateuser = ObjClsUsuarioActiveDirectoryEl.IntId,
                    IntIdUpdateUser = ObjClsUsuarioActiveDirectoryEl.IntId,
                    DtCreateDate = DateTime.Now,
                    DtUpdateDate = DateTime.Now
                };

                var objClsUserDl = new ClsUserDl(ObjClsUsuarioActiveDirectoryEl);
                return objClsUserDl.SetUser(objClsCatUsuarioEl, true);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId }),
                    DtFecha = DateTime.Now
                });
            }

            return objClsSimpleResultEl;
        }
    }
}
