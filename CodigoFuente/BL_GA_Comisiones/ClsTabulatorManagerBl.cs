﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;

namespace BL_GA_Comisiones
{
    public class ClsTabulatorManagerBl
    {
        public ClsUsuarioActiveDirectoryEl ObjClsUsuarioActiveDirectoryEl { get; set; }

        public ClsTabulatorManagerBl(ClsUsuarioActiveDirectoryEl objClsUsuarioActiveDirectoryEl = null)
        {
            ObjClsUsuarioActiveDirectoryEl = objClsUsuarioActiveDirectoryEl ?? new ClsUsuarioActiveDirectoryEl();
        }

        public List<ClsCatTabuladorGerentesEl> GetTabulatorManager(int intId, int intTipoTabulador, int intAnoVigencia)
        {
            var lstClsCatTabuladorGerentesEl = new List<ClsCatTabuladorGerentesEl>();

            try
            {
                var objClsTabulatorManagerDl = new ClsTabulatorManagerDl(ObjClsUsuarioActiveDirectoryEl);
                lstClsCatTabuladorGerentesEl = objClsTabulatorManagerDl.GetTabulatorManager(intId, intTipoTabulador, intAnoVigencia);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, intAnoVigencia }),
                    DtFecha = DateTime.Now
                });
            }

            return lstClsCatTabuladorGerentesEl;
        }

        public ClsSimpleResultEl SetTabulatorManager(int intId, int intIdTipoTabulador, decimal decCostoMensualGaMinimo, decimal decCostoMensualGaMaximo, decimal decMubMinimo, decimal decComision, int intAnoVigencia)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();
            try
            {
                var objClsTabulatorManagerDl = new ClsTabulatorManagerDl(ObjClsUsuarioActiveDirectoryEl);

                foreach (var itm in objClsTabulatorManagerDl.GetTabulatorManager(0, intAnoVigencia, intIdTipoTabulador))
                {
                    if (decCostoMensualGaMinimo >= itm.DecCostoMensualGaMinimo && 
                        decCostoMensualGaMinimo <= itm.DecCostoMensualGaMaximo)
                    {
                        return new ClsSimpleResultEl()
                        {
                            BolSuccess = false,
                            StrMessage = string.Format(StrucMessagesEl.RangoCollapse, 
                                                       $"{decCostoMensualGaMinimo}-{decCostoMensualGaMaximo}", 
                                                       $"{itm.DecCostoMensualGaMinimo}-{itm.DecCostoMensualGaMaximo}", 
                                                       intAnoVigencia, 
                                                       itm.ObjClsCatTipoTabuladorEl.StrNombre)
                        };
                    }

                    if (decCostoMensualGaMaximo >= itm.DecCostoMensualGaMinimo &&
                        decCostoMensualGaMaximo <= itm.DecCostoMensualGaMaximo)
                    {
                        return new ClsSimpleResultEl()
                        {
                            BolSuccess = false,
                            StrMessage = string.Format(StrucMessagesEl.RangoCollapse, 
                                                       $"{decCostoMensualGaMinimo}-{decCostoMensualGaMaximo}", 
                                                       $"{itm.DecCostoMensualGaMinimo}-{itm.DecCostoMensualGaMaximo}", 
                                                       intAnoVigencia, 
                                                       itm.ObjClsCatTipoTabuladorEl.StrNombre)
                        };
                    }
                }

                var objClsCatTabuladorGerentesEl = new ClsCatTabuladorGerentesEl()
                {
                    IntId = intId,
                    IntIdTipoTabulador = intIdTipoTabulador,
                    DecCostoMensualGaMinimo = decCostoMensualGaMinimo,
                    DecCostoMensualGaMaximo = decCostoMensualGaMaximo,
                    DecMubMinimo = decMubMinimo,
                    DecComision = decComision,
                    IntAnoVigencia = intAnoVigencia,
                    BolActivo = true,
                    IntIdUsuarioCreacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                    IntIdUsuarioActulizacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                    DtFechaCreacion = DateTime.Now,
                    DtFechaActualizacion = DateTime.Now,
                };

                
                return objClsTabulatorManagerDl.SetTabulatorManager(objClsCatTabuladorGerentesEl, false);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId, decCostoMensualGaMinimo, decCostoMensualGaMaximo, decMubMinimo, intAnoVigencia }),
                    DtFecha = DateTime.Now
                });
            }

            return objClsSimpleResultEl;
        }

        public ClsSimpleResultEl DelTabulatorManager(int intId)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();
            try
            {
                var objClsCatTabuladorGerentesEl = new ClsCatTabuladorGerentesEl()
                {
                    IntId = intId,
                    IntIdTipoTabulador = 0,
                    DecCostoMensualGaMinimo = 0M,
                    DecCostoMensualGaMaximo = 0M,
                    DecMubMinimo = 0M,
                    IntAnoVigencia = 0,
                    BolActivo = false,
                    IntIdUsuarioCreacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                    IntIdUsuarioActulizacion = ObjClsUsuarioActiveDirectoryEl.IntId,
                    DtFechaCreacion = DateTime.Now,
                    DtFechaActualizacion = DateTime.Now,
                };

                var objClsTabulatorManagerDl = new ClsTabulatorManagerDl(ObjClsUsuarioActiveDirectoryEl);
                return objClsTabulatorManagerDl.SetTabulatorManager(objClsCatTabuladorGerentesEl, true);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { intId }),
                    DtFecha = DateTime.Now
                });
            }

            return objClsSimpleResultEl;
        }

        public ClsSimpleResultEl CopyTabulatorManager(int anoOld, int anoNew)
        {
            var objClsSimpleResultEl = new ClsSimpleResultEl();
            try
            {
                var objClsTabulatorManagerDl = new ClsTabulatorManagerDl(ObjClsUsuarioActiveDirectoryEl);
                if (objClsTabulatorManagerDl.GetTabulatorManager(0, anoNew, 0).Any(imt => imt.BolActivo.Equals(true)))
                {
                    return new ClsSimpleResultEl()
                    {
                        BolSuccess = false,
                        StrMessage = StrucMessagesEl.CopyTabExists
                    };
                }
                return objClsTabulatorManagerDl.CopyTabulatorManager(anoOld, anoNew, ObjClsUsuarioActiveDirectoryEl.IntId, DateTime.Now);
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = 2,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { anoOld, anoNew }),
                    DtFecha = DateTime.Now
                });
            }

            return objClsSimpleResultEl;
        }
    }
}
