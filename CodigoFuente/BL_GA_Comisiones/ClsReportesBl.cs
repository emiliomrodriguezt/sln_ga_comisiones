﻿using DL_GA_Comisiones;
using EL_GA_Comisiones;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Utility_GA_Comisiones;

namespace BL_GA_Comisiones
{
    public class ClsReportesBl
    {
        public DataTable GetReporteMUBGerente(int usuario, int idCol, int anio, int mes)
        {
            try
            {
                ClsReportesDl objDl = new ClsReportesDl();

                DataTable resultado = objDl.GetReporteMUBGerente(idCol, anio, mes);
                if (resultado == null || resultado.Rows.Count == 0)
                    return resultado;

                foreach(DataColumn col in resultado.Columns)
                {
                    col.ReadOnly = false;
                }

                /*DataTable costos = objDl.GetCostosPorAnio(usuario, anio, mes);
                if (costos == null || costos.Rows.Count == 0)
                    return null;*/

                int ultimoAnio = anio;
                int primerAnio = ultimoAnio - ((resultado.Columns.Count - 14) / 4) + 1;
                int anioActual;
                int columnaActual;
                decimal ingresoTotal;
                decimal costoTotal;
                decimal interesTotal;
                decimal mubTotal;
                decimal costoActual;
                DataRow fila = null;
                decimal costo;

                foreach (DataRow centro in resultado.Rows)
                {
                    columnaActual = 11;
                    anioActual = primerAnio;
                    ingresoTotal = 0;
                    costoTotal = 0;
                    interesTotal = 0;
                    mubTotal = 0;

                    while (anioActual <= ultimoAnio)
                    {
                        //fila = costos.Select("zIDH='" + centro["id"].ToString() + "' AND anio="+anioActual.ToString()).FirstOrDefault();
                        //costo = fila == null ? 0 : Convert.ToDecimal(fila["costo"]);
                        costo = 0;
                        ingresoTotal += Convert.ToDecimal(centro[columnaActual]);

                        costoActual = Convert.ToDecimal(centro[columnaActual + 1]);
                        centro[columnaActual + 1] = costo + costoActual;
                        costoTotal += costo + costoActual;

                        interesTotal += Convert.ToDecimal(centro[columnaActual + 2]);

                        centro[columnaActual + 3] = Convert.ToDecimal(centro[columnaActual]) - Convert.ToDecimal(centro[columnaActual + 1]) - Convert.ToDecimal(centro[columnaActual + 2]);
                        mubTotal += Convert.ToDecimal(centro[columnaActual + 3]);

                        columnaActual +=4;
                        anioActual++;
                    }
                    centro[columnaActual] = ingresoTotal;
                    centro[columnaActual + 1] = costoTotal;
                    centro[columnaActual + 2] = interesTotal;
                    centro[columnaActual + 3] = mubTotal;
                }

                return resultado;
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { usuario, idCol }),
                    DtFecha = DateTime.Now
                });
            }

            return null;
        }

        public List<ClsReporteMUBCCEl> GetReporteMUBCC(int usuario, int idCol, int idCC)
        {
            try
            {
                ClsReportesDl objDl = new ClsReportesDl();
                DataTable resultado = objDl.GetReporteMUBCC(idCol, idCC);

                var reporte = new List<ClsReporteMUBCCEl>();
                reporte = (from a in resultado.AsEnumerable()
                           select new ClsReporteMUBCCEl()
                           {
                               Anio = (int)a["anio"],
                               ClaveCC = a["idStr"].ToString(),
                               Congelado = (bool)a["congelado"],
                               Egresos = (decimal)a["egresos"],
                               IdMUB = (int)a["id"],
                               Ingresos = (decimal)a["ingresos"],
                               Intereses = (decimal)a["intereses"],
                               MUBComision = (decimal)a["mubComision"],
                               MUBContable = (decimal)a["mubContable"],
                               NombreCC = a["nombreCC"].ToString(),
                               NombreUnidad = a["nombreUnidad"].ToString(),
                               UnidadNegocio = a["unidadNegocio"].ToString()
                           }).ToList();
                return reporte;
                
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { usuario, idCol, idCC }),
                    DtFecha = DateTime.Now
                });
            }

            return null;
        }

        public byte[] GetReporteMUBCCXls(int usuario, int idCol, int idCC)
        {
            try
            {
                ClsReportesDl objDl = new ClsReportesDl();
                DataTable reporte = objDl.GetReporteMUBCC(idCol, idCC);

                if (reporte == null)
                    return null;

                reporte.Columns.RemoveAt(0);
                reporte.Columns.RemoveAt(reporte.Columns.Count - 1);

                string titulo = "Reporte de MUB por C.C.";
                String[] columnas = new string[10];

                columnas[0] = "No. de UN";
                columnas[1] = "Nombre UN";
                columnas[2] = "No. de C.C.";
                columnas[3] = "Nombre C.C.";
                columnas[4] = "Ingresos";
                columnas[5] = "Egresos";
                columnas[6] = "Intereses";
                columnas[7] = "MUB contable";
                columnas[8] = "MUB base de comisiones";
                columnas[9] = "Año";

                return ClsDataTableToExcelBl.ExportExcel(reporte, titulo, true, columnas);
            }
            catch(Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { usuario, idCol, idCC }),
                    DtFecha = DateTime.Now
                });
            }

            return null;
        }

        public ClsRespuestaSP UpdateInteresesMUB(int usuario, int idMub, decimal intereses, decimal mub)
        {
            ClsRespuestaSP respuesta = new ClsRespuestaSP();

            try
            {
                ClsReportesDl objDl = new ClsReportesDl();
                respuesta = objDl.UpdateInteresesMUB(usuario, idMub, intereses, mub);
            }
            catch (Exception e)
            {
                respuesta.Resultado = -1;
                respuesta.Mensaje = e.Message;
            }
            return respuesta;
        }

        public List<ClsReporteEdoCuentaEl> GetReporteEdoCuenta(int usuario, int idCol)
        {
            try
            {
                ClsReportesDl objDl = new ClsReportesDl();
                DataTable resultado = objDl.GetReporteEdoCuenta(idCol);
                var reporte = new List<ClsReporteEdoCuentaEl>();
                reporte = (from a in resultado.AsEnumerable()
                           select new ClsReporteEdoCuentaEl()
                           {
                               Acumulado = (decimal)a["total"],
                               Anio = (int)a["anio"],
                               ClaveCC = a["idCC"].ToString(),
                               Concepto = a["concepto"].ToString(),
                               Importe = (decimal)a["importe"],
                               NombreCC = a["nombreCC"].ToString(),
                               UnidadNegocio = a["unidadNegocio"].ToString(),
                               NombreColaborador = a["nombreColaborador"].ToString()
                           }).ToList();
                return reporte;
            }
            catch (Exception ex)
            {
                ClsLogDl.InserLog(new ClsTblLogEl()
                {
                    IntIdUsuario = usuario,
                    StrClase = MethodBase.GetCurrentMethod().DeclaringType?.Name ?? "",
                    StrMetodo = MethodBase.GetCurrentMethod().Name,
                    StrMensaje = ex.Message,
                    StrPilaEventos = ex.StackTrace,
                    StrMensajeInterno = ex.InnerException?.Message ?? "",
                    StrPilaEventosInterno = ex.InnerException?.StackTrace ?? "",
                    StrEntrada = JsonConvert.SerializeObject(new { usuario, idCol }),
                    DtFecha = DateTime.Now
                });
            }

            return null;
        }

        public byte[] GetReporteEdoCuentaXls(int idCol)
        {
            ClsReportesDl objDl = new ClsReportesDl();
            DataTable reporte = objDl.GetReporteEdoCuenta(idCol);

            if (reporte == null)
                return null;

            string titulo = "Estado de Cuenta - " + reporte.Rows[0]["nombreColaborador"].ToString();
            reporte.Columns.Remove(reporte.Columns["nombreColaborador"]);

            String[] columnas = new string[7];

            columnas[0] = "UN";
            columnas[1] = "No. de C.C.";
            columnas[2] = "Nombre C.C.";
            columnas[3] = "Concepto";
            columnas[4] = "Importes pagados";
            columnas[5] = "Total";
            columnas[6] = "Año";

            return ClsDataTableToExcelBl.ExportExcel(reporte, titulo, true, columnas);
        }
    }
}
