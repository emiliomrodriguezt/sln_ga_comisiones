﻿namespace EL_GA_Comisiones
{
    public class ClsAddPermissionEl
    {
        public int IntIdRol { get; set; }
        public int IntIdPermiso { get; set; }
        public int IntIdPagina { get; set; }
        public bool BolAcces { get; set; }
        public bool BolEdit { get; set; }
    }
}