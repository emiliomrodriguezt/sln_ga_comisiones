﻿using System;

namespace EL_GA_Comisiones
{
    public class ClsCatTabuladorGerentesEl
    {
        public int IntId { get; set; }
        public int IntIdTipoTabulador { get; set; }
        public decimal DecCostoMensualGaMinimo { get; set; }
        public decimal DecCostoMensualGaMaximo { get; set; }
        public decimal DecMubMinimo { get; set; }
        public decimal DecComision { get; set; }
        public bool BolActivo { get; set; }
        public int IntAnoVigencia { get; set; }
        public int? IntIdUsuarioCreacion { get; set; }
        public int? IntIdUsuarioActulizacion { get; set; }
        public DateTime? DtFechaCreacion { get; set; }
        public DateTime? DtFechaActualizacion { get; set; }
        public ClsCatTipoTabuladorEl ObjClsCatTipoTabuladorEl { get; set; }

        public ClsCatTabuladorGerentesEl()
        {
            ObjClsCatTipoTabuladorEl = new ClsCatTipoTabuladorEl();
        }
    }
}
