﻿using System;

namespace EL_GA_Comisiones
{
    public class ClsAsocCenterCostosColaboradorMubEl
    {
        public int IntId { get; set; }
        public int IntIdCentroCostos { get; set; }
        public int IntIdGerente { get; set; }
        public int IntIdDirector { get; set; }
        public decimal DecPorcentaje { get; set; }
        public int? IntIdUsuarioCreacion { get; set; }
        public DateTime? DtFechaCreacion { get; set; }
        public ClsCatCentroCostosEl ObjClsCatCentroCostosEl { get; set; }
        public ClsTblColaboradorEl ObjGerente { get; set; }
        public ClsTblColaboradorEl ObjDirector { get; set; }

        public ClsAsocCenterCostosColaboradorMubEl()
        {
            ObjClsCatCentroCostosEl = new ClsCatCentroCostosEl();
            ObjGerente = new ClsTblColaboradorEl();
            ObjDirector = new ClsTblColaboradorEl();
        }
    }
}
