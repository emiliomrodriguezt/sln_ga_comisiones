﻿using System;

namespace EL_GA_Comisiones
{
    public class ClsCatRolesEl
    {
        public int IntId { get; set; }
        public string StrNombre { get; set; }
        public bool BolActivo { get; set; }
        public int? IntIdUsuarioCreacion { get; set; }
        public int? IntIdUsuarioActualizacion { get; set; }
        public DateTime? DtFechaCreacion { get; set; }
        public DateTime? DtFechaActualizacion { get; set; }
    }
}
