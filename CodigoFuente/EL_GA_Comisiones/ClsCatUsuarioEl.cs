﻿using System;

namespace EL_GA_Comisiones
{
    public class ClsCatUsuarioEl
    {
        public int IntId { get; set; }
        public int IntIdRol { get; set; }
        public string StrNombre  { get; set; }
        public string StrUser { get; set; }
        public bool BolActivo { get; set; }
        public int? IntIdCreateuser { get; set; }
        public int? IntIdUpdateUser { get; set; }
        public DateTime? DtCreateDate { get; set; }
        public DateTime? DtUpdateDate { get; set; }
    }
}
