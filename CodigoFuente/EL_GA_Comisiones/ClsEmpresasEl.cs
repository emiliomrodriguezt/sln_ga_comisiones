﻿namespace EL_GA_Comisiones
{
    public class ClsEmpresasEl
    {
        public string StrTamanoNombre { get; set; }
        public string StrNo { get; set; }
        public string StrNombre { get; set; }
        public string StrInterciaErp { get; set; }
        public string StrConsolidacionErp { get; set; }
        public string StrInterciaCoi { get; set; }
        public string StrConsolidacionCoi { get; set; }
        public string StrCuentaCostoGasto { get; set; }
        public string StrStatus { get; set; }
        public string StrClasificacion { get; set; }
        public string StrArea { get; set; }
        public string StrSubArea { get; set; }
        public string StrFuente { get; set; }
    }
}
