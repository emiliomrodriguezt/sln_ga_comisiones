﻿using System;

namespace EL_GA_Comisiones
{
    public class ClsCentercostInsertEl
    {
        public int IntId { get; set; }
        public bool BolActivo { get; set; }
        public int? IntIdUsuarioCreacion { get; set; }
        public int? IntIdUsuarioActualizacion { get; set; }
        public DateTime? DtFechaCreacion { get; set; }
        public DateTime? DtFechaActualizacion { get; set; }

        public ClsCenterCostOrgEl ObjClsCenterCostOrgEl { get; set; }

        public ClsCentercostInsertEl()
        {
            ObjClsCenterCostOrgEl = new ClsCenterCostOrgEl();
        }
    }
}
