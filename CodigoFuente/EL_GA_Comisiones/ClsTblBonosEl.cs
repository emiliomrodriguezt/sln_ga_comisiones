﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EL_GA_Comisiones
{
    public class ClsTblBonosEl
    {
        public int IntId { get; set; }
        public int IntIdColaborador { get; set; }
        public decimal DecMontoBono { get; set; }
        public int IntAnio { get; set; }
        public bool BolPagado { get; set; }
        public bool BolActivo { get; set; }
        public int IntIdUsuarioCrecion { get; set; }
        public int? IntIdUsuarioActulizacion { get; set; }
        public DateTime DtFechaCreacion { get; set; }
        public DateTime? DtFechaActulizacion { get; set; }

        public ClsTblColaboradorEl ObjClsTblColaboradorEl { get; set; }

        public ClsTblBonosEl()
        {
            ObjClsTblColaboradorEl = new ClsTblColaboradorEl();
        }
    }
}
