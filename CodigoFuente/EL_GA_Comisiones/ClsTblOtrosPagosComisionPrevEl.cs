﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EL_GA_Comisiones
{
    public class ClsTblOtrosPagosComisionPrevEl
    {
        public int IdOtroPago { get; set; }

        public int IdComision { get; set; }

        public int IdTipoPago { get; set; }

        public int IdPago { get; set; }

        public decimal ImporteTotal { get; set; }

        public decimal ImporteAPagar { get; set; }

        public int Anio { get; set; }

        public bool Pagado { get; set; }
    }
}
