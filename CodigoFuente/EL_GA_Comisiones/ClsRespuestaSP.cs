﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EL_GA_Comisiones
{
    public class ClsRespuestaSP
    {
        public int Resultado { get; set; }
        public string Mensaje { get; set; }
        public int Valor { get; set; }
    }
}
