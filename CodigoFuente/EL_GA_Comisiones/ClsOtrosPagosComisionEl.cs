﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EL_GA_Comisiones
{
    public class ClsOtrosPagosComisionEl
    {
        public int IdTipo { get; set; }

        public int IdOtro { get; set; }

        public string StrCC { get; set; }

        public string NombreCC { get; set; }

        public string Concepto { get; set; }

        public decimal Importe { get; set; }

        public decimal ImportePagar { get; set; }

        public int Anio { get; set; }

        public bool Seleccionado { get; set; }
    }
}
