﻿using System.Data;

namespace EL_GA_Comisiones
{
    public class ClsSimpleResultEl
    {
        public ClsSimpleResultEl()
        {
            DtResult = new DataTable();
        }

        public bool BolSuccess { get; set; }
        public int? IntId { get; set; }
        public DataTable DtResult { get; set; }
        public string StrMessage { get; set; }
    }
}
