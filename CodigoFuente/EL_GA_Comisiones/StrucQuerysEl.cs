﻿namespace EL_GA_Comisiones
{
    public struct StrucQuerysEl
    {
        public const string QryGetCompany = "SELECT [TAMAÑO DEL NOMBRE]  AS 'TamanoNombre', "
                                                  + "[N°] AS 'No', "
                                                  + "[NOMBRE] AS 'Nombre', "
                                                  + "[INTERCIA ERP] AS 'InterciaErp', "
                                                  + "[CONSOLIDACION ERP] AS 'ConsolidacionErp', "
                                                  + "[INTERCIA COI] AS 'InterciaCoi', "
                                                  + "[CONSOLIDACION COI] AS 'ConsolidacionCoi', "
                                                  + "[CUENTA COSTO/GASTO] AS 'CuentaCostoGasto', "
                                                  + "[STATUS] AS 'Status', "
                                                  + "[CLASIFICACION] AS 'Clasificacion', "
                                                  + "[AREA] AS 'Area', "
                                                  + "[SUB AREA] AS 'SubArea', "
                                                  + "[FUENTE] AS 'Fuente' "
                                          + "FROM [BDM_GLFI].[dbo].[IDM_CeCos_Cias_Catalogo_Vi]; ";

        public const string QryGetCostCenter = "SELECT [ID], "
                                                    + "[Centro_De_Costo], "
                                                    + "[Fecha_Inicio], "
                                                    + "[Fecha_Cierre_Administrativo], "
                                                    + "[Empresa_Ganadora], "
                                                    + "[Estatus], "
                                                    + "[U_Negocio], "
                                                    + "[Clasificacion], "
                                                    + "[Area], "
                                                    + "[Sub_Area], "
                                                    + "[clasificacion_alterna_planeacion], "
                                                    + "[area_alterna_planeacion], "
                                                    + "[subarea_alterna_planeacion], "
                                                    + "[zSource], "
                                                    + "[zActive], "
                                                    + "[zComentario], "
                                                    + "[zID_Homologado], "
                                                    + "[zCentro_de_Costo_Homologado], "
                                                    + "[zFuente_Homologada] "
                                            + "FROM [dbo].[IDM_CeCos_Universo_Vi] "
                                            + "WHERE [clasificacion_alterna_planeacion] = 'PROYECTOS' OR [clasificacion_alterna_planeacion] = 'OPORTUNIDADES' OR [clasificacion_alterna_planeacion] = 'UN´S'; ";

        public const string Anios = "2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030";
    }
}
