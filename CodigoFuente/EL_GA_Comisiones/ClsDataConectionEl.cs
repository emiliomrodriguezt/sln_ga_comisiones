﻿namespace EL_GA_Comisiones
{
    public class ClsDataConectionEl
    {
        public string StrHostName { get; set; }
        public string StrInstance { get; set; }
        public string StrPort { get; set; }
        public string StrDataBaseName { get; set; }
        public string StrUser { get; set; }
        public string StrPassword { get; set; }
    }
}
