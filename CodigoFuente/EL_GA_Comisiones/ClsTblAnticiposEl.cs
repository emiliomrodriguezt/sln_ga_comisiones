﻿using System;

namespace EL_GA_Comisiones
{
    public class ClsTblAnticiposEl
    {
        public ClsTblAnticiposEl()
        {
            ObjClsTblColaboradorEl = new ClsTblColaboradorEl();
            ObjClsCatCentroCostosEl = new ClsCatCentroCostosEl();
        }

        public int IntId { get; set; }
        public int IntIdColaborador { get; set; }
        public int IntIdCentroCostos { get; set; }
        public decimal DecImporteBruto { get; set; }
        public decimal DecImporteNeto { get; set; }
        public string StrNoEmpresa { get; set; }
        public string StrNoChequeTransferencia { get; set; }
        public DateTime DtFechaPago { get; set; }
        public string StrNoPolizaContable { get; set; }
        public decimal DecAcomulado { get; set; }
        public bool BolActivo { get; set; }
        public int? IntIdUsuarioCreacion { get; set; }
        public int? IntIdUsuarioActualizacion { get; set; }
        public DateTime? DtFechaCreacion { get; set; }
        public DateTime? DtFechaActualizacion { get; set; }
        public ClsTblColaboradorEl ObjClsTblColaboradorEl { get; set; }
        public ClsCatCentroCostosEl ObjClsCatCentroCostosEl { get; set; }
    }
}
