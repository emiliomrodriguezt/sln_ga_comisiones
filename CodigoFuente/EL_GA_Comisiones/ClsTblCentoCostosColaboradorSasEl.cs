﻿using System;

namespace EL_GA_Comisiones
{
    public class ClsTblCentoCostosColaboradorSasEl
    {
        public int IntId { get; set; }
        public int IntIdColaborador { get; set; }
        public int IntIdDirector { get; set; }
        public int IntIdCentroCostos { get; set; }
        public int IntAno { get; set; }
        public decimal DecPorcentajeParticipacion { get; set; }
        public decimal DecMubContratado { get; set; }
        public decimal DecMonto { get; set; }
        public bool BolPagado { get; set; }
        public int IntIdUsuarioCreacion { get; set; }
        public DateTime DtFechaCreacion { get; set; }
        public ClsTblColaboradorEl ObjClsTblColaboradorEl { get; set; }
        public ClsTblColaboradorEl ObjClsTblColaboradorDirectorEl { get; set; }
        public ClsCatCentroCostosEl ObjClsCatCentroCostosEl { get; set; }

        public ClsTblCentoCostosColaboradorSasEl()
        {
            ObjClsTblColaboradorEl = new ClsTblColaboradorEl();
            ObjClsTblColaboradorDirectorEl = new ClsTblColaboradorEl();
            ObjClsCatCentroCostosEl = new ClsCatCentroCostosEl();
        }
    }
}