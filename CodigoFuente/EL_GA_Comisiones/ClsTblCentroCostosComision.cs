﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EL_GA_Comisiones
{
    public class ClsTblCentroCostosComision
    {
        public int IdTabla { get; set; }

        public int IdComision { get; set; }

        public int IdCentroCostos { get; set; }

        public decimal MUBComision { get; set; }

        public decimal Porcentaje { get; set; }

        public decimal MontoPagado { get; set; }

        public decimal ComisionTotal { get; set; }
    }
}
