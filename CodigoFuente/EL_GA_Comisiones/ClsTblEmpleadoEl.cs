﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EL_GA_Comisiones
{
    public class ClsTblEmpleadoEl
    {
        public int IdEmp { get; set; }

        public string NoColaborador { get; set; }

        public string NombreColaborador { get; set; }

        public DateTime FechaAlta { get; set; }

        public DateTime? FechaBaja { get; set; }

        public int IdPuesto { get; set; }

        public string Puesto { get; set; }

        public int IdCentroCostos { get; set; }

        public string CentroCostos { get; set; }

        public string NoEmpresa { get; set; }

        public string Empresa { get; set; }

        public decimal SueldoNomina { get; set; }

        public decimal SueldoHonorarios { get; set; }

        public decimal SueldoMensual { get; set; }

        public int Activo { get; set; }

        public string Estatus { get; set; }

        public int IdUsuarioCreacion { get; set; }

        public int? IdUsuarioActualizacion { get; set; }

        public DateTime FechaCreacion { get; set; }

        public DateTime? FechaActualizacion { get; set; }
    }
}