﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EL_GA_Comisiones
{
    public class ClsTblMUBCalculadoProyectoEl
    {
        public int IdMUB { get; set; }

        public int IdCentroCostos { get; set; }

        public string IdCC { get; set; }

        public string NombreCC { get; set; }

        public int Periodo { get; set; }

        public int Anio { get; set; }

        public decimal TotalIngresos { get; set; }

        public decimal TotalEgresos { get; set; }

        public decimal TotalIntereses { get; set; }

        public decimal MUBCalculado { get; set; }

        public decimal MUBComision { get; set; }

        public int IdUsuarioCreacion { get; set; }

        public int? IdUsuarioActualizacion { get; set; }

        public DateTime FechaCreacion { get; set; }

        public DateTime? FechaActualizacion { get; set; }

        public bool Congelado { get; set; }

        public ClsCatCentroCostosEl ObjClsCatCentroCostosEl { get; set; }

        public ClsTblMUBCalculadoProyectoEl()
        {
            ObjClsCatCentroCostosEl = new ClsCatCentroCostosEl();
        }
    }
}
