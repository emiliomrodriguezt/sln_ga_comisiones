﻿using System;

namespace EL_GA_Comisiones
{
    public class ClsAsocRubrosCentroCostosEl
    {
        public int IntId { get; set; }
        public int IntIdRubroPago { get; set; }
        public int IntIdCentroCostos { get; set; }
        public int? IntIdUsuarioCreacion { get; set; }
        public DateTime? DtFechaCreacion { get; set; }

        public ClsRubroPagoEl ObjClsRubroPagoEl { get; set; }

        public ClsCatCentroCostosEl ObjClsCatCentroCostosEl { get; set; }

        public ClsAsocRubrosCentroCostosEl()
        {
            ObjClsRubroPagoEl = new ClsRubroPagoEl();
            ObjClsCatCentroCostosEl = new ClsCatCentroCostosEl();
        }
    }
}
