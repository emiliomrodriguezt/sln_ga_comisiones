﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EL_GA_Comisiones
{
    public class ClsProyectosComisionEl
    {
        public int IdCC { get; set; }

        public string StrCC { get; set; }

        public string NombreCC { get; set; }

        public string StrUN { get; set; }

        public int? Payback { get; set; }

        public decimal MUBcomision { get; set; }

        public decimal Porcentaje { get; set; }

        public decimal MontoPagado { get; set; }

        public int Orden { get; set; }

        public bool Seleccionado { get; set; }

    }
}
