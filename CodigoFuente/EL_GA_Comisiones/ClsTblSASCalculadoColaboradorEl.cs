﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EL_GA_Comisiones
{
    public class ClsTblSASCalculadoColaboradorEl
    {
        public int IdSAS { get; set; }

        public int IdColaborador { get; set; }

        public string NoColaborador { get; set; }

        public string Colaborador { get; set; }

        public int DuracionMeses { get; set; }

        public int Anio { get; set; }

        public decimal MUBMinimo {get;set;}

        public decimal MUBContratado { get; set; }

        public decimal SueldoMUBContratado { get; set; }

        public decimal Sueldo { get; set; }

        public bool Pagado { get; set; }

        public decimal CuotaSAS { get; set; }

        public decimal MontoPagado { get; set; }

        public int IdUsuarioCreacion { get; set; }

        public DateTime FechaCreacion { get; set; }
    }
}
