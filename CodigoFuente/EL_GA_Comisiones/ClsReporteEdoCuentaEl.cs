﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EL_GA_Comisiones
{
    public class ClsReporteEdoCuentaEl
    {
        public string UnidadNegocio { get; set; }

        public string ClaveCC { get; set; }

        public string NombreCC { get; set; }

        public string Concepto { get; set; }

        public decimal Importe { get; set; }

        public decimal Acumulado { get; set; }

        public int Anio { get; set; }

        public string NombreColaborador { get; set; }
    }
}
