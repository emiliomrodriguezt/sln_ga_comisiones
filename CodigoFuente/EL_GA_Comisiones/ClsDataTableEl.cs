﻿namespace EL_GA_Comisiones
{
    public class ClsDataTableEl
    {
        public bool BolEdit { get; set; }
        public bool BolDelete { get; set; }
        public bool BolConsult { get; set; }
        public string StrPathEdit { get; set; }
        public string StrPathDelete { get; set; }
    }
}
