﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EL_GA_Comisiones
{
    public class ClsTblCalculoComisionEl
    {
        public int IdComision { get; set; }

        public int IdColaborador { get; set; }

        public int Anio { get; set; }

        public int Periodo { get; set; }

        public int SASVisibles { get; set; }

        public decimal? ComisionTotal { get; set; }

        public decimal? MontoSAS { get; set; }

        public decimal? OtrosPagos { get; set; }

        public decimal? TotalAPagar { get; set; }

        public bool Pagado { get; set; }

        public string FirmaContraloria { get; set; }
        public string FirmaDirectorUn { get; set; }
        public string FirmaDirectorGrl { get; set; }
        public string FirmaPresidente { get; set; }
        public byte[] DocGenerado { get; set; }

        public int IdUsuarioCreacion { get; set; }

        public DateTime FechaCreacion { get; set; }

        public ClsTblColaboradorEl ObjClsTblColaboradorEl { get; set; }

        public ClsTblCalculoComisionEl()
        {
            ObjClsTblColaboradorEl = new ClsTblColaboradorEl();
            DocGenerado = new byte[0];
        }
    }
}
