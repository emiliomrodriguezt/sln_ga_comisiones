﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EL_GA_Comisiones.ReporteResultados
{
    public class ClsGeneralEl
    {
        public int IntAnoGeneral { get; set; }
        public string StrCentroCostos { get; set; }
        public string StrGerente { get; set; }
        public DateTime DtFechaIngreso { get; set; }
        public string StrFirmaContraloria { get; set; }
        public string StrFirmaUn { get; set; }
        public string StrFirmaGrl { get; set; }
        public string StrPresidencia { get; set; }
        public decimal DecMonto { get; set; }
    }
}
