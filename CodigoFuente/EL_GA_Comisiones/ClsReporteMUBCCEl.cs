﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EL_GA_Comisiones
{
    public class ClsReporteMUBCCEl
    {
        public int IdMUB { get; set; }

        public string UnidadNegocio { get; set; }

        public string NombreUnidad { get; set; }

        public string ClaveCC { get; set; }

        public string NombreCC { get; set; }

        public decimal Ingresos { get; set; }

        public decimal Egresos { get; set; }

        public decimal Intereses { get; set; }

        public decimal MUBContable { get; set; }

        public decimal MUBComision { get; set; }

        public int Anio { get; set; }

        public bool Congelado { get; set; }
    }
}
