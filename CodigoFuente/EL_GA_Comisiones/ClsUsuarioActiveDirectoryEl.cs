﻿namespace EL_GA_Comisiones
{
    public class ClsUsuarioActiveDirectoryEl : ClsCatUsuarioEl
    {
        public string StrUserAccountControl { get; set; }
        public string StrSamAccountName { get; set; }
        public string StrDisplayName { get; set; }
        public string StrPassword { get; set; }
    }
}