﻿namespace EL_GA_Comisiones
{
    public class ClsTblCentroCostosComisonEl
    {
        public int IntId { get; set; }
        public int IntIdComision { get; set; }
        public int IntIdCentroCostos { get; set; }
        public decimal DecMubComision { get; set; }
        public decimal DecPorcentaje { get; set; }
        public decimal DecMontoPagado { get; set; }
        public decimal DecComisionTotal { get; set; }
        public string StrObservaciones { get; set; }
        public ClsTblCalculoComisionEl ObjClsTblCalculoComisionEl { get; set; }
        public ClsCatCentroCostosEl ObjClsCatCentroCostosEl { get; set; }

        public ClsTblCentroCostosComisonEl()
        {
            ObjClsTblCalculoComisionEl = new ClsTblCalculoComisionEl();
            ObjClsCatCentroCostosEl = new ClsCatCentroCostosEl();
        }
    }
}
