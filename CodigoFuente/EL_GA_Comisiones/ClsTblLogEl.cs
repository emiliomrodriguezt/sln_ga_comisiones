﻿using System;

namespace EL_GA_Comisiones
{
    public class ClsTblLogEl
    {
        public int? IntId { get; set; }
        public int? IntIdUsuario { get; set; }
        public string StrClase { get; set; }
        public string StrMetodo { get; set; }
        public string StrMensaje { get; set; }
        public string StrPilaEventos { get; set; }
        public string StrMensajeInterno { get; set; }
        public string StrPilaEventosInterno { get; set; }
        public string StrEntrada { get; set; }
        public DateTime DtFecha { get; set; }
    }
}
