﻿namespace EL_GA_Comisiones
{
    public class ClsPermissionEl
    {
        public int IntIdPemriso { get; set; }
        public int IntIdPagina { get; set; }
        public string StrNombrePagina { get; set; }
        public string StrDescripcionPage { get; set; }
        public bool BolEdit { get; set; }
    }
}
