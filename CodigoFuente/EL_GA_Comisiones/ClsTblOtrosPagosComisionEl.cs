﻿namespace EL_GA_Comisiones
{
    public class ClsTblOtrosPagosComisionEl
    {
        public int IntId { get; set; }
        public int IntIdComision { get; set; }
        public int IntIdTipoPago { get; set; }
        public int IntIdPago { get; set; }
        public decimal DecImporteTotal { get; set; }
        public decimal DecImporteAPagar { get; set; }
        public int IntAno { get; set; }
        public bool BolPagado { get; set; }
        public ClsTblAnticiposEl ObjClsTblAnticiposEl { get; set; }

        public ClsTblOtrosPagosComisionEl()
        {
            ObjClsTblAnticiposEl = new ClsTblAnticiposEl();
        }

    }
}
