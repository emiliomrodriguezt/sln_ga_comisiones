﻿using System;

namespace EL_GA_Comisiones
{
    public class ClsTblMubCalculadoProyecto
    {
        public int IntId { get; set; }
        public int IntIdCentroCostos { get; set; }
        public int IntPeriodo { get; set; }
        public int IntAno { get; set; }
        public decimal DecTotalIngresos { get; set; }
        public decimal DecTotalEgresos { get; set; }
        public decimal DecTotalIntereses { get; set; }
        public decimal DecMubCalculado { get; set; }
        public decimal DecMubComision { get; set; }
        public bool BolCongelado { get; set; }
        public int? IntIdUsuarioCreacion { get; set; }
        public int? IntIdUsuarioActualizacion { get; set; }
        public DateTime? DtFechaCreacion { get; set; }
        public DateTime? DtFechaActualizacion { get; set; }
        public ClsCatCentroCostosEl ObjClsCatCentroCostosEl { get; set; }

        public ClsTblMubCalculadoProyecto()
        {
            ObjClsCatCentroCostosEl = new ClsCatCentroCostosEl();
        }
    }
}