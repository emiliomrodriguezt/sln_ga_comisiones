﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EL_GA_Comisiones
{
    public class ClsComisionesPagadas
    {
        public int IdComision {get;set;}

        public string Colaborador { get; set; }

        public string UnidadNegocio { get; set; }

        public int Anio { get; set; }

        public decimal MontoPagado { get; set; }
    }
}
