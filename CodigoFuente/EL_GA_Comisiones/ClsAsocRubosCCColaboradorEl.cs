﻿using System;

namespace EL_GA_Comisiones
{
    public class ClsAsocRubosCcColaboradorEl
    {
        public int IntId { get; set; }
        public int IntIdRubroCc { get; set; }
        public int IntIdColaborador { get; set; }
        public decimal DecPorcentaje { get; set; }
        public int? IntIdUsuarioCreacion { get; set; }
        public DateTime? DtFechaCreacion { get; set; }

        public ClsTblColaboradorEl ObjClsTblColaboradorEl { get; set; }
        public ClsAsocRubrosCentroCostosEl ObjClsAsocRubrosCentroCostosEl { get; set; }

        public ClsAsocRubosCcColaboradorEl()
        {
            ObjClsTblColaboradorEl = new ClsTblColaboradorEl();
            ObjClsAsocRubrosCentroCostosEl = new ClsAsocRubrosCentroCostosEl();
        }
    }
}
