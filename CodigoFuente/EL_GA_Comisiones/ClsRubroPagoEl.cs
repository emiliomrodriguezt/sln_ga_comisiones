﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EL_GA_Comisiones
{
    public class ClsRubroPagoEl
    {
        public int IdRubro { get; set; }

        public string Nombre { get; set; }

        public decimal Porcentaje { get; set; }

        public int Activo { get; set; }

        public string Estatus { get; set; }

        public int IdUsuarioCreacion { get; set; }

        public int? IdUsuarioActulizacion { get; set; }

        public DateTime FechaCreacion { get; set; }

        public DateTime? FechaActulizacion { get; set; }
    }
}
