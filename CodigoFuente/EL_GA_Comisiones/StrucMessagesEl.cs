﻿namespace EL_GA_Comisiones
{
    public struct StrucMessagesEl
    {
        //Login
        public const string LoginPassIncorrect = "Tu usuario y/o contraseña son incorrectos.";
        public const string LoginBloq = "Tu cuenta se encuentra bloqueada en el dominio.";
        public const string LoginDesactivada = "Tu cuenta se encuentra desactivada en el dominio.";
        public const string LoginUnRegister = "Tu cuenta no se encuentra registrada en el sistema.";
        //
        //Permisos
        public const string WithoutPermits = "No cuenta con los permisos necesarios para ver esta página.";
        //
        //Genericos de operaciones
        public const string GenericoInsertOk = "El registro se guardo con éxito";
        public const string GenericoUpdateOk = "El registro se actualizo con éxito";
        public const string GenericoError = "Ocurrió un error al atender su petición, inténtelo mas tarde.";
        //
        //Generico actulizacion de centro de costos
        public const string AddCc = "El registro se agrego con éxito";
        public const string UpdateCc = "El registro se actualizo con éxito";
        public const string IgnorateCc = "El registro no sufrio cambios.";
        public const string DeleteCc = "El registro se marco como eliminado.";
        public const string ErrorCc = "Ocurrió un error al procesar el centro de costos, inténtelo mas tarde.";
        public const string DuplicateCc = "Este registro se encontro duplicado en el origen, se ignora.";
        public const string ErrorEliminarRubro = "Para eliminar el rubro primero debe eliminar a los colaboradores dependientes.";
        public const string ErrorDuplicadoRubro = "El rubro que intenta agregar ya se encuentra confgurado el el centro de costos";
        public const string ErrorDuplicadoColaboradorARubro = "El colaborador ya se encuentra asignado a este rubro en este centro de costos";
        public const string ErrorDuplicadoMubSas = "El colaborador ya se encuentra asignado en la distrubucion de MUB.";
        //
        //Tabuladores
        public const string CopyTabExists = "El año que intenta crear ya cuneta con registros.";
        public const string RangoCollapse = "El rango que intenta agregar [{0}] se enceuntra parcial o totalmente contenindo dentro de otro rango existente [{1}] para este tipo [{2}] y este año [{3}]";
        public const string MaxPorcentaje = "El maximo asignable es del 100% solo le queda por asignar {0}";
        //
        //
        public const string BonoUpdatePagado = "No puede editar un bono ya aplicado.";
        public const string AnticipoUpdatePagado = "No puede editar un anticipo ya aplicado";
        public const string BonoDeletePagado = "No puede eliminar un bono ya aplicado.";
        public const string AnticipoDeletePagado = "No puede eliminar un anticipo ya aplicado";
        //
        public const string TituloGenericoOk = "Operación correcta";
        public const string TituloGenericoError = "Operación con error";
        public const string GenericoNoEncontrado = "El registro no se encontro";
        public const string GenericoErrorParametros = "los parametros no son correctos";
        public const string GenericConfirmacionBorrado = "¡El registro se elimniara de manera pemanente!. ¿Desea continuar?";
        public const string GenericConfirmacionTitulo = "¡Esta eliminando un registro!.";
        //
    }
}
