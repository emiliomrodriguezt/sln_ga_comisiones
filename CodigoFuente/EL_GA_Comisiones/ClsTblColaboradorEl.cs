﻿using System;

namespace EL_GA_Comisiones
{
    public class ClsTblColaboradorEl
    {
        public int IntId { get; set; }
        public int IntIdPuesto { get; set; }
        public int IntIdCentroCostos { get; set; }
        public string StrNoColaborador { get; set; }
        public string StrNombreColaborador { get; set; }
        public DateTime DtFechaAlta { get; set; }
        public DateTime? DtFechaBaja { get; set; }
        public string StrNoEmpresa { get; set; }
        public string StrEmpresa { get; set; }
        public decimal DecSueldoNomina { get; set; }
        public decimal DecSueldoHonorarios { get; set; }
        public bool BolActivo { get; set; }
        public int? IntIdUsuarioCreacion { get; set; }
        public int? IntIdUsuarioActualizacion { get; set; }
        public DateTime? DtFechaCreacion { get; set; }
        public DateTime? DtFechaActualizacion { get; set; }

        public ClsCatCentroCostosEl ObjClsCatCentroCostosEl { get; set; }

        public ClsTblColaboradorEl()
        {
            ObjClsCatCentroCostosEl = new ClsCatCentroCostosEl();
        }
    }
}
