﻿using System;

namespace EL_GA_Comisiones
{
    public class ClsCatTipoTabuladorEl
    {
        public int IntId { get; set; }
        public string StrNombre { get; set; }
        public bool BolActivo { get; set; }
        public int? IntIdUsuarioCreacion { get; set; }
        public int? IntIdUsuarioActulizacion { get; set; }
        public DateTime? DtFechaCreacion { get; set; }
        public DateTime? DtFechaActualizacion { get; set; }
    }
}
