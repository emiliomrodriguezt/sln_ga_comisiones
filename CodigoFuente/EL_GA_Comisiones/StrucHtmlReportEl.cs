﻿namespace EL_GA_Comisiones
{
    public struct StrucHtmlReportEl
    {
        //Login
        public const string ReportFirmasCss = "table{ font-size: 10px; text-align:center; width:90%; } th{ text-align:center; } td{ text-align:center; }";
        public const string ReportFirmasHtml = "<table><tr><th>Contraloria.</th><th>Dirección UN.</th></tr><tr><td><br/><br/>__________________</td><td><br/><br/>__________________</td></tr><tr><td>{0}</td><td>{1}</td></tr>" +
	                                           "<tr><td><br/><br/><br/><br/>&nbsp;</td></tr><tr><th>Direccion Grl.</th><th>Presidencia.</th></tr><tr><td><br/><br/>__________________</td><td><br/><br/>__________________</td></tr><tr><td>{2}</td><td>{3}</td></tr></table>";
    }
}
