﻿using System;

namespace EL_GA_Comisiones
{
    public class ClsAsocRolPaginaEl
    {
        public int IntId { get; set; }
        public int IntIdRol { get; set; }
        public int IntIdPagina { get; set; }
        public bool BolEditar { get; set; }
        public int? IntIdUsuarioCreacion { get; set; }
        public DateTime? DtFechaCreacion { get; set; }
    }
}
