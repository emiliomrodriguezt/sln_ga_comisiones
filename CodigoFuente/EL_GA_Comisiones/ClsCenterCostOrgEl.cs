﻿using System;

namespace EL_GA_Comisiones
{
    public class ClsCenterCostOrgEl
    {
        public string StrId { get; set; }
        public string StrNombre { get; set; }
        public DateTime? DtFechaInicio { get; set; }
        public DateTime? DtFechaCierreAdmin { get; set; }
        public string StrEmpresa { get; set; }
        public string StrEstatus { get; set; }
        public string StrUnidadNegocio { get; set; }
        public string StrClasificacion { get; set; }
        public string StrArea { get; set; }
        public string StrSubArea { get; set; }
        public string StrClasificacionAlterna { get; set; }
        public string StrAreaAlterna { get; set; }
        public string StrSubAreaAlterna { get; set; }
        public string StrZSource { get; set; }
        public bool? BolZActive { get; set; }
        public string StrZComentario { get; set; }
        public string StrZIdHomologado { get; set; }
        public string StrZNombreHomologado { get; set; }
        public string StrZFuenteHomologada { get; set; }
    }
}
