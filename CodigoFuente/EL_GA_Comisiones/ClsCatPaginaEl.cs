﻿using System;

namespace EL_GA_Comisiones
{
    public class ClsCatPaginaEl
    {
        public int IntId { get; set; }
        public int? IntIdPagina { get; set; }
        public string StrNombre { get; set; }
        public string StrUrl { get; set; }
        public string StrIcono { get; set; }
        public int IntOrden { get; set; }
        public string StrDescripcion { get; set; }
        public bool BolActivo { get; set; }
        public int? IntIdUusarioCreacion { get; set; }
        public int? IntIdUsuarioActualizacion { get; set; }
        public DateTime? DtFechaCreacion { get; set; }
        public DateTime? DtActualizacion { get; set; }

    }
}
