﻿using System;

namespace EL_GA_Comisiones
{
    public class ClsCatCentroCostosEl
    {
        public int IntId { get; set; }
        public string StrId { get; set; }
        public int? IntIdDirector { get; set; }
        public int? IntIdGerente { get; set; }
        public string StrNoEmpresa { get; set; }
        public string StrEmpresa { get; set; }
        public DateTime? DtFechaInicio { get; set; }
        public string StrEstatus { get; set; }
        public string StrNombre { get; set; }
        public DateTime? DtFechaCierreAdmin { get; set; }
        public string StrUnidadNegocio { get; set; }
        public string StrClasificacion { get; set; }
        public string StrArea { get; set; }
        public string StrSubArea { get; set; }
        public string StrZIdHomologado { get; set; }
        public string StrZNombreHomologado { get; set; }
        public string StrAreaAlterna { get; set; }
        public string StrSubAreaAlterna { get; set; }
        public string StrClasificacionAlterna { get; set; }
        public string StrZSource { get; set; }
        public bool? BolZActive { get; set; }
        public string StrZComentario { get; set; }
        public string StrZFuenteHomologada { get; set; }
        public DateTime? DtFechaPayBack { get; set; }
        public int? IntDuracionMeses { get; set; }
        public decimal? DecIngresosContrato { get; set; }
        public decimal? DecEgresoContrato { get; set; }
        public decimal? DecMubContrato { get; set; }
        public string StrObservaciones { get; set; }
        public int? IntAnoCreacion { get; set; }
        public bool BolActivo { get; set; }
        public int? IntIdUsuarioCreacion { get; set; }
        public int? IntIdUsuarioActualizacion { get; set; }
        public DateTime? DtFechaCreacion { get; set; }
        public DateTime? DtFechaActualizacion { get; set; }
    }
}
