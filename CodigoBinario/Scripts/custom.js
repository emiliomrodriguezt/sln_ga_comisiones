var jsonPermits = {
    Edit: false,
    Delete : false
}

var fnAjax = function (strUrlApi, jsonData, ajaxType, fnCallback) {
    if (fnCallback == undefined) {
        fnCallback = function (dataResponse) {

        }
    }

    if (ajaxType == undefined) {
        ajaxType = "POST";
    }

    if (jsonData == undefined) {
        jsonData = new {};
    }

    $.ajax({
        type: ajaxType,
        contentType: "application/json; charset=utf-8",
        url: strUrlApi,
        data: JSON.stringify(jsonData),
        dataType: "json",
        success: function (dataResponse) {
            fnCallback(dataResponse);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        },
        failure: function (errMsg) {
            alert(errMsg);
        }
    });
}

var fnDiSabledForm = function (objContenedor) {
    $("#" + objContenedor + " input").each(function () {
        $(this).prop("disabled", "disabled");
    });

    $("#" + objContenedor + " select").each(function () {
        $(this).prop("disabled", "disabled");

    });

    $("#" + objContenedor + " textarea").each(function () {
        $(this).prop("disabled", "disabled");
    });

    $(".validaForm").prop("disabled", "disabled");
}

var fnGetQuery = function () {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf("?") + 1).split("&");
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split("=");
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

var fnEliminaRegistro = function (id, strPath) {
    $.ajax({
        type: "GET",
        url: strPath + id,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (dataResponse) {
            var strType = "alert-danger"; 
            if (dataResponse.BolSuccess) {
                fnFillTable();
                strType = "alert-success";
            }
            fnMessage(strType, dataResponse.StrMessage, true);
            $("#btnCloseModal").trigger("click");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    });
}

var fnConfirDelete = function (id, strPath) {
    $("#alertTitulo").html("<i class=\"fa fa-exclamation-triangle text-warning\"> &nbsp;&nbsp;&nbsp; </i> <span class=\"bg-warning\">&iexcl;Esta eliminando un registro!.</span>");
    $("#alertMensaje").html("&iexcl;El registro se elimninara de manera pemanente!. &iquest;Desea continuar?");
    $("#btnAceptarModal").attr("onclick", "fnEliminaRegistro('" + id + "','" + strPath+"')");
    $("#alertModal").modal("show");
}

var fnGetTable = function (objTable, objData, objColumns, strUrlApi, ajaxType, fnCallback, bolPaginate, bolFilter, bInfo) {
    if (fnCallback == undefined) {
        fnCallback = function () {
           
        }
    }

    if (ajaxType == undefined) {
        ajaxType = "POST";
    }

    if (bolPaginate == undefined) {
        bolPaginate = true;
    }

    if (bolFilter == undefined) {
        bolFilter = true;
    }

    if (bInfo == undefined) {
        bInfo = true;
    }

    $.ajax({
        type: ajaxType,
        url: strUrlApi,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(objData),
        success: function (dataResponse) {
            $(objTable).dataTable({
                "bFilter": bolFilter,
                "bPaginate": bolPaginate,
                "bInfo": bInfo,
                "aaData": dataResponse,
                "sPaginationType": "full_numbers",
                "bAutoWidth": false,
                "serverSide": true,
                "bDestroy": true,
                "fnDrawCallback": function (oSettings) {
                    fnCallback();
                    adjustmainpanelheight();
                    
                },
                "iDisplayLength": 5,
                "oLanguage": {
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "&Uacute;ltimo",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente",
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente"
                    },
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sEmptyTable": "No hay informaci&oacute;n disponible",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sLoadingRecords": "Cargando...",
                    "sZeroRecords": "No se encontraron registros.",
                    "sProcessing": "Espere, por favor...",
                    "sSearch": "B&uacute;squeda R&aacute;pida:",
                    "sLengthMenu": "<select class=\"form-control input-sm mb15\">" +
                    "<option value=\"-1\">Todos</option>" +
                    "<option value=\"5\">5</option>" +
                    "<option value=\"10\">10</option>" +
                    "<option value=\"25\">25</option>" +
                    "<option value=\"50\">50</option>" +
                    "<option value=\"100\">100</option>" +
                    "</select>"
                },
                "aoColumns": objColumns,
                "aaSorting": []
            });
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    });
};

var fnCookiePermit = function () {
    if (!!$.cookie("Permits")) {
        var arryCokies = $.cookie("Permits").split("&");
        $.each(arryCokies, function (index, value) {
            var arrKeyValor = value.split("=");
            switch (arrKeyValor[0]) {
                case "Edit":
                    if (arrKeyValor[1] === "True") {
                        jsonPermits.Edit = true;
                    }
                    break;
                case "Delete":
                    if (arrKeyValor[1] === "True") {
                        jsonPermits.Delete = true;
                    }
                    break;
                }
        });
    } 
}

var fnMessage = function (strType, strMsg, bolShow) {
    if (bolShow == undefined) {
        ajaxType = false;
    }

    $("#divMensaje").removeClass("alert-success");
    $("#divMensaje").removeClass("alert-info");
    $("#divMensaje").removeClass("alert-warning");
    $("#divMensaje").removeClass("alert-danger");
    if (bolShow) {
        $("#divMensaje").removeClass("cssClsHidden");
        $("#divMensaje").addClass(strType);
        $("#strMensaje").html(strMsg);
        $("body").scrollTop(0);
    } else {
        $("#divMensaje").addClass("cssClsHidden");
        $("#strMensaje").html("");
    }
}

var fnEsRequeridoText = function (objeto) {
    var intValido = 0;
    var objValue = $(objeto).val();
    if ($(objeto).is(":visible")) {
        if (objValue === "") {
            $(objeto).parent().next().show();
            $(objeto).parent().next().html("El campo es obligatorio");
            $(objeto).addClass("form-control-error");
            intValido = 1;
        } else {
            $(objeto).parent().next().hide();
            $(objeto).parent().next().html("");
            $(objeto).removeClass("form-control-error");
        }
    }
    return intValido;
}

var fnEsRequeridoSelect = function (objeto) {
    var intValido = 0;
    var objValue = $(objeto).val();
    if ($(objeto).is(":visible")) {
        if (objValue === "" || objValue === "0") {
            $(objeto).parent().next().show();
            $(objeto).parent().next().html("El campo es obligatorio");
            $(objeto).addClass("form-control-error");
            intValido = 1;
        } else {
            $(objeto).parent().next().hide();
            $(objeto).parent().next().html("");
            $(objeto).removeClass("form-control-error");
        }
    }
    return intValido;
}

var fnIsNumberKey = function (evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 45 || charCode > 57)) {
        evt.preventDefault();
    }
}

var fnEsCorreo = function (objeto) {
    var intValido = 0;
    var filter = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if ($(objeto).is(":visible")) {
        var objValue = $(objeto).val();
        if (objValue !== "") {
            if (!filter.test(objValue)) {
                $(objeto).parent().next().show();
                $(objeto).parent().next().html("No parece ser un correo valido");
                $(objeto).addClass("form-control-error");
                intValido = 1;
            } else {
                $(objeto).parent().next().hide();
                $(objeto).parent().next().html("");
                $(objeto).removeClass("form-control-error");
            }
        }
    }
    return intValido;
}

var fnEsFecha = function (objeto) {
    var intValido = 0;
    var filter = /^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;
    if ($(objeto).is(":visible")) {
        var objValue = $(objeto).val();
        if (objValue !== "") {
            if (!filter.test(objValue)) {
                $(objeto).parent().next().show();
                $(objeto).parent().next().html("La fecha no es v&aacute;lida");
                $(objeto).addClass("form-control-error");
                return 1;
            }

            var dia = parseInt(objValue.substring(0, 2));
            var mes = parseInt(objValue.substring(3, 5));
            var anio = parseInt(objValue.substring(6));
            var valido = true;
                
            if (mes === 1 || mes === 3 || mes === 5 || mes === 7 || mes === 8 || mes === 10 || mes === 12) {
                if (dia < 1 || dia > 31) {
                    valido = false;
                }
            } else if (mes === 4 || mes === 6 || mes === 9 || mes === 11) {
                if (dia < 1 || dia > 30) {
                    valido = false;
                }
               
            } else if (mes === 2) {
                if (anio % 4 !== 0 && (dia < 1 || dia > 28)) {
                    valido = false;
                }

                if (anio % 100 === 0 && anio % 400 === 0 && (dia < 1 || dia > 29)) {
                    valido = false;
                }

                if (anio % 100 === 0 && anio % 400 === 0 && (dia < 1 || dia > 28)) {
                    valido = false;
                }
            } else {
                valido = false;
            }

            if (!valido) {
                $(objeto).parent().next().show();
                $(objeto).parent().next().html("La fecha no es v&aacute;lida");
                $(objeto).addClass("form-control-error");
                return 1;
            }

            $(objeto).parent().next().hide();
            $(objeto).parent().next().html("");
            $(objeto).removeClass("form-control-error");
        }
    }
    return intValido;
}

var fnIsNumeric = function (n) {
    n = n.replace(/,/gi, "").replace("$", "").replace(" ", "");
    return !isNaN(parseFloat(n)) && isFinite(n);
}

var fnEsDecimal = function (validacion, objeto) {
    var intResult = 0;
    var objValue = $(objeto).val();
    if ($(objeto).is(":visible")) {
        if (objValue !== "") {
            var arrValidacion = validacion.replace("decimal[", "").replace("]", "").split(",");
            var arrValores = objValue.split(".");
            if (arrValores.length === 2 && arrValidacion.length === 2) {
                if (fnIsNumeric(arrValidacion[0]) && fnIsNumeric(arrValidacion[1]) && fnIsNumeric(arrValores[0]) && fnIsNumeric(arrValores[1])) {
                    var largoEntero = parseInt(arrValidacion[0]);
                    var largoDecimal = parseInt(arrValidacion[1]);
                    var entero = parseInt(arrValores[0]);
                    entero = entero * -1;
                    if (largoEntero === arrValores[0].replace("-", "").length) {
                        if (largoDecimal === arrValores[1].length) {
                            $(objeto).parent().next().hide();
                            $(objeto).parent().next().html("");
                            $(objeto).removeClass("form-control-error");
                        } else {
                            $(objeto).parent().next().html("La parte decimal tiene una longitud diferente de " + largoDecimal );
                            $(objeto).parent().next().show();
                            $(objeto).addClass("form-control-error");
                            intResult = 1;
                        }
                    } else {
                        $(objeto).parent().next().html("La parte entera tiene una longitud diferente de " + largoEntero);
                        $(objeto).parent().next().show();
                        $(objeto).addClass("form-control-error");
                        intResult = 1;
                    }
                } else {
                    $(objeto).parent().next().html("El campo no es un decimal");
                    $(objeto).parent().next().show();
                    $(objeto).addClass("form-control-error");
                    intResult = 1;
                }
            } else {
                $(objeto).parent().next().html("El campo no es un decimal");
                $(objeto).parent().next().show();
                $(objeto).addClass("form-control-error");
                intResult = 1;
            }
        }
    }
    return intResult;
}

var fnRango = function (validacion, objeto) {
    var intResult = 0;
    var objValue = $(objeto).val();
    if ($(objeto).is(":visible")) {
        if ($(objeto).val() !== "") {
            var arrValoresValidos = validacion.replace("rango[", "").replace("]", "").split(",");
            if (arrValoresValidos.length !== 2 &&
                !fnIsNumeric(arrValoresValidos[0]) &&
                !fnIsNumeric(arrValoresValidos[1])) {
                $(objeto).parent().next().show();
                $(objeto).parent().next().html("Los valores configurados no son corrcetos [" + validacion + "]");
                $(objeto).parent().addClass("form-control-error");
                intResult++;
            }

            if (!fnIsNumeric(objValue)) {
                $(objeto).parent().next().show();
                $(objeto).parent().next().html("El valor [" + objValue + "] no  es un numero valido");
                $(objeto).addClass("form-control-error");
                intResult++;
            }

            var bajo = parseFloat(arrValoresValidos[0]);
            var alto = parseFloat(arrValoresValidos[1]);
            var valor = parseFloat(objValue);

            if (valor >= bajo && valor <= alto) {
                $(objeto).parent().next().hide();
                $(objeto).parent().next().html("");
                $(objeto).removeClass("form-control-error");
            } else {
                $(objeto).parent().next().show();
                $(objeto).parent().next().html("El valor [" + valor + "] no se encuentra dentro del rango [" + bajo + "-" + alto + "]");
                $(objeto).addClass("form-control-error");
                intResult++;
            }
        }
    }
    return intResult;
}

var fnValores = function(validacion, objeto) {
    var intResult = 0;

    if ($(objeto).is(":visible")) {
        var objValue = $(objeto).val();
        if (objValue !== "")
        {
            var arrValoresValidos = validacion.replace("valores[", "").replace("]", "").split(",");
            if (arrValoresValidos.length <= 0) {
                $(objeto).parent().next().show();
                $(objeto).parent().next().html("Error de configucacion de validacion [" + validacion + "]");
                $(objeto).addClass("form-control-error");
                intResult++;
            }

            intResult = 1;
            $(objeto).parent().next().show();
            $(objeto).parent().next().html("Valor fuera del esperado [" + arrValoresValidos.toString() + "]");
            $(objeto).addClass("form-control-error");

            for (var itm in arrValoresValidos) {
                if (arrValoresValidos.hasOwnProperty(itm)) {
                    if (itm === objValue) {
                        $(objeto).parent().next().hide();
                        $(objeto).parent().next().html("");
                        $(objeto).removeClass("form-control-error");
                        intResult = 0;
                        break;
                    }
                }
            }
        }
    }

    return intResult;
}

var fnValidaDatos = function (objContenedor) {
    var intErrores = 0;

    $("#" + objContenedor + " input").each(function () {
        var object = $(this);
        var tipoValidacion = $(this).attr("data-validar");
        if (typeof tipoValidacion !== "undefined" && tipoValidacion !== null) {
            var arrayValidacion = tipoValidacion.split("|");
            $.each(arrayValidacion, function (index, value) {
                if (value.indexOf("decimal", 0) !== -1) {
                    var intEsdecimal = fnEsDecimal(value, object);
                    if (intEsdecimal > 0) {
                        intErrores++;
                    }
                } else if (value.indexOf("valores", 0) !== -1) {
                    var intEsValores = fnValores(value, object);
                    if (intEsValores > 0) {
                        intErrores++;
                    }
                } else if (value.indexOf("rango", 0) !== -1) {
                    var intEsRango = fnRango(value, object);
                    if (intEsRango > 0) {
                        intErrores++;
                    }
                } else {
                    switch (value) {
                    case "requerido":
                        var intRequerido = fnEsRequeridoText(object);
                        if (intRequerido > 0) {
                            intErrores++;
                        }
                        break;
                    case "correo":
                        var intCorreo = fnEsCorreo(object);
                        if (intCorreo > 0) {
                            intErrores++;
                        }
                        break;
                    case "fecha":
                        var intFecha = fnEsFecha(object);
                        if (intFecha > 0) {
                            intErrores++;
                        }
                        break;
                    case "numerico":
                        var valor = $(object).val();
                        if (valor !== "") {
                            if (!fnIsNumeric(valor)) {
                                $(object).parent().next().show();
                                $(object).parent().next().html("No es un numero valido");
                                $(object).addClass("form-control-error");
                                intErrores++;
                            }
                        }
                        break;
                    }
                }
            });
        }
    });

    $("#" + objContenedor + " select").each(function () {
        var object = $(this);
        var tipoValidacion = $(this).attr("data-validar");
        if (typeof tipoValidacion !== "undefined" && tipoValidacion !== null) {
            var arrayValidacion = tipoValidacion.split("|");
            $.each(arrayValidacion, function (index, value) {
                switch (value) {
                    case "requerido":
                        var intRequeridoSelect = fnEsRequeridoSelect(object);
                        if (intRequeridoSelect > 0) {
                            intErrores++;
                        }
                        break;
                    default:
                }
            });
        }
    });

    $("#" + objContenedor + " textarea").each(function () {
        var object = $(this);
        var tipoValidacion = $(this).attr("data-validar");
        if (typeof tipoValidacion !== "undefined" && tipoValidacion !== null) {
            var arrayValidacion = tipoValidacion.split("|");
            $.each(arrayValidacion, function (index, value) {
                switch (value) {
                    case "requerido":
                        var intRequeridoText = fnEsRequeridoText(object);
                        if (intRequeridoText > 0) {
                            intErrores++;
                        }
                        break;
                }
            });
        }
    });

    if (intErrores === 0) {
        return true;
    } else {
        fnMessage("alert-danger", "Se encontraron errores.", true);
        return false;
    }
}

var fnShowLoader = function() {
    $("#status").show();
    $("#preloader").show();
}

var fnHideLoader = function () {
    $("#status").fadeOut();
    $("#preloader").delay(350).fadeOut(function () {
        $("body").delay(350).css({ "overflow": "visible" });
    });
}

var adjustmainpanelheight = function () {
    var docHeight = $(".contentpanel").height();
    if (docHeight > $(".mainpanel").height()) {
        $(".mainpanel").height(docHeight);
    } 
}

var closeVisibleSubMenu = function(){
    $(".leftpanel .nav-parent").each(function () {
        var t = $(this);
        if (t.hasClass("nav-active")) {
            t.find("> ul").slideUp(200, function () {
                t.removeClass("nav-active");
            });
        }
    });
}

var reposition_searchform = function() {
    if ($(".searchform").css("position") === "relative") {
        $(".searchform").insertBefore(".leftpanelinner .userlogged");
    } else {
        $(".searchform").insertBefore(".header-right");
    }
}

function reposition_topnav() {
    if ($(".nav-horizontal").length > 0) {
        if ($(".nav-horizontal").css("position") === "relative") {

            if ($(".leftpanel .nav-bracket").length === 2) {
                $(".nav-horizontal").insertAfter(".nav-bracket:eq(1)");
            } else {
                if ($(".leftpanel .nav-horizontal").length === 0)
                    $(".nav-horizontal").appendTo(".leftpanelinner");
            }

            $(".nav-horizontal").css({ display: "block" })
                .addClass("nav-pills nav-stacked nav-bracket");

            $(".nav-horizontal .children").removeClass("dropdown-menu");
            $(".nav-horizontal > li").each(function () {

                $(this).removeClass("open");
                $(this).find("a").removeAttr("class");
                $(this).find("a").removeAttr("data-toggle");

            });

            if ($(".nav-horizontal li:last-child").has("form")) {
                $(".nav-horizontal li:last-child form").addClass("searchform").appendTo(".topnav");
                $(".nav-horizontal li:last-child").hide();
            }

        } else {
            if ($(".leftpanel .nav-horizontal").length > 0) {

                $(".nav-horizontal").removeClass("nav-pills nav-stacked nav-bracket").appendTo(".topnav");
                $(".nav-horizontal .children").addClass("dropdown-menu").removeAttr("style");
                $(".nav-horizontal li:last-child").show();
                $(".searchform").removeClass("searchform").appendTo(".nav-horizontal li:last-child .dropdown-menu");
                $(".nav-horizontal > li > a").each(function () {

                    $(this).parent().removeClass("nav-active");

                    if ($(this).parent().find(".dropdown-menu").length > 0) {
                        $(this).attr("class", "dropdown-toggle");
                        $(this).attr("data-toggle", "dropdown");
                    }

                });
            }
        }
    }
}

$(window).load(function() {
   $("#status").fadeOut();
   $("#preloader").delay(350).fadeOut(function(){
      $("body").delay(350).css({"overflow":"visible"});
   });
});

$(document).ajaxStart(function () {
    fnShowLoader();
});

$(document).ajaxStop(function () {
    $(".tooltips").tooltip({ container: "body" });
    adjustmainpanelheight();
    fnHideLoader();
});

$(document).ready(function () {
    $("body").addClass("leftpanel-collapsed");
    $(".menutoggle").addClass("menu-collapsed");

   $(".leftpanel .nav-parent > a").live("click", function() {
      var parent = $(this).parent();
      var sub = parent.find("> ul");
      
      if(!$("body").hasClass("leftpanel-collapsed")) {
         if(sub.is(":visible")) {
            sub.slideUp(200, function(){
               parent.removeClass("nav-active");
               $(".mainpanel").css({height: ""});
               adjustmainpanelheight();
            });
         } else {
            closeVisibleSubMenu();
            parent.addClass("nav-active");
            sub.slideDown(200, function(){
               adjustmainpanelheight();
            });
         }
      }
      return false;
   });
   
   $(".money").maskMoney({ prefix: "$ ", allowNegative: true, allowZero: true });
   
   $(".tooltips").tooltip({ container: "body" });

   $(".dataTables").dataTable({
       "bFilter": true,
       "bPaginate": true,
       "bInfo": true,
       "sPaginationType": "full_numbers",
       "bAutoWidth": false,
       "bProcessing": true,
       "serverSide": true,
       "bDestroy": true,
       "iDisplayLength": 10,
       "fnDrawCallback": function (oSettings) {
           adjustmainpanelheight();
       },
       "oLanguage": {
           "oPaginate": {
               "sFirst": "Primero",
               "sLast": "&Uacute;ltimo",
               "sNext": "Siguiente",
               "sPrevious": "Anterior"
           },
           "oAria": {
               "sSortDescending": ": Activar para ordenar la columna de manera descendente",
               "sSortAscending": ": Activar para ordenar la columna de manera ascendente"
           },
           "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
           "sEmptyTable": "No hay informaci&oacute;n disponible",
           "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
           "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
           "sInfoPostFix": "",
           "sLoadingRecords": "Cargando...",
           "sZeroRecords": "No se encontraron registros.",
           "sProcessing": "Espere, por favor...",
           "sSearch": "B&uacute;squeda R&aacute;pida:",
           "sLengthMenu": "<select class=\"form-control mb15\">" +
                 "<option value=\"-1\">Todos</option>" +
                 "<option value=\"10\">10</option>" +
                 "</select>"
       }
   });

   $(".validaForm").click(function (event) {
       var objContentValid = $(this).attr("data-form");
       if (!fnValidaDatos(objContentValid)) {
           event.preventDefault();
       }
   });

   $(".onlyNumber").keypress(function (evt) {
       fnIsNumberKey(evt);
   });

   $("form").keypress(function (evt) {
       var charCode = (evt.which) ? evt.which : event.keyCode;
       if (charCode === 13) {
           return false;
       }
       return true;
   });

   $("input").keypress(function (evt) {
       var charCode = (evt.which) ? evt.which : event.keyCode;
       if (charCode === 13) {
           return false;
       }
       return true;
   });

    $(".UpperCase").keyup(function(evt) {
        this.value = this.value.toUpperCase();
    });

   $.datepicker.regional["es"] = {
       closeText: "Cerrar",
       prevText: "<Ant",
       nextText: "Sig>",
       currentText: "Hoy",
       monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
       monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
       dayNames: ["Domingo", "Lunes", "Martes", "Mi�rcoles", "Jueves", "Viernes", "S&aacute;bado"],
       dayNamesShort: ["Dom", "Lun", "Mar", "Mi�", "Juv", "Vie", "S&aacute;b"],
       dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "S&aacute;"],
       weekHeader: "Sm",
       dateFormat: "dd/mm/yy",
       firstDay: 1,
       isRTL: false,
       showMonthAfterYear: false,
       yearSuffix: ""
   };
   $.datepicker.setDefaults($.datepicker.regional["es"]);

   $(".datepickerEs").datepicker({
       changeMonth: true,
       changeYear: true
    });

   $(".datepickerEs").mask("00/00/0000");

   $(".popovers").popover();
   
   $(".panel .panel-close").click(function(){
      $(this).closest(".panel").fadeOut(200);
      return false;
   });
   
   $(".toggle").toggles({on: true});  
   
   $(".minimize").click(function(){
      var t = $(this);
      var p = t.closest(".panel");
      if(!$(this).hasClass("maximize")) {
         p.find(".panel-body, .panel-footer").slideUp(200);
         t.addClass("maximize");
         t.html("&plus;");
      } else {
         p.find(".panel-body, .panel-footer").slideDown(200);
         t.removeClass("maximize");
         t.html("&minus;");
      }
      return false;
   });
   
   $(".nav-bracket > li").hover(function(){
      $(this).addClass("nav-hover");
   }, function(){
      $(this).removeClass("nav-hover");
   });
   
   $(".menutoggle").click(function(){
      
      var body = $("body");
      var bodypos = body.css("position");
      
      if(bodypos !== "relative") {
         
         if(!body.hasClass("leftpanel-collapsed")) {
            body.addClass("leftpanel-collapsed");
            $(".nav-bracket ul").attr("style","");
            
            $(this).addClass("menu-collapsed");
            
         } else {
            body.removeClass("leftpanel-collapsed chat-view");
            $(".nav-bracket li.active ul").css({display: "block"});
            
            $(this).removeClass("menu-collapsed");
            
         }
      } else {
         
         if(body.hasClass("leftpanel-show"))
            body.removeClass("leftpanel-show");
         else
            body.addClass("leftpanel-show");
         
         adjustmainpanelheight();         
      }

   });
   
   $(window).resize(function(){
      if($("body").css("position") === "relative") {

         $("body").removeClass("leftpanel-collapsed chat-view");
         
      } else {
         
         $("body").removeClass("chat-relative-view");         
         $("body").css({left: "", marginRight: ""});
      }

       if ($(".leftpanel .searchform").length === 0) {
           reposition_searchform();
       }
       reposition_topnav();
       adjustmainpanelheight();
   });
   
    if ($.cookie("sticky-header")) {
        $("body").addClass("stickyheader");
    }

    if($.cookie("sticky-leftpanel")) {
      $("body").addClass("stickyheader");
      $(".leftpanel").addClass("sticky-leftpanel");
   }
   
    if ($.cookie("change-skin")) {
        $("head").append("<link id=\"skinswitch\" rel=\"stylesheet\" href=\"css/style." + $.cookie("change-skin")+".css\" />");
    }
   
    if ($.cookie("change-font")) {
        $("head").append("<link id=\"fontswitch\" rel=\"stylesheet\" href=\"css/font." + $.cookie("change-font") + ".css\" />");
    }

    if ($("body").hasClass("leftpanel-collapsed")) {
        $(".nav-bracket .children").css({ display: "" });
    }

    $(".dropdown-menu").find("form").click(function (e) {
      e.stopPropagation();
    });

    fnCookiePermit();
    adjustmainpanelheight();
    reposition_topnav();
    reposition_searchform();

});