﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReporteMUBCC.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.Reportes.ReporteMUBCC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
    <i class="fa fa-sitemap" aria-hidden="true"></i> Reportes / MUB por centro de costos
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <main class="page">
        <form id="form1" runat="server">
            <asp:ScriptManager ID="scriptManager1" runat="server"></asp:ScriptManager>
            <div class="modal fade" id="modalRespuesta" role="dialog" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog" role="document" id="modalDialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button id="btnCloseModal" aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                            <h4 class="modal-title">Modificar datos</h4>
                        </div>
                        <div class="modal-body">
                            <asp:HiddenField ID="hdnIdMUB" runat="server" />
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="txtInteres">Intereses:</label>
                                        <asp:TextBox ID="txtInteres" runat="server" CssClass="form-control" data-validar="requerido"></asp:TextBox>
                                    </div>
                                    <small class="form-text-error"></small>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="txtMUB">MUB base de comisiones:</label>
                                        <asp:TextBox ID="txtMUB" runat="server" CssClass="form-control" data-validar="requerido"></asp:TextBox>
                                    </div>
                                    <small class="form-text-error"></small>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="col-md-6" style="text-align:right;">
                                <asp:LinkButton  ID="btnGuardar" runat="server" CssClass="btn btn-primary validaForm" data-form="form1" OnClick="btnGuardar_Click" OnClientClick="javascript:return validaCambios();"><i class="fa fa-save" aria-hidden="true"></i> Guardar</asp:LinkButton>
                            </div>
                            <div class="col-md-6" style="text-align:left">
                                <button type="button" class="btn btn-danger" data-dismiss="modal" id="btnCerrar" ><i class="fa fa-times-circle" aria-hidden="true"></i> Cerrar</button>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <div>
                <div class="row">
                    <div class="col-md-12 tituloPagina">
                        <asp:Label ID="lblTitulo" runat="server">Reporte de MUB por C.C.</asp:Label>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <asp:UpdatePanel ID="panelBuscar" runat="server" ChildrenAsTriggers="true">
                    <ContentTemplate>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="ddlGerente" style="text-align:right">Gerente:</label>
                                    <asp:DropDownList ID="ddlGerente" runat="server" style="text-align:center;text-align-last:center;"
                                        CssClass="form-control" AppendDataBoundItems="true"
                                         AutoPostBack="true" OnSelectedIndexChanged="ddlGerente_SelectedIndexChanged">
                                        <asp:ListItem Text="--- Todos ---" Value="0" Selected="True"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="ddlProyecto" style="text-align:right">Centro de costos:</label>
                                    <asp:DropDownList ID="ddlProyecto" runat="server" style="text-align:center;text-align-last:center;"
                                        CssClass="form-control" AppendDataBoundItems="true" >
                                        <asp:ListItem Text="--- Todos ---" Value="0" Selected="True"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <small class="form-text-error"></small>
                            </div>
                            <div class="col-md-1" style="margin-top:27px;">
                                <div class="form-group">
                                    <asp:LinkButton  ID="btnBuscar" runat="server" CssClass="btn btn-primary" OnClick="btnBuscar_Click"><i class="fa fa-search" aria-hidden="true"></i> Buscar</asp:LinkButton>
                                </div>
                            </div>
                            <div class="col-md-1" style="margin-top:27px;">
                                <div class="form-group">
                                    <asp:LinkButton  ID="btnExportar" runat="server" CssClass="btn btn-success" OnClick="btnExportar_Click"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Exportar</asp:LinkButton>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                        <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10" style="overflow:auto;">
                        <asp:GridView ID="gvProyectos" runat="server" AutoGenerateColumns="false" CssClass="table table-striped"
                             DataKeyNames="IdMUB">
                            <Columns>
                                <asp:BoundField HeaderText="No. de UN" DataField="UnidadNegocio" />
                                <asp:BoundField HeaderText="Nombre UN" DataField="NombreUnidad" />
                                <asp:BoundField HeaderText="No. de C.C." DataField="ClaveCC" />
                                <asp:BoundField HeaderText="Nombre C.C." DataField="NombreCC" />
                                <asp:BoundField HeaderText="Ingresos" DataField="Ingresos" DataFormatString="{0:c}" />
                                <asp:BoundField HeaderText="Egresos" DataField="Egresos" DataFormatString="{0:c}" />
                                <asp:BoundField HeaderText="Intereses" DataField="Intereses" DataFormatString="{0:c}" />
                                <asp:BoundField HeaderText="MUB Contable" DataField="MUBContable" DataFormatString="{0:c}" />
                                <asp:BoundField HeaderText="MUB base de comisiones" DataField="MUBComision" DataFormatString="{0:c}" />
                                <asp:BoundField HeaderText="Año" DataField="Anio" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <a href='javascript:muestraMontos(<%#Eval("IdMUB") %>,"<%#Eval("Intereses") %>","<%#Eval("MUBComision") %>");' <%# EsVisible(Eval("Congelado")) %>>Editar</a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnBuscar" />
                        <asp:PostBackTrigger ControlID="btnExportar" />
                    </Triggers>
                </asp:UpdatePanel>
                
            </div>
        </form>
    </main>
    <script>
        function validaCampos()
        {
            if ($('#<%=ddlGerente.ClientID%>').val() == 0 || $('#<%=ddlProyecto.ClientID%>').val() == 0)
                return false;

            return true;
        }

        function validaCambios()
        {
            if ($('#<%=txtInteres.ClientID%>').val() == "" || $('#<%=txtMUB.ClientID%>').val() == "")
                return false;

            return true;
        }

        function muestraMontos(id, interes, mub)
        {
            $("#<%=hdnIdMUB.ClientID%>").val(id);
            var strInteres = interes.replace("$", "").replace(",", "");
            var strMub = mub.replace("$", "").replace(",", "");

            $("#<%=txtInteres.ClientID%>").val(strInteres);
            $("#<%=txtMUB.ClientID%>").val(strMub);

            $("#modalRespuesta").modal("show");
        }
    </script>
</asp:Content>
