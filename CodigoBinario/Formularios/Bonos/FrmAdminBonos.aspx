﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FrmAdminBonos.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.Bonos.FrmAdminBonos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
     <i class="fa fa-sitemap" aria-hidden="true"></i> Configuraci&oacute;n / Bonos
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <div class="row">
        <div class="col-md-12 text-right">
            <a href="<%= ResolveUrl("~/Formularios/Bonos/FrmEditBonos.aspx") %>" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Agregar bono</a>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped" id="dtResult"></table>
        </div>
    </div>
    <script type="text/javascript">
        var fnFillTable = function() {
            fnGetTable($("#dtResult"),
                {
                    BolEdit: jsonPermits.Edit,
                    BolDelete: true,
                    BolConsult: true,
                    StrPathEdit: $.cookie("strApplicationPath") + "/Formularios/Bonos/FrmEditBonos.aspx",
                    StrPathDelete: $.cookie("strApplicationPath") + "/api/Bono/DelBono/"
                },
                [
                    { "sTitle": "No Colaborador." },
                    { "sTitle": "Colaborador" },
                    { "sTitle": "CC" },
                    { "sTitle": "Importe Bono" },
                    { "sTitle": "Año" },
                    { "sTitle": "Pagado", "sClass": "text-center", "bSortable": false, "bSearchable": false },
                    { "sTitle": "Activo", "sClass": "text-center", "bSortable": false, "bSearchable": false },
                    { "sTitle": "Acciones", "sClass": "text-center", "bSortable": false, "bSearchable": false  }],
                $.cookie("strApplicationPath") + "/api/Bono/GetBonos/");
        }
        $(document).ready(function () {
            fnFillTable();
        });
    </script>
</asp:Content>
