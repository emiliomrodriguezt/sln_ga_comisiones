﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CalculoMUB.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.Calculos.CalculoMUB" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
    <i class="fa fa-sitemap" aria-hidden="true"></i> Operaciones / Calcular MUB
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <main class="page">
        <form id="form1" runat="server">
            <div>
                <div class="row">
                    <div class="col-md-12 tituloPagina">
                        <asp:Label ID="lblTitulo" runat="server">C&aacute;lculo de cuota MUB acumulada</asp:Label>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="ddlAnio">A&ntilde;o l&iacute;mite *:</label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="ddlPeriodo">Mes l&iacute;mite *:</label>
                        </div>
                    </div>
                    <div class="col-md-8"></div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <asp:DropDownList ID="ddlAnio" runat="server" style="text-align:center;text-align-last:center;" CssClass="form-control" AppendDataBoundItems="true" data-validar="requerido">
                                <asp:ListItem Text="--- Seleccione ---" Value="0" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <small class="form-text-error"></small>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <asp:DropDownList data-validar="requerido" ID="ddlPeriodo" runat="server" style="text-align:center;text-align-last:center;" CssClass="form-control">
                                <asp:ListItem Text="--- Seleccione ---" Value="0" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Enero" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Febrero" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Marzo" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Abril" Value="4"></asp:ListItem>
                                <asp:ListItem Text="Mayo" Value="5"></asp:ListItem>
                                <asp:ListItem Text="Junio" Value="6"></asp:ListItem>
                                <asp:ListItem Text="Julio" Value="7"></asp:ListItem>
                                <asp:ListItem Text="Agosto" Value="8"></asp:ListItem>
                                <asp:ListItem Text="Septiembre" Value="9"></asp:ListItem>
                                <asp:ListItem Text="Octubre" Value="10"></asp:ListItem>
                                <asp:ListItem Text="Noviembre" Value="11"></asp:ListItem>
                                <asp:ListItem Text="Diciembre" Value="12"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <small class="form-text-error"></small>
                    </div>
                    <div class="col-md-4">
                        <asp:LinkButton ID="btnCalcular" runat="server" CssClass="btn btn-primary validaForm" data-form="form1" OnClick="btnCalcular_Click"><i class="fa fa-database" aria-hidden="true"></i> Iniciar C&aacute;lculo</asp:LinkButton>
                    </div>
                    <div class="col-md-4">&nbsp;</div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row" id="resultados" runat="server">
                    <div class="col-md-12">
                        <h3>Resultados</h3>
                        <asp:GridView ID="gvResultados" runat="server" AutoGenerateColumns="false" CssClass="table table-striped dataTables" ShowHeader="true" EmptyDataText="No se encontraron resultados">
                            <Columns>
                                <asp:BoundField HeaderText="No. C.C." DataField="IdCC" />
                                <asp:BoundField HeaderText="Centro Costos" DataField="NombreCC" />
                                <asp:BoundField HeaderText="Ingresos" DataField="TotalIngresos" DataFormatString="{0:c}" />
                                <asp:BoundField HeaderText="Egresos" DataField="TotalEgresos" DataFormatString="{0:c}" />
                                <asp:BoundField HeaderText="Intereses" DataField="TotalIntereses" DataFormatString="{0:c}" />
                                <asp:BoundField HeaderText="MUB" DataField="MUBCalculado" DataFormatString="{0:c}" />
                                <asp:BoundField HeaderText="Fecha de cálculo" DataField="FechaCreacion" DataFormatString="{0:dd/MM/yyyy}" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </form>
    </main>
</asp:Content>
