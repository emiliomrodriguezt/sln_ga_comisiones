﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CalculoSAS.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.Calculos.CalculoSAS" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
    <i class="fa fa-sitemap" aria-hidden="true"></i> Operaciones / Calcular SAS
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <main class="page">
        <form id="form1" runat="server">
            <div>
                <div class="row">
                    <div class="col-md-12 tituloPagina">
                        <asp:Label ID="lblTitulo" runat="server">C&aacute;lculo de cuota SAS acumulada</asp:Label>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="ddlPeriodo">L&iacute;mite a&ntilde;o*:</label>
                        </div>
                    </div>
                    <div class="col-md-10"></div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <asp:DropDownList ID="ddlPeriodo" runat="server" style="text-align:center;text-align-last:center;" CssClass="form-control" AppendDataBoundItems="true">
                                <asp:ListItem Text="--- Seleccione ---" Value="0" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <asp:LinkButton ID="btnCalcular" runat="server" CssClass="btn btn-primary" OnClick="btnCalcular_Click"><i class="fa fa-database" aria-hidden="true"></i> Iniciar C&aacute;lculo</asp:LinkButton>
                    </div>
                    <div class="col-md-6">&nbsp;</div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row" id="resultados" runat="server">
                    <div class="col-md-12">
                        <h3>Resultados</h3>
                        <asp:GridView ID="gvResultados" runat="server" AutoGenerateColumns="false" CssClass="table table-striped dataTables" ShowHeader="true" EmptyDataText="No se encontraron resultados">
                            <Columns>
                                <asp:BoundField HeaderText="No. Colaborador" DataField="NoColaborador" />
                                <asp:BoundField HeaderText="Colaborador" DataField="Colaborador" />
                                <asp:BoundField HeaderText="Cuota SAS" DataField="CuotaSAS" DataFormatString="{0:c}" />
                                <asp:BoundField HeaderText="Fecha de cálculo" DataField="FechaCreacion" DataFormatString="{0:dd/MM/yyyy}" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </form>
    </main>
</asp:Content>
