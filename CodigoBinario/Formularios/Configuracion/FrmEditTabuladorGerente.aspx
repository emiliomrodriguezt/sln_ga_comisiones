﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FrmEditTabuladorGerente.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.Configuracion.FrmEditTabuladorGerente" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
    <i class="fa fa-sitemap" aria-hidden="true"></i> Configuraci&oacute;n / Tabulador Gerentes / <label id="bredCrub">Nuevo Registro</label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
     <form runat="server" ClientIDMode="Static" ID="frmTabulador" class="form-striped" action="FrmEditTabuladorGerente.aspx" method="post">
        <asp:HiddenField runat="server" ClientIDMode="Static" id="hidId" Value="0"/>
        <div class="row">
            <div class="col-md-6">
                <label class="control-label" for="ddlTipoTabulador">Tipo Tabulador:</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                    <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlTipoTabulador" data-validar="requerido" CssClass="form-control"/>    
                </div>
                <small class="form-text-error"></small>
            </div>
            <div class="col-md-6">
                <label class="control-label" for="ddlAno">Año:</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                    <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlAno" data-validar="requerido" CssClass="form-control" />
                </div>
                <small class="form-text-error"></small>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-6">
                <label class="control-label" for="txtCosMenMax">Costo mensual GA maxímo:</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                    <asp:TextBox runat="server" MaxLength="20" ClientIDMode="Static" ID="txtCosMenMax" data-validar="requerido" placeholder="Costo mensual GA maxímo" CssClass="form-control money"></asp:TextBox> 
                </div>
                <small class="form-text-error"></small>
            </div>
            <div class="col-md-6">
                <label class="control-label" for="txtMub">MUB mínimo:</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                    <asp:TextBox runat="server" MaxLength="20" ClientIDMode="Static" ID="txtMub" data-validar="requerido" placeholder="MUB mínimo" CssClass="form-control money"></asp:TextBox> 
                </div>
                <small class="form-text-error"></small>
            </div>
           
        </div>
         <br/>
         <div class="row">
              <div class="col-md-6">
                <label class="control-label" for="txtComision">Comisi&oacute;n:</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                    <asp:TextBox runat="server" MaxLength="20" ClientIDMode="Static" ID="txtComision" data-validar="requerido" placeholder="Comisi&oacute;n" CssClass="form-control money"></asp:TextBox> 
                </div>
                <small class="form-text-error"></small>
            </div>
            <div class="col-md-6">
                <label class="control-label" for="txtCosMenMin">Costo mensual GA minimo:</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                    <asp:TextBox runat="server" MaxLength="20" ClientIDMode="Static" ID="txtCosMenMin" data-validar="requerido" placeholder="Costo mensual GA minimo" CssClass="form-control money"></asp:TextBox> 
                </div>
                <small class="form-text-error"></small>
            </div>
         </div>
        <br/>
        <div class="row">
            <div class="col-md-12 text-right">
                <a href="<%= ResolveUrl("~/Formularios/Configuracion/FrmAdminTabuladorGerente.aspx") %>" class="btn btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Regresar</a>
                <button type="submit" data-form="frmTabulador" class="btn btn-primary validaForm"><i class="fa fa-save" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Guardar</button>
            </div>
        </div>
    </form> 
    <script type="text/javascript">
       $(document).ready(function () {
            if (fnGetQuery()["edit"] === "1") {
                fnDiSabledForm("frmTabulador");
                $("#bredCrub").html("Consulta");
            } else if (fnGetQuery()["id"] != null) {
                $("#bredCrub").html("Actualiza");
            }
        });
    </script>
</asp:Content>
