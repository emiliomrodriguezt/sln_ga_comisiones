﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FrmAdminAnticipos.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.Configuracion.FrmCatAnticipos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
     <i class="fa fa-sitemap" aria-hidden="true"></i> Configuraci&oacute;n / Anticipos
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <div class="row">
        <div class="col-md-12 text-right">
            <a href="<%= ResolveUrl("~/Formularios/Configuracion/FrmEditAnticipos.aspx") %>" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Agregar anticipo</a>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped" id="dtResult"></table>
        </div>
    </div>
    <script type="text/javascript">
        var fnFillTable = function() {
            fnGetTable($("#dtResult"),
                {
                    BolEdit: jsonPermits.Edit,
                    BolDelete: true,
                    BolConsult: true,
                    StrPathEdit: $.cookie("strApplicationPath") + "/Formularios/Configuracion/FrmEditAnticipos.aspx",
                    StrPathDelete: $.cookie("strApplicationPath") + "/api/Anticipos/DelAnticipos/"
                },
                [
                    { "sTitle": "No." },
                    { "sTitle": "Colaborador" },
                    { "sTitle": "CC" },
                    { "sTitle": "Importe Neto" },
                    { "sTitle": "Importe Bruto" },
                    { "sTitle": "Aplicado" },
                    { "sTitle": "Remanente" },
                    { "sTitle": "Fecha" },
                    { "sTitle": "Activo", "sClass": "text-center", "bSortable": false, "bSearchable": false },
                    { "sTitle": "Acciones", "sClass": "text-center", "bSortable": false, "bSearchable": false  }],
                $.cookie("strApplicationPath") + "/api/Anticipos/GetAnticipos/");
        }
        $(document).ready(function () {
            fnFillTable();
        });
    </script>
</asp:Content>
