﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FrmEditCentroCostos.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.CentroCostos.FrmEditCentroCostos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
    <i class="fa fa-sitemap" aria-hidden="true"></i> Catalogos / Centro Costos / Actualiza
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <form runat="server" ClientIDMode="Static" ID="frmCentroCostos" class="form-striped" action="FrmEditCentroCostos.aspx" method="post">
        <ul class="nav nav-tabs nav-justified">
            <li class="active">
                <a data-toggle="tab" href="#tab-01">
                    Centro costos
                </a>
            </li>
            <li>
                <a data-toggle="tab" href="#tab-02">
                    Rubros
                </a>
            </li>
            <li>
                <a data-toggle="tab" href="#tab-03">
                    Distribución MUB
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab-01">
                <asp:HiddenField runat="server" ClientIDMode="Static" id="hidId" Value="0"/>
                <div class="row">
                        <div class="col-md-4">
                            <label class="control-label" for="txtId">ID:</label>
                            <div class="input-group input-group-lg mb15">
                                <span class="input-group-addon"><i class="fa fa-ban" aria-hidden="true"></i></span>
                                <asp:TextBox data-toggle="tooltip" data-trigger="hover" runat="server" ReadOnly="True" ClientIDMode="Static" ID="txtId" placeholder="ID" CssClass="form-control tooltips"></asp:TextBox>
                            </div>
                            <small class="form-text-error"></small>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label" for="txtCentroCostos">Centro costos:</label>
                            <div class="input-group input-group-lg mb15">
                                <span class="input-group-addon"><i class="fa fa-ban" aria-hidden="true"></i></span>
                                <asp:TextBox data-toggle="tooltip" data-trigger="hover" runat="server" ReadOnly="True" ClientIDMode="Static" ID="txtCentroCostos" placeholder="Centro costos" CssClass="form-control tooltips"></asp:TextBox>
                            </div>
                            <small class="form-text-error"></small>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label" for="txtCentroCostos">Estatus:</label>
                            <div class="input-group input-group-lg mb15">
                                <span class="input-group-addon"><i class="fa fa-ban" aria-hidden="true"></i></span>
                                <asp:TextBox data-toggle="tooltip" data-trigger="hover" runat="server" ReadOnly="True" ClientIDMode="Static" ID="txtEstatus" placeholder="Estatus" CssClass="form-control tooltips"></asp:TextBox>
                            </div>
                            <small class="form-text-error"></small>
                        </div>
                    </div>
                <br/>
                <div class="row">
                        <div class="col-md-4">
                            <label class="control-label" for="txtEmpresa">Empresa ganadora:</label>
                            <div class="input-group input-group-lg mb15">
                                <span class="input-group-addon"><i class="fa fa-ban" aria-hidden="true"></i></span>
                                <asp:TextBox data-toggle="tooltip" data-trigger="hover" ReadOnly="True" runat="server" ClientIDMode="Static" ID="txtEmpresa" placeholder="Empresa ganadora" CssClass="form-control tooltips"></asp:TextBox>
                            </div>
                            <small class="form-text-error"></small>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label" for="txtUNegocio">Unidad negocio:</label>
                            <div class="input-group input-group-lg mb15">
                                <span class="input-group-addon"><i class="fa fa-ban" aria-hidden="true"></i></span>
                                <asp:TextBox data-toggle="tooltip" data-trigger="hover" ReadOnly="True" runat="server" ClientIDMode="Static" ID="txtUNegocio" placeholder="Unidad negocio" CssClass="form-control tooltips"></asp:TextBox>
                            </div>
                            <small class="form-text-error"></small>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label" for="txtArea">Area:</label>
                            <div class="input-group input-group-lg mb15">
                                <span class="input-group-addon"><i class="fa fa-ban" aria-hidden="true"></i></span>
                                <asp:TextBox data-toggle="tooltip" data-trigger="hover" ReadOnly="True" runat="server" ClientIDMode="Static" ID="txtArea" placeholder="Area" CssClass="form-control tooltips"></asp:TextBox>
                            </div>
                            <small class="form-text-error"></small>
                        </div>
                    </div>
                <br/>
                <div class="row">
                        <div class="col-md-4">
                            <label class="control-label" for="txtIdHomologado">zID Homologado:</label>
                            <div class="input-group input-group-lg mb15">
                                <span class="input-group-addon"><i class="fa fa-ban" aria-hidden="true"></i></span>
                                 <asp:TextBox data-toggle="tooltip" data-trigger="hover" ReadOnly="True" runat="server" ClientIDMode="Static" ID="txtIdHomologado" placeholder="zID Homologado" CssClass="form-control tooltips"></asp:TextBox>
                            </div>
                            <small class="form-text-error"></small>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label" for="txtCentroCostosHomologado">zCentro Costos Homologado:</label>
                            <div class="input-group input-group-lg mb15">
                                <span class="input-group-addon"><i class="fa fa-ban" aria-hidden="true"></i></span>
                                <asp:TextBox data-toggle="tooltip" data-trigger="hover" ReadOnly="True" runat="server" ClientIDMode="Static" ID="txtCentroCostosHomologado" placeholder="zCentro Costos Homologado" CssClass="form-control tooltips"></asp:TextBox>
                            </div>
                            <small class="form-text-error"></small>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label" for="txtClasificacionAlterna">Clasificación alterna planeación:</label>
                            <div class="input-group input-group-lg mb15">
                                <span class="input-group-addon"><i class="fa fa-ban" aria-hidden="true"></i></span>
                                <asp:TextBox data-toggle="tooltip" data-trigger="hover" ReadOnly="True" runat="server" ClientIDMode="Static" ID="txtClasificacionAlterna" placeholder="Clasificación alterna planeación" CssClass="form-control tooltips"></asp:TextBox>
                            </div>
                            <small class="form-text-error"></small>
                        </div>
                    </div>
                <br/>
                <div class="row">
                        <div class="col-md-4">
                            <label class="control-label" for="txtFechaInicio">Fecha inicio:</label>
                            <div class="input-group input-group-lg mb15">
                                <span class="input-group-addon"><i class="fa fa-ban" aria-hidden="true"></i></span>
                                 <asp:TextBox data-toggle="tooltip" data-trigger="hover" ReadOnly="True" runat="server" ClientIDMode="Static" ID="txtFechaInicio" placeholder="Fecha inicio" CssClass="form-control tooltips"></asp:TextBox>
                            </div>
                            <small class="form-text-error"></small>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label" for="txtFechaCierre">Fecha cierra administrativo:</label>
                            <div class="input-group input-group-lg mb15">
                                <span class="input-group-addon"><i class="fa fa-ban" aria-hidden="true"></i></span>
                                <asp:TextBox data-toggle="tooltip" data-trigger="hover" ReadOnly="True" runat="server" ClientIDMode="Static" ID="txtFechaCierre" placeholder="Fecha cierre administrativo" CssClass="form-control tooltips"></asp:TextBox>
                            </div>
                            <small class="form-text-error"></small>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label" for="txtDuracionMeses">Duración (meses):</label>
                            <div class="input-group input-group-lg mb15">
                                <span class="input-group-addon"><i class="fa fa-ban" aria-hidden="true"></i></span>
                                <asp:TextBox data-toggle="tooltip" data-trigger="hover" ReadOnly="True" runat="server" ClientIDMode="Static" ID="txtDuracionMeses" placeholder="Duración en meses" CssClass="form-control tooltips"></asp:TextBox>
                            </div>
                            <small class="form-text-error"></small>
                        </div>
                    </div>
                <br/>
                <div class="row">
                        <div class="col-md-4">
                            <label class="control-label" for="ddlUnStaff">Director UN/STAFF:</label>
                            <div class="input-group input-group-lg mb15">
                                <span class="input-group-addon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlUnStaff" data-validar="requerido" CssClass="form-control"/>    
                            </div>
                            <small class="form-text-error"></small>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label" for="ddlGerentes">Gerente:</label>
                            <div class="input-group input-group-lg mb15">
                                <span class="input-group-addon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlGerentes" data-validar="requerido" CssClass="form-control"/>
                            </div>
                            <small class="form-text-error"></small>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label" for="txtFechaPayBack">Fecha PayBack:</label>
                            <div class="input-group input-group-lg mb15">
                                <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                <asp:TextBox data-toggle="tooltip" data-trigger="hover" runat="server" MaxLength="15" ClientIDMode="Static" ID="txtFechaPayBack" data-validar="requerido" placeholder="Fecha payback" CssClass="form-control tooltips datepickerEs"></asp:TextBox>
                            </div>
                            <small class="form-text-error"></small>
                        </div>
                    </div>
                <br/>
                <div class="row">
                        <div class="col-md-4">
                            <label class="control-label" for="txtIngreso">Ingreso seg&uacute;n contrato totales:</label>
                            <div class="input-group input-group-lg mb15">
                                <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                <asp:TextBox data-toggle="tooltip" data-trigger="hover" runat="server" MaxLength="20" ClientIDMode="Static" ID="txtIngreso" data-validar="requerido" placeholder="Ingreso total" CssClass="form-control tooltips money"></asp:TextBox> 
                            </div>
                            <small class="form-text-error"></small>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label" for="txtEgreso">Egreso seg&uacute;n contrato totales:</label>
                            <div class="input-group input-group-lg mb15">
                                <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                <asp:TextBox data-toggle="tooltip" data-trigger="hover" runat="server" MaxLength="20" ClientIDMode="Static" ID="txtEgreso" data-validar="requerido" placeholder="Egreso total" CssClass="form-control tooltips money"></asp:TextBox>
                            </div>
                            <small class="form-text-error"></small>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label" for="txtMubContratado">MUB Contratado:</label>
                            <div class="input-group input-group-lg mb15">
                                <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                <asp:TextBox data-toggle="tooltip" data-trigger="hover" runat="server" MaxLength="20" ClientIDMode="Static" ID="txtMubContratado" data-validar="requerido" placeholder="Mub contratado" CssClass="form-control tooltips money"></asp:TextBox>
                            </div>
                            <small class="form-text-error"></small>
                        </div>
                    </div>
                <br/>
                <div class="row">
                        <div class="col-md-4">
                            <label class="control-label" for="ddlAnoCreacion">Año Creación:</label>
                            <div class="input-group input-group-lg mb15">
                                <span class="input-group-addon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlAnoCreacion" data-validar="requerido" CssClass="form-control"/>    
                            </div>
                            <small class="form-text-error"></small>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label" for="txtObservacion">Estatus:</label>
                            <div class="input-group input-group-lg mb15">
                                <span class="input-group-addon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlEstaus" data-validar="requerido" CssClass="form-control"/>
                            </div>
                            <small class="form-text-error"></small>
                        </div>
                        <div class="col-md-4">
                             <label class="control-label" for="txtObservacion">Observación:</label>
                            <div class="input-group input-group-lg mb15">
                                <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                <asp:TextBox data-toggle="tooltip" data-trigger="hover" Rows="10" TextMode="MultiLine" runat="server" MaxLength="500" ClientIDMode="Static" ID="txtObservacion" data-validar="requerido" placeholder="Observaciones" CssClass="form-control tooltips"></asp:TextBox>
                            </div>
                            <small class="form-text-error"></small>
                        </div>
                    </div>
                <br/>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <a href="<%= ResolveUrl("~/Formularios/CentroCostos/FrmAdminCentroCostos.aspx") %>" class="btn btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Regresar</a>
                        <button type="submit" data-form="frmCentroCostos" class="btn btn-primary validaForm"><i class="fa fa-save" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Guardar</button>
                    </div>
                </div>
                <br/> 
            </div>
            <div class="tab-pane" id="tab-02">
                <div class="row">
                    <div class="col-md-12">
                        <label class="control-label">Composición del porcentaje de pago de comisión</label>
                    </div>
                    <div class="col-md-12">
                        <button type="button" onclick="fnAddRubro();" class="btn btn-primary  btn-block"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Agregar rubro</button>
                        <table class="table table-striped" id="tblRubros"></table>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <a href="<%= ResolveUrl("~/Formularios/CentroCostos/FrmAdminCentroCostos.aspx") %>" class="btn btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Regresar</a>
                    </div>
                </div>
                <br/> 
            </div>
            <div class="tab-pane" id="tab-03">
                <div class="row">
                    <div class="col-md-12">
                        <label class="control-label">Distribución de MUB contratado:</label>
                    </div>
                    <div class="col-md-12">
                        <button type="button" onclick="fnAddMub();" class="btn btn-primary btn-block"><i class="fa fa-plus" aria-hidden="true"></i> Agreagr distribución de MUB contratado</button>
                        <table class="table table-striped" id="dtMub"></table>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <a href="<%= ResolveUrl("~/Formularios/CentroCostos/FrmAdminCentroCostos.aspx") %>" class="btn btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Regresar</a>
                    </div>
                </div>
                <br/> 
           </div>
        </div>
        <div id="ModalMub" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" aria-hidden="false">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button id="btnCloseModalMub" aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                        <h4 class="modal-title">
                            <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&iexcl;Seleccione Gerente, Director y Porcentaje!.
                        </h4>
                    </div>
                    <div id="dataModalMub" class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <label class="control-label" for="ddlDirectorMubModal">Director*:</label>
                                <div class="input-group input-group-lg mb15">
                                    <span class="input-group-addon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                    <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlDirectorMubModal" data-validar="requerido" CssClass="form-control"/>
                                </div>
                                <small class="form-text-error"></small>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label class="control-label" for="ddlGerneteMubModal">Gerente*:</label>
                                <div class="input-group input-group-lg mb15">
                                    <span class="input-group-addon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                    <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlGerneteMubModal" data-validar="requerido" CssClass="form-control"/>
                                </div>
                                <small class="form-text-error"></small>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label class="control-label">Porcentaje*:</label>
                                <div class="input-group input-group-lg mb15">
                                    <span class="input-group-addon">%</span>
                                   <input id="txtPorcentajeMubModal" data-validar="requerido|rango[0,100]" type="text" class="form-control onlyNumber" maxlength="10"/>
                               </div>
                               <small class="form-text-error"></small>
                           </div>
                       </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="fnSetMubModal();">Aceptar</button>
                    </div>
               </div>
           </div>
        </div>
        <div id="ModalRubros" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" aria-hidden="false">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button id="btnCloseModalRubros" aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                        <h4 class="modal-title">
                            <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&iexcl;Seleccione Rubro!.
                        </h4>
                    </div>
                    <div id="dataModalRubro" class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <label class="control-label" for="ddlRubrosPago">Rubro:</label>
                                <div class="input-group input-group-lg mb15">
                                    <span class="input-group-addon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                    <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlRubrosPago" data-validar="requerido" CssClass="form-control"/>
                                </div>
                                <small class="form-text-error"></small>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="fnSetRubros();"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="ModalRubrosColaborador" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" data-backdrop="static" aria-hidden="false">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button id="btnCloseModalRubrosColaborador" aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                        <h4 class="modal-title">
                            <i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&iexcl;Seleccione Gerente y Porcentaje!.
                        </h4>
                    </div>
                    <div id="dataModalRubrosColaborador" class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <label class="control-label" for="ddlDirectorMubModal">Gerente:</label>
                                <div class="input-group input-group-lg mb15">
                                    <span class="input-group-addon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                    <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlGerenteModalRubrosColaborador" data-validar="requerido" CssClass="form-control"/>
                                    <input type="hidden" id="intIdRubroCC"/>
                                </div>
                                <small class="form-text-error"></small>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label class="control-label">Porcentaje:</label>
                                <div class="input-group input-group-lg mb15">
                                    <span class="input-group-addon">%</span>
                                    <input id="txtPorcentajeModalRubrosColaborador" data-validar="requerido|rango[0,100]" type="text" class="form-control onlyNumber" maxlength="10"/>
                                </div>
                                <small class="form-text-error"></small>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="fnSetRubrosColaborador();"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>
                    </div>
                </div>
            </div>
        </div>
    </form> 
    <script type="text/javascript">
        var fnEliminaRegistro = function (id, strPath, fnCallBack) {
            $.ajax({
                type: "DELETE",
                url: strPath + id,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (dataResponse) {
                    var strType = "alert-danger";
                    if (dataResponse.BolSuccess) {
                        window[fnCallBack]();
                        strType = "alert-success";
                        fnMessage(strType, dataResponse.StrMessage, true);
                        $("#btnCloseModal").trigger("click");
                    }
                    fnMessage(strType, dataResponse.StrMessage, true);
                    $("#btnCloseModal").trigger("click");
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
        }

        var fnConfirmEliminar = function (id, strPath, strCallBack) {
            $("#alertTitulo").html("<i class=\"fa fa-exclamation-triangle text-warning\"> &nbsp;&nbsp;&nbsp; </i> <span class=\"bg-warning\">&iexcl;Esta eliminando un registro!.</span>");
            $("#alertMensaje").html("&iexcl;El registro se elimninara de manera pemanente!. &iquest;Desea continuar?");
            $("#btnAceptarModal").attr("onclick", "fnEliminaRegistro('" + id + "','" + strPath + "','" + strCallBack + "');");
            $("#alertModal").modal("show");
        }

        var fnEliminaRubro = function(id) {
            fnConfirmEliminar(id, $.cookie("strApplicationPath") + "/api/RubrosCentroCostos/DelRubrosCc/", "fnFillTableRubros");
        }

        var fnEliminaMub = function (id) {
            fnConfirmEliminar(id, $.cookie("strApplicationPath") + "/api/MubContCcCol/DelMub/", "fnFillTableMub");
        }

        var fnEliminaCcRubro = function (id) {
            fnConfirmEliminar(id, $.cookie("strApplicationPath") + "/api/RubrosCcColaborador/DelRubrosCcCol/", "fnFillTableRubros");
        }

        var fnSetRubrosColaborador = function() {
            if (!fnValidaDatos("dataModalRubrosColaborador")) {
                return false;
            }
            var intIdColaborador = $("#ddlGerenteModalRubrosColaborador").val();
            var decPorcentaje =$("#txtPorcentajeModalRubrosColaborador").val();
            var intIdRubroCc = $("#intIdRubroCC").val();

            fnAjax($.cookie("strApplicationPath") + "/api/RubrosCcColaborador/SetRubrosCcCol/",
                {
                    IntIdColaborador: intIdColaborador,
                    DecPorcentaje: decPorcentaje,
                    IntIdRubroCc: intIdRubroCc
                },
                "POST", function (dataResponse) {
                    if (dataResponse.BolSuccess) {
                        fnMessage("alert-success", dataResponse.StrMessage, true);
                        fnFillTableRubros();
                        $("#btnCloseModalRubrosColaborador").trigger("click");
                    } else {
                        fnMessage("alert-danger", dataResponse.StrMessage, true);
                    }
                });
            return true;
        }

        var fnSetMubModal = function () {
            if (!fnValidaDatos("dataModalMub")) {
                return false;
            }
            var idCentroCostos = $("#hidId").val();
            var idDirector = $("#ddlDirectorMubModal").val();
            var idGernete = $("#ddlGerneteMubModal").val();
            var porcentaje = $("#txtPorcentajeMubModal").val();

            fnAjax($.cookie("strApplicationPath") + "/api/MubContCcCol/SeTMUB/",
                {
                    IntIdCentroCostos: idCentroCostos,
                    IntIdGerente: idGernete,
                    IntIdDirector: idDirector,
                    DecPorcentaje: porcentaje
                },
                "POST", function (dataResponse) {
                    if (dataResponse.BolSuccess) {
                        fnMessage("alert-success", dataResponse.StrMessage, true);
                        fnFillTableMub();
                        $("#btnCloseModalMub").trigger("click");
                    } else {
                        fnMessage("alert-danger", dataResponse.StrMessage, true);
                    }
                });
            return true;
        }

        var fnSetRubros = function () {
            if (!fnValidaDatos("dataModalRubro")) {
                return false;
            }
            var idCentroCostos = $("#hidId").val();
            var idRubroPago = $("#ddlRubrosPago").val();

            fnAjax($.cookie("strApplicationPath") + "/api/RubrosCentroCostos/SetRubrosCc/",
                {
                    IntIdRubroPago: idRubroPago,
                    IntIdCentroCostos: idCentroCostos
                },
                "POST", function (dataResponse) {
                    if (dataResponse.BolSuccess) {
                        fnMessage("alert-success", dataResponse.StrMessage, true);
                        fnFillTableRubros();
                        $("#btnCloseModalRubros").trigger("click");
                    } else {
                        fnMessage("alert-danger", dataResponse.StrMessage, true);
                    }
                });
            return true;
        }

        var fnAddColRubro = function (intIdRubroCc) {
            $("#ddlGerenteModalRubrosColaborador").val("0");
            $("#txtPorcentajeModalRubrosColaborador").val("");
            $("#intIdRubroCC").val(intIdRubroCc);
            $("#ModalRubrosColaborador").modal("show");
        }

        var fnAddRubro = function () {
            $("#ddlRubrosPago").val("0");
            $("#ModalRubros").modal("show");
        }

        var fnAddMub = function () {
            $("#ddlDirectorMubModal").val("0");
            $("#ddlGerneteMubModal").val("0");
            $("#txtPorcentajeMubModal").val("");
            $("#ModalMub").modal("show");
        }

        var fnFillDetalleRubros = function () {
            $("#tblRubros > tbody  > tr").each(function () {
                var objTr = this;
                var idRubroCc = $(this).children().html();
                if (idRubroCc !== "No hay información disponible") {
                    $.ajax({
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        url: $.cookie("strApplicationPath") + "/api/RubrosCcColaborador/FillRubrosCcCol/" + idRubroCc,
                        dataType: "json",
                        success: function(dataResponse) {
                            var strResult =
                                "<tr><td colspan=\"4\"><table class=\"table table-striped cssClsSmallFont\"><thead><tr><th>No.</th><th>Colaborador</th><th>Porcentaje</th><th>Acciones</th><tbody>";
                            var intcount = 0;
                            $.each(dataResponse,
                                function(index, value) {
                                    intcount++;
                                    strResult += "<tr><td>" + value[0] + "</td><td>" + value[1] + "</td><td>" + value[2] + "</td><td>" + value[3] + "</td></tr>";
                                });
                            strResult += "</tbody></table></td></tr>";

                            if (intcount > 0) {
                                $(objTr).after(strResult);
                            }
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            alert(xhr.status);
                            alert(thrownError);
                        },
                        failure: function(errMsg) {
                            alert(errMsg);
                        }
                    });
                }
            });
        }

        var fnFillTableRubros = function() {
            fnGetTable($("#tblRubros"), {},
                [
                    { "sTitle": "No." },
                    { "sTitle": "Rubro" },
                    { "sTitle": "Porcentaje" },
                    { "sTitle": "Acciones", "sClass": "text-center", "bSortable": false }
                ],
                $.cookie("strApplicationPath") + "/api/RubrosCentroCostos/FillRubrosCc/" + $("#hidId").val(),
                "GET",
                fnFillDetalleRubros
                , false, false, false);
        }

        var fnFillTableMub = function(){
            fnGetTable($("#dtMub"), {},
                [
                    { "sTitle": "No." },
                    { "sTitle": "Gerente" },
                    { "sTitle": "Director" },
                    { "sTitle": "Porcentaje" },
                    { "sTitle": "Acciones", "sClass": "text-center", "bSortable": false }
                ],
                $.cookie("strApplicationPath") + "/api/MubContCcCol/FilltblMub/" + $("#hidId").val(),
                "GET",
                function () {

                }
                ,false,false,false);
        }

        $(document).ready(function () {
            if (fnGetQuery()["edit"] === "1") {
                fnDiSabledForm("frmCentroCostos");
            }
            fnFillTableMub();
            fnFillTableRubros();
        });
    </script>
</asp:Content>
