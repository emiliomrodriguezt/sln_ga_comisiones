﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FrmAdminCentroCostos.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.CentroCostos.FrmAdminCentroCostos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
    <i class="fa fa-sitemap" aria-hidden="true"></i>  Catalogos / Centro Costos
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <div class="row">
        <div class="col-md-12 text-right">
            <a href="#" onclick="fnActulizaCc();" class="btn btn-primary"><i class="fa fa-refresh" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Actulizar CC's</a>
            <a href="<%= ResolveUrl("~/api/CentroCostos/GenerateXls/") %>" class="btn btn-success"><i class="fa fa-file-excel-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Exportar</a>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped cssClsSmallFont" id="dtResult"></table>
        </div>
    </div>
    <script type="text/javascript">
        var fnActulizaCc = function() {
            fnAjax($.cookie("strApplicationPath") + "/api/CentroCostos/UpdateCenterCosts/", {}, "PUT", function (dataResponse) {
                $("#alertTitulo").html("<i class=\"fa fa- check\" aria-hidden=\"true\"></i>&nbsp;&nbsp;&nbsp;&iexcl;Resultados!.");
                var strmassageTmp = "";
                var strMessage = "<table class=\"table table-striped\"> "            
                                + "<tbody>";

                var intCunt = 0;
                var intCorrectos = 0;
                var intErrores = 0;


            
                $.each(dataResponse, function (index, value) {
                    intCunt++;
                    if (!value.BolSuccess) {
                        intErrores++;
                        strmassageTmp += "<tr> "
                            + "<td>" + value.StrId + "</td> "
                            + "<td><span class=\"alert-danger\">" + value.StrMessage + "</span></td> "
                            + "</tr>";
                    } else {
                        intCorrectos++;
                    }
                });

                strMessage += "<tr><td colspan=\"2\">Se ecnontraron un total de [" + intCunt + "] registros, se procesaron [" + intCorrectos + "] correctaente.</td></tr>"
                if (intErrores > 0)
                {
                    strMessage += "<tr><td colspan=\"2\">!No se prosesaron [" + intErrores + "] registros de los cuales se mustra el resumen a continuacion¡</td></tr>";
                    strMessage += "<tr><th>ID</th><th>Lastname</th></tr>";
                }
                strMessage += strmassageTmp;
                strMessage += "</tbody></table > ";

                $("#alertMensaje").html(strMessage);
                $("#btnAceptarModal").hide();
                $("#alertModal").modal("show");

                fnFillTable();
            });

        };

        var fnFillTable = function() {
            fnGetTable($("#dtResult"),
                {
                    BolEdit: jsonPermits.Edit,
                    BolDelete: false,
                    BolConsult: false,
                    StrPathEdit: $.cookie("strApplicationPath") + "/Formularios/CentroCostos/FrmEditCentroCostos.aspx",
                    StrPathDelete: $.cookie("strApplicationPath") + "/api/CentroCostos/DelCentroCostos/"
                },
                [
                    { "sTitle": "No." },
                    { "sTitle": "Centro costos" },
                    { "sTitle": "Fecha inicio" },
                    { "sTitle": "Fecha cierre admin" },
                    { "sTitle": "Empresa Ganadora" },
                    { "sTitle": "Estaus" },
                    { "sTitle": "U Negocio" },
                    { "sTitle": "&Aacute;rea" },
                    { "sTitle": "zID Homologado" },
                    { "sTitle": "zCentro Costos Homologado" },
                    { "sTitle": "Clasificaci&oacute;n Alterna planeaci&oacute;n" },
                    { "sTitle": "Activo", "sClass": "text-center", "bSortable": false, "bSearchable": false },
                    { "sTitle": "Acciones", "sClass": "text-center", "bSortable": false, "bSearchable": false }
                ],
                $.cookie("strApplicationPath") + "/api/CentroCostos/GetCentroCostos/");
        };

        $(document).ready(function () {
            fnFillTable();
        });
    </script>
</asp:Content>
