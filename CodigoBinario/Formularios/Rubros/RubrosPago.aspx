﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RubrosPago.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.Rubros.RubrosPago" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
    <i class="fa fa-sitemap" aria-hidden="true"></i> Configuración / Conceptos de comisión
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <main class="page">
        <form id="form1" runat="server">
            <div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group pull-right">
                            <asp:LinkButton ID="btnNuevo" runat="server"  CssClass="btn btn-primary" OnClientClick="javascript: window.location.href='AltaRubroPago.aspx'; return false;" ><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Nuevo concepto</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:GridView ID="gvRubros" runat="server" AutoGenerateColumns="false" CssClass="table table-striped dataTables" ShowHeader="true">
                            <Columns>
                                <asp:BoundField HeaderText="ID" DataField="idRubro" />
                                <asp:BoundField HeaderText="Concepto de comisión" DataField="nombre" />
                                <asp:BoundField HeaderText="Porcentaje" DataField="porcentaje" DataFormatString="{0:P} " />
                                <asp:BoundField HeaderText="Estatus" DataField="estatus" />
                                <asp:TemplateField HeaderText="Acciones">
                                    <ItemTemplate>
                                        <%if (editar)
                                            { %>
                                        <a href='EditRubroPago.aspx?id=<%#(HttpContext.Current.Server.UrlEncode(Utility_GA_Comisiones.ClsCryptographyBl.EncodeString(Eval("idRubro").ToString()))) %>'><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        <%} %>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="text-center" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </form>
    </main>
</asp:Content>
