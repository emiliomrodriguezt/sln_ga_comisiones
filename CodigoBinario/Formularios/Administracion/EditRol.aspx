﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditRol.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.Administracion.EditRol" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
    <i class="fa fa-sitemap" aria-hidden="true"></i> Administración / Roles / <label id="bredCrub">Nuevo Registro</label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <form runat="server" ClientIDMode="Static" ID="frmRol" class="form-striped" action="EditRol.aspx" method="post">
       <div class="row">
           <div class="col-md-6">
               <label for="txtNombreRol">Nombre Rol*:</label>
               <div class="input-group input-group-lg mb15">
                  <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                   <asp:HiddenField runat="server" ClientIDMode="Static" id="hidId" Value="0"/>
                   <asp:TextBox runat="server" ClientIDMode="Static" ID="txtNombreRol" data-validar="requerido" placeholder="Nombre del rol" MaxLength="50" CssClass="form-control"></asp:TextBox>
                </div>
               <small class="form-text-error">hola</small>
           </div>
       </div>
        <br/>
       <div class="row">
           <div class="col-md-6 text-right">
               <a href="/Formularios/Administracion/AdminRoles.aspx" class="btn btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Regresar</a>
               <button type="reset" class="btn btn-darkblue"><i class="fa fa-eraser" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Limpiar</button>
               <button type="submit" data-form="frmRol" class="btn btn-primary validaForm"><i class="fa fa-save" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Guardar</button>
           </div>
       </div>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {
            if (fnGetQuery()["edit"] === "1") {
                fnDiSabledForm("frmRol");
                $("#bredCrub").html("Consulta");
            }else if (fnGetQuery()["id"] != null) {
                $("#bredCrub").html("Actualiza");
            }
        });
    </script>
</asp:Content>
