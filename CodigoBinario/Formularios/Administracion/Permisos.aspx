﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Permisos.aspx.cs" Inherits="UL_GA_Comisiones.Formularios.Administracion.Permisos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BreadCrumb" runat="server">
    <i class="fa fa-sitemap" aria-hidden="true"></i> Administración / Permisos
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Boody" runat="server">
    <form runat="server">
        <div class="row">
            <div class="col-md-6">
                <label for="ddlRoles">Rol:</label>
                <div class="input-group input-group-lg mb15">
                    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                    <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlRoles"  CssClass="form-control"/>    
                </div>
            </div>
             <div class="col-md-6 text-right">
                 <br/><br/>
                <button id="btnGuardar" type="button" class="btn btn-primary" disabled="disabled"><i class="fa fa-save" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Guardar</button>
            </div>
        </div>
        <br/>
        <div id="divModificar" class="row cssClsHidden">
            <div class="col-md-2 col-md-offset-11">
                Modificar:
                <div id="divActivaTodos" class="toggle-primary tooltips" data-toggle="tooltip" title="Activar todos"></div>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped" id="dtResult"></table>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        var fnGuardar = function () {
            var data = [];

            $("#dtResult > tbody  > tr").each(function () {
                var objClsAddPermissionEl = new Object();
                objClsAddPermissionEl.IntIdRol = $("#ddlRoles").val();
                objClsAddPermissionEl.IntIdPermiso = $(this).children().html();
                objClsAddPermissionEl.IntIdPagina = $(this).children().next().html();
                objClsAddPermissionEl.BolAcces = $(this).children().next().next().next().next().next().children().children().hasClass("active");
                objClsAddPermissionEl.BolEdit = $(this).children().next().next().next().next().next().next().children().children().hasClass("active");
                data.push(objClsAddPermissionEl);
            });

            fnAjax($.cookie("strApplicationPath") + "/api/Permisos/SetPermisos/", data, "POST", function (dataResponse) {
                $("#alertTitulo").html("<i class=\"fa fa- check\" aria-hidden=\"true\"></i>&nbsp;&nbsp;&nbsp;<span class=\"bg-success\">&iexcl;Resultados!.</span>");
                var strMessage = "";

                $.each(dataResponse, function (index, value) {
                    var strCssCls = value.BolSuccess ? "alert-success" : "alert-danger";
                    strMessage += "<span class=\"" + strCssCls + "\"> [No: " + value.IntId + " ] " + value.StrMessage + "</span> \n\r";
                });

                $("#alertMensaje").html(strMessage);
                $("#btnAceptarModal").hide();
                $("#alertModal").modal("show");
            });
        }

        var fnToggleEdit = function (idPagina) {
            var bolActivar = $("#divAcces" + idPagina).children().hasClass("active");
            if (bolActivar) {
                $("#divEdit" + idPagina).toggles({ on: false, text: { on: "<i class=\"fa fa-check\" aria-hidden=\"true\"></i>", off: "<i class=\"fa fa-times\" aria-hidden=\"true\"></i>" } }).removeAttr("data-original-title");
            } else {
                $("#divEdit" + idPagina).toggles({ on: false, click: false, drag: false, text: { on: "<i class=\"fa fa-check\" aria-hidden=\"true\"></i>", off: "<i class=\"fa fa-times\" aria-hidden=\"true\"></i>" } }).attr("data-original-title", "Debe conceder acceso antes de modificar esta opción");
            }
        }

        var fnFillTable = function () {
            $("#btnGuardar").removeAttr("disabled");
            $("#divModificar").removeClass("cssClsHidden");
            fnGetTable($("#dtResult"),
               "",
                [
                    { "sClass": "cssClsHidden" },
                    { "sTitle": "No." },
                    { "sClass": "cssClsHidden" },
                    { "sTitle": "Nombre" },
                    { "sTitle": "Descripcion" },
                    { "sTitle": "Acceso", "sClass": "text-center", "bSortable": false, "bSearchable": false },
                    { "sTitle": "Editar", "sClass": "text-center", "bSortable": false, "bSearchable": false }
                ],
                $.cookie("strApplicationPath") + "/api/Permisos/GetPermisos/"+$("#ddlRoles").val(), "GET", function () {
                    $("#dtResult > tbody  > tr").each(function () {
                        var idPagina = $(this).children().next().html();
                        var tienePermiso = $(this).children().html() === "0" ? false : true;
                        var puedeEditar = $(this).children().next().next().html() === "False" ? false : true;
                        $(this).children().next().next().next().next().next().html("<div id=\"divAcces" + idPagina +"\" class=\"toggle toggle-primary tooltips toggleUno\" data-toggle=\"tooltip\" ></div>");
                        $(this).children().next().next().next().next().next().next().html("<div  id=\"divEdit" + idPagina + "\" class=\"toggle toggle-primary tooltips toggleDos\" data-toggle=\"tooltip\" ></div>");


                        $("#divAcces" + idPagina).toggles({ on: tienePermiso, text: { on: "<i class=\"fa fa-check\" aria-hidden=\"true\"></i>", off: "<i class=\"fa fa-times\" aria-hidden=\"true\"></i>" } }).click(function() {
                            fnToggleEdit(idPagina, puedeEditar);
                        });

                        if (tienePermiso) {
                            $("#divEdit" + idPagina).toggles({ on: puedeEditar, text: { on: "<i class=\"fa fa-check\" aria-hidden=\"true\"></i>", off: "<i class=\"fa fa-times\" aria-hidden=\"true\"></i>" } }).removeAttr("title");
                        }else{
                            $("#divEdit" + idPagina).toggles({ on: false, click: false, drag: false, text: { on: "<i class=\"fa fa-check\" aria-hidden=\"true\"></i>", off: "<i class=\"fa fa-times\" aria-hidden=\"true\"></i>" } }).attr("title", "Debe conceder acceso antes de modificar esta opción");
                        }

                    });  
                    $(".tooltips").tooltip();
                }, false, true, true);
        }

        $(document).ready(function () {
            $("#divActivaTodos").toggles({ on: false, text: { on: "<i class=\"fa fa-check\" aria-hidden=\"true\"></i>", off: "<i class=\"fa fa-times\" aria-hidden=\"true\"></i>" } }).click(function () {
                var boAccion = $(this).children().hasClass("active");
                $(".toggleUno").each(function () {
                    var bolActivado = $(this).children().hasClass("active");
                    if (boAccion !== bolActivado) {
                        $(this).trigger("click");
                    }
                });

                $(".toggleDos").each(function () {
                    var bolActivado = $(this).children().hasClass("active");
                    if (boAccion !== bolActivado) {
                        $(this).trigger("click");
                    }
                });
            });

            $("#ddlRoles").change(function() {
                fnFillTable();
            });

            $("#btnGuardar").click(function () {
                fnGuardar();
            });
        });
    </script>
</asp:Content>
